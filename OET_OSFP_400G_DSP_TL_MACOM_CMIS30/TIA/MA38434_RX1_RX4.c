/*
 * MA38434_RX1_RX4.c
 *
 *  Created on: 2019�~6��20��
 *      Author: Ivan_Lin
 */
#include <SI_EFM8LB1_Register_Enums.h>
#include "Master_I2C_GPIO.h"
#include "MA38434_RX1_RX4.h"
#include "EFM8LB_ADC.h"

#define MATA38434_CH0   0
#define MATA38434_CH1   1
#define MATA38434_CH2   2
#define MATA38434_CH3   3

#define CDR_PAGE_0_00	0x00
#define CDR_PAGE_REG_FF 0xFF
#define CDR_BC_PAGE     0x1F
#define CDR_PAGE_20  	0x20
#define CDR_PAGE_21	    0x21
#define CDR_PAGE_22	    0x22
#define CDR_PAGE_23	    0x23

xdata struct MATA38434_RX1_RX4_TIA_MEMORY   MATA38434_RX1_RX4_TIA_MEMORY_MAP;

//-----------------------------------------------------------------------------------------------------------------//
//------------------------------------ MATA38434_RX1_RX4 I2C PAGE SEL ---------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------//
void MATA38434_RX1_RX4_SET_PAGE( unsigned char PAGE )
{
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xFF , PAGE , Master_I2C1 );
}

void MATA38434_RX1_RX4_RESET()
{
	MATA38434_RX1_RX4_SET_PAGE( CDR_PAGE_0_00 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0x02 , 0xAA , Master_I2C1 );
	Delay_ms(10);
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0x02 , 0x00 , Master_I2C1 );
	Delay_ms(10);
}
//-----------------------------------------------------------------------------------------------------------------//
//------------------------------------ MATA38434 Macom TIA Suggest reg. Write -------------------------------------//
// 2019 - 10 - 01 Ivan modify
//-----------------------------------------------------------------------------------------------------------------//
void MATA38434_MACOM_Suggest_R14_1F_SETTING()
{
	MATA38434_RX1_RX4_SET_PAGE( 0x1F );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xF8 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_Sequence_normal , Master_I2C1 );

	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xF0 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.DAC67_SET_normal , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0x3A , MATA38434_RX1_RX4_TIA_MEMORY_MAP.DAC6 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0x3B , MATA38434_RX1_RX4_TIA_MEMORY_MAP.DAC7 , Master_I2C1 );

	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xF1 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.DAC1415_SET_normal , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0x42 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.DAC14 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0x43 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.DAC15 , Master_I2C1 );

	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xF4 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.DAC3435_SET_normal , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0x57 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.DAC34 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0x56 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.DAC35 , Master_I2C1 );

	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xC5 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Control_TC_C5 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xC6 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Control_TC_C6 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xC7 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.SET_Average_C7 , Master_I2C1 );

	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xD5 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Control_TC_D5 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xD6 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Control_TC_D6 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xD7 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.SET_Average_D7 , Master_I2C1 );

	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xE5 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Control_TC_E5 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xE6 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Control_TC_E6 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xE7 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.SET_Average_E7 , Master_I2C1 );
}
void MATA38434_MACOM_Suggest_Rx14_SETTING()
{
	MATA38434_RX1_RX4_RESET();
	MATA38434_RX1_RX4_SET_PAGE( 0x1F );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0x00 , 0xA8 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0x01 , 0x04 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0x02 , 0x07 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0x0C , 0xE4 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xF8 , 0x00 , Master_I2C1 );

	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xF0 , 0x3F , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0x3A , 0x00 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0x3B , 0x00 , Master_I2C1 );

	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xF1 , 0x3F , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0x42 , 0x00 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0x43 , 0x00 , Master_I2C1 );

	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xF4 , 0xF3 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0x57 , 0x00 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0x56 , 0x00 , Master_I2C1 );

	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xC5 , 0x03 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xC6 , 0x00 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xC7 , 0x0C , Master_I2C1 );

	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xD5 , 0x03 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xD6 , 0x00 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xD7 , 0x0C , Master_I2C1 );

	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xE5 , 0x03 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xE6 , 0x00 , Master_I2C1 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0xE7 , 0x0C , Master_I2C1 );
}
//-----------------------------------------------------------------------------------------------------------------//
//------------------------------------ MATA38434_RX1_RX4 I2C Write    ---------------------------------------------//
// 2019-11-10 0xC2 setting to 0x31 - Macom Hank Suggest
//-----------------------------------------------------------------------------------------------------------------//
void MATA38434_RX1_RX4_W_CH_SET(unsigned char Channel)
{
	switch( Channel )
	{
		case MATA38434_CH0 :
			 	 	// CDR PAGE 20 CH0
					MATA38434_RX1_RX4_SET_PAGE( CDR_PAGE_20 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x00 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.OUTPUT_EQ_CH0          , Master_I2C1 );

					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x01 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_VGA_REGULATOR_CH0  , Master_I2C1 );

					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x02 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.REFERNCE_SWING_CH0     , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x03 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA0_Filter_CH0 , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x04 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA1_Filter_CH0 , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x05 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA2_Filter_CH0 , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x0A , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Tune_REF_SEL_LOW_CH0   , Master_I2C1 );

					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x0C , MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_VGA_SWEEP_CH0      , Master_I2C1 );

					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x0D , MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_Filter_Swing_CH0   , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x19 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.LOS_THRESH_HST_CH0     , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x1A , MATA38434_RX1_RX4_TIA_MEMORY_MAP.LOS_THRESHOLD_CH0      , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0xC2 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_AD_MODE_CH0        , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0xE2 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.B_AD_MODE_CH0          , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0xD2 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_AD_MODE_CH0        , Master_I2C1 );

					if( ( MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_AD_MODE_CH0 & 0x01 ) == 0x00 )
					{
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xC0 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_LSB_CH0 , Master_I2C1 );
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xC1 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_MSB_CH0 , Master_I2C1 );
					}

					if( ( MATA38434_RX1_RX4_TIA_MEMORY_MAP.B_AD_MODE_CH0 & 0x01 ) == 0x00 )
					{
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xE0 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Bias_LSB_CH0 , Master_I2C1 );
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xE1 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Bias_MSB_CH0 , Master_I2C1 );
					}

					if( ( MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_AD_MODE_CH0 & 0x01 ) == 0x00 )
					{
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xD0 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_LSB_CH0 , Master_I2C1 );
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xD1 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_MSB_CH0 , Master_I2C1 );
					}

					break;
		case MATA38434_CH1 :
					// CDR PAGE 21 CH1
					MATA38434_RX1_RX4_SET_PAGE( CDR_PAGE_21 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x00 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.OUTPUT_EQ_CH1          , Master_I2C1 );

					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x01 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_VGA_REGULATOR_CH1  , Master_I2C1 );

					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x02 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.REFERNCE_SWING_CH1     , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x03 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA0_Filter_CH1 , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x04 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA1_Filter_CH1 , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x05 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA2_Filter_CH1 , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x0A , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Tune_REF_SEL_LOW_CH1   , Master_I2C1 );

					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x0C , MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_VGA_SWEEP_CH1      , Master_I2C1 );

					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x0D , MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_Filter_Swing_CH1   , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x19 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.LOS_THRESH_HST_CH1     , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x1A , MATA38434_RX1_RX4_TIA_MEMORY_MAP.LOS_THRESHOLD_CH1      , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0xC2 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_AD_MODE_CH1        , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0xE2 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.B_AD_MODE_CH1          , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0xD2 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_AD_MODE_CH1        , Master_I2C1 );

					if( ( MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_AD_MODE_CH1 & 0x01 ) == 0x00 )
					{
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xC0 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_LSB_CH1 , Master_I2C1 );
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xC1 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_MSB_CH1 , Master_I2C1 );
					}

					if( ( MATA38434_RX1_RX4_TIA_MEMORY_MAP.B_AD_MODE_CH1 & 0x01 ) == 0x00 )
					{
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xE0 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Bias_LSB_CH1 , Master_I2C1 );
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xE1 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Bias_MSB_CH1 , Master_I2C1 );
					}

					if( ( MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_AD_MODE_CH1 & 0x01 ) == 0x00 )
					{
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xD0 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_LSB_CH1 , Master_I2C1 );
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xD1 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_MSB_CH1 , Master_I2C1 );
					}

					break;
		case MATA38434_CH2 :
					// CDR PAGE 22 CH2
					MATA38434_RX1_RX4_SET_PAGE( CDR_PAGE_22 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x00 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.OUTPUT_EQ_CH2          , Master_I2C1 );

					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x01 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_VGA_REGULATOR_CH2  , Master_I2C1 );

					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x02 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.REFERNCE_SWING_CH2     , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x03 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA0_Filter_CH2 , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x04 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA1_Filter_CH2 , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x05 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA2_Filter_CH2 , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x0A , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Tune_REF_SEL_LOW_CH2   , Master_I2C1 );

					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x0C , MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_VGA_SWEEP_CH2      , Master_I2C1 );

					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x0D , MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_Filter_Swing_CH2   , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x19 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.LOS_THRESH_HST_CH2     , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x1A , MATA38434_RX1_RX4_TIA_MEMORY_MAP.LOS_THRESHOLD_CH2      , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0xC2 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_AD_MODE_CH2        , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0xE2 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.B_AD_MODE_CH2          , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0xD2 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_AD_MODE_CH2        , Master_I2C1 );

					if( ( MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_AD_MODE_CH2 & 0x01 ) == 0x00 )
					{
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xC0 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_LSB_CH2 , Master_I2C1 );
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xC1 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_MSB_CH2 , Master_I2C1 );
					}

					if( ( MATA38434_RX1_RX4_TIA_MEMORY_MAP.B_AD_MODE_CH2 & 0x01 ) == 0x00 )
					{
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xE0 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Bias_LSB_CH2 , Master_I2C1 );
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xE1 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Bias_MSB_CH2 , Master_I2C1 );
					}

					if( ( MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_AD_MODE_CH2 & 0x01 ) == 0x00 )
					{
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xD0 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_LSB_CH2 , Master_I2C1 );
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xD1 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_MSB_CH2 , Master_I2C1 );
					}

					break;
		case MATA38434_CH3 :
					// CDR PAGE 23 CH3
					MATA38434_RX1_RX4_SET_PAGE( CDR_PAGE_23 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x00 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.OUTPUT_EQ_CH3          , Master_I2C1 );

					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x01 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_VGA_REGULATOR_CH3  , Master_I2C1 );

					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x02 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.REFERNCE_SWING_CH3     , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x03 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA0_Filter_CH3 , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x04 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA1_Filter_CH3 , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x05 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA2_Filter_CH3 , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x0A , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Tune_REF_SEL_LOW_CH3   , Master_I2C1 );

					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x0C , MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_VGA_SWEEP_CH3      , Master_I2C1 );

					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x0D , MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_Filter_Swing_CH3   , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x19 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.LOS_THRESH_HST_CH3     , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0x1A , MATA38434_RX1_RX4_TIA_MEMORY_MAP.LOS_THRESHOLD_CH3      , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0xC2 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_AD_MODE_CH3        , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0xE2 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.B_AD_MODE_CH3          , Master_I2C1 );
					Master_I2C_WriteByte( M38434_SLAVE_AD , 0xD2 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_AD_MODE_CH3        , Master_I2C1 );

					if( ( MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_AD_MODE_CH3 & 0x01 ) == 0x00 )
					{
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xC0 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_LSB_CH3 , Master_I2C1 );
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xC1 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_MSB_CH3 , Master_I2C1 );
					}

					if( ( MATA38434_RX1_RX4_TIA_MEMORY_MAP.B_AD_MODE_CH3 & 0x01 ) == 0x00 )
					{
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xE0 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Bias_LSB_CH3 , Master_I2C1 );
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xE1 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Bias_MSB_CH3 , Master_I2C1 );
					}

					if( ( MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_AD_MODE_CH3 & 0x01 ) == 0x00 )
					{
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xD0 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_LSB_CH3 , Master_I2C1 );
						Master_I2C_WriteByte( M38434_SLAVE_AD , 0xD1 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_MSB_CH3 , Master_I2C1 );
					}

					break;
		default :
				 break;
	}
}

void MATA38434_RX1_RX4_W_GLOBAL_SET()
{
	MATA38434_RX1_RX4_SET_PAGE( CDR_PAGE_0_00 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0x03 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.I2C_ADR_MODE , Master_I2C1 );
}

//-----------------------------------------------------------------------------------------------------------------//
//------------------------------------ MATA38434_RX1_RX4 I2C READ     ---------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------//
void MATA38434_RX1_RX4_READ_ONLY()
{
	// PAGE 00
	MATA38434_RX1_RX4_SET_PAGE( CDR_PAGE_0_00 );
	MATA38434_RX1_RX4_TIA_MEMORY_MAP.CHIP_ID          = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x00 , Master_I2C1 );
	MATA38434_RX1_RX4_TIA_MEMORY_MAP.REVID            = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x01 , Master_I2C1 );
	MATA38434_RX1_RX4_TIA_MEMORY_MAP.I2C_ADR_MODE     = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x03 , Master_I2C1 );
}

void MATA38434_RX1_RX4_R_CH_SET(unsigned char Channel)
{
	switch( Channel )
	{
		case MATA38434_CH0 :
			 	 	// CDR PAGE 20 CH0
					MATA38434_RX1_RX4_SET_PAGE( CDR_PAGE_20 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.OUTPUT_EQ_CH0          = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x00 , Master_I2C1 );

					MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_VGA_REGULATOR_CH0  = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x01 , Master_I2C1 );

					MATA38434_RX1_RX4_TIA_MEMORY_MAP.REFERNCE_SWING_CH0     = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x02 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA0_Filter_CH0 = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x03 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA1_Filter_CH0 = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x04 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA2_Filter_CH0 = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x05 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Tune_REF_SEL_LOW_CH0   = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x0A , Master_I2C1 );

					MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_VGA_SWEEP_CH0      = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x0C , Master_I2C1 );

					MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_Filter_Swing_CH0   = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x0D , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.LOS_THRESH_HST_CH0     = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x19 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.LOS_THRESHOLD_CH0      = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x1A , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_LSB_CH0            = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xC0 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_MSB_CH0            = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xC1 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_AD_MODE_CH0        = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xC2 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Bias_LSB_CH0           = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xE0 , Master_I2C1 );
			 	 	MATA38434_RX1_RX4_TIA_MEMORY_MAP.Bias_MSB_CH0           = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xE1 , Master_I2C1 );
			 	 	MATA38434_RX1_RX4_TIA_MEMORY_MAP.B_AD_MODE_CH0          = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xE2 , Master_I2C1 );
			 	 	MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_LSB_CH0            = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xD0 , Master_I2C1 );
			 	 	MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_MSB_CH0            = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xD1 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_AD_MODE_CH0        = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xD2 , Master_I2C1 );

					break;

		case MATA38434_CH1 :
					// CDR PAGE 21 CH1
					MATA38434_RX1_RX4_SET_PAGE( CDR_PAGE_21 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.OUTPUT_EQ_CH1          = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x00 , Master_I2C1 );

					MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_VGA_REGULATOR_CH1  = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x01 , Master_I2C1 );

					MATA38434_RX1_RX4_TIA_MEMORY_MAP.REFERNCE_SWING_CH1     = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x02 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA0_Filter_CH1 = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x03 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA1_Filter_CH1 = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x04 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA2_Filter_CH1 = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x05 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Tune_REF_SEL_LOW_CH1   = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x0A , Master_I2C1 );

					MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_VGA_SWEEP_CH1      = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x0C , Master_I2C1 );

					MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_Filter_Swing_CH1   = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x0D , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.LOS_THRESH_HST_CH1     = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x19 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.LOS_THRESHOLD_CH1      = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x1A , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_LSB_CH1            = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xC0 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_MSB_CH1            = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xC1 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_AD_MODE_CH1        = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xC2 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Bias_LSB_CH1           = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xE0 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Bias_MSB_CH1           = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xE1 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.B_AD_MODE_CH1          = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xE2 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_LSB_CH1            = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xD0 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_MSB_CH1            = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xD1 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_AD_MODE_CH1        = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xD2 , Master_I2C1 );

					break;

		case MATA38434_CH2 :
					// CDR PAGE 22 CH2
					MATA38434_RX1_RX4_SET_PAGE( CDR_PAGE_22 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.OUTPUT_EQ_CH2          = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x00 , Master_I2C1 );

					MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_VGA_REGULATOR_CH2  = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x01 , Master_I2C1 );

					MATA38434_RX1_RX4_TIA_MEMORY_MAP.REFERNCE_SWING_CH2     = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x02 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA0_Filter_CH2 = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x03 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA1_Filter_CH2 = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x04 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA2_Filter_CH2 = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x05 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Tune_REF_SEL_LOW_CH2   = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x0A , Master_I2C1 );

					MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_VGA_SWEEP_CH2      = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x0C , Master_I2C1 );

					MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_Filter_Swing_CH2   = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x0D , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.LOS_THRESH_HST_CH2     = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x19 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.LOS_THRESHOLD_CH2      = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x1A , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_LSB_CH2            = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xC0 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_MSB_CH2            = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xC1 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_AD_MODE_CH2        = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xC2 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Bias_LSB_CH2           = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xE0 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Bias_MSB_CH2           = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xE1 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.B_AD_MODE_CH2          = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xE2 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_LSB_CH2            = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xD0 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_MSB_CH2            = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xD1 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_AD_MODE_CH2        = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xD2 , Master_I2C1 );
					break;

		case MATA38434_CH3 :
					// CDR PAGE 22 CH2
					MATA38434_RX1_RX4_SET_PAGE( CDR_PAGE_23 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.OUTPUT_EQ_CH3          = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x00 , Master_I2C1 );

					MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_VGA_REGULATOR_CH3  = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x01 , Master_I2C1 );

					MATA38434_RX1_RX4_TIA_MEMORY_MAP.REFERNCE_SWING_CH3     = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x02 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA0_Filter_CH3 = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x03 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA1_Filter_CH3 = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x04 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Manual_VGA2_Filter_CH3 = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x05 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Tune_REF_SEL_LOW_CH3   = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x0A , Master_I2C1 );

					MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_VGA_SWEEP_CH3      = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x0C , Master_I2C1 );

					MATA38434_RX1_RX4_TIA_MEMORY_MAP.TIA_Filter_Swing_CH3   = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x0D , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.LOS_THRESH_HST_CH3     = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x19 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.LOS_THRESHOLD_CH3      = Master_I2C_ReadByte( M38434_SLAVE_AD , 0x1A , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_LSB_CH3            = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xC0 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_MSB_CH3            = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xC1 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.AGC_AD_MODE_CH3        = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xC2 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Bias_LSB_CH3           = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xE0 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.Bias_MSB_CH3           = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xE1 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.B_AD_MODE_CH3          = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xE2 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_LSB_CH3            = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xD0 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_MSB_CH3            = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xD1 , Master_I2C1 );
					MATA38434_RX1_RX4_TIA_MEMORY_MAP.DCD_AD_MODE_CH3        = Master_I2C_ReadByte( M38434_SLAVE_AD , 0xD2 , Master_I2C1 );

					break;
		default :
				 break;
	}
}

void MATA38434_P8A_Write()
{
	MATA38434_RX1_RX4_W_GLOBAL_SET();
	MATA38434_RX1_RX4_W_CH_SET( MATA38434_CH0 );
	MATA38434_RX1_RX4_W_CH_SET( MATA38434_CH1 );
	MATA38434_RX1_RX4_W_CH_SET( MATA38434_CH2 );
	MATA38434_RX1_RX4_W_CH_SET( MATA38434_CH3 );
	MATA38434_MACOM_Suggest_R14_1F_SETTING();
}


void MATA38434_P8A_READ()
{
	MATA38434_RX1_RX4_READ_ONLY();
	MATA38434_RX1_RX4_R_CH_SET( MATA38434_CH0 );
	MATA38434_RX1_RX4_R_CH_SET( MATA38434_CH1 );
	MATA38434_RX1_RX4_R_CH_SET( MATA38434_CH2 );
	MATA38434_RX1_RX4_R_CH_SET( MATA38434_CH3 );
}

unsigned int GET_RRSI_CH0_CH3(unsigned char RSSI_CH)
{
	unsigned int RSSI_Value;
	unsigned char TEMP ;

	TEMP = ( RSSI_CH << 5 ) & 0x60 ;
	MATA38434_RX1_RX4_TIA_MEMORY_MAP.I2C_ADR_MODE = TEMP ;
	MATA38434_RX1_RX4_SET_PAGE( CDR_PAGE_0_00 );
	Master_I2C_WriteByte( M38434_SLAVE_AD , 0x03 , MATA38434_RX1_RX4_TIA_MEMORY_MAP.I2C_ADR_MODE , Master_I2C1 );

	RSSI_Value = ADCV_Conversion_Voltage( RSSI_RX14 );

	return RSSI_Value;
}

























