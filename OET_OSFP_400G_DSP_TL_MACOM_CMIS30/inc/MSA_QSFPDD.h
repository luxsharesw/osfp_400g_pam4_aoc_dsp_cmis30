/*
 * MSA_QSFPDD.h
 *
 *  Created on: 2017�~11��9��
 *      Author: Ivan_Lin
 */

#ifndef INC_MSA_QSFPDD_H_
#define INC_MSA_QSFPDD_H_
//----------------------------------------------------//
//AW
//----------------------------------------------------//
#define ADDR_TXFAULT       7
#define ADDR_TXLOS         8
#define ADDR_TXCDR_LOL     9
#define ADDR_TX_EQ_FAULT  10
#define ADDR_TXP_HA       11
#define ADDR_TXP_LA       12
#define ADDR_TXP_HW       13
#define ADDR_TXP_LW       14
#define ADDR_TXB_HA       15
#define ADDR_TXB_LA       16
#define ADDR_TXB_HW       17
#define ADDR_TXB_LW       18
#define ADDR_RXLOS        19
#define ADDR_RXCDR_LOL    20
#define ADDR_RXP_HA       21
#define ADDR_RXP_LA       22
#define ADDR_RXP_HW       23
#define ADDR_RXP_LW       24

#define TXFAULT_INT    0x0001
#define TXLOS_INT      0x0002
#define TXCDR_LOL_INT  0x0004
#define TXEQ_FAULT_INT 0x0008
#define TXPHA_INT      0x0010
#define TXPLA_INT      0x0020
#define TXPHW_INT      0x0040
#define TXPLW_INT      0x0080
#define TXBHA_INT      0x0100
#define TXBLA_INT      0x0200
#define TXBHW_INT      0x0400
#define TXBLW_INT      0x0800

#define RX_ROS_INT     0x0001
#define RXCDR_LOL_INT  0x0002
#define RXHA_INT       0x0004
#define RXLA_INT       0x0008
#define RXHW_INT       0x0010
#define RXLW_INT       0x0020
#define VCC_TEMP_INT   0x0040
//----------------------------------------------------//
//SRAM
//----------------------------------------------------//
extern xdata unsigned char QSFPDD_A0[256];
extern xdata unsigned char QSFPDD_Page11[128];
extern xdata unsigned char QSFPDD_Page1[128];
extern xdata unsigned char QSFPDD_Page2[128];

extern xdata unsigned char QSFPDD_Page10[128];
extern xdata unsigned char QSFPDD_Page11[128];


extern xdata unsigned int CHECKSUM;
extern xdata unsigned char CTEL_Table[16];
extern xdata unsigned char Pro_Table[16];
extern xdata unsigned char OP_AMP_Table[16];
extern xdata unsigned char Post_Table[16];

//----------------------------------------------------//
//PowerOn_Table_Init.c
//----------------------------------------------------//
extern void PowerON_Table_Init();
//----------------------------------------------------//
//QSFPDD_MSA_DDMI.c
//----------------------------------------------------//
extern void QSFPDD_DDMI_StateMachine(unsigned char StateMachine );
extern void RX_POWER_M_CH0_CH3();
extern void RX_POWER_M_CH4_CH7();
extern void Temperature_Monitor(int tempsensor_value);
extern void VCC_Monitor(unsigned int VCC_Vaule);
extern void QSFPDD_DDMI_ONLY_ADC_GET();
//----------------------------------------------------//
//QSFPDD_MSA_AW.c
//----------------------------------------------------//
extern void DDMI_AW_Monitor();
extern void Clear_VCC_TEMP_Flag();
extern void Clear_Flag(unsigned char Current_AD);
extern void Clear_Module_state_Byte8();
extern void QSFPDD_TXLOS_LOL_AW();
extern void QSFPDD_RXLOS_LOL_AW();
//----------------------------------------------------//
// Power_Sequencing.c
//----------------------------------------------------//
extern void Power_sequencing();
extern void ModSelL_Function();
extern void MODSEL_MODE_TRIG_SET();
//----------------------------------------------------//
//EFM8LB_SMBUS_ISR.c
//----------------------------------------------------//
extern void SRMA_Flash_Function();
//----------------------------------------------------//
//QSFPDD_MSA_StateMachine.c
//----------------------------------------------------//
extern void QSFPDD_MSA_StateMachine();
//----------------------------------------------------//
//QDD_MSA_Optional_Control.c
//----------------------------------------------------//
extern void QDD_MSA_CTLE_PRE_POST_AMP_CONTROL( unsigned char Control_Write_Enable );

#endif /* INC_MSA_QSFPDD_H_ */
