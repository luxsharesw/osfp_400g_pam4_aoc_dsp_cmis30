/*
 * MA38435_TX1_TX5.h
 *
 *  Created on: 2019�~6��20��
 *      Author: Ivan_Lin
 */

#ifndef INC_MA38435_TX1_TX4_H_
#define INC_MA38435_TX1_TX4_H_

#define MALD38435_SADR    0x1C

extern void MALD38435_TX1_TX4_Control_Write_ALL();
extern void MALD38435_TX1_TX4_Control_READ_ALL();
extern void MALD38435_TX1_TX4_WRITE_P82();
extern void MALD38435_TX1_TX4_READ_P82();

extern unsigned int TXBIAS_TX1_TX4(unsigned char BIAS_CH);
extern void MALD38435_TX1_TX4_RESET();

xdata struct MATA38435_TX1_TX4_MEMORY
{
	// Read only
	unsigned char CHIP_ID ;                 // reg. address 00  R   SRAM Address 80
	unsigned char REVID ;                   // reg. address 01  R   SRAM Address 81
	unsigned char LOS_Status ;              // reg. address 17  R   SRAM Address 82
	unsigned char TX_Status ;               // reg. address 18  R   SRAM Address 83
	unsigned char Tx_Fault_State ;          // reg. address 1A  R   SRAM Address 84
	unsigned char ADC_out0_msbs ;           // reg. address 68  R   SRAM Address 85
	unsigned char ADC_out0_lsbs ;           // reg. address 69  R   SRAM Address 86
	unsigned char RSVD_0 ;                  //                  R   SRAM Address 87
	unsigned char RSVD_1 ;                  //                  R   SRAM Address 88
	unsigned char RSVD_2 ;                  //                  R   SRAM Address 89
	unsigned char RSVD_3 ;                  //                  R   SRAM Address 8A
	// Write Function
	unsigned char RESET ;                   // reg. address 02  RW  SRAM Address 8B
	unsigned char IO_Control ;              // reg. address 03  RW  SRAM Address 8C
	unsigned char IC_RSVD_04 ;              // reg. address 04  RW  SRAM Address 8D
	unsigned char I2C_ADR_MODE ;            // reg. address 05  RW  SRAM Address 8E
	unsigned char CH_MODE ;                 // reg. address 10  RW  SRAM Address 8F
	unsigned char LOS_MASK ;                // reg. address 16  RW  SRAM Address 90
	unsigned char LOS_ALARM ;               // reg. address 19  RW  SRAM Address 91
	unsigned char TX_F_A_MASK ;             // reg. address 1B  RW  SRAM Address 92
	unsigned char TX_F_Alarm ;              // reg. address 1C  RW  SRAM Address 93
	unsigned char Ignore_TXF ;              // reg. address 1D  RW  SRAM Address 94
	unsigned char IC_RSVD_1E ;              // reg. address 1E  RW  SRAM Address 95
	unsigned char LOS_THRS ;     			// reg. address 21  RW  SRAM Address 96
	unsigned char LOS_HYST ; 			 	// reg. address 22  RW  SRAM Address 97
	unsigned char Rvcsel_CH0 ;  			// reg. address 25  RW  SRAM Address 98
	unsigned char Rvcsel_CH1 ;              // reg. address 26  RW  SRAM Address 99
	unsigned char Rvcsel_CH2 ;              // reg. address 27  RW  SRAM Address 9A
	unsigned char Rvcsel_CH3 ;              // reg. address 28  RW  SRAM Address 9B
	unsigned char IC_RSVD_29 ;              // reg. address 29  RW  SRAM Address 9C
	unsigned char OUTPUT_MUTE ;             // reg. address 40  RW  SRAM Address 9D
	unsigned char TX_DISABLE ;              // reg. address 41  RW  SRAM Address 9E
	unsigned char IBias_CH0 ;               // reg. address 42  RW  SRAM Address 9F
	unsigned char IBias_CH1 ;               // reg. address 43  RW  SRAM Address A0
	unsigned char IBias_CH2 ;               // reg. address 44  RW  SRAM Address A1
	unsigned char IBias_CH3 ;               // reg. address 45  RW  SRAM Address A2
	unsigned char Imod_CH0 ;                // reg. address 46  RW  SRAM Address A3
	unsigned char Imod_CH1 ;                // reg. address 47  RW  SRAM Address A4
	unsigned char Imod_CH2 ;                // reg. address 48  RW  SRAM Address A5
	unsigned char Imod_CH3 ;                // reg. address 49  RW  SRAM Address A6
	unsigned char AC_Gain_CH0 ;             // reg. address 4A  RW  SRAM Address A7
	unsigned char AC_Gain_CH1 ;             // reg. address 4B  RW  SRAM Address A8
	unsigned char AC_Gain_CH2 ;             // reg. address 4C  RW  SRAM Address A9
	unsigned char AC_Gain_CH3 ;             // reg. address 4D  RW  SRAM Address AA
	unsigned char DC_Gain_CH0 ;             // reg. address 4F  RW  SRAM Address AB
	unsigned char DC_Gain_CH1 ;  		    // reg. address 50  RW  SRAM Address AC
	unsigned char DC_Gain_CH2 ; 			// reg. address 51  RW  SRAM Address AD
	unsigned char DC_Gain_CH3 ;  			// reg. address 52  RW  SRAM Address AE
	unsigned char IC_RSVD_5A ;              // reg. address 5A  RW  SRAM Address AF

	unsigned char IBurnIn ;                 // reg. address 5E  RW  SRAM Address B0
	unsigned char BurnIn_EN ;               // reg. address 5F  RW  SRAM Address B1
	unsigned char ADC_Config0 ;             // reg. address 60  RW  SRAM Address B2
	unsigned char ADC_Config2 ;             // reg. address 62  RW  SRAM Address B3
	unsigned char IC_RSVD_64 ;              // reg. address 64  RW  SRAM Address B4

	unsigned char Driver_ADC_CHSEL ;        // reg. address 91  RW  SRAM Address B5
	unsigned char Driver_ADC_Measmt ;       // reg. address 92  RW  SRAM Address B6

	unsigned char RSVD_Buffer[65] ;         //                  RW  SRAM Address B7 - F7
	// Direct_Control
	unsigned char Direct_AD;                // Direct_Control Address RW  SRAM Address 0xF8
	unsigned char Direct_Data[5];           // Direct_Control Data    RW  SRAM Address 0xF9 - 0xFD
	unsigned char Direct_RW;                // Direct_Control RW      RW  SRAM Address 0xFE
	unsigned char Direct_EN;                // Direct_Control RW      RW  SRAM Address 0xFF
};

extern xdata struct MATA38435_TX1_TX4_MEMORY   MATA38435_TX1_TX4_MEMORY_MAP;

#endif /* INC_MA38435_TX1_TX4_H_ */
