/*
 * MA38434_RX1_RX4.h
 *
 *  Created on: 2019�~6��20��
 *      Author: Ivan_Lin
 */

#ifndef INC_MA38434_RX1_RX4_H_
#define INC_MA38434_RX1_RX4_H_

#define M38434_SLAVE_AD   0x20

extern void MATA38434_RX1_RX4_SET_PAGE( unsigned char PAGE );
extern unsigned int GET_RRSI_CH0_CH3(unsigned char RSSI_CH);
extern void MATA38434_P8A_READ();
extern void MATA38434_P8A_Write();
extern void MATA38434_RX1_RX4_RESET();
extern void MATA38434_MACOM_Suggest_Rx14_SETTING();

xdata struct MATA38434_RX1_RX4_TIA_MEMORY
{
	unsigned char CHIP_ID ;                 // reg. address 00  RW  SRAM Address 80
	unsigned char REVID ;                   // reg. address 01  RW  SRAM Address 81
	unsigned char I2C_ADR_MODE ;            // reg. address 03  RW  SRAM Address 82
	// CDR Page 20 CH0
	unsigned char OUTPUT_EQ_CH0 ;           // reg. address 00  RW  SRAM Address 83
	unsigned char REFERNCE_SWING_CH0 ;      // reg. address 02  RW  SRAM Address 84
	unsigned char Manual_VGA0_Filter_CH0 ;  // reg. address 03  RW  SRAM Address 85
	unsigned char Manual_VGA1_Filter_CH0 ;  // reg. address 04  RW  SRAM Address 86
	unsigned char Manual_VGA2_Filter_CH0 ;  // reg. address 05  RW  SRAM Address 87
	unsigned char Tune_REF_SEL_LOW_CH0 ;    // reg. address 0A  RW  SRAM Address 88
	unsigned char TIA_Filter_Swing_CH0 ;    // reg. address 0D  RW  SRAM Address 89
	unsigned char LOS_THRESH_HST_CH0 ;      // reg. address 19  RW  SRAM Address 8A
	unsigned char LOS_THRESHOLD_CH0 ;       // reg. address 1A  RW  SRAM Address 8B
	unsigned char AGC_LSB_CH0 ;             // reg. address C0  RW  SRAM Address 8C
	unsigned char AGC_MSB_CH0 ;             // reg. address C1  RW  SRAM Address 8D
	unsigned char AGC_AD_MODE_CH0 ;         // reg. address C2  RW  SRAM Address 8E
	unsigned char Bias_LSB_CH0 ;            // reg. address E0  RW  SRAM Address 8F
	unsigned char Bias_MSB_CH0 ;            // reg. address E1  RW  SRAM Address 90
	unsigned char B_AD_MODE_CH0 ;           // reg. address E2  RW  SRAM Address 91
	unsigned char DCD_LSB_CH0 ;             // reg. address D0  RW  SRAM Address 92
	unsigned char DCD_MSB_CH0 ;             // reg. address D1  RW  SRAM Address 93
	unsigned char DCD_AD_MODE_CH0 ;         // reg. address D2  RW  SRAM Address 94
	unsigned char TIA_VGA_REGULATOR_CH0 ;   // reg. address 01  RW  SRAM Address 95
	unsigned char TIA_VGA_SWEEP_CH0 ;  	    // reg. address 0C  RW  SRAM Address 96
	unsigned char RSVD_2_CH0 ; 			 	//                  RW  SRAM Address 97
	unsigned char RSVD_3_CH0 ;  			//                  RW  SRAM Address 98
	// CDR Page 21 CH1
	unsigned char OUTPUT_EQ_CH1 ;           // reg. address 00  RW  SRAM Address 99
	unsigned char REFERNCE_SWING_CH1 ;      // reg. address 02  RW  SRAM Address 9A
	unsigned char Manual_VGA0_Filter_CH1 ;  // reg. address 03  RW  SRAM Address 9B
	unsigned char Manual_VGA1_Filter_CH1 ;  // reg. address 04  RW  SRAM Address 9C
	unsigned char Manual_VGA2_Filter_CH1 ;  // reg. address 05  RW  SRAM Address 9D
	unsigned char Tune_REF_SEL_LOW_CH1 ;    // reg. address 0A  RW  SRAM Address 9E
	unsigned char TIA_Filter_Swing_CH1 ;    // reg. address 0D  RW  SRAM Address 9F
	unsigned char LOS_THRESH_HST_CH1 ;      // reg. address 19  RW  SRAM Address A0
	unsigned char LOS_THRESHOLD_CH1 ;       // reg. address 1A  RW  SRAM Address A1
	unsigned char AGC_LSB_CH1 ;             // reg. address C0  RW  SRAM Address A2
	unsigned char AGC_MSB_CH1 ;             // reg. address C1  RW  SRAM Address A3
	unsigned char AGC_AD_MODE_CH1 ;         // reg. address C2  RW  SRAM Address A4
	unsigned char Bias_LSB_CH1 ;            // reg. address E0  RW  SRAM Address A5
	unsigned char Bias_MSB_CH1 ;            // reg. address E1  RW  SRAM Address A6
	unsigned char B_AD_MODE_CH1 ;           // reg. address E2  RW  SRAM Address A7
	unsigned char DCD_LSB_CH1 ;             // reg. address D0  RW  SRAM Address A8
	unsigned char DCD_MSB_CH1 ;             // reg. address D1  RW  SRAM Address A9
	unsigned char DCD_AD_MODE_CH1 ;         // reg. address D2  RW  SRAM Address AA
	unsigned char TIA_VGA_REGULATOR_CH1 ;   // reg. address 01  RW  SRAM Address AB
	unsigned char TIA_VGA_SWEEP_CH1 ;  	    // reg. address 0C  RW  SRAM Address AC
	unsigned char RSVD_2_CH1 ; 			 	//                  RW  SRAM Address AD
	unsigned char RSVD_3_CH1 ;  			//                  RW  SRAM Address AE
	// CDR Page 22 CH2
	unsigned char OUTPUT_EQ_CH2 ;           // reg. address 00  RW  SRAM Address AF
	unsigned char REFERNCE_SWING_CH2 ;      // reg. address 02  RW  SRAM Address B0
	unsigned char Manual_VGA0_Filter_CH2 ;  // reg. address 03  RW  SRAM Address B1
	unsigned char Manual_VGA1_Filter_CH2 ;  // reg. address 04  RW  SRAM Address B2
	unsigned char Manual_VGA2_Filter_CH2 ;  // reg. address 05  RW  SRAM Address B3
	unsigned char Tune_REF_SEL_LOW_CH2 ;    // reg. address 0A  RW  SRAM Address B4
	unsigned char TIA_Filter_Swing_CH2 ;    // reg. address 0D  RW  SRAM Address B5
	unsigned char LOS_THRESH_HST_CH2 ;      // reg. address 19  RW  SRAM Address B6
	unsigned char LOS_THRESHOLD_CH2 ;       // reg. address 1A  RW  SRAM Address B7
	unsigned char AGC_LSB_CH2 ;             // reg. address C0  RW  SRAM Address B8
	unsigned char AGC_MSB_CH2 ;             // reg. address C1  RW  SRAM Address B9
	unsigned char AGC_AD_MODE_CH2 ;         // reg. address C2  RW  SRAM Address BA
	unsigned char Bias_LSB_CH2 ;            // reg. address E0  RW  SRAM Address BB
	unsigned char Bias_MSB_CH2 ;            // reg. address E1  RW  SRAM Address BC
	unsigned char B_AD_MODE_CH2 ;           // reg. address E2  RW  SRAM Address BD
	unsigned char DCD_LSB_CH2 ;             // reg. address D0  RW  SRAM Address BE
	unsigned char DCD_MSB_CH2 ;             // reg. address D1  RW  SRAM Address BF
	unsigned char DCD_AD_MODE_CH2 ;         // reg. address D2  RW  SRAM Address C0
	unsigned char TIA_VGA_REGULATOR_CH2 ;   // reg. address 01  RW  SRAM Address C1
	unsigned char TIA_VGA_SWEEP_CH2 ;  	    // reg. address 0C  RW  SRAM Address C2
	unsigned char RSVD_2_CH2 ; 			 	//                  RW  SRAM Address C3
	unsigned char RSVD_3_CH2 ;  			//                  RW  SRAM Address C4
	// CDR Page 23 CH3
	unsigned char OUTPUT_EQ_CH3 ;           // reg. address 00  RW  SRAM Address C5
	unsigned char REFERNCE_SWING_CH3 ;      // reg. address 02  RW  SRAM Address C6
	unsigned char Manual_VGA0_Filter_CH3 ;  // reg. address 03  RW  SRAM Address C7
	unsigned char Manual_VGA1_Filter_CH3 ;  // reg. address 04  RW  SRAM Address C8
	unsigned char Manual_VGA2_Filter_CH3 ;  // reg. address 05  RW  SRAM Address C9
	unsigned char Tune_REF_SEL_LOW_CH3 ;    // reg. address 0A  RW  SRAM Address CA
	unsigned char TIA_Filter_Swing_CH3 ;    // reg. address 0D  RW  SRAM Address CB
	unsigned char LOS_THRESH_HST_CH3 ;      // reg. address 19  RW  SRAM Address CC
	unsigned char LOS_THRESHOLD_CH3 ;       // reg. address 1A  RW  SRAM Address CD
	unsigned char AGC_LSB_CH3 ;             // reg. address C0  RW  SRAM Address CE
	unsigned char AGC_MSB_CH3 ;             // reg. address C1  RW  SRAM Address CF
	unsigned char AGC_AD_MODE_CH3 ;         // reg. address C2  RW  SRAM Address D0
	unsigned char Bias_LSB_CH3 ;            // reg. address E0  RW  SRAM Address D1
	unsigned char Bias_MSB_CH3 ;            // reg. address E1  RW  SRAM Address D2
	unsigned char B_AD_MODE_CH3 ;           // reg. address E2  RW  SRAM Address D3
	unsigned char DCD_LSB_CH3 ;             // reg. address D0  RW  SRAM Address D4
	unsigned char DCD_MSB_CH3 ;             // reg. address D1  RW  SRAM Address D5
	unsigned char DCD_AD_MODE_CH3 ;         // reg. address D2  RW  SRAM Address D6
	unsigned char TIA_VGA_REGULATOR_CH3 ;   // reg. address 01  RW  SRAM Address D7
	unsigned char TIA_VGA_SWEEP_CH3 ;  	    // reg. address 0C  RW  SRAM Address D8
	unsigned char RSVD_2_CH3 ; 			 	//                  RW  SRAM Address D9
	unsigned char RSVD_3_CH3 ;  			//                  RW  SRAM Address DA

	unsigned char AGC_Sequence_normal;      //reg. address F8   RW  SRAM Address DB
	unsigned char DAC67_SET_normal;         //reg. address F0   RW  SRAM Address DC
	unsigned char DAC6;                     //reg. address 3A   RW  SRAM Address DD
	unsigned char DAC7;                     //reg. address 3B   RW  SRAM Address DE

	unsigned char DAC1415_SET_normal;       //reg. address F1   RW  SRAM Address DF
	unsigned char DAC14;                    //reg. address 42   RW  SRAM Address E0
	unsigned char DAC15;                    //reg. address 43   RW  SRAM Address E1

	unsigned char DAC3435_SET_normal;       //reg. address F4   RW  SRAM Address E2
	unsigned char DAC34;                    //reg. address 56   RW  SRAM Address E3
	unsigned char DAC35;                    //reg. address 57   RW  SRAM Address E4

	unsigned char Control_TC_C5;            //reg. address C5   RW  SRAM Address E5
	unsigned char Control_TC_C6;            //reg. address C6   RW  SRAM Address E6
	unsigned char SET_Average_C7;           //reg. address C7   RW  SRAM Address E7

	unsigned char Control_TC_D5;            //reg. address D5   RW  SRAM Address E8
	unsigned char Control_TC_D6;            //reg. address D6   RW  SRAM Address E9
	unsigned char SET_Average_D7;           //reg. address D7   RW  SRAM Address EA

	unsigned char Control_TC_E5;            //reg. address E5   RW  SRAM Address EB
	unsigned char Control_TC_E6;            //reg. address E6   RW  SRAM Address EC
	unsigned char SET_Average_E7;           //reg. address E7   RW  SRAM Address ED
	unsigned char Bias_Offset_17;           //reg. address 17   RW  SRAM Address EE

	unsigned char RSVD_Buffer[9] ;         //                  RW  SRAM Address EF - F7
	// Direct_Control
	unsigned char Direct_AD;                // Direct_Control Address RW  SRAM Address 0xF8
	unsigned char Direct_Data[5];           // Direct_Control Data    RW  SRAM Address 0xF9 - 0xFD
	unsigned char Direct_RW;                // Direct_Control RW      RW  SRAM Address 0xFE
	unsigned char Direct_EN;                // Direct_Control RW      RW  SRAM Address 0xFF
};

extern xdata struct MATA38434_RX1_RX4_TIA_MEMORY   MATA38434_RX1_RX4_TIA_MEMORY_MAP;


#endif /* INC_MA38434_RX1_RX4_H_ */
