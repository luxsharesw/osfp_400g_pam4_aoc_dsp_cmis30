/*
 * BRCM_DSP58281_Optional.h
 *
 *  Created on: 2019�~8��28��
 *      Author: Ivan_Lin
 */

#ifndef INC_BRCM_DSP58281_OPTIONAL_H_
#define INC_BRCM_DSP58281_OPTIONAL_H_

extern void DSP59281_SSPRQ_Control_Enable(unsigned char Lan_Channel);
extern void DSP59281_SSPRQ_Control_Disable(unsigned char Lan_Channel);
extern void DSP59281_System_side_Remote_Loopback_Control(unsigned char Lan_Channel,unsigned char Enable);
extern void DSP59281_Line_Side_G_Loopback_Control(unsigned char Lan_Channel,unsigned char Enable);
extern void DSP59281_PRBS13Q_Control_Enable(unsigned char Lan_Channel);
extern void DSP59281_PRBS13Q_Control_Disable(unsigned char Lan_Channel);
extern void DSP59281_PREFORMANCE_Medium_SETTING();
extern void DSP59281_RX_BW_SETTING();

extern void DSP59281_RX_NLdet_Enable_Control(unsigned char Enable);
extern void DSP59281_RX_Exsilcer_Control(unsigned char Mode_Change);

extern void System_Side_PRE1(unsigned char Channel_SETING);
extern void System_Side_Main(unsigned char Channel_SETING);
extern void System_Side_POST1(unsigned char Channel_SETING);

#endif /* INC_BRCM_DSP58281_OPTIONAL_H_ */
