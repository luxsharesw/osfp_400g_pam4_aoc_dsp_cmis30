/*
 * EFM8LB_Flash_Map.h
 *
 *  Created on: 2017�~3��17��
 *      Author: 100476
 */

#ifndef EFM8LB_FLASH_MAP_H_
#define EFM8LB_FLASH_MAP_H_

#define MCU_FW_VERSION       0x0314

#define Flash_LUT_Start      0xE800   // Flash 53K

#define FS_QSFPDD_A0         0xE800   // 53 K
#define FS_QSFPDD_P0         0xE880   // 53.128 K
#define FS_QSFPDD_P1         0xEA00   // 53.5 K
#define FS_QSFPDD_P2         0xEA80   // 54 K

#define Calibration_Table    0xEC00   // 55 K
#define Cablbration_Table2   0xEC80   // 55.5 K
#define MSA_Option_Table     0xEE00   // 56 K

#define FS_MA38435_T14_P82   0xF000   // 56.5K
#define FS_MA38435_T58_P84   0xF080   // 57 K

#define FS_59281PHY0_LS_P86  0xF200   // 57.5 K
#define FS_59281PHY0_SS_P87  0xF280   // 57.628 K

#define FS_MA38434_R14_P8A   0xF400   // 59 K
#define FS_MA38434_R58_P8B   0xF480   // 59.5 K

#define Product_Line         0xF600   // 61.5 K

#define FS_59281PHY1_LS_P8C  0xF800   // 62 K
#define FS_59281PHY1_SS_P8E  0xF880   // 62.128 K

#define Flash_End	         0xF9FF

#endif /* EFM8LB_FLASH_MAP_H_ */
