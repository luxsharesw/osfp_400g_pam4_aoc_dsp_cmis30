/*
 * Calibration_Struct_2.h
 *
 *  Created on: 2017�~10��25��
 *      Author: Ivan_Lin
 */

#ifndef INC_CALIBRATION_STRUCT_2_H_
#define INC_CALIBRATION_STRUCT_2_H_


xdata struct CALIB_MEMORY_1
{
	unsigned char VCC1_SCALEM;           //Address = 0x80
	unsigned char VCC1_SCALEL;           //Address = 0x81
			 int  VCC1_OFFSET;           //Address = 0x82 - 0x83

	unsigned char TEMP1_SCALE1M;         //Address = 0x84
	unsigned char TEMP1_SCALE1L;         //Address = 0x85
			 char TEMP1_OFFSET1;         //Address = 0x86

	unsigned char IBias4_SCALEM;        //Address = 0x87
	unsigned char IBias4_SCALE1L;       //Address = 0x88
		     int  IBias4_OFFSET;        //Address = 0x89 - 0x8A

	unsigned char IBias5_SCALEM;        //Address = 0x8B
	unsigned char IBias5_SCALE1L;       //Address = 0x8C
			 int  IBias5_OFFSET;        //Address = 0x8D - 0x8E

	unsigned char IBias6_SCALEM;        //Address = 0x8F
	unsigned char IBias6_SCALE1L;       //Address = 0x90
			 int  IBias6_OFFSET;        //Address = 0x91 - 0x92

	unsigned char IBias7_SCALEM;        //Address = 0x93
	unsigned char IBias7_SCALE1L;       //Address = 0x94
			 int  IBias7_OFFSET;        //Address = 0x95 - 0x96

	unsigned char TXP4_SCALEM;          //Address = 0x97
	unsigned char TXP4_SCALEL;          //Address = 0x98
	         int  TXP4_OFFSET;          //Address = 0x99 - 0x9A

	unsigned char TXP5_SCALEM;          //Address = 0x9B
	unsigned char TXP5_SCALEL;          //Address = 0x9C
			 int  TXP5_OFFSET;          //Address = 0x9D - 0x9E

	unsigned char TXP6_SCALEM;          //Address = 0x9F
	unsigned char TXP6_SCALEL;          //Address = 0xA0
			 int  TXP6_OFFSET;          //Address = 0xA1 - 0xA2

	unsigned char TXP7_SCALEM;          //Address = 0xA3
	unsigned char TXP7_SCALEL;          //Address = 0xA4
			 int  TXP7_OFFSET;          //Address = 0xA5 - 0xA6

	unsigned char RX4_SCALEM;           //Address = 0xA7
	unsigned char RX4_SCALEL;           //Address = 0xA8
			 int  RX4_OFFSET;           //Address = 0xA9 - 0xAA

	unsigned char RX5_SCALEM;           //Address = 0xAB
	unsigned char RX5_SCALEL;           //Address = 0xAC
			 int  RX5_OFFSET;           //Address = 0xAD - 0xAE

	unsigned char RX6_SCALEM;           //Address = 0xAF
	unsigned char RX6_SCALEL;           //Address = 0xB0
			 int  RX6_OFFSET;           //Address = 0xB1 - 0xB2

	unsigned char RX7_SCALEM;           //Address = 0xB3
	unsigned char RX7_SCALEL;           //Address = 0xB4
			 int  RX7_OFFSET;           //Address = 0xB5 - 0xB6

    unsigned int  Rx4_LOS_Assret;       //Address = 0xB7 - 0xB8
    unsigned int  Rx4_LOS_DeAssret;     //Address = 0xB9 - 0xBA
    unsigned int  Rx5_LOS_Assret;       //Address = 0xBB - 0xBC
    unsigned int  Rx5_LOS_DeAssret;     //Address = 0xBD - 0xBE
    unsigned int  Rx6_LOS_Assret;       //Address = 0xBF - 0xC0
    unsigned int  Rx6_LOS_DeAssret;     //Address = 0xC1 - 0xC2
    unsigned int  Rx7_LOS_Assret;       //Address = 0xC3 - 0xC4
    unsigned int  Rx7_LOS_DeAssret;     //Address = 0xC5 - 0xC6

    unsigned int  TX_P2V5_ADC;          //Address = 0xC7 - 0xC8
    unsigned int  TRX_P4V5_ADC;         //Address = 0xC9 - 0xCA
    unsigned int  TX_P3V3_ADC;          //Address = 0xCB - 0xCC
    unsigned int  RX_P3V3_ADC;          //Address = 0xCD - 0xCE
    unsigned int  RX_CDR_P1V8_ADC;      //Address = 0xCF - 0xD0
    unsigned int  TX_CDR_P1V8_ADC;      //Address = 0xD1 - 0xD2
             int  TEMP_ADC;             //Address = 0xD3 - 0xD4
    unsigned char Power_C_Status;       //Address = 0xD5
    unsigned char Power_SET_Value;	    //Address = 0xD6
    unsigned char Power_SET_Start;      //Address = 0xD7

    unsigned int  Pre1_Get_Data;        //Address = 0xD8 - 0xD9
    unsigned char CAL_Buffer[38];       //Address = 0xDA - 0xFF
};

extern xdata struct CALIB_MEMORY_1 CALIB_MEMORY_1_MAP;


#endif /* INC_CALIBRATION_STRUCT_2_H_ */
