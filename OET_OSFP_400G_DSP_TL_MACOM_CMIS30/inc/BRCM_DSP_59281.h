/*
 * BRCM_DSP_59281.h
 *
 *  Created on: 2019�~2��26��
 *      Author: Ivan_Lin
 */

#ifndef INC_BRCM_DSP_59281_H_
#define INC_BRCM_DSP_59281_H_

#define QDD400G_PAM4_MODE                0x10
#define QDD400G_NRZ_MODE                 0x20
#define QDD400G_PAM4_8P_PAM4_MODE        0x30

extern void DSP59281_Init(unsigned char CHIP_MODE);
extern void Trigger_CMD_Update_DSP_REG();
extern void SET_CHIP_MODE( unsigned char DSP_CHIP_MODE );
extern void DSP59281_LowPower_Control(unsigned char LowPower_EN);
extern void System_side_Squelch_Rx_Control(unsigned int Enable);
extern void DSP59281_SET_ALL_CH_FIR();
extern void System_Side_Squelch_Control(unsigned char Enable);
extern void System_side_MSA_Squelch_Control(unsigned int Enable);
extern unsigned int DSP59281_TX_SystemSide_LOL_LOS_Status();
extern unsigned int DSP59281_RX_LineSide_LOL_LOS_Status();
extern void BRCM_Control_WRITE_Data( long BRCM_ADDR , unsigned int Data_Value , unsigned int Data_Length , unsigned char PHY_ID );
extern void System_Side_PRE2_Tap_Load(unsigned char Channel_SETING);
extern void BRCM_SUGGEST_LOSS_RX_OUTPUT_SETTING( unsigned char LOSS_LEVEL ,unsigned char Channel );

extern void BRCM_Control_WRITE_Data( long BRCM_ADDR , unsigned int Data_Value , unsigned int Data_Length , unsigned char PHY_ID );
extern unsigned int BRCM_Control_READ_Data( long BRCM_ADDR , unsigned char PHY_ID );
extern void DSP_59281_Line_Side_SET_FW_COMMAND(unsigned char Channel,unsigned int Write_command_0,unsigned int Write_command_1);
extern void DSP_59281_System_Side_SET_FW_COMMAND(unsigned char Channel,unsigned int Write_command_0,unsigned int Write_command_1);
extern void DSP59281_SPI_EEPROM_DOWNLOAD_CHECK_Status();


xdata struct BRCM_59281_DSP_LS_PHY0_MEMORY
{
	unsigned int DSP_CHIP_MODE ;              // RW DSP 5200C8C7  SRAM Address 80 - 81
	unsigned int Trigger_MODE ;               // RW DSP 5200C884  SRAM Address 82 - 83

	unsigned int Line_Side_PRE1_CH0 ;         // RW DSP 580034D0  SRAM Address 84 - 85
	unsigned int Line_Side_PRE1_CH1 ;         // RW DSP 580037D0  SRAM Address 86 - 87
	unsigned int Line_Side_PRE1_CH2 ;         // RW DSP 58003BD0  SRAM Address 88 - 89
	unsigned int Line_Side_PRE1_CH3 ;         // RW DSP 58003FD0  SRAM Address 8A - 8B

	unsigned int Line_Side_Main_CH0 ;         // RW DSP 580034D4  SRAM Address 8C - 8D
	unsigned int Line_Side_Main_CH1 ;         // RW DSP 580037D4  SRAM Address 8E - 8F
	unsigned int Line_Side_Main_CH2 ;         // RW DSP 58003BD4  SRAM Address 90 - 91
	unsigned int Line_Side_Main_CH3 ;         // RW DSP 58003FD4  SRAM Address 92 - 93

	unsigned int Line_Side_POST1_CH0 ;        // RW DSP 580034D8  SRAM Address 94 - 95
	unsigned int Line_Side_POST1_CH1 ;        // RW DSP 580037D8  SRAM Address 96 - 97
	unsigned int Line_Side_POST1_CH2 ;        // RW DSP 58003BD8  SRAM Address 98 - 99
	unsigned int Line_Side_POST1_CH3 ;        // RW DSP 58003FD8  SRAM Address 9A - 9B

	unsigned int Line_Side_POST2_CH0 ;        // RW DSP 580034DC  SRAM Address 9C - 9D
	unsigned int Line_Side_POST2_CH1 ;        // RW DSP 580037DC  SRAM Address 9E - 9F
	unsigned int Line_Side_POST2_CH2 ;        // RW DSP 58003BDC  SRAM Address A0 - A1
	unsigned int Line_Side_POST2_CH3 ;        // RW DSP 58003FDC  SRAM Address A2 - A3

	unsigned int Line_Side_POST3_CH0 ;        // RW DSP 580034E0  SRAM Address A4 - A5
	unsigned int Line_Side_POST3_CH1 ;        // RW DSP 580037E0  SRAM Address A6 - A7
	unsigned int Line_Side_POST3_CH2 ;        // RW DSP 58003BE0  SRAM Address A8 - A9
	unsigned int Line_Side_POST3_CH3 ;        // RW DSP 58003FE0  SRAM Address AA - AB

	unsigned int Line_Side_PRE2_CH0 ;         // RW DSP 580034CC  SRAM Address AC - AD
	unsigned int Line_Side_PRE2_CH1 ;         // RW DSP 580037CC  SRAM Address AE - AF
	unsigned int Line_Side_PRE2_CH2 ;         // RW DSP 58003BCC  SRAM Address B0 - B1
	unsigned int Line_Side_PRE2_CH3 ;         // RW DSP 58003FCC  SRAM Address B2 - B3

	unsigned int Line_Side_TX_Polarity_CH0 ;  // RW DSP 580035CC  SRAM Address B4 - B5
	unsigned int Line_Side_TX_Polarity_CH1 ;  // RW DSP 580075CC  SRAM Address B6 - B7
	unsigned int Line_Side_TX_Polarity_CH2 ;  // RW DSP 5800B5CC  SRAM Address B8 - B9
	unsigned int Line_Side_TX_Polarity_CH3 ;  // RW DSP 5800F5CC  SRAM Address BA - BB

	unsigned int Line_Side_RX_Polarity_CH0 ;  // RW DSP 5800110C  SRAM Address BC - BD
	unsigned int Line_Side_RX_Polarity_CH1 ;  // RW DSP 5800510C  SRAM Address BE - BF
	unsigned int Line_Side_RX_Polarity_CH2 ;  // RW DSP 5800910C  SRAM Address C0 - C1
	unsigned int Line_Side_RX_Polarity_CH3 ;  // RW DSP 5800D10C  SRAM Address C2 - C3

	unsigned int Line_side_LShift_LSB_CH0 ;   // RW DSP 58001828  SRAM Address C4 - C5
	unsigned int Line_side_LShift_MSB_CH0 ;   // RW DSP 5800182C  SRAM Address C6 - C7
	unsigned int Line_side_LShift_LSB_CH1 ;   // RW DSP 58005828  SRAM Address C8 - C9
	unsigned int Line_side_LShift_MSB_CH1 ;   // RW DSP 5800582C  SRAM Address CA - CB
	unsigned int Line_side_LShift_LSB_CH2 ;   // RW DSP 58009828  SRAM Address CC - CD
	unsigned int Line_side_LShift_MSB_CH2 ;   // RW DSP 5800982C  SRAM Address CE - CF
	unsigned int Line_side_LShift_LSB_CH3 ;   // RW DSP 5800D828  SRAM Address D0 - D1
	unsigned int Line_side_LShift_MSB_CH3 ;   // RW DSP 5800D82C  SRAM Address D2 - D3

	unsigned int Dynamic_Phase_THR_CH0 ;      // RW DSP 5800D82C  SRAM Address D4 - D5
	unsigned int Dynamic_Phase_THR_CH1 ;      // RW DSP 5800D82C  SRAM Address D6 - D7
	unsigned int Dynamic_Phase_THR_CH2 ;      // RW DSP 5800D82C  SRAM Address D8 - D9
	unsigned int Dynamic_Phase_THR_CH3 ;      // RW DSP 5800D82C  SRAM Address DA - DB

	unsigned char Line_Side_Buffer[36] ;      //  SRAM Address DC - FF
};

xdata struct BRCM_59281_DSP_SS_PHY0_MEMORY
{
	unsigned int System_Side_PRE1_CH0 ;        // RW DSP 500344D0  SRAM Address 80 - 81
	unsigned int System_Side_PRE1_CH1 ;        // RW DSP 501344D0  SRAM Address 82 - 83
	unsigned int System_Side_PRE1_CH2 ;        // RW DSP 502344D0  SRAM Address 84 - 85
	unsigned int System_Side_PRE1_CH3 ;        // RW DSP 503344D0  SRAM Address 86 - 87

	unsigned int System_Side_Main_CH0 ;        // RW DSP 500344D4  SRAM Address 88 - 89
	unsigned int System_Side_Main_CH1 ;        // RW DSP 501344D4  SRAM Address 8A - 8B
	unsigned int System_Side_Main_CH2 ;        // RW DSP 502344D4  SRAM Address 8C - 8D
	unsigned int System_Side_Main_CH3 ;        // RW DSP 503344D4  SRAM Address 8E - 8F

	unsigned int System_Side_POST1_CH0 ;       // RW DSP 500344D8  SRAM Address 90 - 91
	unsigned int System_Side_POST1_CH1 ;       // RW DSP 501344D8  SRAM Address 92 - 93
	unsigned int System_Side_POST1_CH2 ;       // RW DSP 502344D8  SRAM Address 94 - 95
	unsigned int System_Side_POST1_CH3 ;       // RW DSP 503344D8  SRAM Address 96 - 97

	unsigned int System_Side_POST2_CH0 ;       // RW DSP 500344DC  SRAM Address 98 - 99
	unsigned int System_Side_POST2_CH1 ;       // RW DSP 501344DC  SRAM Address 9A - 9B
	unsigned int System_Side_POST2_CH2 ;       // RW DSP 502344DC  SRAM Address 9C - 9D
	unsigned int System_Side_POST2_CH3 ;       // RW DSP 503344DC  SRAM Address 9E - 9F

	unsigned int System_Side_POST3_CH0 ;       // RW DSP 500344E0  SRAM Address A0 - A1
	unsigned int System_Side_POST3_CH1 ;       // RW DSP 501344E0  SRAM Address A2 - A3
	unsigned int System_Side_POST3_CH2 ;       // RW DSP 502344E0  SRAM Address A4 - A5
	unsigned int System_Side_POST3_CH3 ;       // RW DSP 503344E0  SRAM Address A6 - A7

	unsigned int System_Side_PRE2_CH0 ;        // RW DSP 500344CC  SRAM Address A8 - A9
	unsigned int System_Side_PRE2_CH1 ;        // RW DSP 501344CC  SRAM Address AA - AB
	unsigned int System_Side_PRE2_CH2 ;        // RW DSP 502344CC  SRAM Address AC - AD
	unsigned int System_Side_PRE2_CH3 ;        // RW DSP 503344CC  SRAM Address AE - AF

	unsigned int System_Side_TX_Polarity_CH0 ; // RW DSP 500345CC  SRAM Address B0 - B1
	unsigned int System_Side_TX_Polarity_CH1 ; // RW DSP 501345CC  SRAM Address B2 - B3
	unsigned int System_Side_TX_Polarity_CH2 ; // RW DSP 502345CC  SRAM Address B4 - B5
	unsigned int System_Side_TX_Polarity_CH3 ; // RW DSP 503345CC  SRAM Address B6 - B7

	unsigned int System_Side_RX_Polarity_CH0 ; // RW DSP 5003458C  SRAM Address B8 - B9
	unsigned int System_Side_RX_Polarity_CH1 ; // RW DSP 5013458C  SRAM Address BA - BB
	unsigned int System_Side_RX_Polarity_CH2 ; // RW DSP 5023458C  SRAM Address BC - BD
	unsigned int System_Side_RX_Polarity_CH3 ; // RW DSP 5033458C  SRAM Address BE - BF

	unsigned char System_Side_Buffer[64];        // SRAM Address C0 - FF
};

xdata struct BRCM_59281_DSP_LS_PHY1_MEMORY
{
	unsigned int DSP_CHIP_MODE ;              // RW DSP 5200C8C7  SRAM Address 80 - 81
	unsigned int Trigger_MODE ;               // RW DSP 5200C884  SRAM Address 82 - 83

	unsigned int Line_Side_PRE1_CH0 ;         // RW DSP 580034D0  SRAM Address 84 - 85
	unsigned int Line_Side_PRE1_CH1 ;         // RW DSP 580037D0  SRAM Address 86 - 87
	unsigned int Line_Side_PRE1_CH2 ;         // RW DSP 58003BD0  SRAM Address 88 - 89
	unsigned int Line_Side_PRE1_CH3 ;         // RW DSP 58003FD0  SRAM Address 8A - 8B

	unsigned int Line_Side_Main_CH0 ;         // RW DSP 580034D4  SRAM Address 8C - 8D
	unsigned int Line_Side_Main_CH1 ;         // RW DSP 580037D4  SRAM Address 8E - 8F
	unsigned int Line_Side_Main_CH2 ;         // RW DSP 58003BD4  SRAM Address 90 - 91
	unsigned int Line_Side_Main_CH3 ;         // RW DSP 58003FD4  SRAM Address 92 - 93

	unsigned int Line_Side_POST1_CH0 ;        // RW DSP 580034D8  SRAM Address 94 - 95
	unsigned int Line_Side_POST1_CH1 ;        // RW DSP 580037D8  SRAM Address 96 - 97
	unsigned int Line_Side_POST1_CH2 ;        // RW DSP 58003BD8  SRAM Address 98 - 99
	unsigned int Line_Side_POST1_CH3 ;        // RW DSP 58003FD8  SRAM Address 9A - 9B

	unsigned int Line_Side_POST2_CH0 ;        // RW DSP 580034DC  SRAM Address 9C - 9D
	unsigned int Line_Side_POST2_CH1 ;        // RW DSP 580037DC  SRAM Address 9E - 9F
	unsigned int Line_Side_POST2_CH2 ;        // RW DSP 58003BDC  SRAM Address A0 - A1
	unsigned int Line_Side_POST2_CH3 ;        // RW DSP 58003FDC  SRAM Address A2 - A3

	unsigned int Line_Side_POST3_CH0 ;        // RW DSP 580034E0  SRAM Address A4 - A5
	unsigned int Line_Side_POST3_CH1 ;        // RW DSP 580037E0  SRAM Address A6 - A7
	unsigned int Line_Side_POST3_CH2 ;        // RW DSP 58003BE0  SRAM Address A8 - A9
	unsigned int Line_Side_POST3_CH3 ;        // RW DSP 58003FE0  SRAM Address AA - AB

	unsigned int Line_Side_PRE2_CH0 ;         // RW DSP 580034CC  SRAM Address AC - AD
	unsigned int Line_Side_PRE2_CH1 ;         // RW DSP 580037CC  SRAM Address AE - AF
	unsigned int Line_Side_PRE2_CH2 ;         // RW DSP 58003BCC  SRAM Address B0 - B1
	unsigned int Line_Side_PRE2_CH3 ;         // RW DSP 58003FCC  SRAM Address B2 - B3

	unsigned int Line_Side_TX_Polarity_CH0 ;  // RW DSP 580035CC  SRAM Address B4 - B5
	unsigned int Line_Side_TX_Polarity_CH1 ;  // RW DSP 580075CC  SRAM Address B6 - B7
	unsigned int Line_Side_TX_Polarity_CH2 ;  // RW DSP 5800B5CC  SRAM Address B8 - B9
	unsigned int Line_Side_TX_Polarity_CH3 ;  // RW DSP 5800F5CC  SRAM Address BA - BB

	unsigned int Line_Side_RX_Polarity_CH0 ;  // RW DSP 5800110C  SRAM Address BC - BD
	unsigned int Line_Side_RX_Polarity_CH1 ;  // RW DSP 5800510C  SRAM Address BE - BF
	unsigned int Line_Side_RX_Polarity_CH2 ;  // RW DSP 5800910C  SRAM Address C0 - C1
	unsigned int Line_Side_RX_Polarity_CH3 ;  // RW DSP 5800D10C  SRAM Address C2 - C3

	unsigned int Line_side_LShift_LSB_CH0 ;   // RW DSP 58001828  SRAM Address C4 - C5
	unsigned int Line_side_LShift_MSB_CH0 ;   // RW DSP 5800182C  SRAM Address C6 - C7
	unsigned int Line_side_LShift_LSB_CH1 ;   // RW DSP 58005828  SRAM Address C8 - C9
	unsigned int Line_side_LShift_MSB_CH1 ;   // RW DSP 5800582C  SRAM Address CA - CB
	unsigned int Line_side_LShift_LSB_CH2 ;   // RW DSP 58009828  SRAM Address CC - CD
	unsigned int Line_side_LShift_MSB_CH2 ;   // RW DSP 5800982C  SRAM Address CE - CF
	unsigned int Line_side_LShift_LSB_CH3 ;   // RW DSP 5800D828  SRAM Address D0 - D1
	unsigned int Line_side_LShift_MSB_CH3 ;   // RW DSP 5800D82C  SRAM Address D2 - D3

	unsigned int Dynamic_Phase_THR_CH4 ;      // RW DSP 5800D82C  SRAM Address D4 - D5
	unsigned int Dynamic_Phase_THR_CH5 ;      // RW DSP 5800D82C  SRAM Address D6 - D7
	unsigned int Dynamic_Phase_THR_CH6 ;      // RW DSP 5800D82C  SRAM Address D8 - D9
	unsigned int Dynamic_Phase_THR_CH7 ;      // RW DSP 5800D82C  SRAM Address DA - DB

	unsigned char Line_Side_Buffer[36] ;      //  SRAM Address DC - FF
};

xdata struct BRCM_59281_DSP_SS_PHY1_MEMORY
{
	unsigned int System_Side_PRE1_CH0 ;        // RW DSP 500344D0  SRAM Address 80 - 81
	unsigned int System_Side_PRE1_CH1 ;        // RW DSP 501344D0  SRAM Address 82 - 83
	unsigned int System_Side_PRE1_CH2 ;        // RW DSP 502344D0  SRAM Address 84 - 85
	unsigned int System_Side_PRE1_CH3 ;        // RW DSP 503344D0  SRAM Address 86 - 87

	unsigned int System_Side_Main_CH0 ;        // RW DSP 500344D4  SRAM Address 88 - 89
	unsigned int System_Side_Main_CH1 ;        // RW DSP 501344D4  SRAM Address 8A - 8B
	unsigned int System_Side_Main_CH2 ;        // RW DSP 502344D4  SRAM Address 8C - 8D
	unsigned int System_Side_Main_CH3 ;        // RW DSP 503344D4  SRAM Address 8E - 8F

	unsigned int System_Side_POST1_CH0 ;       // RW DSP 500344D8  SRAM Address 90 - 91
	unsigned int System_Side_POST1_CH1 ;       // RW DSP 501344D8  SRAM Address 92 - 93
	unsigned int System_Side_POST1_CH2 ;       // RW DSP 502344D8  SRAM Address 94 - 95
	unsigned int System_Side_POST1_CH3 ;       // RW DSP 503344D8  SRAM Address 96 - 97

	unsigned int System_Side_POST2_CH0 ;       // RW DSP 500344DC  SRAM Address 98 - 99
	unsigned int System_Side_POST2_CH1 ;       // RW DSP 501344DC  SRAM Address 9A - 9B
	unsigned int System_Side_POST2_CH2 ;       // RW DSP 502344DC  SRAM Address 9C - 9D
	unsigned int System_Side_POST2_CH3 ;       // RW DSP 503344DC  SRAM Address 9E - 9F

	unsigned int System_Side_POST3_CH0 ;       // RW DSP 500344E0  SRAM Address A0 - A1
	unsigned int System_Side_POST3_CH1 ;       // RW DSP 501344E0  SRAM Address A2 - A3
	unsigned int System_Side_POST3_CH2 ;       // RW DSP 502344E0  SRAM Address A4 - A5
	unsigned int System_Side_POST3_CH3 ;       // RW DSP 503344E0  SRAM Address A6 - A7

	unsigned int System_Side_PRE2_CH0 ;        // RW DSP 500344CC  SRAM Address A8 - A9
	unsigned int System_Side_PRE2_CH1 ;        // RW DSP 501344CC  SRAM Address AA - AB
	unsigned int System_Side_PRE2_CH2 ;        // RW DSP 502344CC  SRAM Address AC - AD
	unsigned int System_Side_PRE2_CH3 ;        // RW DSP 503344CC  SRAM Address AE - AF

	unsigned int System_Side_TX_Polarity_CH0 ; // RW DSP 500345CC  SRAM Address B0 - B1
	unsigned int System_Side_TX_Polarity_CH1 ; // RW DSP 501345CC  SRAM Address B2 - B3
	unsigned int System_Side_TX_Polarity_CH2 ; // RW DSP 502345CC  SRAM Address B4 - B5
	unsigned int System_Side_TX_Polarity_CH3 ; // RW DSP 503345CC  SRAM Address B6 - B7

	unsigned int System_Side_RX_Polarity_CH0 ; // RW DSP 5003458C  SRAM Address B8 - B9
	unsigned int System_Side_RX_Polarity_CH1 ; // RW DSP 5013458C  SRAM Address BA - BB
	unsigned int System_Side_RX_Polarity_CH2 ; // RW DSP 5023458C  SRAM Address BC - BD
	unsigned int System_Side_RX_Polarity_CH3 ; // RW DSP 5033458C  SRAM Address BE - BF

	unsigned char System_Side_Buffer[64];        // SRAM Address C0 - FF
};

extern xdata struct BRCM_59281_DSP_LS_PHY0_MEMORY BRCM_59281_DSP_LS_PHY0_MEMORY_MAP;
extern xdata struct BRCM_59281_DSP_SS_PHY0_MEMORY BRCM_59281_DSP_SS_PHY0_MEMORY_MAP;
extern xdata struct BRCM_59281_DSP_LS_PHY1_MEMORY BRCM_59281_DSP_LS_PHY1_MEMORY_MAP;
extern xdata struct BRCM_59281_DSP_SS_PHY1_MEMORY BRCM_59281_DSP_SS_PHY1_MEMORY_MAP;


#endif /* INC_BRCM_DSP_59281_H_ */
