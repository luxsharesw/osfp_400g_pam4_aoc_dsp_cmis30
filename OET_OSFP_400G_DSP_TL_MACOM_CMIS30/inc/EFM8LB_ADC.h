/*
 * EFM8LB_ADC.h
 *
 *  Created on: 2018�~6��1��
 *      Author: Ivan_Lin
 */

#ifndef INC_EFM8LB_ADC_H_
#define INC_EFM8LB_ADC_H_

// P0.1 ADC0P0    P0.2 ADC0P1	 P0.4 ADC0P2	 P0.5 ADC0P3
// P0.6 ADC0P4    P0.7 ADC0P5    P1.0 ADC0P6     P1.1 ADC0P7
// P1.2 ADC0P8    P1.3 ADC0P9    P1.4 ADC0P10    P1.5 ADC0P11
// P1.6 ADC0P12   P1.7 ADC0P13   P2.1 ADC0P14    P2.2 ADC0P15
// P2.3 ADC0P16   P2.4 ADC0P17   P2.5 ADC0P18    P2.6 ADC0P19
//
// P0.3 & P3.0 - P3.4 no ADC Function
#define LPMODE_ADC     ADC0MX_ADC0MX__ADC0P2
#define RSSI_RX14      ADC0MX_ADC0MX__ADC0P9
#define P3V3_TRX_VCC   ADC0MX_ADC0MX__ADC0P13
//#define P4V5_TRX_VCC   ADC0MX_ADC0MX__ADC0P16
#define P1V8_TRX_VCC   ADC0MX_ADC0MX__ADC0P15
#define CDR_0V8_TX     ADC0MX_ADC0MX__ADC0P17
#define PDCDD          ADC0MX_ADC0MX__ADC0P18
#define RSSI_RX58      ADC0MX_ADC0MX__ADC0P19



extern unsigned int Get_ADC_Value(unsigned char CH , unsigned char Sampling);
extern unsigned int ADCV_Conversion_Voltage(unsigned char ADC_CH);
extern int Get_Tempsensor();
extern void Tempsonr_0C_internal_Cal();
extern void Get_module_Power_Monitor();

#endif /* INC_EFM8LB_ADC_H_ */
