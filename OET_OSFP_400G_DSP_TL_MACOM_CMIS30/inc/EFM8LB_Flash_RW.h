/*
 * EFM8LB_Flash_RW.h
 *
 *  Created on: 2016/1/13
 *      Author: Ivan_Lin
 */

#ifndef EFM8LB_FLASH_RW_H_
#define EFM8LB_FLASH_RW_H_

#define M37645   1
#define M37644   2
extern void Device_Table_Get_Data();


extern void FLASH_PageErase(unsigned int addr,char Check_byte);
extern void FLASH_ByteWrite(unsigned int addr, char byte,char Check_byte);
extern unsigned char FLASH_ByteRead(unsigned int addr);
extern void Flash_ReadFunction(unsigned int FlashAddress,unsigned char *Data_Buffer, unsigned int DataL);
extern void Flash_EWFunction(unsigned int FlashAddress,unsigned char *Data_Buffer, unsigned int DataL);
extern void Flash_EW_NoErase(unsigned int FlashAddress,unsigned char *Data_Buffer, unsigned int DataL);

extern void Flash_ReadBlock(unsigned char code *FlashAddress,const unsigned char *Data_Buffer, unsigned int DataL);


#endif /* EFM8LB_FLASH_RW_H_ */
