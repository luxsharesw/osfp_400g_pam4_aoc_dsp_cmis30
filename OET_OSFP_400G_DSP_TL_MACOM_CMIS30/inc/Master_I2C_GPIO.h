/*
 * Master_I2C_GPIO.h
 *
 *  Created on: 2019�~6��19��
 *      Author: Ivan_Lin
 */

#ifndef INC_MASTER_I2C_GPIO_H_
#define INC_MASTER_I2C_GPIO_H_

#define Low  0
#define High 1

#define Slave_ACK  0
#define Slave_nACK 1

#define Master_I2C1  4
#define Master_I2C2  5

extern unsigned char Master_I2C_ReadByte(unsigned char Slave_address,unsigned char Regcommand_Address,unsigned char I2C_CH);
extern void Master_I2C_WriteByte(unsigned char Slave_address,unsigned char Regcommand_Address,unsigned char SendData,unsigned char I2C_CH);
extern unsigned int  Master_I2C_Read_Two_Byte(unsigned char Slave_address,unsigned char Regcommand_Address,unsigned char I2C_CH);

extern void Delay_ms(int time_ms);


#endif /* INC_MASTER_I2C_GPIO_H_ */
