/*
 * BRCM_DSP_1181.h
 *
 *  Created on: 2018�~11��13��
 *      Author: Ivan_Lin
 */

#ifndef INC_BRCM_DSP_1181_H_
#define INC_BRCM_DSP_1181_H_

//----------------------------------------------------------------------------------------//
// DSP 1181 I2C Command
//----------------------------------------------------------------------------------------//
//#define BRCM_11181_I2C_SAD    0xA0    // BSC Slave Address
#define BRCM_PAGE_SELECT      0x7F    // QSFP Page Select Register
#define BRCM_IND_ADDR0        0x80    // Indirect Address register 0
#define BRCM_IND_ADDR1        0x81    // Indirect Address register 1
#define BRCM_IND_ADDR2        0x82    // Indirect Address register 2
#define BRCM_IND_ADDR3        0x83    // Indirect Address register 3
// BRCM Read/Write Data length
#define BRCM_IND_LEN0         0x84    // Indirect Length register 0
#define BRCM_IND_LEN1         0x85    // Indirect Length register 1
// R/W Command Address
// 0x03 Write Command
// 0x01 Read  Command
#define BRCM_IND_CTRL         0x86    // Indirect Read/Write Control register
#define BRCM_WFIFO            0x87    // Write FIFO register
#define BRCM_RFIFO            0x90    // Read  FIFO register
#define BRCM_PAGE_ADDR        0xFF    // QSFP Page ADDR

extern void BRCM_READ_Data( unsigned char *BRCM_ADDR , unsigned char *Data , unsigned int Data_Length );

xdata struct BRCM_11181_DSP_MEMORY
{
	unsigned char BRCM_REG_ADDR[4] ;        // RW  SRAM Address 80 - 83
	unsigned char WRITE_BUFFER[4] ;         // RW  SRAM Address 84 - 87
	unsigned char READ_BUFFER[4] ;          // RW  SRAM Address 88 - 8B
	unsigned int  LENGITH ;                 // RW  SRAM Address 8C - 8D
	unsigned char Direct_Control ;          // RW  SRAM Address 8E
	unsigned char RW_EN ;                   // RW  SRAM Address 8F

	unsigned char CHIP_INFO_LSB[4];         // RW  SRAM Address 90 - 93
	unsigned char CHIP_INFO_MSB[4];         // RW  SRAM Address 94 - 97

	unsigned char Buffer[8];                // RW  SRAM Address 98 - 9F
	unsigned char Phy_ID;                   // RW  SRAM Address A0
	unsigned char BRCM_ADDR_DATA;           // RW  SRAM Address A1
	unsigned char Debug_flag;				// RW  SRAM Address A2
	unsigned int  Ready_Byte;               // RW  SRAM Address A3 - A4
	unsigned int  BRCM_CHID;                // RW  SRAM Address A5 - A6
	unsigned char CMD_CON;                  // RW  SRAM Address A7
	unsigned char Trigger_CMD;              // RW  SRAM Address A8
	unsigned char CHIP_MODE_VALUE;          // RW  SRAM Address A9 - AA
	unsigned int  DSP_Delay_Count;          // RW  SRAM Address AB - AC
	unsigned char Temp_Buffer[83];          // RW  SRAM Address AD - FF
};

/*
xdata struct BRCM_59281_DSP_CONTROL_MEMORY
{
	unsigned int FIR_TX_CONTROL_1;          //
};
*/
extern xdata struct BRCM_11181_DSP_MEMORY BRCM_11181_DSP_MEMORY_MAP;
//extern xdata struct BRCM_59281_DSP_CONTROL_MEMORY BRCM_59281_DSP_CONTROL_MEMORY_MAP;


#endif /* INC_BRCM_DSP_1181_H_ */
