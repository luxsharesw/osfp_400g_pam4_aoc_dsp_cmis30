/*
 * MA38435_TX1_TX4.c
 *
 *  Created on: 2019�~6��20��
 *      Author: Ivan_Lin
 */

#include <SI_EFM8LB1_Register_Enums.h>
#include "Master_I2C_GPIO.h"
#include "MA38435_TX1_TX4.h"
#include "EFM8LB_ADC.h"

xdata struct MATA38435_TX1_TX4_MEMORY   MATA38435_TX1_TX4_MEMORY_MAP;

//-----------------------------------------------------------------------------------------------------------------//
//------------------------------------ MATA38435_TX1_TX4 I2C Write    ---------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------//
void MALD38435_TX1_TX4_Control_Write_ALL()
{
	Master_I2C_WriteByte( MALD38435_SADR , 0x03 , MATA38435_TX1_TX4_MEMORY_MAP.IO_Control   , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x05 , MATA38435_TX1_TX4_MEMORY_MAP.I2C_ADR_MODE , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x10 , MATA38435_TX1_TX4_MEMORY_MAP.CH_MODE      , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x16 , MATA38435_TX1_TX4_MEMORY_MAP.LOS_MASK     , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x19 , MATA38435_TX1_TX4_MEMORY_MAP.LOS_ALARM    , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x1B , MATA38435_TX1_TX4_MEMORY_MAP.TX_F_A_MASK  , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x1C , MATA38435_TX1_TX4_MEMORY_MAP.TX_F_Alarm   , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x1D , MATA38435_TX1_TX4_MEMORY_MAP.Ignore_TXF   , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x21 , MATA38435_TX1_TX4_MEMORY_MAP.LOS_THRS     , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x22 , MATA38435_TX1_TX4_MEMORY_MAP.LOS_HYST     , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x25 , MATA38435_TX1_TX4_MEMORY_MAP.Rvcsel_CH0   , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x26 , MATA38435_TX1_TX4_MEMORY_MAP.Rvcsel_CH1   , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x27 , MATA38435_TX1_TX4_MEMORY_MAP.Rvcsel_CH2   , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x28 , MATA38435_TX1_TX4_MEMORY_MAP.Rvcsel_CH3   , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x40 , MATA38435_TX1_TX4_MEMORY_MAP.OUTPUT_MUTE  , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x41 , MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE   , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x4A , MATA38435_TX1_TX4_MEMORY_MAP.AC_Gain_CH0   , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x4B , MATA38435_TX1_TX4_MEMORY_MAP.AC_Gain_CH1   , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x4C , MATA38435_TX1_TX4_MEMORY_MAP.AC_Gain_CH2   , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x4D , MATA38435_TX1_TX4_MEMORY_MAP.AC_Gain_CH3   , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x4F , MATA38435_TX1_TX4_MEMORY_MAP.DC_Gain_CH0   , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x50 , MATA38435_TX1_TX4_MEMORY_MAP.DC_Gain_CH1   , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x51 , MATA38435_TX1_TX4_MEMORY_MAP.DC_Gain_CH2   , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x52 , MATA38435_TX1_TX4_MEMORY_MAP.DC_Gain_CH3   , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x5E , MATA38435_TX1_TX4_MEMORY_MAP.IBurnIn       , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x5F , MATA38435_TX1_TX4_MEMORY_MAP.BurnIn_EN     , Master_I2C1 );

	Master_I2C_WriteByte( MALD38435_SADR , 0x42 , MATA38435_TX1_TX4_MEMORY_MAP.IBias_CH0     , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x43 , MATA38435_TX1_TX4_MEMORY_MAP.IBias_CH1     , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x44 , MATA38435_TX1_TX4_MEMORY_MAP.IBias_CH2     , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x45 , MATA38435_TX1_TX4_MEMORY_MAP.IBias_CH3     , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x46 , MATA38435_TX1_TX4_MEMORY_MAP.Imod_CH0      , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x47 , MATA38435_TX1_TX4_MEMORY_MAP.Imod_CH1      , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x48 , MATA38435_TX1_TX4_MEMORY_MAP.Imod_CH2      , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x49 , MATA38435_TX1_TX4_MEMORY_MAP.Imod_CH3      , Master_I2C1 );
}

//-----------------------------------------------------------------------------------------------------------------//
//------------------------------------ MATA38435_TX1_TX4 I2C READ     ---------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------//
void MALD38435_TX1_TX4_Control_READ_ALL()
{
	// Read only Byte
	MATA38435_TX1_TX4_MEMORY_MAP.CHIP_ID        =  Master_I2C_ReadByte( MALD38435_SADR , 0x00 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.REVID          =  Master_I2C_ReadByte( MALD38435_SADR , 0x01 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.LOS_Status     =  Master_I2C_ReadByte( MALD38435_SADR , 0x17 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.TX_Status      =  Master_I2C_ReadByte( MALD38435_SADR , 0x18 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.Tx_Fault_State =  Master_I2C_ReadByte( MALD38435_SADR , 0x1A , Master_I2C1 );
	// Read/Write Byte
	MATA38435_TX1_TX4_MEMORY_MAP.IO_Control     =  Master_I2C_ReadByte( MALD38435_SADR , 0x03 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.I2C_ADR_MODE   =  Master_I2C_ReadByte( MALD38435_SADR , 0x05 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.CH_MODE        =  Master_I2C_ReadByte( MALD38435_SADR , 0x10 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.LOS_MASK       =  Master_I2C_ReadByte( MALD38435_SADR , 0x16 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.LOS_ALARM      =  Master_I2C_ReadByte( MALD38435_SADR , 0x19 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.TX_F_A_MASK    =  Master_I2C_ReadByte( MALD38435_SADR , 0x1B , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.TX_F_Alarm     =  Master_I2C_ReadByte( MALD38435_SADR , 0x1C , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.Ignore_TXF     =  Master_I2C_ReadByte( MALD38435_SADR , 0x1D , Master_I2C1 );

	MATA38435_TX1_TX4_MEMORY_MAP.LOS_THRS       =  Master_I2C_ReadByte( MALD38435_SADR , 0x21 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.LOS_HYST       =  Master_I2C_ReadByte( MALD38435_SADR , 0x22 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.Rvcsel_CH0     =  Master_I2C_ReadByte( MALD38435_SADR , 0x25 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.Rvcsel_CH1     =  Master_I2C_ReadByte( MALD38435_SADR , 0x26 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.Rvcsel_CH2     =  Master_I2C_ReadByte( MALD38435_SADR , 0x27 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.Rvcsel_CH3     =  Master_I2C_ReadByte( MALD38435_SADR , 0x28 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.OUTPUT_MUTE    =  Master_I2C_ReadByte( MALD38435_SADR , 0x40 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE     =  Master_I2C_ReadByte( MALD38435_SADR , 0x41 , Master_I2C1 );

	MATA38435_TX1_TX4_MEMORY_MAP.IBias_CH0      =  Master_I2C_ReadByte( MALD38435_SADR , 0x42 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.IBias_CH1      =  Master_I2C_ReadByte( MALD38435_SADR , 0x43 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.IBias_CH2      =  Master_I2C_ReadByte( MALD38435_SADR , 0x44 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.IBias_CH3      =  Master_I2C_ReadByte( MALD38435_SADR , 0x45 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.Imod_CH0       =  Master_I2C_ReadByte( MALD38435_SADR , 0x46 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.Imod_CH1       =  Master_I2C_ReadByte( MALD38435_SADR , 0x47 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.Imod_CH2       =  Master_I2C_ReadByte( MALD38435_SADR , 0x48 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.Imod_CH3       =  Master_I2C_ReadByte( MALD38435_SADR , 0x49 , Master_I2C1 );

	MATA38435_TX1_TX4_MEMORY_MAP.AC_Gain_CH0    =  Master_I2C_ReadByte( MALD38435_SADR , 0x4A , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.AC_Gain_CH1    =  Master_I2C_ReadByte( MALD38435_SADR , 0x4B , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.AC_Gain_CH2    =  Master_I2C_ReadByte( MALD38435_SADR , 0x4C , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.AC_Gain_CH3    =  Master_I2C_ReadByte( MALD38435_SADR , 0x4D , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.DC_Gain_CH0    =  Master_I2C_ReadByte( MALD38435_SADR , 0x4F , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.DC_Gain_CH1    =  Master_I2C_ReadByte( MALD38435_SADR , 0x50 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.DC_Gain_CH2    =  Master_I2C_ReadByte( MALD38435_SADR , 0x51 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.DC_Gain_CH3    =  Master_I2C_ReadByte( MALD38435_SADR , 0x52 , Master_I2C1 );

	MATA38435_TX1_TX4_MEMORY_MAP.IBurnIn        =  Master_I2C_ReadByte( MALD38435_SADR , 0x5E , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.BurnIn_EN      =  Master_I2C_ReadByte( MALD38435_SADR , 0x5F , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.ADC_Config0    =  Master_I2C_ReadByte( MALD38435_SADR , 0x60 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.ADC_Config2    =  Master_I2C_ReadByte( MALD38435_SADR , 0x62 , Master_I2C1 );

	MATA38435_TX1_TX4_MEMORY_MAP.Driver_ADC_CHSEL    =  Master_I2C_ReadByte( MALD38435_SADR , 0x91 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.Driver_ADC_Measmt   =  Master_I2C_ReadByte( MALD38435_SADR , 0x92 , Master_I2C1 );
}

unsigned int TXBIAS_TX1_TX4(unsigned char BIAS_CH)
{
	unsigned int TX_BIAS_CURRENT = 0 ;
	unsigned int VAR2,VAR3 ;
	// Set Reg0x60h = 00h, Reg0x91h = 00/20/40/60h (select channel), Reg0x92h = 04h
	// Read Reg0x68h[7:0] and Reg0x69h[3:0],
	// Store VAR2 = The decimal value of Reg0x68h[7:0] and Reg0x69h[3:0] together
	// Set Reg0x60h = 00h, Reg0x91h = 00/20/40/60h (select channel), Reg0x92h=08h
	// Read Reg0x68h[7:0] and Reg0x69h[3:0]
	// Store VAR3 = The decimal value of Reg0x68h[7:0] and Reg0x69h[3:0] together
	// Calculate IBIAS = |VAR2 - VAR3| x VAR1 x 40.1uA

	Master_I2C_WriteByte( MALD38435_SADR , 0x60 , 0x10    , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x91 , BIAS_CH , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x92 , 0x08 , Master_I2C1 );
	Delay_ms(1);
	VAR3 = Master_I2C_Read_Two_Byte( MALD38435_SADR , 0x68 , Master_I2C1 );
	Master_I2C_WriteByte( MALD38435_SADR , 0x92 , 0x04 , Master_I2C1 );
	Delay_ms(1);
	VAR2 = Master_I2C_Read_Two_Byte( MALD38435_SADR , 0x68 , Master_I2C1 );

	TX_BIAS_CURRENT = ( VAR2 - VAR3 )*20;

	return TX_BIAS_CURRENT;
}

void MALD38435_TX1_TX4_RESET()
{
	Master_I2C_WriteByte( MALD38435_SADR , 0x02 , 0xAA , Master_I2C1 );
	Delay_ms(10);
	Master_I2C_WriteByte( MALD38435_SADR , 0x02 , 0x00 , Master_I2C1 );
	Delay_ms(10);
}

void MALD38435_TX1_TX4_WRITE_P82()
{
	MALD38435_TX1_TX4_Control_Write_ALL();
}

void MALD38435_TX1_TX4_READ_P82()
{
	MALD38435_TX1_TX4_Control_READ_ALL();
	MATA38435_TX1_TX4_MEMORY_MAP.IC_RSVD_04    =  Master_I2C_ReadByte( MALD38435_SADR , 0x04 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.IC_RSVD_1E    =  Master_I2C_ReadByte( MALD38435_SADR , 0x1E , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.IC_RSVD_29    =  Master_I2C_ReadByte( MALD38435_SADR , 0x29 , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.IC_RSVD_5A    =  Master_I2C_ReadByte( MALD38435_SADR , 0x5A , Master_I2C1 );
	MATA38435_TX1_TX4_MEMORY_MAP.IC_RSVD_64    =  Master_I2C_ReadByte( MALD38435_SADR , 0x64 , Master_I2C1 );
}




