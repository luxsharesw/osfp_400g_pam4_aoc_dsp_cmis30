/*
 * BRCM_1181.c
 *
 *  Created on: 2018�~11��13��
 *      Author: Ivan_Lin
 */

#include "BRCM_DSP_1181.h"
#include "Master_I2C_GPIO.h"
#include <string.h>

xdata struct BRCM_11181_DSP_MEMORY BRCM_11181_DSP_MEMORY_MAP;

xdata unsigned char BRCM_11181_I2C_SAD = 0xA0 ;   // BSC Slave Address

void BRCM_59241_Default_Table()
{
	memset( &BRCM_11181_DSP_MEMORY_MAP , 0x00 , 128 ) ;
}

void BRCM_WRITE_Data( unsigned char *BRCM_ADDR , unsigned char *Data , unsigned int Data_Length )
{
	unsigned char ADDR_Byte3,ADDR_Byte2,ADDR_Byte1,ADDR_Byte0;
	unsigned char LEN_LSB,LEN_MSB ;
	unsigned char IIcount ;

	ADDR_Byte3 = *BRCM_ADDR++ ;
	ADDR_Byte2 = *BRCM_ADDR++ ;
	ADDR_Byte1 = *BRCM_ADDR++ ;
	ADDR_Byte0 = *BRCM_ADDR++ ;

	LEN_LSB = ( Data_Length & 0xFF ) ;
	LEN_MSB = ( ( Data_Length >> 8 ) & 0xFF ) ;
	// Step1 : Write 0xFF to QSFP PAGE SELECT Control register
	Master_I2C_WriteByte( BRCM_11181_I2C_SAD , BRCM_PAGE_SELECT , 0xFF , Master_I2C2 );
	// Step2 : Write INDADDR0 - INDADDR3
	Master_I2C_WriteByte( BRCM_11181_I2C_SAD , BRCM_IND_ADDR0 , ADDR_Byte0 , Master_I2C2 );
	Master_I2C_WriteByte( BRCM_11181_I2C_SAD , BRCM_IND_ADDR1 , ADDR_Byte1 , Master_I2C2 );
	Master_I2C_WriteByte( BRCM_11181_I2C_SAD , BRCM_IND_ADDR2 , ADDR_Byte2 , Master_I2C2 );
	Master_I2C_WriteByte( BRCM_11181_I2C_SAD , BRCM_IND_ADDR3 , ADDR_Byte3 , Master_I2C2 );
	// Step3 : Write Data Length
	Master_I2C_WriteByte( BRCM_11181_I2C_SAD , BRCM_IND_LEN0 , LEN_LSB , Master_I2C2 );
	Master_I2C_WriteByte( BRCM_11181_I2C_SAD , BRCM_IND_LEN1 , LEN_MSB , Master_I2C2 );
	// Step4 : Write Data to WFIFO 0x03 write command
	Master_I2C_WriteByte( BRCM_11181_I2C_SAD , BRCM_IND_CTRL , 0x03 , Master_I2C2 );
	for(IIcount=0;IIcount<LEN_LSB;IIcount++)
		Master_I2C_WriteByte( BRCM_11181_I2C_SAD , BRCM_WFIFO , *Data++ , Master_I2C2 );
}

void BRCM_READ_Data( unsigned char *BRCM_ADDR , unsigned char *Data , unsigned int Data_Length )
{
	unsigned char ADDR_Byte3,ADDR_Byte2,ADDR_Byte1,ADDR_Byte0;
	unsigned char LEN_LSB,LEN_MSB ;
	unsigned char IIcount = 0 ;

	ADDR_Byte3 = *BRCM_ADDR++ ;
	ADDR_Byte2 = *BRCM_ADDR++ ;
	ADDR_Byte1 = *BRCM_ADDR++ ;
	ADDR_Byte0 = *BRCM_ADDR++ ;

	LEN_LSB = ( Data_Length & 0xFF ) ;
	LEN_MSB = ( ( Data_Length >> 8 ) & 0xFF ) ;
	// Step1 : Write 0xFF to QSFP PAGE SELECT Control register
	Master_I2C_WriteByte( BRCM_11181_I2C_SAD , BRCM_PAGE_SELECT , 0xFF , Master_I2C2 );
	// Step2 : Write INDADDR0 - INDADDR3
	Master_I2C_WriteByte( BRCM_11181_I2C_SAD , BRCM_IND_ADDR0 , ADDR_Byte0 , Master_I2C2 );
	Master_I2C_WriteByte( BRCM_11181_I2C_SAD , BRCM_IND_ADDR1 , ADDR_Byte1 , Master_I2C2 );
	Master_I2C_WriteByte( BRCM_11181_I2C_SAD , BRCM_IND_ADDR2 , ADDR_Byte2 , Master_I2C2 );
	Master_I2C_WriteByte( BRCM_11181_I2C_SAD , BRCM_IND_ADDR3 , ADDR_Byte3 , Master_I2C2 );
	// Step3 : Write Data Length
	Master_I2C_WriteByte( BRCM_11181_I2C_SAD , BRCM_IND_LEN0 , LEN_LSB , Master_I2C2 );
	Master_I2C_WriteByte( BRCM_11181_I2C_SAD , BRCM_IND_LEN1 , LEN_MSB , Master_I2C2 );
	// Step4 : Write Data to WFIFO 0x01 READ command
	Master_I2C_WriteByte( BRCM_11181_I2C_SAD , BRCM_IND_CTRL , 0x01 , Master_I2C2 );
	for(IIcount=0;IIcount<LEN_LSB;IIcount++)
		*Data++ = Master_I2C_ReadByte( BRCM_11181_I2C_SAD , BRCM_RFIFO , Master_I2C2 );
}

void TEST_DSP_Command_Direct_Control()
{
	if( BRCM_11181_DSP_MEMORY_MAP.RW_EN == 0x01 )
	{
		if( BRCM_11181_DSP_MEMORY_MAP.Phy_ID == 0x00 )
		{
			BRCM_11181_I2C_SAD = 0xA0 ;
			BRCM_11181_DSP_MEMORY_MAP.BRCM_ADDR_DATA = BRCM_11181_I2C_SAD ;
		}
		else
		{
			BRCM_11181_I2C_SAD = 0xA2 ;
			BRCM_11181_DSP_MEMORY_MAP.BRCM_ADDR_DATA = BRCM_11181_I2C_SAD ;
		}

		if( BRCM_11181_DSP_MEMORY_MAP.Direct_Control == 0x00 )
			BRCM_WRITE_Data( &BRCM_11181_DSP_MEMORY_MAP.BRCM_REG_ADDR[0] , &BRCM_11181_DSP_MEMORY_MAP.WRITE_BUFFER[0] , BRCM_11181_DSP_MEMORY_MAP.LENGITH );
		else if( BRCM_11181_DSP_MEMORY_MAP.Direct_Control == 0x01 )
			BRCM_READ_Data( &BRCM_11181_DSP_MEMORY_MAP.BRCM_REG_ADDR[0] , &BRCM_11181_DSP_MEMORY_MAP.READ_BUFFER[0] , BRCM_11181_DSP_MEMORY_MAP.LENGITH );

		BRCM_11181_DSP_MEMORY_MAP.RW_EN = 0x00 ;
	}
}




