/*
 * BRCM_DSP59281_Control_CMD.c
 *
 *  Created on: 2019�~2��26��
 *      Author: Ivan_Lin
 */

#include "BRCM_DSP_1181.h"
#include "BRCM_DSP_59281.h"
#include "Master_I2C_GPIO.h"
#include <string.h>
#include "BRCM_DSP58281_Optional.h"
#include "Calibration_Struct.h"

#define BRCM_59281_DATA_LENG     4
// DSP_FW_CRC_MSB ADDR�@5200CDA0
#define DSP_UC_READY             0x001F

xdata struct BRCM_59281_DSP_LS_PHY0_MEMORY BRCM_59281_DSP_LS_PHY0_MEMORY_MAP;
xdata struct BRCM_59281_DSP_SS_PHY0_MEMORY BRCM_59281_DSP_SS_PHY0_MEMORY_MAP;
xdata struct BRCM_59281_DSP_LS_PHY1_MEMORY BRCM_59281_DSP_LS_PHY1_MEMORY_MAP;
xdata struct BRCM_59281_DSP_SS_PHY1_MEMORY BRCM_59281_DSP_SS_PHY1_MEMORY_MAP;

unsigned char Init_flag = 0 ;

void BRCM_Control_WRITE_Data( long BRCM_ADDR , unsigned int Data_Value , unsigned int Data_Length , unsigned char PHY_ID )
{
	unsigned char ADDR_Byte3,ADDR_Byte2,ADDR_Byte1,ADDR_Byte0;
	unsigned char DataLSB,DataMSB;
	unsigned char BRCM_59281_I2C_ADR ;
	unsigned char LEN_LSB,LEN_MSB ;

	ADDR_Byte0 = BRCM_ADDR ;
	ADDR_Byte1 = BRCM_ADDR>>8 ;
	ADDR_Byte2 = BRCM_ADDR>>16 ;
	ADDR_Byte3 = BRCM_ADDR>>24 ;

	LEN_LSB = ( Data_Length & 0xFF ) ;
	LEN_MSB = ( ( Data_Length >> 8 ) & 0xFF ) ;

	DataLSB = Data_Value ;
	DataMSB = Data_Value >> 8 ;

	if(PHY_ID==0)
		BRCM_59281_I2C_ADR = 0xA0;
	else
		BRCM_59281_I2C_ADR = 0xA2;

	// Step1 : Write 0xFF to QSFP PAGE SELECT Control register
	Master_I2C_WriteByte( BRCM_59281_I2C_ADR , BRCM_PAGE_SELECT , 0xFF , Master_I2C2 );
	// Step2 : Write INDADDR0 - INDADDR3
	Master_I2C_WriteByte( BRCM_59281_I2C_ADR , BRCM_IND_ADDR0 , ADDR_Byte0 , Master_I2C2 );
	Master_I2C_WriteByte( BRCM_59281_I2C_ADR , BRCM_IND_ADDR1 , ADDR_Byte1 , Master_I2C2 );
	Master_I2C_WriteByte( BRCM_59281_I2C_ADR , BRCM_IND_ADDR2 , ADDR_Byte2 , Master_I2C2 );
	Master_I2C_WriteByte( BRCM_59281_I2C_ADR , BRCM_IND_ADDR3 , ADDR_Byte3 , Master_I2C2 );
	// Step3 : Write Data Length
	Master_I2C_WriteByte( BRCM_59281_I2C_ADR , BRCM_IND_LEN0 , LEN_LSB , Master_I2C2 );
	Master_I2C_WriteByte( BRCM_59281_I2C_ADR , BRCM_IND_LEN1 , LEN_MSB , Master_I2C2 );
	// Step4 : Write Data to WFIFO 0x03 write command
	Master_I2C_WriteByte( BRCM_59281_I2C_ADR , BRCM_IND_CTRL , 0x03 , Master_I2C2 );
	// Step5 : Write Data
	Master_I2C_WriteByte( BRCM_59281_I2C_ADR , BRCM_WFIFO , DataLSB , Master_I2C2 );
	Master_I2C_WriteByte( BRCM_59281_I2C_ADR , BRCM_WFIFO , DataMSB , Master_I2C2 );
	Master_I2C_WriteByte( BRCM_59281_I2C_ADR , BRCM_WFIFO , 0x00 , Master_I2C2 );
	Master_I2C_WriteByte( BRCM_59281_I2C_ADR , BRCM_WFIFO , 0x00 , Master_I2C2 );
}

unsigned int BRCM_Control_READ_Data( long BRCM_ADDR , unsigned char PHY_ID )
{
	unsigned char ADDR_Byte3,ADDR_Byte2,ADDR_Byte1,ADDR_Byte0;
	unsigned char BRCM_59281_I2C_ADR ;
	unsigned char IIcount = 0;
	unsigned char DataLSB,DataMSB;
	unsigned int  return_DATA ;
	unsigned char TEMP_BUUFFER;

/*	ADDR_Byte3 = *BRCM_ADDR++ ;
	ADDR_Byte2 = *BRCM_ADDR++ ;
	ADDR_Byte1 = *BRCM_ADDR++ ;
	ADDR_Byte0 = *BRCM_ADDR++ ;
*/
	ADDR_Byte0 = BRCM_ADDR ;
	ADDR_Byte1 = BRCM_ADDR>>8 ;
	ADDR_Byte2 = BRCM_ADDR>>16 ;
	ADDR_Byte3 = BRCM_ADDR>>24 ;

	if(PHY_ID==0)
		BRCM_59281_I2C_ADR = 0xA0;
	else
		BRCM_59281_I2C_ADR = 0xA2;

	// Step1 : Write 0xFF to QSFP PAGE SELECT Control register
	Master_I2C_WriteByte( BRCM_59281_I2C_ADR , BRCM_PAGE_SELECT , 0xFF , Master_I2C2 );
	// Step2 : Write INDADDR0 - INDADDR3
	Master_I2C_WriteByte( BRCM_59281_I2C_ADR , BRCM_IND_ADDR0 , ADDR_Byte0 , Master_I2C2 );
	Master_I2C_WriteByte( BRCM_59281_I2C_ADR , BRCM_IND_ADDR1 , ADDR_Byte1 , Master_I2C2 );
	Master_I2C_WriteByte( BRCM_59281_I2C_ADR , BRCM_IND_ADDR2 , ADDR_Byte2 , Master_I2C2 );
	Master_I2C_WriteByte( BRCM_59281_I2C_ADR , BRCM_IND_ADDR3 , ADDR_Byte3 , Master_I2C2 );
	// Step3 : Write Data Length
	Master_I2C_WriteByte( BRCM_59281_I2C_ADR , BRCM_IND_LEN0 , 4 , Master_I2C2 );
	Master_I2C_WriteByte( BRCM_59281_I2C_ADR , BRCM_IND_LEN1 , 0 , Master_I2C2 );
	// Step4 : Write Data to WFIFO 0x03 write command
	Master_I2C_WriteByte( BRCM_59281_I2C_ADR , BRCM_IND_CTRL , 0x01 , Master_I2C2 );
	// Step5 : Write Data
	DataLSB = Master_I2C_ReadByte( BRCM_59281_I2C_ADR , BRCM_RFIFO , Master_I2C2 );
	DataMSB = Master_I2C_ReadByte( BRCM_59281_I2C_ADR , BRCM_RFIFO , Master_I2C2 );
	TEMP_BUUFFER = Master_I2C_ReadByte( BRCM_59281_I2C_ADR , BRCM_RFIFO , Master_I2C2 );
	TEMP_BUUFFER = Master_I2C_ReadByte( BRCM_59281_I2C_ADR , BRCM_RFIFO , Master_I2C2 );

	return_DATA = ( DataMSB << 8 );
	return_DATA = return_DATA + DataLSB ;

	return return_DATA;
}

//-----------------------------------------------------------------------------------------------------//
// DSP 59281 CHIP MODE
//-----------------------------------------------------------------------------------------------------//
void SET_CHIP_MODE( unsigned char DSP_CHIP_MODE )
{
	unsigned int Read_Buffer;
	unsigned int Break_count = 0 ;
	unsigned int ChipMode_CHECK;
	if( DSP_CHIP_MODE == QDD400G_PAM4_MODE )
	{
		BRCM_Control_WRITE_Data( 0x5200C87C , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.DSP_CHIP_MODE , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x5200C884 , 0x0001 , BRCM_59281_DATA_LENG , 0 );
	}
	else if( DSP_CHIP_MODE == QDD400G_NRZ_MODE )
	{
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.DSP_CHIP_MODE = 0x6A00 ;
		BRCM_Control_WRITE_Data( 0x5200C87C , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.DSP_CHIP_MODE , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x5200C884 , 0x0001 , BRCM_59281_DATA_LENG , 0 );
	}
	else if( DSP_CHIP_MODE == QDD400G_PAM4_8P_PAM4_MODE )
	{
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.DSP_CHIP_MODE = 0x4A00 ;
		BRCM_Control_WRITE_Data( 0x5200C87C , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.DSP_CHIP_MODE , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x5200C884 , 0x0001 , BRCM_59281_DATA_LENG , 0 );
	}

	Delay_ms(100);

	while( 1 )
	{

		Read_Buffer = BRCM_Control_READ_Data( 0x5200C884 , 0 );

		if( Read_Buffer == 0 )
		{
			ChipMode_CHECK = BRCM_Control_READ_Data( 0x5200C880 , 0 );
			if( ChipMode_CHECK == BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.DSP_CHIP_MODE )
			{
				Init_flag = 2;
				break;
			}
		}

		Break_count ++ ;

		if(Break_count>200)
			break;
	}
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 Line_Side_PRE1
//-----------------------------------------------------------------------------------------------------//
void Line_Side_PRE1(unsigned char Channel_SETING)
{
	unsigned int Read_Buffer;
	if(Channel_SETING==1)
	{
		//----------------------------------------------------------------//
		//Read for Replace 0:8
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  1  1  1  1  1  1 1 0 0 0 0 0 0 0 0 0
		// Value = 0xFE00
		//----------------------------------------------------------------//
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580034D0 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE1_CH0 = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE1_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580034D0 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE1_CH0 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==2)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580074D0 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE1_CH1 = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE1_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580074D0 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE1_CH1 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==3)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800B4D0 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE1_CH2 = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE1_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800B4D0 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE1_CH2 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==4)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800F4D0 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE1_CH3 = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE1_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800F4D0 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE1_CH3 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==5)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580034D0 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE1_CH0 = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE1_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580034D0 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE1_CH0 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==6)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580074D0 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE1_CH1 = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE1_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580074D0 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE1_CH1 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==7)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800B4D0 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE1_CH2 = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE1_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800B4D0 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE1_CH2 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==8)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800F4D0 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE1_CH3 = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE1_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800F4D0 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE1_CH3 , BRCM_59281_DATA_LENG , 1 );
	}
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 Line_Side_Main
//-----------------------------------------------------------------------------------------------------//
void Line_Side_Main(unsigned char Channel_SETING)
{
	unsigned int Read_Buffer;
	if(Channel_SETING==1)
	{
		//----------------------------------------------------------------//
		//Read for Replace 0:8
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  1  1  1  1  1  1 1 0 0 0 0 0 0 0 0 0
		// Value = 0xFE00
		//----------------------------------------------------------------//
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580034D4 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_Main_CH0 = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_Main_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580034D4 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_Main_CH0 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==2)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580074D4 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_Main_CH1 = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_Main_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580074D4 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_Main_CH1 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==3)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800B4D4 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_Main_CH2 = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_Main_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800B4D4 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_Main_CH2 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==4)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800F4D4 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_Main_CH3 = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_Main_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800F4D4 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_Main_CH3 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==5)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580034D4 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_Main_CH0 = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_Main_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580034D4 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_Main_CH0 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==6)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580074D4 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_Main_CH1 = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_Main_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580074D4 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_Main_CH1 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==7)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800B4D4 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_Main_CH2 = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_Main_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800B4D4 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_Main_CH2 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==8)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800F4D4 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_Main_CH3 = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_Main_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800F4D4 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_Main_CH3 , BRCM_59281_DATA_LENG , 1 );
	}
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 Line_Side_POST1
//-----------------------------------------------------------------------------------------------------//
void Line_Side_POST1(unsigned char Channel_SETING)
{
	unsigned int Read_Buffer;
	if(Channel_SETING==1)
	{
		//----------------------------------------------------------------//
		//Read for Replace 0:8
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  1  1  1  1  1  1 1 0 0 0 0 0 0 0 0 0
		// Value = 0xFE00
		//----------------------------------------------------------------//
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580034D8 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST1_CH0 = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST1_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580034D8 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST1_CH0 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==2)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580074D8 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST1_CH1 = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST1_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580074D8 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST1_CH1 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==3)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800B4D8 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST1_CH2 = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST1_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800B4D8 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST1_CH2 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==4)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800F4D8 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST1_CH3 = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST1_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800F4D8 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST1_CH3 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==5)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580034D8 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST1_CH0 = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST1_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580034D8 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST1_CH0 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==6)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580074D8 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST1_CH1 = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST1_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580074D8 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST1_CH1 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==7)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800B4D8 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST1_CH2 = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST1_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800B4D8 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST1_CH2 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==8)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800F4D8 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST1_CH3 = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST1_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800F4D8 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST1_CH3 , BRCM_59281_DATA_LENG , 1 );
	}
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 Line_Side_POST2
//-----------------------------------------------------------------------------------------------------//
void Line_Side_POST2(unsigned char Channel_SETING)
{
	unsigned int Read_Buffer;
	if(Channel_SETING==1)
	{
		//----------------------------------------------------------------//
		//Read for Replace 0:8
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  1  1  1  1  1  1 1 0 0 0 0 0 0 0 0 0
		// Value = 0xFE00
		//----------------------------------------------------------------//
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580034DC , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST2_CH0 = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST2_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580034DC , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST2_CH0 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==2)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580074DC , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST2_CH1 = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST2_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580074DC , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST2_CH1 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==3)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800B4DC , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST2_CH2 = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST2_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800B4DC , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST2_CH2 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==4)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800F4DC , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST2_CH3 = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST2_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800F4DC , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST2_CH3 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==5)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580034DC , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST2_CH0 = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST2_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580034DC , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST2_CH0 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==6)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580074DC , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST2_CH1 = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST2_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580074DC , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST2_CH1 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==7)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800B4DC , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST2_CH2 = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST2_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800B4DC , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST2_CH2 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==8)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800F4DC , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST2_CH3 = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST2_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800F4DC , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST2_CH3 , BRCM_59281_DATA_LENG , 1 );
	}
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 Line_Side_POST3
//-----------------------------------------------------------------------------------------------------//
void Line_Side_POST3(unsigned char Channel_SETING)
{
	unsigned int Read_Buffer;
	if(Channel_SETING==1)
	{
		//----------------------------------------------------------------//
		//Read for Replace 0:8
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  1  1  1  1  1  1 1 0 0 0 0 0 0 0 0 0
		// Value = 0xFE00
		//----------------------------------------------------------------//
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580034E0 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST3_CH0 = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST3_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580034E0 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST3_CH0 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==2)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580074E0 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST3_CH1 = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST3_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580074E0 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST3_CH1 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==3)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800B4E0 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST3_CH2 = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST3_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800B4E0 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST3_CH2 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==4)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800F4E0 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST3_CH3 = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST3_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800F4E0 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_POST3_CH3 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==5)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580034E0 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST3_CH0 = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST3_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580034E0 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST3_CH0 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==6)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580074E0 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST3_CH1 = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST3_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580074E0 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST3_CH1 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==7)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800B4E0 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST3_CH2 = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST3_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800B4E0 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST3_CH2 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==8)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800F4E0 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST3_CH3 = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST3_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800F4E0 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_POST3_CH3 , BRCM_59281_DATA_LENG , 1 );
	}
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 Line_Side_PRE2_Tap_Load
//-----------------------------------------------------------------------------------------------------//
void Line_Side_PRE2_Tap_Load(unsigned char Channel_SETING)
{
	unsigned int Read_Buffer;
	if(Channel_SETING==1)
	{
		//----------------------------------------------------------------//
		// Read for Replace Bit 11
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  0  0  0  0  1  0 0 0 0 0 0 0 0 0 0 0
		// Setting to 1 Value = 0x0800
		// Setting to 0 Value = 0xF7FF
		//----------------------------------------------------------------//
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580034CC , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH0 |= Read_Buffer ;
		BRCM_Control_WRITE_Data( 0x580034CC , ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH0 | 0x0800 ) , BRCM_59281_DATA_LENG , 0 );

		Read_Buffer = ( BRCM_Control_READ_Data( 0x580034CC , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH0 |= Read_Buffer ;
		BRCM_Control_WRITE_Data( 0x580034CC , ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH0 & 0xF7FF ) , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==2)
	{
		//----------------------------------------------------------------//
		// Read for Replace Bit 11
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  0  0  0  0  1  0 0 0 0 0 0 0 0 0 0 0
		// Setting to 1 Value = 0x0800
		// Setting to 0 Value = 0xF7FF
		//----------------------------------------------------------------//
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580074CC , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH1 |= Read_Buffer ;
		BRCM_Control_WRITE_Data( 0x580074CC , ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH1 | 0x0800 ) , BRCM_59281_DATA_LENG , 0 );

		Read_Buffer = ( BRCM_Control_READ_Data( 0x580074CC , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH1 |= Read_Buffer ;
		BRCM_Control_WRITE_Data( 0x580074CC , ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH1 & 0xF7FF ) , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==3)
	{
		//----------------------------------------------------------------//
		// Read for Replace Bit 11
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  0  0  0  0  1  0 0 0 0 0 0 0 0 0 0 0
		// Setting to 1 Value = 0x0800
		// Setting to 0 Value = 0xF7FF
		//----------------------------------------------------------------//
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800B4CC , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH2 |= Read_Buffer ;
		BRCM_Control_WRITE_Data( 0x5800B4CC , ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH2 | 0x0800 ) , BRCM_59281_DATA_LENG , 0 );

		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800B4CC , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH2 |= Read_Buffer ;
		BRCM_Control_WRITE_Data( 0x5800B4CC , ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH2 & 0xF7FF ) , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==4)
	{
		//----------------------------------------------------------------//
		// Read for Replace Bit 11
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  0  0  0  0  1  0 0 0 0 0 0 0 0 0 0 0
		// Setting to 1 Value = 0x0800
		// Setting to 0 Value = 0xF7FF
		//----------------------------------------------------------------//
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800F4CC , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH3 |= Read_Buffer ;
		BRCM_Control_WRITE_Data( 0x5800F4CC , ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH3 | 0x0800 ) , BRCM_59281_DATA_LENG , 0 );

		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800F4CC , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH3 |= Read_Buffer ;
		BRCM_Control_WRITE_Data( 0x5800F4CC , ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_PRE2_CH3 & 0xF7FF ) , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==5)
	{
		//----------------------------------------------------------------//
		// Read for Replace Bit 11
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  0  0  0  0  1  0 0 0 0 0 0 0 0 0 0 0
		// Setting to 1 Value = 0x0800
		// Setting to 0 Value = 0xF7FF
		//----------------------------------------------------------------//
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580034CC , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE2_CH0 |= Read_Buffer ;
		BRCM_Control_WRITE_Data( 0x580034CC , ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE2_CH0 | 0x0800 ) , BRCM_59281_DATA_LENG , 1 );

		Read_Buffer = ( BRCM_Control_READ_Data( 0x580034CC , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE2_CH0 |= Read_Buffer ;
		BRCM_Control_WRITE_Data( 0x580034CC , ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE2_CH0 & 0xF7FF ) , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==6)
	{
		//----------------------------------------------------------------//
		// Read for Replace Bit 11
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  0  0  0  0  1  0 0 0 0 0 0 0 0 0 0 0
		// Setting to 1 Value = 0x0800
		// Setting to 0 Value = 0xF7FF
		//----------------------------------------------------------------//
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580074CC , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE2_CH1 |= Read_Buffer;
		BRCM_Control_WRITE_Data( 0x580074CC , ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE2_CH1 | 0x0800 ) , BRCM_59281_DATA_LENG , 1 );

		Read_Buffer = ( BRCM_Control_READ_Data( 0x580074CC , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE2_CH1 |= Read_Buffer;
		BRCM_Control_WRITE_Data( 0x580074CC , ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE2_CH1 & 0xF7FF ) , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==7)
	{
		//----------------------------------------------------------------//
		// Read for Replace Bit 11
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  0  0  0  0  1  0 0 0 0 0 0 0 0 0 0 0
		// Setting to 1 Value = 0x0800
		// Setting to 0 Value = 0xF7FF
		//----------------------------------------------------------------//
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800B4CC , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE2_CH2 |= Read_Buffer;
		BRCM_Control_WRITE_Data( 0x5800B4CC , ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE2_CH2 | 0x0800 ) , BRCM_59281_DATA_LENG , 1 );

		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800B4CC , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE2_CH2 |= Read_Buffer;
		BRCM_Control_WRITE_Data( 0x5800B4CC , ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE2_CH2 & 0xF7FF ) , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==8)
	{
		//----------------------------------------------------------------//
		// Read for Replace Bit 11
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  0  0  0  0  1  0 0 0 0 0 0 0 0 0 0 0
		// Setting to 1 Value = 0x0800
		// Setting to 0 Value = 0xF7FF
		//----------------------------------------------------------------//
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800F4CC , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE2_CH3 |= Read_Buffer ;
		BRCM_Control_WRITE_Data( 0x5800F4CC , ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE2_CH3 | 0x0800 ) , BRCM_59281_DATA_LENG , 1 );
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800F4CC , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE2_CH3 |= Read_Buffer ;
		BRCM_Control_WRITE_Data( 0x5800F4CC , ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_PRE2_CH3 & 0xF7FF ) , BRCM_59281_DATA_LENG , 1 );
	}
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 Line_Side_Squelch
//-----------------------------------------------------------------------------------------------------//
void Line_Side_Squelch_Control(unsigned char Enable)
{
	if(Enable)
		BRCM_Control_WRITE_Data( 0x5200C774 , 0xFFFF , BRCM_59281_DATA_LENG , 0 );
	else
		BRCM_Control_WRITE_Data( 0x5200C774 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 Line_Side_Channel_Control
//-----------------------------------------------------------------------------------------------------//
void Line_Side_Skip_SET_FW_Command(unsigned char Channel_SETING)
{
	unsigned int Read_Buffer;
	if(Channel_SETING==1)
	{
		//----------------------------------------------------------------//
		// Read for Replace Bit 13
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  0  0  1  0  0  0 0 0 0 0 0 0 0 0 0 0
		// Setting to 1 Value = 0x2000
		// Setting to 0 Value = 0xDFFF
		//----------------------------------------------------------------//
		Read_Buffer = BRCM_Control_READ_Data( 0x580021A4 , 0 ) ;
		BRCM_Control_WRITE_Data( 0x580021A4 , ( Read_Buffer | 0x2000 ) , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==2)
	{
		Read_Buffer = BRCM_Control_READ_Data( 0x580061A4 , 0 ) ;
		BRCM_Control_WRITE_Data( 0x580061A4 , ( Read_Buffer | 0x2000 ) , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==3)
	{
		Read_Buffer = BRCM_Control_READ_Data( 0x5800A1A4 , 0 ) ;
		BRCM_Control_WRITE_Data( 0x5800A1A4 , ( Read_Buffer | 0x2000 ) , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==4)
	{
		Read_Buffer = BRCM_Control_READ_Data( 0x5800E1A4 , 0 ) ;
		BRCM_Control_WRITE_Data( 0x5800E1A4 , ( Read_Buffer | 0x2000 ) , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==5)
	{
		Read_Buffer = BRCM_Control_READ_Data( 0x580021A4 , 1 ) ;
		BRCM_Control_WRITE_Data( 0x580021A4 , ( Read_Buffer | 0x2000 ) , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==6)
	{
		Read_Buffer = BRCM_Control_READ_Data( 0x580061A4 , 1 ) ;
		BRCM_Control_WRITE_Data( 0x580061A4 , ( Read_Buffer | 0x2000 ) , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==7)
	{
		Read_Buffer = BRCM_Control_READ_Data( 0x5800A1A4 , 1 ) ;
		BRCM_Control_WRITE_Data( 0x5800A1A4 , ( Read_Buffer | 0x2000 ) , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==8)
	{
		Read_Buffer = BRCM_Control_READ_Data( 0x5800E1A4 , 1 ) ;
		BRCM_Control_WRITE_Data( 0x5800E1A4 , ( Read_Buffer | 0x2000 ) , BRCM_59281_DATA_LENG , 1 );
	}
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 Line_Side_Channel_Control
//-----------------------------------------------------------------------------------------------------//
void Line_Side_Channel_Control(unsigned char Channel_SETING)
{
	Line_Side_Skip_SET_FW_Command(Channel_SETING);
	DSP_59281_Line_Side_SET_FW_COMMAND( (Channel_SETING-1) , 0x8004 , 0x0004 );
	Line_Side_PRE1(Channel_SETING);
	Line_Side_Main(Channel_SETING);
	Line_Side_POST1(Channel_SETING);
	Line_Side_POST2(Channel_SETING);
	Line_Side_POST3(Channel_SETING);
	Line_Side_PRE2_Tap_Load(Channel_SETING);
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 System_Side_PRE1
//-----------------------------------------------------------------------------------------------------//
void System_Side_PRE1(unsigned char Channel_SETING)
{
	unsigned int Read_Buffer;

	if(Channel_SETING==1)
	{
		//----------------------------------------------------------------//
		//Read for Replace 0:8
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  1  1  1  1  1  1 1 0 0 0 0 0 0 0 0 0
		// Value = 0xFE00
		//----------------------------------------------------------------//
		Read_Buffer = ( BRCM_Control_READ_Data( 0x500344D0 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH0 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x500344D0 , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH0 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==2)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x501344D0 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH1 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x501344D0 , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH1 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==3)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x502344D0 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH2 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x502344D0 , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH2 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==4)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x503344D0 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH3 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x503344D0 , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH3 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==5)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x500344D0 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH0 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x500344D0 , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH0 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==6)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x501344D0 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH1 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x501344D0 , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH1 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==7)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x502344D0 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH2 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x502344D0 , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH2 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==8)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x503344D0 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH3 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x503344D0 , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH3 , BRCM_59281_DATA_LENG , 1 );
	}
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 System_Side_Main
//-----------------------------------------------------------------------------------------------------//
void System_Side_Main(unsigned char Channel_SETING)
{
	unsigned int Read_Buffer;
	if(Channel_SETING==1)
	{
		//----------------------------------------------------------------//
		//Read for Replace 0:8
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  1  1  1  1  1  1 1 0 0 0 0 0 0 0 0 0
		// Value = 0xFE00
		//----------------------------------------------------------------//
		Read_Buffer = ( BRCM_Control_READ_Data( 0x500344D4 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH0 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x500344D4 , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH0 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==2)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x501344D4 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH1 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x501344D4 , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH1 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==3)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x502344D4 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH2 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x502344D4 , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH2 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==4)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x503344D4 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH3 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x503344D4 , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH3 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==5)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x500344D4 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH0 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x500344D4 , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH0 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==6)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x501344D4 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH1 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x501344D4 , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH1 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==7)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x502344D4 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH2 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x502344D4 , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH2 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==8)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x503344D4 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH3 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x503344D4 , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH3 , BRCM_59281_DATA_LENG , 1 );
	}
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 System_Side_POST1
//-----------------------------------------------------------------------------------------------------//
void System_Side_POST1(unsigned char Channel_SETING)
{
	unsigned int Read_Buffer;
	if(Channel_SETING==1)
	{
		//----------------------------------------------------------------//
		//Read for Replace 0:8
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  1  1  1  1  1  1 1 0 0 0 0 0 0 0 0 0
		// Value = 0xFE00
		//----------------------------------------------------------------//
		Read_Buffer = ( BRCM_Control_READ_Data( 0x500344D8 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH0 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x500344D8 , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH0 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==2)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x501344D8 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH1 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x501344D8 , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH1 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==3)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x502344D8 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH2 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x502344D8 , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH2 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==4)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x503344D8 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH3 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x503344D8 , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH3 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==5)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x500344D8 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH0 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x500344D8 , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH0 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==6)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x501344D8 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH1 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x501344D8 , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH1 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==7)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x502344D8 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH2 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x502344D8 , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH2 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==8)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x503344D8 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH3 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x503344D8 , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH3 , BRCM_59281_DATA_LENG , 1 );
	}
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 System_Side_POST2
//-----------------------------------------------------------------------------------------------------//
void System_Side_POST2(unsigned char Channel_SETING)
{
	unsigned int Read_Buffer;
	if(Channel_SETING==1)
	{
		//----------------------------------------------------------------//
		//Read for Replace 0:8
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  1  1  1  1  1  1 1 0 0 0 0 0 0 0 0 0
		// Value = 0xFE00
		//----------------------------------------------------------------//
		Read_Buffer = ( BRCM_Control_READ_Data( 0x500344DC , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST2_CH0 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST2_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x500344DC , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST2_CH0 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==2)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x501344DC , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST2_CH1 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST2_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x501344DC , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST2_CH1 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==3)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x502344DC , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST2_CH2 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST2_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x502344DC , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST2_CH2 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==4)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x503344DC , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST2_CH3 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST2_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x503344DC , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST2_CH3 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==5)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x500344DC , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST2_CH0 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST2_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x500344DC , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST2_CH0 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==6)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x501344DC , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST2_CH1 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST2_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x501344DC , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST2_CH1 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==7)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x502344DC , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST2_CH2 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST2_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x502344DC , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST2_CH2 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==8)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x503344DC , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST2_CH3 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST2_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x503344DC , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST2_CH3 , BRCM_59281_DATA_LENG , 1 );
	}
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 System_Side_POST3
//-----------------------------------------------------------------------------------------------------//
void System_Side_POST3(unsigned char Channel_SETING)
{
	unsigned int Read_Buffer;
	if(Channel_SETING==1)
	{
		//----------------------------------------------------------------//
		//Read for Replace 0:8
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  1  1  1  1  1  1 1 0 0 0 0 0 0 0 0 0
		// Value = 0xFE00
		//----------------------------------------------------------------//
		Read_Buffer = ( BRCM_Control_READ_Data( 0x500344E0 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST3_CH0 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST3_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x500344E0 , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST3_CH0 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==2)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x501344E0 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST3_CH1 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST3_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x501344E0 , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST3_CH1 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==3)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x502344E0 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST3_CH2 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST3_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x502344E0 , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST3_CH2 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==4)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x503344E0 , 0 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST3_CH3 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST3_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x503344E0 , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST3_CH3 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==5)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x500344E0 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST3_CH0 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST3_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x500344E0 , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST3_CH0 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==6)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x501344E0 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST3_CH1 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST3_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x501344E0 , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST3_CH1 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==7)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x502344E0 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST3_CH2 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST3_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x502344E0 , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST3_CH2 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==8)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x503344E0 , 1 ) & 0xFE00 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST3_CH3 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST3_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x503344E0 , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST3_CH3 , BRCM_59281_DATA_LENG , 1 );
	}
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 System_Side_PRE2_Tap_Load
//-----------------------------------------------------------------------------------------------------//
void System_Side_PRE2_Tap_Load(unsigned char Channel_SETING)
{
	unsigned int Read_Buffer;
	if(Channel_SETING==1)
	{
		//----------------------------------------------------------------//
		// Read for replace Bit 0:8
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  1  1  1  1  1  1 1 0 0 0 0 0 0 0 0 0
		// Value = 0xFE00
		// TXFIR_TAP_LOAD Bit 11 : 1
		// Value = 0x0800
		// BH_CUSTOM_TXFIR Read for replace Bit 5
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  0  0  0  0  0  0 0 0 0 0 1 0 0 0 0 0
		// Value = 0x0020
		//----------------------------------------------------------------//
		Read_Buffer = ( ( BRCM_Control_READ_Data( 0x500344CC , 0 ) & 0xFE00 ) | 0x0800 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH0 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x500344CC , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH0 , BRCM_59281_DATA_LENG , 0 );

		Read_Buffer = ( BRCM_Control_READ_Data( 0x500344CC , 0 ) & 0xF600 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH0 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x500344CC , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH0 , BRCM_59281_DATA_LENG , 0 );

		Read_Buffer = BRCM_Control_READ_Data( 0x50034A2C , 0 );
		BRCM_Control_WRITE_Data( 0x50034A2C , Read_Buffer | 0x0020 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==2)
	{
		Read_Buffer = ( ( BRCM_Control_READ_Data( 0x501344CC , 0 ) & 0xFE00 ) | 0x0800 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH1 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x501344CC , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH1 , BRCM_59281_DATA_LENG , 0 );

		Read_Buffer = ( BRCM_Control_READ_Data( 0x501344CC , 0 ) & 0xF600 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH1 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x501344CC , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH1 , BRCM_59281_DATA_LENG , 0 );

		Read_Buffer = BRCM_Control_READ_Data( 0x50134A2C , 0 );
		BRCM_Control_WRITE_Data( 0x50134A2C , Read_Buffer | 0x0020 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==3)
	{
		Read_Buffer = ( ( BRCM_Control_READ_Data( 0x502344CC , 0 ) & 0xFE00 ) | 0x0800 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH2 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x502344CC , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH2 , BRCM_59281_DATA_LENG , 0 );

		Read_Buffer = ( BRCM_Control_READ_Data( 0x502344CC , 0 ) & 0xF600 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH2 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x502344CC , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH2 , BRCM_59281_DATA_LENG , 0 );

		Read_Buffer = BRCM_Control_READ_Data( 0x50234A2C , 0 );
		BRCM_Control_WRITE_Data( 0x50234A2C , Read_Buffer | 0x0020 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==4)
	{
		Read_Buffer = ( ( BRCM_Control_READ_Data( 0x503344CC , 0 ) & 0xFE00 ) | 0x0800 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH3 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x503344CC , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH3 , BRCM_59281_DATA_LENG , 0 );

		Read_Buffer = ( BRCM_Control_READ_Data( 0x503344CC , 0 ) & 0xF600 ) ;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH3 = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x503344CC , BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE2_CH3 , BRCM_59281_DATA_LENG , 0 );

		Read_Buffer = BRCM_Control_READ_Data( 0x50334A2C , 0 );
		BRCM_Control_WRITE_Data( 0x50334A2C , Read_Buffer | 0x0020 , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==5)
	{
		Read_Buffer = ( ( BRCM_Control_READ_Data( 0x500344CC , 1 ) & 0xFE00 ) | 0x0800 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH0 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x500344CC , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH0 , BRCM_59281_DATA_LENG , 1 );

		Read_Buffer = ( BRCM_Control_READ_Data( 0x500344CC , 1 ) & 0xF600 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH0 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x500344CC , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH0 , BRCM_59281_DATA_LENG , 1 );

		Read_Buffer = BRCM_Control_READ_Data( 0x50034A2C , 1 );
		BRCM_Control_WRITE_Data( 0x50034A2C , Read_Buffer | 0x0020 , BRCM_59281_DATA_LENG , 1 );

	}
	else if(Channel_SETING==6)
	{
		Read_Buffer = ( ( BRCM_Control_READ_Data( 0x501344CC , 1 ) & 0xFE00 ) | 0x0800 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH1 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x501344CC , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH1 , BRCM_59281_DATA_LENG , 1 );

		Read_Buffer = ( BRCM_Control_READ_Data( 0x501344CC , 1 ) & 0xF600 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH1 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x501344CC , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH1 , BRCM_59281_DATA_LENG , 1 );

		Read_Buffer = BRCM_Control_READ_Data( 0x50134A2C , 1 );
		BRCM_Control_WRITE_Data( 0x50134A2C , Read_Buffer | 0x0020 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==7)
	{
		Read_Buffer = ( ( BRCM_Control_READ_Data( 0x502344CC , 1 ) & 0xFE00 ) | 0x0800 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH2 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x502344CC , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH2 , BRCM_59281_DATA_LENG , 1 );

		Read_Buffer = ( BRCM_Control_READ_Data( 0x502344CC , 1 ) & 0xF600 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH2 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x502344CC , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH2 , BRCM_59281_DATA_LENG , 1 );

		Read_Buffer = BRCM_Control_READ_Data( 0x50234A2C , 1 );
		BRCM_Control_WRITE_Data( 0x50234A2C , Read_Buffer | 0x0020 , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==8)
	{
		Read_Buffer = ( ( BRCM_Control_READ_Data( 0x503344CC , 1 ) & 0xFE00 ) | 0x0800 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH3 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x503344CC , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH3 , BRCM_59281_DATA_LENG , 1 );

		Read_Buffer = ( BRCM_Control_READ_Data( 0x503344CC , 1 ) & 0xF600 ) ;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH3 = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x503344CC , BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE2_CH3 , BRCM_59281_DATA_LENG , 1 );

		Read_Buffer = BRCM_Control_READ_Data( 0x50334A2C , 1 );
		BRCM_Control_WRITE_Data( 0x50334A2C , Read_Buffer | 0x0020 , BRCM_59281_DATA_LENG , 1 );
	}
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 System_Side_Channel_Control
//-----------------------------------------------------------------------------------------------------//
void System_Side_Channel_Control(unsigned char Channel_SETING)
{
	System_Side_PRE1(Channel_SETING);
	System_Side_Main(Channel_SETING);
	System_Side_POST1(Channel_SETING);
	System_Side_POST2(Channel_SETING);
	System_Side_POST3(Channel_SETING);
	System_Side_PRE2_Tap_Load(Channel_SETING);
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 Line_Side_TX_Polarity
//-----------------------------------------------------------------------------------------------------//
void Line_Side_TX_Polarity(unsigned char Channel_SETING)
{
	unsigned int Read_Buffer;
	unsigned int Write_Buffer;
	if(Channel_SETING==1)
	{
		// Step1: Read for replace Bit 0
		// Normal = 0
		// Invert = 1
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580035CC , 0 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_TX_Polarity_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580035CC , Write_Buffer , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==2)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580075CC , 0 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_TX_Polarity_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580075CC , Write_Buffer , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==3)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800B5CC , 0 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_TX_Polarity_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800B5CC , Write_Buffer , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==4)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800F5CC , 0 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_TX_Polarity_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800F5CC , Write_Buffer , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==5)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580035CC , 1 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_TX_Polarity_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580035CC , Write_Buffer , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==6)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x580075CC , 1 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_TX_Polarity_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x580075CC , Write_Buffer , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==7)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800B5CC , 1 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_TX_Polarity_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800B5CC , Write_Buffer , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==8)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800F5CC , 1 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_TX_Polarity_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800F5CC , Write_Buffer , BRCM_59281_DATA_LENG , 1 );
	}
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 Line_Side_RX_Polarity
//-----------------------------------------------------------------------------------------------------//
void Line_Side_RX_Polarity(unsigned char Channel_SETING)
{
	unsigned int Read_Buffer;
	unsigned int Write_Buffer;
	if(Channel_SETING==1)
	{
		// Step1: Read for replace Bit 4
		// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//  0  0  0  0  0  0 0 0 0 0 0 1 0 0 0 0   = 0x0010
		// Normal = 0
		// Invert = 1
		// Step2: Read for replace Bit 0
		// Normal = 0
		// Invert = 1
		// Step3: Read for replace Bit 15 LW_RX_DISABLE Bit 15 = 1
		// Step4: Read for replace Bit 15 LW_RX_Enable Bit  15 = 0
		// Setp1
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800110C , 0 ) & 0xFFEF );
		Write_Buffer = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_RX_Polarity_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800110C , Write_Buffer , BRCM_59281_DATA_LENG , 0 );
		// Setp2
		Read_Buffer = BRCM_Control_READ_Data( 0x580010DC , 0 ) ;
		if(( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_RX_Polarity_CH0 & 0x0010 ))
			Write_Buffer = Read_Buffer | 0x0001 ;
		else
			Write_Buffer = Read_Buffer & 0xFFFE ;
		BRCM_Control_WRITE_Data( 0x580010DC , Write_Buffer , BRCM_59281_DATA_LENG , 0 );
		// Setp3
		Read_Buffer = BRCM_Control_READ_Data( 0x58002198 , 0 ) ;
		BRCM_Control_WRITE_Data( 0x58002198 , ( Read_Buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 0 );
		DSP_59281_Line_Side_SET_FW_COMMAND( 0 , 0x8004 , 0x0004 );
		Delay_ms(10);
		// Setp4
		Read_Buffer = BRCM_Control_READ_Data( 0x58002198 , 0 ) ;
		BRCM_Control_WRITE_Data( 0x58002198 , ( Read_Buffer & 0x7FFF ) , BRCM_59281_DATA_LENG , 0 );
		DSP_59281_Line_Side_SET_FW_COMMAND( 0 , 0x8004 , 0x0004 );

	}
	else if(Channel_SETING==2)
	{
		// Setp1
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800510C , 0 ) & 0xFFEF );
		Write_Buffer = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_RX_Polarity_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800510C , Write_Buffer , BRCM_59281_DATA_LENG , 0 );
		// Setp2
		Read_Buffer = BRCM_Control_READ_Data( 0x580050DC , 0 ) ;
		if(( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_RX_Polarity_CH1 & 0x0010 ))
			Write_Buffer = Read_Buffer | 0x0001 ;
		else
			Write_Buffer = Read_Buffer & 0xFFFE ;
		BRCM_Control_WRITE_Data( 0x580050DC , Write_Buffer , BRCM_59281_DATA_LENG , 0 );
		// Setp3
		Read_Buffer = BRCM_Control_READ_Data( 0x58006198 , 0 ) ;
		BRCM_Control_WRITE_Data( 0x58006198 , ( Read_Buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 0 );
		DSP_59281_Line_Side_SET_FW_COMMAND( 1 , 0x8004 , 0x0004 );
		Delay_ms(10);
		// Setp4
		Read_Buffer = BRCM_Control_READ_Data( 0x58006198 , 0 ) ;
		BRCM_Control_WRITE_Data( 0x58006198 , ( Read_Buffer & 0x7FFF ) , BRCM_59281_DATA_LENG , 0 );
		DSP_59281_Line_Side_SET_FW_COMMAND( 1 , 0x8004 , 0x0004 );

	}
	else if(Channel_SETING==3)
	{
		// Setp1
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800910C , 0 ) & 0xFFEF );
		Write_Buffer = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_RX_Polarity_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800910C , Write_Buffer , BRCM_59281_DATA_LENG , 0 );
		// Setp2
		Read_Buffer = BRCM_Control_READ_Data( 0x580090DC , 0 ) ;
		if(( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_RX_Polarity_CH2 & 0x0010 ))
			Write_Buffer = Read_Buffer | 0x0001 ;
		else
			Write_Buffer = Read_Buffer & 0xFFFE ;
		BRCM_Control_WRITE_Data( 0x580090DC , Write_Buffer , BRCM_59281_DATA_LENG , 0 );
		// Setp3
		Read_Buffer = BRCM_Control_READ_Data( 0x5800A198 , 0 ) ;
		BRCM_Control_WRITE_Data( 0x5800A198 , ( Read_Buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 0 );
		DSP_59281_Line_Side_SET_FW_COMMAND( 2 , 0x8004 , 0x0004 );
		Delay_ms(10);
		// Setp4
		Read_Buffer = BRCM_Control_READ_Data( 0x5800A198 , 0 ) ;
		BRCM_Control_WRITE_Data( 0x5800A198 , ( Read_Buffer & 0x7FFF ) , BRCM_59281_DATA_LENG , 0 );
		DSP_59281_Line_Side_SET_FW_COMMAND( 2 , 0x8004 , 0x0004 );
	}
	else if(Channel_SETING==4)
	{
		// Setp1
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800D10C , 0 ) & 0xFFEF );
		Write_Buffer = ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_RX_Polarity_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800D10C , Write_Buffer , BRCM_59281_DATA_LENG , 0 );
		// Setp2
		Read_Buffer = BRCM_Control_READ_Data( 0x5800D0DC , 0 ) ;
		if(( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_Side_RX_Polarity_CH3 & 0x0010 ))
			Write_Buffer = Read_Buffer | 0x0001 ;
		else
			Write_Buffer = Read_Buffer & 0xFFFE ;
		BRCM_Control_WRITE_Data( 0x5800D0DC , Write_Buffer , BRCM_59281_DATA_LENG , 0 );
		// Setp3
		Read_Buffer = BRCM_Control_READ_Data( 0x5800E198 , 0 ) ;
		BRCM_Control_WRITE_Data( 0x5800E198 , ( Read_Buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 0 );
		DSP_59281_Line_Side_SET_FW_COMMAND( 3 , 0x8004 , 0x0004 );
		Delay_ms(10);
		// Setp4
		Read_Buffer = BRCM_Control_READ_Data( 0x5800E198 , 0 ) ;
		BRCM_Control_WRITE_Data( 0x5800E198 , ( Read_Buffer & 0x7FFF ) , BRCM_59281_DATA_LENG , 0 );
		DSP_59281_Line_Side_SET_FW_COMMAND( 3 , 0x8004 , 0x0004 );
	}
	else if(Channel_SETING==5)
	{
		// Setp1
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800110C , 1 ) & 0xFFEF );
		Write_Buffer = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_RX_Polarity_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800110C , Write_Buffer , BRCM_59281_DATA_LENG , 1 );
		// Setp2
		Read_Buffer = BRCM_Control_READ_Data( 0x580010DC , 1 ) ;
		if(( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_RX_Polarity_CH0 & 0x0010 ))
			Write_Buffer = Read_Buffer | 0x0001 ;
		else
			Write_Buffer = Read_Buffer & 0xFFFE ;
		BRCM_Control_WRITE_Data( 0x580010DC , Write_Buffer , BRCM_59281_DATA_LENG , 1 );
		// Setp3
		Read_Buffer = BRCM_Control_READ_Data( 0x58002198 , 1 ) ;
		BRCM_Control_WRITE_Data( 0x58002198 , ( Read_Buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 1 );
		DSP_59281_Line_Side_SET_FW_COMMAND( 4 , 0x8004 , 0x0004 );
		Delay_ms(10);
		// Setp4
		Read_Buffer = BRCM_Control_READ_Data( 0x58002198 , 1 ) ;
		BRCM_Control_WRITE_Data( 0x58002198 , ( Read_Buffer & 0x7FFF ) , BRCM_59281_DATA_LENG , 1 );
		DSP_59281_Line_Side_SET_FW_COMMAND( 4 , 0x8004 , 0x0004 );

	}
	else if(Channel_SETING==6)
	{
		// Setp1
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800510C , 1 ) & 0xFFEF );
		Write_Buffer = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_RX_Polarity_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800510C , Write_Buffer , BRCM_59281_DATA_LENG , 1 );
		// Setp2
		Read_Buffer = BRCM_Control_READ_Data( 0x580050DC , 1 ) ;
		if(( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_RX_Polarity_CH1 & 0x0010 ))
			Write_Buffer = Read_Buffer | 0x0001 ;
		else
			Write_Buffer = Read_Buffer & 0xFFFE ;
		BRCM_Control_WRITE_Data( 0x580050DC , Write_Buffer , BRCM_59281_DATA_LENG , 1 );
		// Setp3
		Read_Buffer = BRCM_Control_READ_Data( 0x58006198 , 1 ) ;
		BRCM_Control_WRITE_Data( 0x58006198 , ( Read_Buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 1 );
		DSP_59281_Line_Side_SET_FW_COMMAND( 5 , 0x8004 , 0x0004 );
		Delay_ms(10);
		// Setp4
		Read_Buffer = BRCM_Control_READ_Data( 0x58006198 , 1 ) ;
		BRCM_Control_WRITE_Data( 0x58006198 , ( Read_Buffer & 0x7FFF ) , BRCM_59281_DATA_LENG , 1 );
		DSP_59281_Line_Side_SET_FW_COMMAND( 5 , 0x8004 , 0x0004 );

	}
	else if(Channel_SETING==7)
	{
		// Setp1
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800910C , 1 ) & 0xFFEF );
		Write_Buffer = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_RX_Polarity_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800910C , Write_Buffer , BRCM_59281_DATA_LENG , 1 );
		// Setp2
		Read_Buffer = BRCM_Control_READ_Data( 0x580090DC , 1 ) ;
		if(( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_RX_Polarity_CH2 & 0x0010 ))
			Write_Buffer = Read_Buffer | 0x0001 ;
		else
			Write_Buffer = Read_Buffer & 0xFFFE ;
		BRCM_Control_WRITE_Data( 0x580090DC , Write_Buffer , BRCM_59281_DATA_LENG , 1 );
		// Setp3
		Read_Buffer = BRCM_Control_READ_Data( 0x5800A198 , 1 ) ;
		BRCM_Control_WRITE_Data( 0x5800A198 , ( Read_Buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 1 );
		DSP_59281_Line_Side_SET_FW_COMMAND( 6 , 0x8004 , 0x0004 );
		Delay_ms(10);
		// Setp4
		Read_Buffer = BRCM_Control_READ_Data( 0x5800A198 , 1 ) ;
		BRCM_Control_WRITE_Data( 0x5800A198 , ( Read_Buffer & 0x7FFF ) , BRCM_59281_DATA_LENG , 1 );
		DSP_59281_Line_Side_SET_FW_COMMAND( 6 , 0x8004 , 0x0004 );

	}
	else if(Channel_SETING==8)
	{
		// Setp1
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5800D10C , 1 ) & 0xFFEF );
		Write_Buffer = ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_RX_Polarity_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5800D10C , Write_Buffer , BRCM_59281_DATA_LENG , 1 );
		// Setp2
		Read_Buffer = BRCM_Control_READ_Data( 0x5800D0DC , 1 ) ;
		if(( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_Side_RX_Polarity_CH3 & 0x0010 ))
			Write_Buffer = Read_Buffer | 0x0001 ;
		else
			Write_Buffer = Read_Buffer & 0xFFFE ;
		BRCM_Control_WRITE_Data( 0x5800D0DC , Write_Buffer , BRCM_59281_DATA_LENG , 1 );

		// Setp3
		Read_Buffer = BRCM_Control_READ_Data( 0x5800E198 , 1 ) ;
		BRCM_Control_WRITE_Data( 0x5800E198 , ( Read_Buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 1 );
		DSP_59281_Line_Side_SET_FW_COMMAND( 7 , 0x8004 , 0x0004 );
		Delay_ms(10);
		// Setp4
		Read_Buffer = BRCM_Control_READ_Data( 0x5800E198 , 1 ) ;
		BRCM_Control_WRITE_Data( 0x5800E198 , ( Read_Buffer & 0x7FFF ) , BRCM_59281_DATA_LENG , 1 );
		DSP_59281_Line_Side_SET_FW_COMMAND( 7 , 0x8004 , 0x0004 );
	}
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 System_Side_TX_Polarity
//-----------------------------------------------------------------------------------------------------//
void System_Side_TX_Polarity(unsigned char Channel_SETING)
{
	unsigned int Read_Buffer;
	unsigned int Write_Buffer;
	if(Channel_SETING==1)
	{
		// Step1: Read for replace Bit 0
		// Normal = 0
		// Invert = 1
		Read_Buffer = ( BRCM_Control_READ_Data( 0x500345CC , 0 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_TX_Polarity_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x500345CC , Write_Buffer , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==2)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x501345CC , 0 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_TX_Polarity_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x501345CC , Write_Buffer , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==3)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x502345CC , 0 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_TX_Polarity_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x502345CC , Write_Buffer , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==4)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x503345CC , 0 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_TX_Polarity_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x503345CC , Write_Buffer , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==5)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x500345CC , 1 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_TX_Polarity_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x500345CC , Write_Buffer , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==6)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x501345CC , 1 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_TX_Polarity_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x501345CC , Write_Buffer , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==7)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x502345CC , 1 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_TX_Polarity_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x502345CC , Write_Buffer , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==8)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x503345CC , 1 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_TX_Polarity_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x503345CC , Write_Buffer , BRCM_59281_DATA_LENG , 1 );
	}
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 System_Side_RX_Polarity
//-----------------------------------------------------------------------------------------------------//
void System_Side_RX_Polarity(unsigned char Channel_SETING)
{
	unsigned int Read_Buffer;
	unsigned int Write_Buffer;
	if(Channel_SETING==1)
	{
		// Step1: Read for replace Bit 0
		// Normal = 0
		// Invert = 1
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5003458C , 0 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_RX_Polarity_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5003458C , Write_Buffer , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==2)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5013458C , 0 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_RX_Polarity_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5013458C , Write_Buffer , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==3)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5023458C , 0 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_RX_Polarity_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5023458C , Write_Buffer , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==4)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5033458C , 0 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_RX_Polarity_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5033458C , Write_Buffer , BRCM_59281_DATA_LENG , 0 );
	}
	else if(Channel_SETING==5)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5003458C , 1 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_RX_Polarity_CH0 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5003458C , Write_Buffer , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==6)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5013458C , 1 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_RX_Polarity_CH1 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5013458C , Write_Buffer , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==7)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5023458C , 1 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_RX_Polarity_CH2 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5023458C , Write_Buffer , BRCM_59281_DATA_LENG , 1 );
	}
	else if(Channel_SETING==8)
	{
		Read_Buffer = ( BRCM_Control_READ_Data( 0x5033458C , 1 ) & 0xFFFE );
		Write_Buffer = ( BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_RX_Polarity_CH3 | Read_Buffer );
		BRCM_Control_WRITE_Data( 0x5033458C , Write_Buffer , BRCM_59281_DATA_LENG , 1 );
	}

}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 Line_Side_Level_Shift
//-----------------------------------------------------------------------------------------------------//
void Line_Side_Level_Shift(unsigned char Channel_SETING)
{
	//unsigned int Read_buffer;
	//unsigned int Write_buffer;
	if(Channel_SETING==1)
	{
		// old control function
		BRCM_Control_WRITE_Data( 0x58001828 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH0 | 0x8000 , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x58001828 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH0 | 0x8000 , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x58001828 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH0 | 0x8000 , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x5800182C , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_MSB_CH0 , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x5800182C , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_MSB_CH0 , BRCM_59281_DATA_LENG , 0 );
		//-----------------------------------------------------------//
		// Step1: Read for replace Bit 14:15 < 0x58001828 >
		//        DAC_NL_MTAP
		//        Write Value 0x02 ( 0x8000 )
		// Step2: Read for replace Bit 7:13  < 0x58001828 >
		//        15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//         0  0  1  1  1  1 1 1 1 0 0 0 0 0 0 0 = 0x3F80 - keep bit 7:13 value
		//         1  1  0  0  0  0 0 0 0 1 1 1 1 1 1 1 = 0x307F - clear bit 7:13 value
		// Step3: Read for replace Bit 0:6   < 0x58001828 >
		//        15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
		//         0  0  0  0  0  0 0 0 0 1 1 1 1 1 1 1 = 0x007F - keep bit 0:6 value
		//         1  1  1  1  1  1 1 1 1 0 0 0 0 0 0 0 = 0xFF80 - clear bit 0:6 value
		// Step4: Read for replace Bit 7:13  < 0x5800182C >
		// Step5: Read for replace Bit 0:6   < 0x5800182C >
		//-----------------------------------------------------------//
		// Setp1
		/*Read_buffer = BRCM_Control_READ_Data( 0x58001828 , 0 ) ;
		BRCM_Control_WRITE_Data( 0x58001828 , ( Read_buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 0 );
		// Setp2
		Read_buffer = ( BRCM_Control_READ_Data( 0x58001828 , 0 ) & 0x307F );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH0 & 0x3F80 ) );
		BRCM_Control_WRITE_Data( 0x58001828 , Write_buffer , BRCM_59281_DATA_LENG , 0 );
		// Setp3
		Read_buffer = ( BRCM_Control_READ_Data( 0x58001828 , 0 ) & 0xFF80 );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH0 & 0x007F ) );
		BRCM_Control_WRITE_Data( 0x58001828 , Write_buffer , BRCM_59281_DATA_LENG , 0 );
		// Setp4
		Read_buffer = ( BRCM_Control_READ_Data( 0x5800182C , 0 ) & 0x307F );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH0 & 0x3F80 ) );
		BRCM_Control_WRITE_Data( 0x5800182C , Write_buffer , BRCM_59281_DATA_LENG , 0 );
		// Setp5
		Read_buffer = ( BRCM_Control_READ_Data( 0x5800182C , 0 ) & 0xFF80 );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH0 & 0x007F ) );
		BRCM_Control_WRITE_Data( 0x5800182C , Write_buffer , BRCM_59281_DATA_LENG , 0 );*/
	}
	else if(Channel_SETING==2)
	{
		// old control function
		BRCM_Control_WRITE_Data( 0x58005828 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH1 | 0x8000 , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x58005828 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH1 | 0x8000 , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x58005828 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH1 | 0x8000 , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x5800582C , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_MSB_CH1 , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x5800582C , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_MSB_CH1 , BRCM_59281_DATA_LENG , 0 );
		// Setp1
		/*Read_buffer = BRCM_Control_READ_Data( 0x58005828 , 0 ) ;
		BRCM_Control_WRITE_Data( 0x58005828 , ( Read_buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 0 );
		// Setp2
		Read_buffer = ( BRCM_Control_READ_Data( 0x58005828 , 0 ) & 0x307F );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH1 & 0x3F80 ) );
		BRCM_Control_WRITE_Data( 0x58005828 , Write_buffer , BRCM_59281_DATA_LENG , 0 );
		// Setp3
		Read_buffer = ( BRCM_Control_READ_Data( 0x58005828 , 0 ) & 0xFF80 );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH1 & 0x007F ) );
		BRCM_Control_WRITE_Data( 0x58005828 , Write_buffer , BRCM_59281_DATA_LENG , 0 );
		// Setp4
		Read_buffer = ( BRCM_Control_READ_Data( 0x5800582C , 0 ) & 0x307F );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH1 & 0x3F80 ) );
		BRCM_Control_WRITE_Data( 0x5800582C , Write_buffer , BRCM_59281_DATA_LENG , 0 );
		// Setp5
		Read_buffer = ( BRCM_Control_READ_Data( 0x5800582C , 0 ) & 0xFF80 );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH1 & 0x007F ) );
		BRCM_Control_WRITE_Data( 0x5800582C , Write_buffer , BRCM_59281_DATA_LENG , 0 );*/
	}
	else if(Channel_SETING==3)
	{
		// old control function
		BRCM_Control_WRITE_Data( 0x58009828 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH2 | 0x8000 , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x58009828 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH2 | 0x8000 , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x58009828 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH2 | 0x8000 , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x5800982C , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_MSB_CH2 , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x5800982C , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_MSB_CH2 , BRCM_59281_DATA_LENG , 0 );
		// Setp1
		/*Read_buffer = BRCM_Control_READ_Data( 0x58009828 , 0 ) ;
		BRCM_Control_WRITE_Data( 0x58009828 , ( Read_buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 0 );
		// Setp2
		Read_buffer = ( BRCM_Control_READ_Data( 0x58009828 , 0 ) & 0x307F );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH2 & 0x3F80 ) );
		BRCM_Control_WRITE_Data( 0x58009828 , Write_buffer , BRCM_59281_DATA_LENG , 0 );
		// Setp3
		Read_buffer = ( BRCM_Control_READ_Data( 0x58009828 , 0 ) & 0xFF80 );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH2 & 0x007F ) );
		BRCM_Control_WRITE_Data( 0x58009828 , Write_buffer , BRCM_59281_DATA_LENG , 0 );
		// Setp4
		Read_buffer = ( BRCM_Control_READ_Data( 0x5800982C , 0 ) & 0x307F );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH2 & 0x3F80 ) );
		BRCM_Control_WRITE_Data( 0x5800982C , Write_buffer , BRCM_59281_DATA_LENG , 0 );
		// Setp5
		Read_buffer = ( BRCM_Control_READ_Data( 0x5800982C , 0 ) & 0xFF80 );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH2 & 0x007F ) );
		BRCM_Control_WRITE_Data( 0x5800982C , Write_buffer , BRCM_59281_DATA_LENG , 0 );*/
	}
	else if(Channel_SETING==4)
	{
		// old control function
		BRCM_Control_WRITE_Data( 0x5800D828 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH3 | 0x8000 , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x5800D828 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH3 | 0x8000 , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x5800D828 , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH3 | 0x8000 , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x5800D82C , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_MSB_CH3 , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x5800D82C , BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_MSB_CH3 , BRCM_59281_DATA_LENG , 0 );
		// Setp1
		/*Read_buffer = BRCM_Control_READ_Data( 0x5800D828 , 0 ) ;
		BRCM_Control_WRITE_Data( 0x5800D828 , ( Read_buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 0 );
		// Setp2
		Read_buffer = ( BRCM_Control_READ_Data( 0x5800D828 , 0 ) & 0x307F );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH3 & 0x3F80 ) );
		BRCM_Control_WRITE_Data( 0x5800D828 , Write_buffer , BRCM_59281_DATA_LENG , 0 );
		// Setp3
		Read_buffer = ( BRCM_Control_READ_Data( 0x5800D828 , 0 ) & 0xFF80 );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH3 & 0x007F ) );
		BRCM_Control_WRITE_Data( 0x5800D828 , Write_buffer , BRCM_59281_DATA_LENG , 0 );
		// Setp4
		Read_buffer = ( BRCM_Control_READ_Data( 0x5800D82C , 0 ) & 0x307F );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH3 & 0x3F80 ) );
		BRCM_Control_WRITE_Data( 0x5800D82C , Write_buffer , BRCM_59281_DATA_LENG , 0 );
		// Setp5
		Read_buffer = ( BRCM_Control_READ_Data( 0x5800D82C , 0 ) & 0xFF80 );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Line_side_LShift_LSB_CH3 & 0x007F ) );
		BRCM_Control_WRITE_Data( 0x5800D82C , Write_buffer , BRCM_59281_DATA_LENG , 0 );*/
	}
	else if(Channel_SETING==5)
	{
		// old control function
		BRCM_Control_WRITE_Data( 0x58001828 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH0 | 0x8000 , BRCM_59281_DATA_LENG , 1 );
		BRCM_Control_WRITE_Data( 0x58001828 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH0 | 0x8000 , BRCM_59281_DATA_LENG , 1 );
		BRCM_Control_WRITE_Data( 0x58001828 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH0 | 0x8000 , BRCM_59281_DATA_LENG , 1 );
		BRCM_Control_WRITE_Data( 0x5800182C , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_MSB_CH0 , BRCM_59281_DATA_LENG , 1 );
		BRCM_Control_WRITE_Data( 0x5800182C , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_MSB_CH0 , BRCM_59281_DATA_LENG , 1 );
		// Setp1
		/*Read_buffer = BRCM_Control_READ_Data( 0x58001828 , 1 ) ;
		BRCM_Control_WRITE_Data( 0x58001828 , ( Read_buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 1 );
		// Setp2
		Read_buffer = ( BRCM_Control_READ_Data( 0x58001828 , 1 ) & 0x307F );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH0 & 0x3F80 ) );
		BRCM_Control_WRITE_Data( 0x58001828 , Write_buffer , BRCM_59281_DATA_LENG , 1 );
		// Setp3
		Read_buffer = ( BRCM_Control_READ_Data( 0x58001828 , 1 ) & 0xFF80 );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH0 & 0x007F ) );
		BRCM_Control_WRITE_Data( 0x58001828 , Write_buffer , BRCM_59281_DATA_LENG , 1 );
		// Setp4
		Read_buffer = ( BRCM_Control_READ_Data( 0x5800182C , 1 ) & 0x307F );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH0 & 0x3F80 ) );
		BRCM_Control_WRITE_Data( 0x5800182C , Write_buffer , BRCM_59281_DATA_LENG , 1 );
		// Setp5
		Read_buffer = ( BRCM_Control_READ_Data( 0x5800182C , 1 ) & 0xFF80 );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH0 & 0x007F ) );
		BRCM_Control_WRITE_Data( 0x5800182C , Write_buffer , BRCM_59281_DATA_LENG , 1 );*/
	}
	else if(Channel_SETING==6)
	{
		// old control function
		BRCM_Control_WRITE_Data( 0x58005828 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH1 | 0x8000 , BRCM_59281_DATA_LENG , 1 );
		BRCM_Control_WRITE_Data( 0x58005828 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH1 | 0x8000 , BRCM_59281_DATA_LENG , 1 );
		BRCM_Control_WRITE_Data( 0x58005828 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH1 | 0x8000 , BRCM_59281_DATA_LENG , 1 );
		BRCM_Control_WRITE_Data( 0x5800582C , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_MSB_CH1 , BRCM_59281_DATA_LENG , 1 );
		BRCM_Control_WRITE_Data( 0x5800582C , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_MSB_CH1 , BRCM_59281_DATA_LENG , 1 );
		// Setp1
		/*Read_buffer = BRCM_Control_READ_Data( 0x58005828 , 1 ) ;
		BRCM_Control_WRITE_Data( 0x58005828 , ( Read_buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 1 );
		// Setp2
		Read_buffer = ( BRCM_Control_READ_Data( 0x58005828 , 1 ) & 0x307F );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH1 & 0x3F80 ) );
		BRCM_Control_WRITE_Data( 0x58005828 , Write_buffer , BRCM_59281_DATA_LENG , 1 );
		// Setp3
		Read_buffer = ( BRCM_Control_READ_Data( 0x58005828 , 1 ) & 0xFF80 );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH1 & 0x007F ) );
		BRCM_Control_WRITE_Data( 0x58005828 , Write_buffer , BRCM_59281_DATA_LENG , 1 );
		// Setp4
		Read_buffer = ( BRCM_Control_READ_Data( 0x5800582C , 1 ) & 0x307F );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH1 & 0x3F80 ) );
		BRCM_Control_WRITE_Data( 0x5800582C , Write_buffer , BRCM_59281_DATA_LENG , 1 );
		// Setp5
		Read_buffer = ( BRCM_Control_READ_Data( 0x5800582C , 1 ) & 0xFF80 );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH1 & 0x007F ) );
		BRCM_Control_WRITE_Data( 0x5800582C , Write_buffer , BRCM_59281_DATA_LENG , 1 );
		*/
	}
	else if(Channel_SETING==7)
	{
		// old control function
		BRCM_Control_WRITE_Data( 0x58009828 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH2 | 0x8000 , BRCM_59281_DATA_LENG , 1 );
		BRCM_Control_WRITE_Data( 0x58009828 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH2 | 0x8000 , BRCM_59281_DATA_LENG , 1 );
		BRCM_Control_WRITE_Data( 0x58009828 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH2 | 0x8000 , BRCM_59281_DATA_LENG , 1 );
		BRCM_Control_WRITE_Data( 0x5800982C , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_MSB_CH2 , BRCM_59281_DATA_LENG , 1 );
		BRCM_Control_WRITE_Data( 0x5800982C , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_MSB_CH2 , BRCM_59281_DATA_LENG , 1 );
		// Setp1
		/*Read_buffer = BRCM_Control_READ_Data( 0x58009828 , 1 ) ;
		BRCM_Control_WRITE_Data( 0x58009828 , ( Read_buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 1 );
		// Setp2
		Read_buffer = ( BRCM_Control_READ_Data( 0x58009828 , 1 ) & 0x307F );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH2 & 0x3F80 ) );
		BRCM_Control_WRITE_Data( 0x58009828 , Write_buffer , BRCM_59281_DATA_LENG , 1 );
		// Setp3
		Read_buffer = ( BRCM_Control_READ_Data( 0x58009828 , 1 ) & 0xFF80 );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH2 & 0x007F ) );
		BRCM_Control_WRITE_Data( 0x58009828 , Write_buffer , BRCM_59281_DATA_LENG , 1 );
		// Setp4
		Read_buffer = ( BRCM_Control_READ_Data( 0x5800982C , 1 ) & 0x307F );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH2 & 0x3F80 ) );
		BRCM_Control_WRITE_Data( 0x5800982C , Write_buffer , BRCM_59281_DATA_LENG , 1 );
		// Setp5
		Read_buffer = ( BRCM_Control_READ_Data( 0x5800982C , 1 ) & 0xFF80 );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH2 & 0x007F ) );
		BRCM_Control_WRITE_Data( 0x5800982C , Write_buffer , BRCM_59281_DATA_LENG , 1 );*/
	}
	else if(Channel_SETING==8)
	{
		// old control function
		BRCM_Control_WRITE_Data( 0x5800D828 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH3 | 0x8000 , BRCM_59281_DATA_LENG , 1 );
		BRCM_Control_WRITE_Data( 0x5800D828 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH3 | 0x8000 , BRCM_59281_DATA_LENG , 1 );
		BRCM_Control_WRITE_Data( 0x5800D828 , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH3 | 0x8000 , BRCM_59281_DATA_LENG , 1 );
		BRCM_Control_WRITE_Data( 0x5800D82C , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_MSB_CH3 , BRCM_59281_DATA_LENG , 1 );
		BRCM_Control_WRITE_Data( 0x5800D82C , BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_MSB_CH3 , BRCM_59281_DATA_LENG , 1 );
		// Setp1
		/*Read_buffer = BRCM_Control_READ_Data( 0x5800D828 , 1 ) ;
		BRCM_Control_WRITE_Data( 0x5800D828 , ( Read_buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 1 );
		// Setp2
		Read_buffer = ( BRCM_Control_READ_Data( 0x5800D828 , 1 ) & 0x307F );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH3 & 0x3F80 ) );
		BRCM_Control_WRITE_Data( 0x5800D828 , Write_buffer , BRCM_59281_DATA_LENG , 1 );
		// Setp3
		Read_buffer = ( BRCM_Control_READ_Data( 0x5800D828 , 1 ) & 0xFF80 );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH3 & 0x007F ) );
		BRCM_Control_WRITE_Data( 0x5800D828 , Write_buffer , BRCM_59281_DATA_LENG , 1 );
		// Setp4
		Read_buffer = ( BRCM_Control_READ_Data( 0x5800D82C , 1 ) & 0x307F );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH3 & 0x3F80 ) );
		BRCM_Control_WRITE_Data( 0x5800D82C , Write_buffer , BRCM_59281_DATA_LENG , 1 );
		// Setp5
		Read_buffer = ( BRCM_Control_READ_Data( 0x5800D82C , 1 ) & 0xFF80 );
		Write_buffer = ( Read_buffer | ( BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Line_side_LShift_LSB_CH3 & 0x007F ) );
		BRCM_Control_WRITE_Data( 0x5800D82C , Write_buffer , BRCM_59281_DATA_LENG , 1 );*/
	}
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 System_Side_Squelch
//-----------------------------------------------------------------------------------------------------//
void System_Side_Squelch_Control(unsigned char Enable)
{
	// Pcb Gold finger Tx LSB
	// Pcb Gold finger Rx MSB
	if(Enable)
		BRCM_Control_WRITE_Data( 0x5200C794 , 0xFFFF , BRCM_59281_DATA_LENG , 0 );
	else
		BRCM_Control_WRITE_Data( 0x5200C794 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
}

void System_side_MSA_Squelch_Control(unsigned int Enable)
{
	BRCM_Control_WRITE_Data( 0x5200C794 , Enable , BRCM_59281_DATA_LENG , 0 );
}

void System_side_Squelch_Rx_Control(unsigned int CH_Enable)
{
	// Rx Squelch is Pcb Gold finger  / DSP is tx side
	// so Control MSB byte
	// FF00 Disable Gold finger Rx side
	BRCM_Control_WRITE_Data( 0x5200C794 , CH_Enable , BRCM_59281_DATA_LENG , 0 );
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 PHY AVS Enable
//-----------------------------------------------------------------------------------------------------//
void DSP59281_AVS_Control()
{
	unsigned int Data_count = 0;
	BRCM_Control_WRITE_Data( 0x5200C784 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200C708 , 0x0081 , BRCM_59281_DATA_LENG , 0 );
	while(1)
	{
		if(BRCM_Control_READ_Data( 0x5200C708 , 0 ) == 0x2081 )
			break;
		Data_count++;

		if(Data_count>3000)
			break;
	}
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 Software LowPower Mode
//-----------------------------------------------------------------------------------------------------//
void DSP59281_LP_MODE_Control(unsigned char LP_MODE_EN )
{
	// 15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
	//  0  0  0  1  0  0  0  0  0  0  0  0  0  0  0  0
	unsigned int Read_Buffer;

	// lan0 - lan3 phy id 0
	// get correct mdio port id Read for replace Bit : 4
	Read_Buffer = BRCM_Control_READ_Data( 0x5003C014 , 0 );
	if(LP_MODE_EN)
		BRCM_Control_WRITE_Data( 0x5003C014 , Read_Buffer | 0x0010 , BRCM_59281_DATA_LENG , 0 );
	else
		BRCM_Control_WRITE_Data( 0x5003C014 , Read_Buffer & 0xFFEF , BRCM_59281_DATA_LENG , 0 );
	// get correct mdio port id Read for replace Bit : 12
	Read_Buffer = BRCM_Control_READ_Data( 0x5003C014 , 0 );
	if(LP_MODE_EN)
		BRCM_Control_WRITE_Data( 0x5003C014 , Read_Buffer | 0x1000 , BRCM_59281_DATA_LENG , 0 );
	else
		BRCM_Control_WRITE_Data( 0x5003C014 , Read_Buffer & 0xEFFF , BRCM_59281_DATA_LENG , 0 );
	// lan4 - lan7 phy id 1
	// get correct mdio port id Read for replace Bit : 4
	Read_Buffer = BRCM_Control_READ_Data( 0x5003C014 , 1 );
	if(LP_MODE_EN)
		BRCM_Control_WRITE_Data( 0x5003C014 , Read_Buffer | 0x0010 , BRCM_59281_DATA_LENG , 1 );
	else
		BRCM_Control_WRITE_Data( 0x5003C014 , Read_Buffer & 0xFFEF , BRCM_59281_DATA_LENG , 1 );
	// get correct mdio port id Read for replace Bit : 12
	Read_Buffer = BRCM_Control_READ_Data( 0x5003C014 , 1 );
	if(LP_MODE_EN)
		BRCM_Control_WRITE_Data( 0x5003C014 , Read_Buffer | 0x1000 , BRCM_59281_DATA_LENG , 1 );
	else
		BRCM_Control_WRITE_Data( 0x5003C014 , Read_Buffer & 0xEFFF , BRCM_59281_DATA_LENG , 1 );
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 all power down
//-----------------------------------------------------------------------------------------------------//
void DSP59281_PowerDown_Control(unsigned char PowerDown)
{
	unsigned int Read_Buffer;
	//---------------------------------------------------------------//
	//                   Lan0 power down Control                     //
	//---------------------------------------------------------------//
	// get_port_lane_mask
	Read_Buffer = BRCM_Control_READ_Data( 0x0002C41C , 1 );
	// Read for replace Bit : 0-1
	Read_Buffer = BRCM_Control_READ_Data( 0x5200C864 , 0 );
	if(PowerDown)
		BRCM_Control_WRITE_Data( 0x5200C864 , Read_Buffer | 0x0003 , BRCM_59281_DATA_LENG , 0 );
	else
		BRCM_Control_WRITE_Data( 0x5200C864 , Read_Buffer & 0xFFFC , BRCM_59281_DATA_LENG , 0 );

	// Wait for the powerdown to complete
	Delay_ms(50);
	Read_Buffer = BRCM_Control_READ_Data( 0x5200C868 , 0 );
	//---------------------------------------------------------------//
	//                   Lan1 power down Control                     //
	//---------------------------------------------------------------//
	// get_port_lane_mask
	Read_Buffer = BRCM_Control_READ_Data( 0x0002C424 , 1 );
	// Read for replace Bit : 0-1
	Read_Buffer = BRCM_Control_READ_Data( 0x5200C864 , 0 );
	if(PowerDown)
		BRCM_Control_WRITE_Data( 0x5200C864 , Read_Buffer | 0x000C , BRCM_59281_DATA_LENG , 0 );
	else
		BRCM_Control_WRITE_Data( 0x5200C864 , Read_Buffer & 0xFFF3 , BRCM_59281_DATA_LENG , 0 );
	// Wait for the powerdown to complete
	Delay_ms(50);
	Read_Buffer = BRCM_Control_READ_Data( 0x5200C868 , 0 );
	//---------------------------------------------------------------//
	//                   Lan2 power down Control                     //
	//---------------------------------------------------------------//
	// get_port_lane_mask
	Read_Buffer = BRCM_Control_READ_Data( 0x0002C42C , 1 );
	// Read for replace Bit : 0-1
	Read_Buffer = BRCM_Control_READ_Data( 0x5200C864 , 0 );
	if(PowerDown)
		BRCM_Control_WRITE_Data( 0x5200C864 , Read_Buffer | 0x0030 , BRCM_59281_DATA_LENG , 0 );
	else
		BRCM_Control_WRITE_Data( 0x5200C864 , Read_Buffer & 0xFFCF , BRCM_59281_DATA_LENG , 0 );
	// Wait for the powerdown to complete
	Delay_ms(50);
	Read_Buffer = BRCM_Control_READ_Data( 0x5200C868 , 0 );
	//---------------------------------------------------------------//
	//                   Lan3 power down Control                     //
	//---------------------------------------------------------------//
	// get_port_lane_mask
	Read_Buffer = BRCM_Control_READ_Data( 0x0002C434 , 1 );
	// Read for replace Bit : 0-1
	Read_Buffer = BRCM_Control_READ_Data( 0x5200C864 , 0 );
	if(PowerDown)
		BRCM_Control_WRITE_Data( 0x5200C864 , Read_Buffer | 0x00C0 , BRCM_59281_DATA_LENG , 0 );
	else
		BRCM_Control_WRITE_Data( 0x5200C864 , Read_Buffer & 0xFF3F , BRCM_59281_DATA_LENG , 0 );
	// Wait for the powerdown to complete
	Delay_ms(50);
	Read_Buffer = BRCM_Control_READ_Data( 0x5200C868 , 0 );
	//---------------------------------------------------------------//
	//                   Lan4 power down Control                     //
	//---------------------------------------------------------------//
	// get_port_lane_mask
	Read_Buffer = BRCM_Control_READ_Data( 0x0002C43C , 1 );
	// Read for replace Bit : 0-1
	Read_Buffer = BRCM_Control_READ_Data( 0x5200C864 , 0 );
	if(PowerDown)
		BRCM_Control_WRITE_Data( 0x5200C864 , Read_Buffer | 0x0300 , BRCM_59281_DATA_LENG , 0 );
	else
		BRCM_Control_WRITE_Data( 0x5200C864 , Read_Buffer & 0xFCFF , BRCM_59281_DATA_LENG , 0 );
	// Wait for the powerdown to complete
	Delay_ms(50);
	Read_Buffer = BRCM_Control_READ_Data( 0x5200C868 , 0 );
	//---------------------------------------------------------------//
	//                   Lan5 power down Control                     //
	//---------------------------------------------------------------//
	// get_port_lane_mask
	Read_Buffer = BRCM_Control_READ_Data( 0x0002C444 , 1 );
	// Read for replace Bit : 0-1
	Read_Buffer = BRCM_Control_READ_Data( 0x5200C864 , 0 );
	if(PowerDown)
		BRCM_Control_WRITE_Data( 0x5200C864 , Read_Buffer | 0x0C00 , BRCM_59281_DATA_LENG , 0 );
	else
		BRCM_Control_WRITE_Data( 0x5200C864 , Read_Buffer & 0xF3FF , BRCM_59281_DATA_LENG , 0 );
	// Wait for the powerdown to complete
	Delay_ms(50);
	Read_Buffer = BRCM_Control_READ_Data( 0x5200C868 , 0 );
	//---------------------------------------------------------------//
	//                   Lan6 power down Control                     //
	//---------------------------------------------------------------//
	// get_port_lane_mask
	Read_Buffer = BRCM_Control_READ_Data( 0x0002C44C , 1 );
	// Read for replace Bit : 0-1
	Read_Buffer = BRCM_Control_READ_Data( 0x5200C864 , 0 );
	if(PowerDown)
		BRCM_Control_WRITE_Data( 0x5200C864 , Read_Buffer | 0x3000 , BRCM_59281_DATA_LENG , 0 );
	else
		BRCM_Control_WRITE_Data( 0x5200C864 , Read_Buffer & 0xCFFF , BRCM_59281_DATA_LENG , 0 );
	// Wait for the powerdown to complete
	Delay_ms(50);
	Read_Buffer = BRCM_Control_READ_Data( 0x5200C868 , 0 );
	//---------------------------------------------------------------//
	//                   Lan7 power down Control                     //
	//---------------------------------------------------------------//
	// get_port_lane_mask
	Read_Buffer = BRCM_Control_READ_Data( 0x0002C454 , 1 );
	// Read for replace Bit : 0-1
	Read_Buffer = BRCM_Control_READ_Data( 0x5200C864 , 0 );
	if(PowerDown)
		BRCM_Control_WRITE_Data( 0x5200C864 , Read_Buffer | 0xC000 , BRCM_59281_DATA_LENG , 0 );
	else
		BRCM_Control_WRITE_Data( 0x5200C864 , Read_Buffer & 0x3FFF , BRCM_59281_DATA_LENG , 0 );
	// Wait for the powerdown to complete
	Delay_ms(50);
	Read_Buffer = BRCM_Control_READ_Data( 0x5200C868 , 0 );
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 CDR TX SystemSide LOL LOS Status Get
//-----------------------------------------------------------------------------------------------------//
unsigned int DSP59281_TX_SystemSide_LOL_LOS_Status()
{
	// 52007EE4
	// Bit0-7  => LOL CH0-CH7
	// Bit8-15 => LOS CH0-CH7
	unsigned int LOLLOS_STATUS;

	BRCM_Control_WRITE_Data( 0x52007EE4 , 0xFFFF , 4 , 0 );
	Delay_ms(1);
	LOLLOS_STATUS = BRCM_Control_READ_Data( 0x52007EE4 , 0 );

	return LOLLOS_STATUS;
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 CDR RX SystemSide LOL LOS Status Get
//-----------------------------------------------------------------------------------------------------//
unsigned int DSP59281_RX_LineSide_LOL_LOS_Status()
{
	unsigned int LOLLOS_STATUS;
	// 52007F54
	// Bit0-7  => LOL CH0-CH7
	// Bit8-15 => LOS CH0-CH7
	BRCM_Control_WRITE_Data( 0x52007F54 , 0xFFFF , 4 , 0 );
	Delay_ms(1);
	LOLLOS_STATUS = BRCM_Control_READ_Data( 0x52007F54 , 0 );

	return LOLLOS_STATUS;
}

//-----------------------------------------------------------------------------------------------------//
// DSP 59281 Low Power mode
//-----------------------------------------------------------------------------------------------------//
void DSP59281_LowPower_Control(unsigned char LowPower_EN)
{
	if( LowPower_EN == 1 )
	{
		// Enable Low Power mode
		BRCM_Control_WRITE_Data( 0x5003C014 , 0xC010 , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x5003C014 , 0xD010 , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x5003C014 , 0xC010 , BRCM_59281_DATA_LENG , 1 );
		BRCM_Control_WRITE_Data( 0x5003C014 , 0xD010 , BRCM_59281_DATA_LENG , 1 );
	}
	else
	{
		// Disable Low Power mode
		BRCM_Control_WRITE_Data( 0x5003C014 , 0xC000 , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x5003C014 , 0xC000 , BRCM_59281_DATA_LENG , 0 );
		BRCM_Control_WRITE_Data( 0x5003C014 , 0xC000 , BRCM_59281_DATA_LENG , 1 );
		BRCM_Control_WRITE_Data( 0x5003C014 , 0xC000 , BRCM_59281_DATA_LENG , 1 );
	}
}

//-----------------------------------------------------------------------------------------------------//
// SETTING ALL Channel FIR
//-----------------------------------------------------------------------------------------------------//
void DSP59281_SET_ALL_CH_FIR()
{
	unsigned char CH_Count=0;

	for(CH_Count=1;CH_Count<9;CH_Count++)
	{
		Line_Side_Channel_Control(CH_Count);
		System_Side_Channel_Control(CH_Count);
	}

}

void System_Side_CHANNEL_SETTING( int PRE1 ,int MAIN ,int POST ,unsigned char CHANNEL )
{
	/*
	 * 	System_Side_PRE1(Channel_SETING);
	System_Side_Main(Channel_SETING);
	System_Side_POST1(Channel_SETING);
	System_Side_POST2(Channel_SETING);
	System_Side_POST3(Channel_SETING);
	 */
	switch(CHANNEL)
	{
		case 1 :
				BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH0 = PRE1 ;
				BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH0 = MAIN ;
				BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH0 = POST ;
		        break;
		case 2 :
				BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH1 = PRE1 ;
				BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH1 = MAIN ;
				BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH1 = POST ;
		        break;
		case 3 :
				BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH2 = PRE1 ;
				BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH2 = MAIN ;
				BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH2 = POST ;
		        break;
		case 4 :
				BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH3 = PRE1 ;
				BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH3 = MAIN ;
				BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH3 = POST ;
		        break;
		case 5 :
				BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH0 = PRE1 ;
				BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH0 = MAIN ;
				BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH0 = POST ;
		        break;
		case 6 :
				BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH1 = PRE1 ;
				BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH1 = MAIN ;
				BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH1 = POST ;
		        break;
		case 7 :
				BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH2 = PRE1 ;
				BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH2 = MAIN ;
				BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH2 = POST ;
		        break;
		case 8 :
				BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH3 = PRE1 ;
				BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH3 = MAIN ;
				BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH3 = POST ;
		        break;
	}

	System_Side_PRE1(CHANNEL);
	System_Side_Main(CHANNEL);
	System_Side_POST1(CHANNEL);

}

void BRCM_SUGGEST_LOSS_RX_OUTPUT_SETTING( unsigned char LOSS_LEVEL ,unsigned char Channel )
{
	//-----------------------------------------------------------------------------------------//
	// MSA AMP BRCM suggest Output Swing / post /pre
	// Leve0        pre1 -8  main 128 post   0    <  3
	// Leve1        pre1 -16 main 128 post   0       3 - 10
	// Leve2        pre1 -20 main 148 post   0      10 - 14
	// Leve3        pre1 -24 main 144 post   0      14 - 18
	// Leve4        pre1 -24 main 140 post - 4      18 - 19
	// Leve5        pre1 -24 main 136 post - 8      19 - 20
	// Leve6        pre1 -24 main 132 post -12      20 - 21
	// Leve7        pre1 -24 main 128 post -16      21 - 22
	// Leve8 pre2 4 pre1 -24 main 120 post -20      22 - 23
	// Leve9 pre2 4 pre1 -28 main 112 post -24    > 23
	//----------------------------------------------------------------------------------------//
	switch(LOSS_LEVEL)
	{
		case 0 :
				System_Side_CHANNEL_SETTING( -8  , 128 , 0  , Channel );
			    break;
		case 1 :
				System_Side_CHANNEL_SETTING( -16 , 128 , 0  , Channel );
			    break;
		case 2 :
				System_Side_CHANNEL_SETTING( -20 , 148 , 0  , Channel );
			    break;
		case 3 :
				System_Side_CHANNEL_SETTING( -24 , 144 , 0  , Channel );
			    break;
		case 4 :
				System_Side_CHANNEL_SETTING( -24 , 140 , -4  , Channel );
			    break;
		case 5 :
				System_Side_CHANNEL_SETTING( -24 , 136 , -8  , Channel );
			    break;
		case 6 :
				System_Side_CHANNEL_SETTING( -24 , 132 , -12 , Channel );
			    break;
		case 7 :
				System_Side_CHANNEL_SETTING( -24 , 128 , -16 , Channel );
			    break;
		case 8 :
				System_Side_CHANNEL_SETTING( -24 , 128 , -16 , Channel );
			    break;
		case 9 :
				System_Side_CHANNEL_SETTING( -24 , 128 , -16 , Channel );
			    break;
	}
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 Dynamic Max/Min Phase Bias Threshold Setting
//-----------------------------------------------------------------------------------------------------//
void Line_Side_Dynamic_Phase_Bias_THR_SETTING()
{
	unsigned int Write_Temp_buffer = 0 ;
	//--------------------------------------------------------------------//
	// Lan0 Dynamic phase Max +15 Min -15 setting
	//--------------------------------------------------------------------//
	// Read for replace bit 0:4 min  bit 4 is +-  1: - 0: +
	// Read for replace bit 5:9 max  bit 9 is +-  1: - 0: +
	Write_Temp_buffer = BRCM_Control_READ_Data( 0x58001028 , 0 );
	Write_Temp_buffer = ( Write_Temp_buffer | BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Dynamic_Phase_THR_CH0 );
	BRCM_Control_WRITE_Data( 0x58001028 , Write_Temp_buffer , BRCM_59281_DATA_LENG , 0 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	DSP_59281_Line_Side_SET_FW_COMMAND(0,0x8004,0x0004);
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0001 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800218C , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800218C , 0 );*/
	//--------------------------------------------------------------------//
	// Lan1 Dynamic phase Max +15 Min -15 setting
	//--------------------------------------------------------------------//
	// Read for replace bit 0:4 min  bit 4 is +-  1: - 0: +
	// Read for replace bit 5:9 max  bit 9 is +-  1: - 0: +
	Write_Temp_buffer = BRCM_Control_READ_Data( 0x58005028 , 0 );
	Write_Temp_buffer = ( Write_Temp_buffer | BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Dynamic_Phase_THR_CH1 );
	BRCM_Control_WRITE_Data( 0x58005028 , Write_Temp_buffer , BRCM_59281_DATA_LENG , 0 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	DSP_59281_Line_Side_SET_FW_COMMAND(1,0x8004,0x0004);
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0002 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800618C , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800618C , 0 );*/
	//--------------------------------------------------------------------//
	// Lan2 Dynamic phase Max +15 Min -15 setting
	//--------------------------------------------------------------------//
	// Read for replace bit 0:4 min  bit 4 is +-  1: - 0: +
	// Read for replace bit 5:9 max  bit 9 is +-  1: - 0: +
	Write_Temp_buffer = BRCM_Control_READ_Data( 0x58009028 , 0 );
	Write_Temp_buffer = ( Write_Temp_buffer | BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Dynamic_Phase_THR_CH2 );
	BRCM_Control_WRITE_Data( 0x58009028 , Write_Temp_buffer , BRCM_59281_DATA_LENG , 0 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	DSP_59281_Line_Side_SET_FW_COMMAND(2,0x8004,0x0004);
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800A18C , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800A18C , 0 );*/
	//--------------------------------------------------------------------//
	// Lan3 Dynamic phase Max +15 Min -15 setting
	//--------------------------------------------------------------------//
	// Read for replace bit 0:4 min  bit 4 is +-  1: - 0: +
	// Read for replace bit 5:9 max  bit 9 is +-  1: - 0: +
	Write_Temp_buffer = BRCM_Control_READ_Data( 0x5800D028 , 0 );
	Write_Temp_buffer = ( Write_Temp_buffer | BRCM_59281_DSP_LS_PHY0_MEMORY_MAP.Dynamic_Phase_THR_CH3 );
	BRCM_Control_WRITE_Data( 0x5800D028 , Write_Temp_buffer , BRCM_59281_DATA_LENG , 0 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	DSP_59281_Line_Side_SET_FW_COMMAND(3,0x8004,0x0004);
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0008 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800E18C , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800E18C , 0 );*/
	//--------------------------------------------------------------------//
	// Lan4 Dynamic phase Max +15 Min -15 setting
	//--------------------------------------------------------------------//
	// Read for replace bit 0:4 min  bit 4 is +-  1: - 0: +
	// Read for replace bit 5:9 max  bit 9 is +-  1: - 0: +
	Write_Temp_buffer = BRCM_Control_READ_Data( 0x58001028 , 1 );
	Write_Temp_buffer = ( Write_Temp_buffer | BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Dynamic_Phase_THR_CH4 );
	BRCM_Control_WRITE_Data( 0x58001028 , Write_Temp_buffer , BRCM_59281_DATA_LENG , 1 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	DSP_59281_Line_Side_SET_FW_COMMAND(4,0x8004,0x0004);
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0010 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800218C , 1 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800218C , 1 );*/
	//--------------------------------------------------------------------//
	// Lan5 Dynamic phase Max +15 Min -15 setting
	//--------------------------------------------------------------------//
	// Read for replace bit 0:4 min  bit 4 is +-  1: - 0: +
	// Read for replace bit 5:9 max  bit 9 is +-  1: - 0: +
	Write_Temp_buffer = BRCM_Control_READ_Data( 0x58005028 , 1 );
	Write_Temp_buffer = ( Write_Temp_buffer | BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Dynamic_Phase_THR_CH5 );
	BRCM_Control_WRITE_Data( 0x58005028 , Write_Temp_buffer , BRCM_59281_DATA_LENG , 1 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	DSP_59281_Line_Side_SET_FW_COMMAND(5,0x8004,0x0004);
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0020 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800618C , 1 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800618C , 1 );*/
	//--------------------------------------------------------------------//
	// Lan6 Dynamic phase Max +15 Min -15 setting
	//--------------------------------------------------------------------//
	// Read for replace bit 0:4 min  bit 4 is +-  1: - 0: +
	// Read for replace bit 5:9 max  bit 9 is +-  1: - 0: +
	Write_Temp_buffer = BRCM_Control_READ_Data( 0x58009028 , 1 );
	Write_Temp_buffer = ( Write_Temp_buffer | BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Dynamic_Phase_THR_CH6 );
	BRCM_Control_WRITE_Data( 0x58009028 , Write_Temp_buffer , BRCM_59281_DATA_LENG , 1 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	DSP_59281_Line_Side_SET_FW_COMMAND(6,0x8004,0x0004);
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0040 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800A18C , 1 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800A18C , 1 );*/
	//--------------------------------------------------------------------//
	// Lan3 Dynamic phase Max +15 Min -15 setting
	//--------------------------------------------------------------------//
	// Read for replace bit 0:4 min  bit 4 is +-  1: - 0: +
	// Read for replace bit 5:9 max  bit 9 is +-  1: - 0: +
	Write_Temp_buffer = BRCM_Control_READ_Data( 0x5800D028 , 1 );
	Write_Temp_buffer = ( Write_Temp_buffer | BRCM_59281_DSP_LS_PHY1_MEMORY_MAP.Dynamic_Phase_THR_CH7 );
	BRCM_Control_WRITE_Data( 0x5800D028 , Write_Temp_buffer , BRCM_59281_DATA_LENG , 1 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	DSP_59281_Line_Side_SET_FW_COMMAND(7,0x8004,0x0004);
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0080 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800E18C , 1 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800E18C , 1 );*/
}

//-----------------------------------------------------------------------------------------------------//
// DSP 59281 Function control
//-----------------------------------------------------------------------------------------------------//
void Line_Side_ALL_CH1_CH4_Control_P86()
{
	Line_Side_TX_Polarity(1);
	Line_Side_TX_Polarity(2);
	Line_Side_TX_Polarity(3);
	Line_Side_TX_Polarity(4);

	Line_Side_RX_Polarity(1);
	Line_Side_RX_Polarity(2);
	Line_Side_RX_Polarity(3);
	Line_Side_RX_Polarity(4);

	Line_Side_Level_Shift(1);
	Line_Side_Level_Shift(2);
	Line_Side_Level_Shift(3);
	Line_Side_Level_Shift(4);

	Line_Side_Channel_Control(1);
	Line_Side_Channel_Control(2);
	Line_Side_Channel_Control(3);
	Line_Side_Channel_Control(4);
}
void System_Side_ALL_CH1_CH4_Control_P87()
{
	System_Side_TX_Polarity(1);
	System_Side_TX_Polarity(2);
	System_Side_TX_Polarity(3);
	System_Side_TX_Polarity(4);

	System_Side_RX_Polarity(1);
	System_Side_RX_Polarity(2);
	System_Side_RX_Polarity(3);
	System_Side_RX_Polarity(4);

	System_Side_Channel_Control(1);
	System_Side_Channel_Control(2);
	System_Side_Channel_Control(3);
	System_Side_Channel_Control(4);
}

void Line_Side_ALL_CH5_CH8_Control_P8C()
{
	Line_Side_Channel_Control(5);
	Line_Side_Channel_Control(6);
	Line_Side_Channel_Control(7);
	Line_Side_Channel_Control(8);

	Line_Side_TX_Polarity(5);
	Line_Side_TX_Polarity(6);
	Line_Side_TX_Polarity(7);
	Line_Side_TX_Polarity(8);

	Line_Side_RX_Polarity(5);
	Line_Side_RX_Polarity(6);
	Line_Side_RX_Polarity(7);
	Line_Side_RX_Polarity(8);

	Line_Side_Level_Shift(5);
	Line_Side_Level_Shift(6);
	Line_Side_Level_Shift(7);
	Line_Side_Level_Shift(8);
}

void System_Side_ALL_CH5_CH8_Control_P8E()
{
	System_Side_Channel_Control(5);
	System_Side_Channel_Control(6);
	System_Side_Channel_Control(7);
	System_Side_Channel_Control(8);

	System_Side_TX_Polarity(5);
	System_Side_TX_Polarity(6);
	System_Side_TX_Polarity(7);
	System_Side_TX_Polarity(8);

	System_Side_RX_Polarity(5);
	System_Side_RX_Polarity(6);
	System_Side_RX_Polarity(7);
	System_Side_RX_Polarity(8);
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 Power Switch CHIP MODE
//-----------------------------------------------------------------------------------------------------//
void Line_Side_ALL_CH_PowerON_Init(unsigned char CHIP_MODE)
{
	// CH0 - CH3
	Line_Side_Channel_Control(1);
	Line_Side_Channel_Control(2);
	Line_Side_Channel_Control(3);
	Line_Side_Channel_Control(4);

	Line_Side_TX_Polarity(1);
	Line_Side_TX_Polarity(2);
	Line_Side_TX_Polarity(3);
	Line_Side_TX_Polarity(4);

	Line_Side_RX_Polarity(1);
	Line_Side_RX_Polarity(2);
	Line_Side_RX_Polarity(3);
	Line_Side_RX_Polarity(4);

	if( CHIP_MODE == QDD400G_PAM4_MODE )
	{
		Line_Side_Level_Shift(1);
		Line_Side_Level_Shift(2);
		Line_Side_Level_Shift(3);
		Line_Side_Level_Shift(4);
	}
	// CH4 - CH7
	Line_Side_Channel_Control(5);
	Line_Side_Channel_Control(6);
	Line_Side_Channel_Control(7);
	Line_Side_Channel_Control(8);

	Line_Side_TX_Polarity(5);
	Line_Side_TX_Polarity(6);
	Line_Side_TX_Polarity(7);
	Line_Side_TX_Polarity(8);

	Line_Side_RX_Polarity(5);
	Line_Side_RX_Polarity(6);
	Line_Side_RX_Polarity(7);
	Line_Side_RX_Polarity(8);

	if( CHIP_MODE == QDD400G_PAM4_MODE )
	{
		Line_Side_Level_Shift(5);
		Line_Side_Level_Shift(6);
		Line_Side_Level_Shift(7);
		Line_Side_Level_Shift(8);
	}

}

void System_Side_ALL_CH_PowerON_Init(unsigned char CHIP_MODE)
{
	if( CHIP_MODE == QDD400G_NRZ_MODE )
	{
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH0 = 0xFFF4;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH1 = 0xFFF4;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH2 = 0xFFF4;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH3 = 0xFFF4;

		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH0 = 0xFFF4;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH1 = 0xFFF4;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH2 = 0xFFF4;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH3 = 0xFFF4;

		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH0 = 0x0070;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH1 = 0x0070;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH2 = 0x0070;
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH3 = 0x0070;

		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH0 = 0x0070;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH1 = 0x0070;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH2 = 0x0070;
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH3 = 0x0070;
	}
	System_Side_ALL_CH1_CH4_Control_P87();
	System_Side_ALL_CH5_CH8_Control_P8E();
}


void Trigger_CMD_Update_DSP_REG()
{
	Line_Side_ALL_CH1_CH4_Control_P86();
	System_Side_ALL_CH1_CH4_Control_P87();
	Line_Side_ALL_CH5_CH8_Control_P8C();
	System_Side_ALL_CH5_CH8_Control_P8E();
}

void DSP59281_SPI_EEPROM_DOWNLOAD_CHECK_Status()
{
	unsigned int  JJ_Count = 0 ;
	unsigned int  II_Count = 0 ;
	while(1)
	{
		// Step 2: Check FW Status
		if( BRCM_Control_READ_Data( 0x5200CDA0 , 0 ) == DSP_UC_READY )
		{
			Init_flag = 1 ;
			break;
		}
		else
		{
			JJ_Count++;
			for(II_Count=0;II_Count<100;II_Count++);
			if( JJ_Count >= 5000 )
			{
				JJ_Count = 0 ;
				Init_flag = 0 ;
				BRCM_11181_DSP_MEMORY_MAP.Debug_flag = 0 ;
				break;
			}
		}
	}
}
void DSP59281_CHECK_Mode_Config()
{
	unsigned int Read_Buffer;

	Read_Buffer = BRCM_Control_READ_Data( 0x5200C880 , 0 );
	Read_Buffer = BRCM_Control_READ_Data( 0x0002C418 , 0 );
	Read_Buffer = BRCM_Control_READ_Data( 0x0002C420 , 0 );
	Read_Buffer = BRCM_Control_READ_Data( 0x0002C428 , 0 );
	Read_Buffer = BRCM_Control_READ_Data( 0x0002C430 , 0 );
	Read_Buffer = BRCM_Control_READ_Data( 0x0002C438 , 0 );
	Read_Buffer = BRCM_Control_READ_Data( 0x0002C440 , 0 );
	Read_Buffer = BRCM_Control_READ_Data( 0x0002C448 , 0 );
	Read_Buffer = BRCM_Control_READ_Data( 0x0002C450 , 0 );
}

void DSP59281_Init(unsigned char CHIP_MODE)
{
	//----------------------------------------------------------------//
	// 2019/1015 Ivan modify power Ramping Sequence
	//Step1 : Check SPI EEPROM download status
	//Step2 : Enable AVS & Delay 2 S
	//Step3 : Power Down all port
	//Step4 : DSP Chip mode Setting
	//Step5 : All lane configuration
	//Step6 : Power Up port by port
	//----------------------------------------------------------------//

	// Step 1
	DSP59281_SPI_EEPROM_DOWNLOAD_CHECK_Status();

	BRCM_11181_DSP_MEMORY_MAP.BRCM_CHID  = BRCM_Control_READ_Data( 0x5200CB20 , 0 ) ;
	BRCM_11181_DSP_MEMORY_MAP.Ready_Byte = BRCM_Control_READ_Data( 0x50034404 , 0 ) ;

	if(Init_flag==1)
	{
		// Step2 : Enable AVS
		DSP59281_AVS_Control();
		// Step3 : Squelch Enable
		Line_Side_Squelch_Control(1);
		System_Side_Squelch_Control(1);
		//Delay_ms(10);
		// Step4 : Chip mode Setting
		SET_CHIP_MODE( CHIP_MODE );
	}

	if(Init_flag==2)
	{
		// Step5 : all function configuration
		//Line_Side_ALL_CH1_CH4_Control_P86();
		//Line_Side_ALL_CH5_CH8_Control_P8C();
		Line_Side_ALL_CH_PowerON_Init( CHIP_MODE );
		System_Side_ALL_CH_PowerON_Init( CHIP_MODE );
		// Step6 : Squelch disable
		Line_Side_Squelch_Control(0);
		System_Side_Squelch_Control(0);
	}

	BRCM_11181_DSP_MEMORY_MAP.Debug_flag = Init_flag  ;
}
/*
void DSP59281_Init(unsigned char CHIP_MODE)
{
	//----------------------------------------------------------------//
	// 2019/1015 Ivan modify power Ramping Sequence
	//Step1 : Check SPI EEPROM download status
	//Step2 : Enable AVS & Delay 2 S
	//Step3 : Enable Full chip LPM ( low power Enable )
	//Step4 : DSP Chip mode Setting
	//Step5 : All lane configuration
	//Step6 : Power Down all port
	//Step7 : Disable LPM
	//Step8 : Power Up port by port
	//----------------------------------------------------------------//

	// Step 1
	DSP59281_SPI_EEPROM_DOWNLOAD_CHECK_Status();

	BRCM_11181_DSP_MEMORY_MAP.BRCM_CHID  = BRCM_Control_READ_Data( 0x5200CB20 , 0 ) ;
	BRCM_11181_DSP_MEMORY_MAP.Ready_Byte = BRCM_Control_READ_Data( 0x50034404 , 0 ) ;

	if(Init_flag==1)
	{
		// Step3 : Enable AVS
		DSP59281_AVS_Control();
		Delay_ms(2000);

		// Step4 : Squelch TRX
		Line_Side_Squelch_Control(1);
		System_Side_Squelch_Control(1);
		Delay_ms(10);
		// Step5 : Chip mode Setting
		SET_CHIP_MODE( CHIP_MODE );
	}

	if(Init_flag==2)
	{
		// DSP59281_LowPower_Control(1);
		// Step6 : all function init
		Line_Side_ALL_CH1_CH4_Control_P86();
		Line_Side_ALL_CH5_CH8_Control_P8C();
		System_Side_ALL_CH1_CH4_Control_P87();
		System_Side_ALL_CH5_CH8_Control_P8E();
		// 2019-0829 Ivan New add Rx preformance modify high to medium
		//DSP59281_PREFORMANCE_Medium_SETTING();
		// 2019-0829 Ivan New add Rx Bandwidth modify 0 to 2
		//DSP59281_RX_BW_SETTING();
		//Delay_ms(10);
		// 2019-0903 new add Dynamic THR SETTING
		//Line_Side_Dynamic_Phase_Bias_THR_SETTING();
		// Step7 : Un_Squelch
		// 2019_0911 Ivan modify new Function
		if(CALIB_MEMORY_MAP.DSP59281_Nlnet==0x01)
			DSP59281_RX_NLdet_Enable_Control(1);

		if( CALIB_MEMORY_MAP.DSP59281_ExSlicer > 0 )
			DSP59281_RX_Exsilcer_Control( CALIB_MEMORY_MAP.DSP59281_ExSlicer );

		Line_Side_Squelch_Control(0);
		System_Side_Squelch_Control(0);
	}

	BRCM_11181_DSP_MEMORY_MAP.Debug_flag = Init_flag  ;
}

*/




