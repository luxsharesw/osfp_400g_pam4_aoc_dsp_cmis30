/*
 * BRCM_DSP58281_Optional.c
 *
 *  Created on: 2019�~8��28��
 *      Author: Ivan_Lin
 */

#include "BRCM_DSP_1181.h"
#include "BRCM_DSP_59281.h"
#include "Master_I2C_GPIO.h"
#include <string.h>

#define BRCM_59281_DATA_LENG 4

//-----------------------------------------------------------------------------------------------------//
// DSP 59281 Line Side Set FW Command
//-----------------------------------------------------------------------------------------------------//
void DSP_59281_Line_Side_Channel_READ_CHECK(unsigned char Channel)
{
	switch(Channel)
	{
		case 0:
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 0 );
				break;
		case 1:
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 0 );
				break;
		case 2:
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 0 );
				break;
		case 3:
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 0 );
				break;
		case 4:
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 1 );
				break;
		case 5:
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 1 );
				break;
		case 6:
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 1 );
				break;
		case 7:
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 1 );
				break;
		default:
				break;
	}
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 Line Side Set FW Command
//-----------------------------------------------------------------------------------------------------//
void DSP_59281_Line_Side_SET_FW_COMMAND(unsigned char Channel,unsigned int Write_command_0,unsigned int Write_command_1)
{
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );

	if(Channel==0)
		BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0001 , BRCM_59281_DATA_LENG , 0 );
	else if(Channel==1)
		BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0002 , BRCM_59281_DATA_LENG , 0 );
	else if(Channel==2)
		BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	else if(Channel==3)
		BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0008 , BRCM_59281_DATA_LENG , 0 );
	else if(Channel==4)
		BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0010 , BRCM_59281_DATA_LENG , 0 );
	else if(Channel==5)
		BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0020 , BRCM_59281_DATA_LENG , 0 );
	else if(Channel==6)
		BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0040 , BRCM_59281_DATA_LENG , 0 );
	else if(Channel==7)
		BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0080 , BRCM_59281_DATA_LENG , 0 );

	BRCM_Control_WRITE_Data( 0x5200CC10 , Write_command_0 , BRCM_59281_DATA_LENG , 0 );

	DSP_59281_Line_Side_Channel_READ_CHECK(Channel);
	BRCM_Control_WRITE_Data( 0x5200CC10 , Write_command_1 , BRCM_59281_DATA_LENG , 0 );
	DSP_59281_Line_Side_Channel_READ_CHECK(Channel);
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 System Side Set FW Command
//-----------------------------------------------------------------------------------------------------//
void DSP_59281_System_Side_Channel_READ_CHECK(unsigned char Channel)
{
	switch(Channel)
	{
		case 0:
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5003C044 , 0 );
				break;
		case 1:
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5003C048 , 0 );
				break;
		case 2:
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5003C04C , 0 );
				break;
		case 3:
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5003C050 , 0 );
				break;
		case 4:
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5003C044 , 1 );
				break;
		case 5:
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5003C048 , 1 );
				break;
		case 6:
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5003C04C , 1 );
				break;
		case 7:
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5003C050 , 1 );
				break;
		default:
				break;
	}
}
void DSP_59281_System_Side_SET_FW_COMMAND(unsigned char Channel,unsigned int Write_command_0,unsigned int Write_command_1)
{
	if(Channel==0)
		BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0001 , BRCM_59281_DATA_LENG , 0 );
	else if(Channel==1)
		BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0002 , BRCM_59281_DATA_LENG , 0 );
	else if(Channel==2)
		BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	else if(Channel==3)
		BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0008 , BRCM_59281_DATA_LENG , 0 );
	else if(Channel==4)
		BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0010 , BRCM_59281_DATA_LENG , 0 );
	else if(Channel==5)
		BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0020 , BRCM_59281_DATA_LENG , 0 );
	else if(Channel==6)
		BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0040 , BRCM_59281_DATA_LENG , 0 );
	else if(Channel==7)
		BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0080 , BRCM_59281_DATA_LENG , 0 );

	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , Write_command_0 , BRCM_59281_DATA_LENG , 0 );

	DSP_59281_System_Side_Channel_READ_CHECK(Channel);
	BRCM_Control_WRITE_Data( 0x5200CC10 , Write_command_1 , BRCM_59281_DATA_LENG , 0 );
	DSP_59281_System_Side_Channel_READ_CHECK(Channel);

}

//-----------------------------------------------------------------------------------------------------//
// DSP 59281 SSPRQ Control
//-----------------------------------------------------------------------------------------------------//
void DSP59281_SSPRQ_Control_Enable(unsigned char Lan_Channel)
{
	DSP_59281_Line_Side_SET_FW_COMMAND(Lan_Channel,0x8021,0x0021);
	switch(Lan_Channel)
	{
		case 0:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 0 );*/
				//DSP_59281_Line_Side_SET_FW_COMMAND(Lan_Channel);
				//--------------------------------------------------//
				// Start out by putting this into reset state = all 0s
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000050 , 0 );
				//--------------------------------------------------//
				// Un-Reset CW TX SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000000 , 0 );
				BRCM_Control_WRITE_Data( 0x52000000 , 0x381D , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Take CP94 PGEN Out Of TX Datapath
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52000040 , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x52000040 , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put SSPRQ Gen Into TX Datapath
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000050 , 0 );
				BRCM_Control_WRITE_Data( 0x52000050 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable/Disable Gray Coding Inside SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000050 , 0 );
				BRCM_Control_WRITE_Data( 0x52000050 , 0x8008 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable/Disable 2-bit Swap Inside SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000050 , 0 );
				BRCM_Control_WRITE_Data( 0x52000050 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable SSPRQ Generator Clock
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000050 , 0 );
				BRCM_Control_WRITE_Data( 0x52000050 , 0x8019 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Un S/W Reset SSPRQ Generator
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000050 , 0 );
				BRCM_Control_WRITE_Data( 0x52000050 , 0x801B , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Enable SSPRQ Generator
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000050 , 0 );
				BRCM_Control_WRITE_Data( 0x52000050 , 0x801F , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52000004 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x52000004 , 0 );
				BRCM_Control_WRITE_Data( 0x52000004 , 0x8008 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Override Scrambler Din
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000004 , 0 );
				BRCM_Control_WRITE_Data( 0x52000004 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Put Scrambler Into Bypass Mode
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000004 , 0 );
				BRCM_Control_WRITE_Data( 0x52000004 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
				break;
		case 1:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0002 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 0 );*/
				//DSP_59281_Line_Side_SET_FW_COMMAND(Lan_Channel);
				//--------------------------------------------------//
				// Start out by putting this into reset state = all 0s
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52004050 , 0 );
				//--------------------------------------------------//
				// Un-Reset CW TX SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000400 , 0 );
				BRCM_Control_WRITE_Data( 0x52000400 , 0x381D , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Take CP94 PGEN Out Of TX Datapath
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52000440 , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x52000440 , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put SSPRQ Gen Into TX Datapath
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000450 , 0 );
				BRCM_Control_WRITE_Data( 0x5200450 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable/Disable Gray Coding Inside SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000450 , 0 );
				BRCM_Control_WRITE_Data( 0x52000450 , 0x8008 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable/Disable 2-bit Swap Inside SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000450 , 0 );
				BRCM_Control_WRITE_Data( 0x52000450 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable SSPRQ Generator Clock
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000450 , 0 );
				BRCM_Control_WRITE_Data( 0x52000450 , 0x8019 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Un S/W Reset SSPRQ Generator
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000450 , 0 );
				BRCM_Control_WRITE_Data( 0x52000450 , 0x801B , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Enable SSPRQ Generator
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000450 , 0 );
				BRCM_Control_WRITE_Data( 0x52000450 , 0x801F , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52000404 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x52000404 , 0 );
				BRCM_Control_WRITE_Data( 0x52000404 , 0x8008 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Override Scrambler Din
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000404 , 0 );
				BRCM_Control_WRITE_Data( 0x52000404 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Put Scrambler Into Bypass Mode
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000404 , 0 );
				BRCM_Control_WRITE_Data( 0x52000404 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
				   break;
		case 2:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 0 );*/
				//DSP_59281_Line_Side_SET_FW_COMMAND(2);
				//--------------------------------------------------//
				// Start out by putting this into reset state = all 0s
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52008050 , 0 );
				//--------------------------------------------------//
				// Un-Reset CW TX SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000800 , 0 );
				BRCM_Control_WRITE_Data( 0x52000800 , 0x381D , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Take CP94 PGEN Out Of TX Datapath
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52000840 , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x52000840 , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put SSPRQ Gen Into TX Datapath
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000850 , 0 );
				BRCM_Control_WRITE_Data( 0x5200850 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable/Disable Gray Coding Inside SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000850 , 0 );
				BRCM_Control_WRITE_Data( 0x52000850 , 0x8008 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable/Disable 2-bit Swap Inside SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000850 , 0 );
				BRCM_Control_WRITE_Data( 0x52000850 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable SSPRQ Generator Clock
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000850 , 0 );
				BRCM_Control_WRITE_Data( 0x52000850 , 0x8019 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Un S/W Reset SSPRQ Generator
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000850 , 0 );
				BRCM_Control_WRITE_Data( 0x52000850 , 0x801B , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Enable SSPRQ Generator
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000850 , 0 );
				BRCM_Control_WRITE_Data( 0x52000850 , 0x801F , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52000804 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x52000804 , 0 );
				BRCM_Control_WRITE_Data( 0x52000804 , 0x8008 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Override Scrambler Din
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000804 , 0 );
				BRCM_Control_WRITE_Data( 0x52000804 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Put Scrambler Into Bypass Mode
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000804 , 0 );
				BRCM_Control_WRITE_Data( 0x52000804 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
			   break;
		case 3:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0008 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 0 );*/
				//--------------------------------------------------//
				// Start out by putting this into reset state = all 0s
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x5200C050 , 0 );
				//--------------------------------------------------//
				// Un-Reset CW TX SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000C00 , 0 );
				BRCM_Control_WRITE_Data( 0x52000C00 , 0x381D , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Take CP94 PGEN Out Of TX Datapath
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52000C40 , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x52000C40 , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put SSPRQ Gen Into TX Datapath
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000C50 , 0 );
				BRCM_Control_WRITE_Data( 0x5200C50 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable/Disable Gray Coding Inside SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000C50 , 0 );
				BRCM_Control_WRITE_Data( 0x52000C50 , 0x8008 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable/Disable 2-bit Swap Inside SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000C50 , 0 );
				BRCM_Control_WRITE_Data( 0x52000C50 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable SSPRQ Generator Clock
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000C50 , 0 );
				BRCM_Control_WRITE_Data( 0x52000C50 , 0x8019 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Un S/W Reset SSPRQ Generator
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000C50 , 0 );
				BRCM_Control_WRITE_Data( 0x52000C50 , 0x801B , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Enable SSPRQ Generator
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000C50 , 0 );
				BRCM_Control_WRITE_Data( 0x52000C50 , 0x801F , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52000C04 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x52000C04 , 0 );
				BRCM_Control_WRITE_Data( 0x52000C04 , 0x8008 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Override Scrambler Din
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000C04 , 0 );
				BRCM_Control_WRITE_Data( 0x52000C04 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Put Scrambler Into Bypass Mode
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000C04 , 0 );
				BRCM_Control_WRITE_Data( 0x52000C04 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
			   break;
		case 4:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0010 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 1 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 1 );*/
				//--------------------------------------------------//
				// Start out by putting this into reset state = all 0s
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010050 , 0 );
				//--------------------------------------------------//
				// Un-Reset CW TX SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010000 , 0 );
				BRCM_Control_WRITE_Data( 0x52010000 , 0x381D , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Take CP94 PGEN Out Of TX Datapath
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52010040 , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x52010040 , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put SSPRQ Gen Into TX Datapath
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010050 , 0 );
				BRCM_Control_WRITE_Data( 0x52010050 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable/Disable Gray Coding Inside SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010050 , 0 );
				BRCM_Control_WRITE_Data( 0x52010050 , 0x8008 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable/Disable 2-bit Swap Inside SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010050 , 0 );
				BRCM_Control_WRITE_Data( 0x52010050 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable SSPRQ Generator Clock
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010050 , 0 );
				BRCM_Control_WRITE_Data( 0x52010050 , 0x8019 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Un S/W Reset SSPRQ Generator
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010050 , 0 );
				BRCM_Control_WRITE_Data( 0x52010050 , 0x801B , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Enable SSPRQ Generator
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010050 , 0 );
				BRCM_Control_WRITE_Data( 0x52010050 , 0x801F , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52010004 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x52010004 , 0 );
				BRCM_Control_WRITE_Data( 0x52010004 , 0x8008 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Override Scrambler Din
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010004 , 0 );
				BRCM_Control_WRITE_Data( 0x52010004 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Put Scrambler Into Bypass Mode
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010004 , 0 );
				BRCM_Control_WRITE_Data( 0x52010004 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
			   break;
		case 5:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0020 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 1 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 1 );*/
				//--------------------------------------------------//
				// Start out by putting this into reset state = all 0s
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010450 , 0 );
				//--------------------------------------------------//
				// Un-Reset CW TX SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010400 , 0 );
				BRCM_Control_WRITE_Data( 0x52010400 , 0x381D , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Take CP94 PGEN Out Of TX Datapath
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52010440 , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x52010440 , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put SSPRQ Gen Into TX Datapath
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010450 , 0 );
				BRCM_Control_WRITE_Data( 0x52010450 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable/Disable Gray Coding Inside SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010450 , 0 );
				BRCM_Control_WRITE_Data( 0x52010450 , 0x8008 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable/Disable 2-bit Swap Inside SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010450 , 0 );
				BRCM_Control_WRITE_Data( 0x52010450 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable SSPRQ Generator Clock
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010450 , 0 );
				BRCM_Control_WRITE_Data( 0x52010450 , 0x8019 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Un S/W Reset SSPRQ Generator
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010450 , 0 );
				BRCM_Control_WRITE_Data( 0x52010450 , 0x801B , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Enable SSPRQ Generator
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010450 , 0 );
				BRCM_Control_WRITE_Data( 0x52010450 , 0x801F , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52010404 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x52010404 , 0 );
				BRCM_Control_WRITE_Data( 0x52010404 , 0x8008 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Override Scrambler Din
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010404 , 0 );
				BRCM_Control_WRITE_Data( 0x52010404 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Put Scrambler Into Bypass Mode
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010404 , 0 );
				BRCM_Control_WRITE_Data( 0x52010404 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
			   break;
		case 6:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0040 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 1 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 1 );*/
				//--------------------------------------------------//
				// Start out by putting this into reset state = all 0s
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010850 , 0 );
				//--------------------------------------------------//
				// Un-Reset CW TX SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010800 , 0 );
				BRCM_Control_WRITE_Data( 0x52010800 , 0x381D , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Take CP94 PGEN Out Of TX Datapath
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52010840 , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x52010840 , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put SSPRQ Gen Into TX Datapath
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010850 , 0 );
				BRCM_Control_WRITE_Data( 0x52010850 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable/Disable Gray Coding Inside SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010850 , 0 );
				BRCM_Control_WRITE_Data( 0x52010850 , 0x8008 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable/Disable 2-bit Swap Inside SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010850 , 0 );
				BRCM_Control_WRITE_Data( 0x52010850 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable SSPRQ Generator Clock
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010850 , 0 );
				BRCM_Control_WRITE_Data( 0x52010850 , 0x8019 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Un S/W Reset SSPRQ Generator
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010850 , 0 );
				BRCM_Control_WRITE_Data( 0x52010850 , 0x801B , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Enable SSPRQ Generator
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010850 , 0 );
				BRCM_Control_WRITE_Data( 0x52010850 , 0x801F , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52010804 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x52010804 , 0 );
				BRCM_Control_WRITE_Data( 0x52010804 , 0x8008 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Override Scrambler Din
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010804 , 0 );
				BRCM_Control_WRITE_Data( 0x52010804 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Put Scrambler Into Bypass Mode
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010804 , 0 );
				BRCM_Control_WRITE_Data( 0x52010804 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
			   break;
		case 7:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0080 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 1 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 1 );*/
				//--------------------------------------------------//
				// Start out by putting this into reset state = all 0s
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010C50 , 0 );
				//--------------------------------------------------//
				// Un-Reset CW TX SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010C00 , 0 );
				BRCM_Control_WRITE_Data( 0x52010C00 , 0x381D , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Take CP94 PGEN Out Of TX Datapath
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52010C40 , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x52010C40 , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put SSPRQ Gen Into TX Datapath
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010C50 , 0 );
				BRCM_Control_WRITE_Data( 0x52010C50 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable/Disable Gray Coding Inside SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010C50 , 0 );
				BRCM_Control_WRITE_Data( 0x52010C50 , 0x8008 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable/Disable 2-bit Swap Inside SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010C50 , 0 );
				BRCM_Control_WRITE_Data( 0x52010C50 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Enable SSPRQ Generator Clock
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010C50 , 0 );
				BRCM_Control_WRITE_Data( 0x52010C50 , 0x8019 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Un S/W Reset SSPRQ Generator
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010C50 , 0 );
				BRCM_Control_WRITE_Data( 0x52010C50 , 0x801B , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Enable SSPRQ Generator
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010C50 , 0 );
				BRCM_Control_WRITE_Data( 0x52010C50 , 0x801F , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  SSPRQ Gen
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52010C04 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x52010C04 , 0 );
				BRCM_Control_WRITE_Data( 0x52010C04 , 0x8008 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Override Scrambler Din
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010C04 , 0 );
				BRCM_Control_WRITE_Data( 0x52010C04 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				//  Put Scrambler Into Bypass Mode
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010C04 , 0 );
				BRCM_Control_WRITE_Data( 0x52010C04 , 0x8018 , BRCM_59281_DATA_LENG , 0 );
			   break;
	}
}

void DSP59281_SSPRQ_Control_Disable(unsigned char Lan_Channel)
{
	DSP_59281_Line_Side_SET_FW_COMMAND(Lan_Channel,0x8024,0x0024);
	switch(Lan_Channel)
	{
		case 0:
			    //--------------------------------------------------//
			    // Set FW Command
			    //--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 0 );*/
			    //--------------------------------------------------//
			    // Don't Override Scrambler Din
			    //--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000004 , 0 );
				BRCM_Control_WRITE_Data( 0x52000004 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
			    //--------------------------------------------------//
			    // Put CW TX SSPRQ Gen in reset
			    //--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000000 , 0 );
				BRCM_Control_WRITE_Data( 0x52000000 , 0x181D , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put this into reset state = all 0s
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52000050 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				break;
		case 1:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0002 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 0 );*/
				//--------------------------------------------------//
				// Don't Override Scrambler Din
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000404 , 0 );
				BRCM_Control_WRITE_Data( 0x52000404 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put CW TX SSPRQ Gen in reset
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000400 , 0 );
				BRCM_Control_WRITE_Data( 0x52000400 , 0x181D , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put this into reset state = all 0s
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52000450 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
			   break;
		case 2:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 0 );*/
				//--------------------------------------------------//
				// Don't Override Scrambler Din
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000804 , 0 );
				BRCM_Control_WRITE_Data( 0x52000804 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put CW TX SSPRQ Gen in reset
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000800 , 0 );
				BRCM_Control_WRITE_Data( 0x52000800 , 0x181D , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put this into reset state = all 0s
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52000850 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
			   break;
		case 3:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0008 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 0 );*/
				//--------------------------------------------------//
				// Don't Override Scrambler Din
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000C04 , 0 );
				BRCM_Control_WRITE_Data( 0x52000C04 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put CW TX SSPRQ Gen in reset
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52000C00 , 0 );
				BRCM_Control_WRITE_Data( 0x52000C00 , 0x181D , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put this into reset state = all 0s
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52000C50 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
			   break;
		case 4:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0010 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 1 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 1 );*/
				//--------------------------------------------------//
				// Don't Override Scrambler Din
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010004 , 0 );
				BRCM_Control_WRITE_Data( 0x52010004 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put CW TX SSPRQ Gen in reset
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010000 , 0 );
				BRCM_Control_WRITE_Data( 0x52010000 , 0x181D , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put this into reset state = all 0s
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52010050 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
			   break;
		case 5:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0020 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 1 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 1 );*/
				//--------------------------------------------------//
				// Don't Override Scrambler Din
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010404 , 0 );
				BRCM_Control_WRITE_Data( 0x52010404 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put CW TX SSPRQ Gen in reset
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010400 , 0 );
				BRCM_Control_WRITE_Data( 0x52010400 , 0x181D , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put this into reset state = all 0s
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52000450 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
			   break;
		case 6:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0020 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 1 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 1 );*/
				//--------------------------------------------------//
				// Don't Override Scrambler Din
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010804 , 0 );
				BRCM_Control_WRITE_Data( 0x52010804 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put CW TX SSPRQ Gen in reset
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010800 , 0 );
				BRCM_Control_WRITE_Data( 0x52010800 , 0x181D , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put this into reset state = all 0s
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52010850 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
			   break;
		case 7:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0020 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 1 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 1 );*/
				//--------------------------------------------------//
				// Don't Override Scrambler Din
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010C04 , 0 );
				BRCM_Control_WRITE_Data( 0x52010C04 , 0x8000 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put CW TX SSPRQ Gen in reset
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x52010C00 , 0 );
				BRCM_Control_WRITE_Data( 0x52100C00 , 0x181D , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Put this into reset state = all 0s
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x52010C50 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
			   break;
		default:
			   break;
	}
}

//-----------------------------------------------------------------------------------------------------//
// DSP 59281 SYSTEM SIDE REMOTE LOOPBACK Control
//-----------------------------------------------------------------------------------------------------//
void DSP59281_System_side_Remote_Loopback_Control(unsigned char Lan_Channel,unsigned char Enable)
{
	unsigned int LoopBack_Temp;
	switch(Lan_Channel)
	{
		case 0:
				//--------------------------------------------------//
				// Read for replace bit5 or skip
				//--------------------------------------------------//
				LoopBack_Temp = BRCM_Control_READ_Data( 0x50034A0C , 0 );
				//--------------------------------------------------//
				// Enable or Disable on bit5
				// 00001000 : Disable
				// 00001020 : Enable
				//--------------------------------------------------//
				if(Enable)
				{
					LoopBack_Temp = ( LoopBack_Temp | 0x0020 );
					BRCM_Control_WRITE_Data( 0x50034A0C , LoopBack_Temp , BRCM_59281_DATA_LENG , 0 );
				}
				else
				{
					LoopBack_Temp = ( LoopBack_Temp & 0xFFDF );
					BRCM_Control_WRITE_Data( 0x50034A0C , LoopBack_Temp , BRCM_59281_DATA_LENG , 0 );
				}
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/**BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8003 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5003C044 , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0003 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5003C044 , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );*/

				break;
		case 1:
				//--------------------------------------------------//
				// Read for replace bit5 or skip
				//--------------------------------------------------//
				LoopBack_Temp = BRCM_Control_READ_Data( 0x50134A0C , 0 );
				//--------------------------------------------------//
				// Enable or Disable on bit5
				// 00001000 : Disable
				// 00001020 : Enable
				//--------------------------------------------------//
				if(Enable)
				{
					LoopBack_Temp = ( LoopBack_Temp | 0x0020 );
					BRCM_Control_WRITE_Data( 0x50134A0C , LoopBack_Temp , BRCM_59281_DATA_LENG , 0 );
				}
				else
				{
					LoopBack_Temp = ( LoopBack_Temp & 0xFFDF );
					BRCM_Control_WRITE_Data( 0x50134A0C , LoopBack_Temp , BRCM_59281_DATA_LENG , 0 );
				}
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0002 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8003 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5003C048 , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0003 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5003C048 , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );*/
				break;
		case 2:
				//--------------------------------------------------//
				// Read for replace bit5 or skip
				//--------------------------------------------------//
				LoopBack_Temp = BRCM_Control_READ_Data( 0x50234A0C , 0 );
				//--------------------------------------------------//
				// Enable or Disable on bit5
				// 00001000 : Disable
				// 00001020 : Enable
				//--------------------------------------------------//
				if(Enable)
				{
					LoopBack_Temp = ( LoopBack_Temp | 0x0020 );
					BRCM_Control_WRITE_Data( 0x50234A0C , LoopBack_Temp , BRCM_59281_DATA_LENG , 0 );
				}
				else
				{
					LoopBack_Temp = ( LoopBack_Temp & 0xFFDF );
					BRCM_Control_WRITE_Data( 0x50234A0C , LoopBack_Temp , BRCM_59281_DATA_LENG , 0 );
				}
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8003 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5003C04C , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0003 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5003C04C , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );*/
				break;
		case 3:
				//--------------------------------------------------//
				// Read for replace bit5 or skip
				//--------------------------------------------------//
				LoopBack_Temp = BRCM_Control_READ_Data( 0x50334A0C , 0 );
				//--------------------------------------------------//
				// Enable or Disable on bit5
				// 00001000 : Disable
				// 00001020 : Enable
				//--------------------------------------------------//
				if(Enable)
				{
					LoopBack_Temp = ( LoopBack_Temp | 0x0020 );
					BRCM_Control_WRITE_Data( 0x50334A0C , LoopBack_Temp , BRCM_59281_DATA_LENG , 0 );
				}
				else
				{
					LoopBack_Temp = ( LoopBack_Temp & 0xFFDF );
					BRCM_Control_WRITE_Data( 0x50334A0C , LoopBack_Temp , BRCM_59281_DATA_LENG , 0 );
				}
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0008 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8003 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5003C050 , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0003 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5003C050 , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );*/
				break;
		case 4:
				//--------------------------------------------------//
				// Read for replace bit5 or skip
				//--------------------------------------------------//
				LoopBack_Temp = BRCM_Control_READ_Data( 0x50034A0C , 1 );
				//--------------------------------------------------//
				// Enable or Disable on bit5
				// 00001000 : Disable
				// 00001020 : Enable
				//--------------------------------------------------//
				if(Enable)
				{
					LoopBack_Temp = ( LoopBack_Temp | 0x0020 );
					BRCM_Control_WRITE_Data( 0x50034A0C , LoopBack_Temp , BRCM_59281_DATA_LENG , 1 );
				}
				else
				{
					LoopBack_Temp = ( LoopBack_Temp & 0xFFDF );
					BRCM_Control_WRITE_Data( 0x50034A0C , LoopBack_Temp , BRCM_59281_DATA_LENG , 1 );
				}
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0010 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8003 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5003C044 , 1 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0003 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5003C044 , 1 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );*/
				break;
		case 5:
				//--------------------------------------------------//
				// Read for replace bit5 or skip
				//--------------------------------------------------//
				LoopBack_Temp = BRCM_Control_READ_Data( 0x50134A0C , 1 );
				//--------------------------------------------------//
				// Enable or Disable on bit5
				// 00001000 : Disable
				// 00001020 : Enable
				//--------------------------------------------------//
				if(Enable)
				{
					LoopBack_Temp = ( LoopBack_Temp | 0x0020 );
					BRCM_Control_WRITE_Data( 0x50134A0C , LoopBack_Temp , BRCM_59281_DATA_LENG , 1 );
				}
				else
				{
					LoopBack_Temp = ( LoopBack_Temp & 0xFFDF );
					BRCM_Control_WRITE_Data( 0x50134A0C , LoopBack_Temp , BRCM_59281_DATA_LENG , 1 );
				}
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0020 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8003 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5003C048 , 1 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0003 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5003C048 , 1 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );*/
				break;
		case 6:
				//--------------------------------------------------//
				// Read for replace bit5 or skip
				//--------------------------------------------------//
				LoopBack_Temp = BRCM_Control_READ_Data( 0x50234A0C , 1 );
				//--------------------------------------------------//
				// Enable or Disable on bit5
				// 00001000 : Disable
				// 00001020 : Enable
				//--------------------------------------------------//
				if(Enable)
				{
					LoopBack_Temp = ( LoopBack_Temp | 0x0020 );
					BRCM_Control_WRITE_Data( 0x50234A0C , LoopBack_Temp , BRCM_59281_DATA_LENG , 1 );
				}
				else
				{
					LoopBack_Temp = ( LoopBack_Temp & 0xFFDF );
					BRCM_Control_WRITE_Data( 0x50234A0C , LoopBack_Temp , BRCM_59281_DATA_LENG , 1 );
				}
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0040 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8003 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5003C04C , 1 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0003 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5003C04C , 1 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );*/
				break;
		case 7:
				//--------------------------------------------------//
				// Read for replace bit5 or skip
				//--------------------------------------------------//
				LoopBack_Temp = BRCM_Control_READ_Data( 0x50334A0C , 1 );
				//--------------------------------------------------//
				// Enable or Disable on bit5
				// 00001000 : Disable
				// 00001020 : Enable
				//--------------------------------------------------//
				if(Enable)
				{
					LoopBack_Temp = ( LoopBack_Temp | 0x0020 );
					BRCM_Control_WRITE_Data( 0x50334A0C , LoopBack_Temp , BRCM_59281_DATA_LENG , 1 );
				}
				else
				{
					LoopBack_Temp = ( LoopBack_Temp & 0xFFDF );
					BRCM_Control_WRITE_Data( 0x50334A0C , LoopBack_Temp , BRCM_59281_DATA_LENG , 1 );
				}
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0080 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8003 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5003C050 , 1 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0003 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5003C050 , 1 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );*/
				break;
		default:
				break;
	}
	DSP_59281_System_Side_SET_FW_COMMAND( Lan_Channel , 0x8003 , 0x0003 );
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 Line SIDE LOOPBACK Control
//-----------------------------------------------------------------------------------------------------//
void DSP59281_Line_Side_G_Loopback_Control(unsigned char Lan_Channel,unsigned char Enable)
{
	unsigned int LoopBack_Temp;
	switch(Lan_Channel)
	{
		case 0:
				//--------------------------------------------------//
				// Read for replace bit4 or skip
				//--------------------------------------------------//
				LoopBack_Temp = BRCM_Control_READ_Data( 0x58002194 , 0 );
				//--------------------------------------------------//
				// Enable or Disable on bit4
				// 00004100 : Disable
				// 00004110 : Enable
				//--------------------------------------------------//
				if(Enable)
				{
					LoopBack_Temp = ( LoopBack_Temp | 0x0010 );
					BRCM_Control_WRITE_Data( 0x58002194 , LoopBack_Temp , BRCM_59281_DATA_LENG , 0 );
				}
				else
				{
					LoopBack_Temp = ( LoopBack_Temp & 0xFFEF );
					BRCM_Control_WRITE_Data( 0x58002194 , LoopBack_Temp , BRCM_59281_DATA_LENG , 0 );
				}
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );*/
				break;
		case 1:
				//--------------------------------------------------//
				// Read for replace bit4 or skip
				//--------------------------------------------------//
				LoopBack_Temp = BRCM_Control_READ_Data( 0x58006194 , 0 );
				//--------------------------------------------------//
				// Enable or Disable on bit4
				// 00004100 : Disable
				// 00004110 : Enable
				//--------------------------------------------------//
				if(Enable)
				{
					LoopBack_Temp = ( LoopBack_Temp | 0x0010 );
					BRCM_Control_WRITE_Data( 0x58006194 , LoopBack_Temp , BRCM_59281_DATA_LENG , 0 );
				}
				else
				{
					LoopBack_Temp = ( LoopBack_Temp & 0xFFEF );
					BRCM_Control_WRITE_Data( 0x58006194 , LoopBack_Temp , BRCM_59281_DATA_LENG , 0 );
				}
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0002 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );*/
				break;
		case 2:
				//--------------------------------------------------//
				// Read for replace bit4 or skip
				//--------------------------------------------------//
				LoopBack_Temp = BRCM_Control_READ_Data( 0x5800A194 , 0 );
				//--------------------------------------------------//
				// Enable or Disable on bit4
				// 00004100 : Disable
				// 00004110 : Enable
				//--------------------------------------------------//
				if(Enable)
				{
					LoopBack_Temp = ( LoopBack_Temp | 0x0010 );
					BRCM_Control_WRITE_Data( 0x5800A194 , LoopBack_Temp , BRCM_59281_DATA_LENG , 0 );
				}
				else
				{
					LoopBack_Temp = ( LoopBack_Temp & 0xFFEF );
					BRCM_Control_WRITE_Data( 0x5800A194 , LoopBack_Temp , BRCM_59281_DATA_LENG , 0 );
				}
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );*/
				break;
		case 3:
				//--------------------------------------------------//
				// Read for replace bit4 or skip
				//--------------------------------------------------//
				LoopBack_Temp = BRCM_Control_READ_Data( 0x5800E194 , 0 );
				//--------------------------------------------------//
				// Enable or Disable on bit4
				// 00004100 : Disable
				// 00004110 : Enable
				//--------------------------------------------------//
				if(Enable)
				{
					LoopBack_Temp = ( LoopBack_Temp | 0x0010 );
					BRCM_Control_WRITE_Data( 0x5800E194 , LoopBack_Temp , BRCM_59281_DATA_LENG , 0 );
				}
				else
				{
					LoopBack_Temp = ( LoopBack_Temp & 0xFFEF );
					BRCM_Control_WRITE_Data( 0x5800E194 , LoopBack_Temp , BRCM_59281_DATA_LENG , 0 );
				}
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0008 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );*/
				break;
		case 4:
				//--------------------------------------------------//
				// Read for replace bit4 or skip
				//--------------------------------------------------//
				LoopBack_Temp = BRCM_Control_READ_Data( 0x58002194 , 1 );
				//--------------------------------------------------//
				// Enable or Disable on bit4
				// 00004100 : Disable
				// 00004110 : Enable
				//--------------------------------------------------//
				if(Enable)
				{
					LoopBack_Temp = ( LoopBack_Temp | 0x0010 );
					BRCM_Control_WRITE_Data( 0x58002194 , LoopBack_Temp , BRCM_59281_DATA_LENG , 1 );
				}
				else
				{
					LoopBack_Temp = ( LoopBack_Temp & 0xFFEF );
					BRCM_Control_WRITE_Data( 0x58002194 , LoopBack_Temp , BRCM_59281_DATA_LENG , 1 );
				}
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0010 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 1 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 1 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );*/
				break;
		case 5:
				//--------------------------------------------------//
				// Read for replace bit4 or skip
				//--------------------------------------------------//
				LoopBack_Temp = BRCM_Control_READ_Data( 0x58006194 , 1 );
				//--------------------------------------------------//
				// Enable or Disable on bit4
				// 00004100 : Disable
				// 00004110 : Enable
				//--------------------------------------------------//
				if(Enable)
				{
					LoopBack_Temp = ( LoopBack_Temp | 0x0010 );
					BRCM_Control_WRITE_Data( 0x58006194 , LoopBack_Temp , BRCM_59281_DATA_LENG , 1 );
				}
				else
				{
					LoopBack_Temp = ( LoopBack_Temp & 0xFFEF );
					BRCM_Control_WRITE_Data( 0x58006194 , LoopBack_Temp , BRCM_59281_DATA_LENG , 1 );
				}
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0020 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 1 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 1 );*/
				break;
		case 6:
				//--------------------------------------------------//
				// Read for replace bit4 or skip
				//--------------------------------------------------//
				LoopBack_Temp = BRCM_Control_READ_Data( 0x5800A194 , 1 );
				//--------------------------------------------------//
				// Enable or Disable on bit4
				// 00004100 : Disable
				// 00004110 : Enable
				//--------------------------------------------------//
				if(Enable)
				{
					LoopBack_Temp = ( LoopBack_Temp | 0x0010 );
					BRCM_Control_WRITE_Data( 0x5800A194 , LoopBack_Temp , BRCM_59281_DATA_LENG , 1 );
				}
				else
				{
					LoopBack_Temp = ( LoopBack_Temp & 0xFFEF );
					BRCM_Control_WRITE_Data( 0x5800A194 , LoopBack_Temp , BRCM_59281_DATA_LENG , 1 );
				}
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0040 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 1 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 1 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );*/
				break;
		case 7:
				//--------------------------------------------------//
				// Read for replace bit4 or skip
				//--------------------------------------------------//
				LoopBack_Temp = BRCM_Control_READ_Data( 0x5800E194 , 1 );
				//--------------------------------------------------//
				// Enable or Disable on bit4
				// 00004100 : Disable
				// 00004110 : Enable
				//--------------------------------------------------//
				if(Enable)
				{
					LoopBack_Temp = ( LoopBack_Temp | 0x0010 );
					BRCM_Control_WRITE_Data( 0x5800E194 , LoopBack_Temp , BRCM_59281_DATA_LENG , 1 );
				}
				else
				{
					LoopBack_Temp = ( LoopBack_Temp & 0xFFEF );
					BRCM_Control_WRITE_Data( 0x5800E194 , LoopBack_Temp , BRCM_59281_DATA_LENG , 1 );
				}
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0080 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 1 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 1 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );*/
				break;
		default:
			    break;

	}
	DSP_59281_Line_Side_SET_FW_COMMAND(Lan_Channel,0x8004,0x0004);
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 PRBSGEN Control
//-----------------------------------------------------------------------------------------------------//
void DSP59281_PRBS13Q_Control_Enable(unsigned char Lan_Channel)
{
	DSP_59281_Line_Side_SET_FW_COMMAND(Lan_Channel,0x8021,0x0021);
	switch(Lan_Channel)
	{
		case 0:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 0 );*/
				//--------------------------------------------------//
				// Write PRBS Type
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x580035C4 , 0x1004 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Read Check
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x580035C4 , 0 );
				//--------------------------------------------------//
				// Enable PRBS
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x580035C4 , 0x1005 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Read Check
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x580035C4 , 0 );
				break;
		case 1:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0002 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 0 );*/
				//--------------------------------------------------//
				// Write PRBS Type
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x580075C4 , 0x1004 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Read Check
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x580075C4 , 0 );
				//--------------------------------------------------//
				// Enable PRBS
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x580075C4 , 0x1005 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Read Check
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x580075C4 , 0 );
				break;
		case 2:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 0 );*/
				//--------------------------------------------------//
				// Write PRBS Type
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x5800B5C4 , 0x1004 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Read Check
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x5800B5C4 , 0 );
				//--------------------------------------------------//
				// Enable PRBS
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x5800B5C4 , 0x1005 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Read Check
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x5800B5C4 , 0 );
				break;
		case 3:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0008 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 0 );*/
				//--------------------------------------------------//
				// Write PRBS Type
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x5800F5C4 , 0x1004 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Read Check
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x5800F5C4 , 0 );
				//--------------------------------------------------//
				// Enable PRBS
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x5800F5C4 , 0x1005 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Read Check
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x5800F5C4 , 0 );
				break;
		case 4:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0010 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 1 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 1 );*/
				//--------------------------------------------------//
				// Write PRBS Type
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x580035C4 , 0x1004 , BRCM_59281_DATA_LENG , 1 );
				//--------------------------------------------------//
				// Read Check
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x580035C4 , 1 );
				//--------------------------------------------------//
				// Enable PRBS
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x580035C4 , 0x1005 , BRCM_59281_DATA_LENG , 1 );
				//--------------------------------------------------//
				// Read Check
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x580035C4 , 1 );
				break;
		case 5:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0020 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 1 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 1 );*/
				//--------------------------------------------------//
				// Write PRBS Type
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x580075C4 , 0x1004 , BRCM_59281_DATA_LENG , 1 );
				//--------------------------------------------------//
				// Read Check
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x580075C4 , 1 );
				//--------------------------------------------------//
				// Enable PRBS
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x580075C4 , 0x1005 , BRCM_59281_DATA_LENG , 1 );
				//--------------------------------------------------//
				// Read Check
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x580075C4 , 1 );

				break;
		case 6:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0040 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 1 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 1 );*/
				//--------------------------------------------------//
				// Write PRBS Type
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x5800B5C4 , 0x1004 , BRCM_59281_DATA_LENG , 1 );
				//--------------------------------------------------//
				// Read Check
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x5800B5C4 , 1 );
				//--------------------------------------------------//
				// Enable PRBS
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x5800B5C4 , 0x1005 , BRCM_59281_DATA_LENG , 1 );
				//--------------------------------------------------//
				// Read Check
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x5800B5C4 , 1 );
				break;
		case 7:
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0080 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 1 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0021 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 1 );*/
				//--------------------------------------------------//
				// Write PRBS Type
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x5800F5C4 , 0x1004 , BRCM_59281_DATA_LENG , 1 );
				//--------------------------------------------------//
				// Read Check
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x5800F5C4 , 1 );
				//--------------------------------------------------//
				// Enable PRBS
				//--------------------------------------------------//
				BRCM_Control_WRITE_Data( 0x5800F5C4 , 0x1005 , BRCM_59281_DATA_LENG , 1 );
				//--------------------------------------------------//
				// Read Check
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x5800F5C4 , 1 );
				break;
	}

}

void DSP59281_PRBS13Q_Control_Disable(unsigned char Lan_Channel)
{

	switch(Lan_Channel)
	{
		case 0:
				//--------------------------------------------------//
				// Read for replace bit 0 or skip
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x580035C4 , 0 );
				BRCM_Control_WRITE_Data( 0x580035C4 , 0x1004 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0001 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 0 );*/
				break;
		case 1:
				//--------------------------------------------------//
				// Read for replace bit 0 or skip
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x580075C4 , 0 );
				BRCM_Control_WRITE_Data( 0x580075C4 , 0x1004 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0002 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 0 );*/
				break;
		case 2:
				//--------------------------------------------------//
				// Read for replace bit 0 or skip
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x5800B5C4 , 0 );
				BRCM_Control_WRITE_Data( 0x5800B5C4 , 0x1004 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0004 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 0 );*/
				break;
		case 3:
				//--------------------------------------------------//
				// Read for replace bit 0 or skip
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x5800F5C4 , 0 );
				BRCM_Control_WRITE_Data( 0x5800F5C4 , 0x1004 , BRCM_59281_DATA_LENG , 0 );
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0008 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 0 );*/

				break;
		case 4:
				//--------------------------------------------------//
				// Read for replace bit 0 or skip
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x580035C4 , 1 );
				BRCM_Control_WRITE_Data( 0x580035C4 , 0x1004 , BRCM_59281_DATA_LENG , 1 );
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0010 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 1 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800218C , 1 );*/
				break;
		case 5:
				//--------------------------------------------------//
				// Read for replace bit 0 or skip
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x580075C4 , 1 );
				BRCM_Control_WRITE_Data( 0x580075C4 , 0x1004 , BRCM_59281_DATA_LENG , 1 );
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0020 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 1 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800618C , 1 );*/
				break;
		case 6:
				//--------------------------------------------------//
				// Read for replace bit 0 or skip
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x5800B5C4 , 1 );
				BRCM_Control_WRITE_Data( 0x5800B5C4 , 0x1004 , BRCM_59281_DATA_LENG , 1 );
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0040 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 1 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800A18C , 1 );*/
				break;
		case 7:
				//--------------------------------------------------//
				// Read for replace bit 0 or skip
				//--------------------------------------------------//
				BRCM_Control_READ_Data( 0x5800F5C4 , 1 );
				BRCM_Control_WRITE_Data( 0x5800F5C4 , 0x1004 , BRCM_59281_DATA_LENG , 1 );
				//--------------------------------------------------//
				// Set FW Command
				//--------------------------------------------------//
				/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0080 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 1 );
				BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0024 , BRCM_59281_DATA_LENG , 0 );
				BRCM_Control_READ_Data( 0x5200CC14 , 0 );
				BRCM_Control_READ_Data( 0x5800E18C , 1 );*/
				break;
	}
	DSP_59281_Line_Side_SET_FW_COMMAND(0,0x8024,0x0024);
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 Rx performance Control
//-----------------------------------------------------------------------------------------------------//
void DSP59281_PREFORMANCE_Medium_SETTING()
{
	//--------------------------------------------------//
	// Lan0
	//--------------------------------------------------//
	BRCM_Control_READ_Data( 0x580021A0 , 0 );
	BRCM_Control_WRITE_Data( 0x580021A0 , 0x1000 , BRCM_59281_DATA_LENG , 0 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0001 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800218C , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800218C , 0 );*/
	DSP_59281_Line_Side_SET_FW_COMMAND(0,0x8004,0x0004);
	//--------------------------------------------------//
	// Lan1
	//--------------------------------------------------//
	BRCM_Control_READ_Data( 0x580061A0 , 0 );
	BRCM_Control_WRITE_Data( 0x580061A0 , 0x1000 , BRCM_59281_DATA_LENG , 0 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0002 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800618C , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800618C , 0 );*/
	DSP_59281_Line_Side_SET_FW_COMMAND(1,0x8004,0x0004);
	//--------------------------------------------------//
	// Lan2
	//--------------------------------------------------//
	BRCM_Control_READ_Data( 0x5800A1A0 , 0 );
	BRCM_Control_WRITE_Data( 0x5800A1A0 , 0x1000 , BRCM_59281_DATA_LENG , 0 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800A18C , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800A18C , 0 );*/
	DSP_59281_Line_Side_SET_FW_COMMAND(2,0x8004,0x0004);
	//--------------------------------------------------//
	// Lan3
	//--------------------------------------------------//
	BRCM_Control_READ_Data( 0x5800E1A0 , 0 );
	BRCM_Control_WRITE_Data( 0x5800E1A0 , 0x1000 , BRCM_59281_DATA_LENG , 0 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0008 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800E18C , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800E18C , 0 );*/
	DSP_59281_Line_Side_SET_FW_COMMAND(3,0x8004,0x0004);
	//--------------------------------------------------//
	// Lan4
	//--------------------------------------------------//
	BRCM_Control_READ_Data( 0x580021A0 , 1 );
	BRCM_Control_WRITE_Data( 0x580021A0 , 0x1000 , BRCM_59281_DATA_LENG , 1 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0010 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800218C , 1 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800218C , 1 );*/
	DSP_59281_Line_Side_SET_FW_COMMAND(4,0x8004,0x0004);
	//--------------------------------------------------//
	// Lan5
	//--------------------------------------------------//
	BRCM_Control_READ_Data( 0x580061A0 , 1 );
	BRCM_Control_WRITE_Data( 0x580061A0 , 0x1000 , BRCM_59281_DATA_LENG , 1 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0020 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800618C , 1 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800618C , 1 );*/
	DSP_59281_Line_Side_SET_FW_COMMAND(5,0x8004,0x0004);
	//--------------------------------------------------//
	// Lan6
	//--------------------------------------------------//
	BRCM_Control_READ_Data( 0x5800A1A0 , 1 );
	BRCM_Control_WRITE_Data( 0x5800A1A0 , 0x1000 , BRCM_59281_DATA_LENG , 1 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0040 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800A18C , 1 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800A18C , 1 );*/
	DSP_59281_Line_Side_SET_FW_COMMAND(6,0x8004,0x0004);
	//--------------------------------------------------//
	// Lan7
	//--------------------------------------------------//
	BRCM_Control_READ_Data( 0x5800E1A0 , 1 );
	BRCM_Control_WRITE_Data( 0x5800E1A0 , 0x1000 , BRCM_59281_DATA_LENG , 1 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0080 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800E18C , 1 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800E18C , 1 );*/
	DSP_59281_Line_Side_SET_FW_COMMAND(7,0x8004,0x0004);
}

//-----------------------------------------------------------------------------------------------------//
// DSP 59281 RX BW Control is 2
//-----------------------------------------------------------------------------------------------------//
void DSP59281_RX_BW_SETTING()
{
	//--------------------------------------------------//
	// Lan0
	//--------------------------------------------------//
	BRCM_Control_READ_Data( 0x580021C0 , 0 );
	BRCM_Control_WRITE_Data( 0x580021C0 , 0x81C1 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x580021C0 , 0 );
	BRCM_Control_WRITE_Data( 0x580021C0 , 0x81C5 , BRCM_59281_DATA_LENG , 0 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0001 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800218C , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800218C , 0 );*/
	DSP_59281_Line_Side_SET_FW_COMMAND(0,0x8004,0x0004);
	//--------------------------------------------------//
	// Lan1
	//--------------------------------------------------//
	BRCM_Control_READ_Data( 0x580061C0 , 0 );
	BRCM_Control_WRITE_Data( 0x580061C0 , 0x81C1 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x580061C0 , 0 );
	BRCM_Control_WRITE_Data( 0x580061C0 , 0x81C5 , BRCM_59281_DATA_LENG , 0 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0002 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800618C , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800618C , 0 );*/
	DSP_59281_Line_Side_SET_FW_COMMAND(1,0x8004,0x0004);
	//--------------------------------------------------//
	// Lan2
	//--------------------------------------------------//
	BRCM_Control_READ_Data( 0x5800A1C0 , 0 );
	BRCM_Control_WRITE_Data( 0x5800A1C0 , 0x81C1 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5800A1C0 , 0 );
	BRCM_Control_WRITE_Data( 0x5800A1C0 , 0x81C5 , BRCM_59281_DATA_LENG , 0 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800A18C , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800A18C , 0 );*/
	DSP_59281_Line_Side_SET_FW_COMMAND(2,0x8004,0x0004);
	//--------------------------------------------------//
	// Lan3
	//--------------------------------------------------//
	BRCM_Control_READ_Data( 0x5800E1C0 , 0 );
	BRCM_Control_WRITE_Data( 0x5800E1C0 , 0x81C1 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5800E1C0 , 0 );
	BRCM_Control_WRITE_Data( 0x5800E1C0 , 0x81C5 , BRCM_59281_DATA_LENG , 0 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0008 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800E18C , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800E18C , 0 );*/
	DSP_59281_Line_Side_SET_FW_COMMAND(3,0x8004,0x0004);
	//--------------------------------------------------//
	// Lan4
	//--------------------------------------------------//
	BRCM_Control_READ_Data( 0x580021C0 , 1 );
	BRCM_Control_WRITE_Data( 0x580021C0 , 0x81C1 , BRCM_59281_DATA_LENG , 1 );
	BRCM_Control_READ_Data( 0x580021C0 , 1 );
	BRCM_Control_WRITE_Data( 0x580021C0 , 0x81C5 , BRCM_59281_DATA_LENG , 1 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0010 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800218C , 1 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800218C , 1 );*/
	DSP_59281_Line_Side_SET_FW_COMMAND(4,0x8004,0x0004);
	//--------------------------------------------------//
	// Lan5
	//--------------------------------------------------//
	BRCM_Control_READ_Data( 0x580061C0 , 1 );
	BRCM_Control_WRITE_Data( 0x580061C0 , 0x81C1 , BRCM_59281_DATA_LENG , 1 );
	BRCM_Control_READ_Data( 0x580061C0 , 1 );
	BRCM_Control_WRITE_Data( 0x580061C0 , 0x81C5 , BRCM_59281_DATA_LENG , 1 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0020 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800618C , 1 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800618C , 1 );*/
	DSP_59281_Line_Side_SET_FW_COMMAND(5,0x8004,0x0004);
	//--------------------------------------------------//
	// Lan6
	//--------------------------------------------------//
	BRCM_Control_READ_Data( 0x5800A1C0 , 1 );
	BRCM_Control_WRITE_Data( 0x5800A1C0 , 0x81C1 , BRCM_59281_DATA_LENG , 1 );
	BRCM_Control_READ_Data( 0x5800A1C0 , 1 );
	BRCM_Control_WRITE_Data( 0x5800A1C0 , 0x81C5 , BRCM_59281_DATA_LENG , 1 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0040 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800A18C , 1 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800A18C , 1 );*/
	DSP_59281_Line_Side_SET_FW_COMMAND(6,0x8004,0x0004);
	//--------------------------------------------------//
	// Lan7
	//--------------------------------------------------//
	BRCM_Control_READ_Data( 0x5800E1C0 , 1 );
	BRCM_Control_WRITE_Data( 0x5800E1C0 , 0x81C1 , BRCM_59281_DATA_LENG , 1 );
	BRCM_Control_READ_Data( 0x5800E1C0 , 1 );
	BRCM_Control_WRITE_Data( 0x5800E1C0 , 0x81C5 , BRCM_59281_DATA_LENG , 1 );
	//--------------------------------------------------//
	// Set FW Command
	//--------------------------------------------------//
	/*BRCM_Control_WRITE_Data( 0x5200CC08 , 0x0000 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC0C , 0x0080 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x8004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800E18C , 1 );
	BRCM_Control_WRITE_Data( 0x5200CC10 , 0x0004 , BRCM_59281_DATA_LENG , 0 );
	BRCM_Control_READ_Data( 0x5200CC14 , 0 );
	BRCM_Control_READ_Data( 0x5800E18C , 1 );*/
	DSP_59281_Line_Side_SET_FW_COMMAND(7,0x8004,0x0004);
}

//-----------------------------------------------------------------------------------------------------//
// DSP 59281 RX Info Nldet
//-----------------------------------------------------------------------------------------------------//
void DSP59281_RX_NLdet_Enable_Control(unsigned char Enable)
{
	unsigned int Read_buffer ;
	//--------------------------------------------------//
	// Lan0
	// Read for replace bit:15
	// nldet enable = 1 or disable = 0
	// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
	//  1  0  0  0  0  0 0 0 0 0 0 0 0 0 0 0
	// Setting to 1 Value = 0x8000
	// Setting to 0 Value = 0x7FFF
	//--------------------------------------------------//
	Read_buffer = BRCM_Control_READ_Data( 0x580021A0 , 0 );
	if(Enable)
		BRCM_Control_WRITE_Data( 0x580021A0 , ( Read_buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 0 );
	else
		BRCM_Control_WRITE_Data( 0x580021A0 , ( Read_buffer & 0x7FFF ) , BRCM_59281_DATA_LENG , 0 );
	DSP_59281_Line_Side_SET_FW_COMMAND( 0 , 0x8004 , 0x0004 );
	//--------------------------------------------------//
	// Lan1
	//--------------------------------------------------//
	Read_buffer = BRCM_Control_READ_Data( 0x580061A0 , 0 );
	if(Enable)
		BRCM_Control_WRITE_Data( 0x580061A0 , ( Read_buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 0 );
	else
		BRCM_Control_WRITE_Data( 0x580061A0 , ( Read_buffer & 0x7FFF ) , BRCM_59281_DATA_LENG , 0 );

	DSP_59281_Line_Side_SET_FW_COMMAND( 1 , 0x8004 , 0x0004 );
	//--------------------------------------------------//
	// Lan2
	//--------------------------------------------------//
	Read_buffer = BRCM_Control_READ_Data( 0x5800A1A0 , 0 );
	if(Enable)
		BRCM_Control_WRITE_Data( 0x5800A1A0 , ( Read_buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 0 );
	else
		BRCM_Control_WRITE_Data( 0x5800A1A0 , ( Read_buffer & 0x7FFF ) , BRCM_59281_DATA_LENG , 0 );
	DSP_59281_Line_Side_SET_FW_COMMAND( 2 , 0x8004 , 0x0004 );
	//--------------------------------------------------//
	// Lan3
	//--------------------------------------------------//
	Read_buffer = BRCM_Control_READ_Data( 0x5800E1A0 , 0 );
	if(Enable)
		BRCM_Control_WRITE_Data( 0x5800E1A0 , ( Read_buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 0 );
	else
		BRCM_Control_WRITE_Data( 0x5800E1A0 , ( Read_buffer & 0x7FFF ) , BRCM_59281_DATA_LENG , 0 );
	DSP_59281_Line_Side_SET_FW_COMMAND( 3 , 0x8004 , 0x0004 );
	//--------------------------------------------------//
	// Lan4
	//--------------------------------------------------//
	Read_buffer = BRCM_Control_READ_Data( 0x580021A0 , 1 );
	if(Enable)
		BRCM_Control_WRITE_Data( 0x580021A0 , ( Read_buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 1 );
	else
		BRCM_Control_WRITE_Data( 0x580021A0 , ( Read_buffer & 0x7FFF ) , BRCM_59281_DATA_LENG , 1 );
	DSP_59281_Line_Side_SET_FW_COMMAND( 4 , 0x8004 , 0x0004 );
	//--------------------------------------------------//
	// Lan5
	//--------------------------------------------------//
	Read_buffer = BRCM_Control_READ_Data( 0x580061A0 , 1 );
	if(Enable)
		BRCM_Control_WRITE_Data( 0x580061A0 , ( Read_buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 1 );
	else
		BRCM_Control_WRITE_Data( 0x580061A0 , ( Read_buffer & 0x7FFF ) , BRCM_59281_DATA_LENG , 1 );

	DSP_59281_Line_Side_SET_FW_COMMAND( 5 , 0x8004 , 0x0004 );
	//--------------------------------------------------//
	// Lan6
	//--------------------------------------------------//
	Read_buffer = BRCM_Control_READ_Data( 0x5800A1A0 , 1 );
	if(Enable)
		BRCM_Control_WRITE_Data( 0x5800A1A0 , ( Read_buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 1 );
	else
		BRCM_Control_WRITE_Data( 0x5800A1A0 , ( Read_buffer & 0x7FFF ) , BRCM_59281_DATA_LENG , 1 );
	DSP_59281_Line_Side_SET_FW_COMMAND( 6 , 0x8004 , 0x0004 );
	//--------------------------------------------------//
	// Lan7
	//--------------------------------------------------//
	Read_buffer = BRCM_Control_READ_Data( 0x5800E1A0 , 1 );
	if(Enable)
		BRCM_Control_WRITE_Data( 0x5800E1A0 , ( Read_buffer | 0x8000 ) , BRCM_59281_DATA_LENG , 1 );
	else
		BRCM_Control_WRITE_Data( 0x5800E1A0 , ( Read_buffer & 0x7FFF ) , BRCM_59281_DATA_LENG , 1 );
	DSP_59281_Line_Side_SET_FW_COMMAND( 7 , 0x8004 , 0x0004 );
}
//-----------------------------------------------------------------------------------------------------//
// DSP 59281 RX Info ExSlicer
//-----------------------------------------------------------------------------------------------------//
void DSP59281_RX_Exsilcer_Control(unsigned char Mode_Change)
{
	unsigned int Read_buffer ;
	//--------------------------------------------------//
	// Lan0
	// Read for replace bit: 1:2
	// nldet enable = 1 or disable = 0
	// 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
	//  0  0  0  0  0  0 0 0 0 0 0 0 0 1 1 0
	// 2 = regular slicer  = 0x0002
	// 6 = externed slicer = 0x0006
	//--------------------------------------------------//
	Read_buffer = BRCM_Control_READ_Data( 0x58002194 , 0 );
	if(Mode_Change==0x01)
		BRCM_Control_WRITE_Data( 0x58002194 , ( Read_buffer | 0x0002 ) , BRCM_59281_DATA_LENG , 0 );
	else if(Mode_Change==0x02)
		BRCM_Control_WRITE_Data( 0x58002194 , ( Read_buffer | 0x0006 ) , BRCM_59281_DATA_LENG , 0 );
	else
		BRCM_Control_WRITE_Data( 0x58002194 , ( Read_buffer & 0xFFF9 ) , BRCM_59281_DATA_LENG , 0 );

	DSP_59281_Line_Side_SET_FW_COMMAND( 0 , 0x8004 , 0x0004 );
	//--------------------------------------------------//
	// Lan1
	//--------------------------------------------------//
	Read_buffer = BRCM_Control_READ_Data( 0x58006194 , 0 );
	if(Mode_Change==0x01)
		BRCM_Control_WRITE_Data( 0x58006194 , ( Read_buffer | 0x0002 ) , BRCM_59281_DATA_LENG , 0 );
	else if(Mode_Change==0x02)
		BRCM_Control_WRITE_Data( 0x58006194 , ( Read_buffer | 0x0006 ) , BRCM_59281_DATA_LENG , 0 );
	else
		BRCM_Control_WRITE_Data( 0x58006194 , ( Read_buffer & 0xFFF9 ) , BRCM_59281_DATA_LENG , 0 );

	DSP_59281_Line_Side_SET_FW_COMMAND( 1 , 0x8004 , 0x0004 );
	//--------------------------------------------------//
	// Lan2
	//--------------------------------------------------//
	Read_buffer = BRCM_Control_READ_Data( 0x5800A194 , 0 );
	if(Mode_Change==0x01)
		BRCM_Control_WRITE_Data( 0x5800A194 , ( Read_buffer | 0x0002 ) , BRCM_59281_DATA_LENG , 0 );
	else if(Mode_Change==0x02)
		BRCM_Control_WRITE_Data( 0x5800A194 , ( Read_buffer | 0x0006 ) , BRCM_59281_DATA_LENG , 0 );
	else
		BRCM_Control_WRITE_Data( 0x5800A194 , ( Read_buffer & 0xFFF9 ) , BRCM_59281_DATA_LENG , 0 );

	DSP_59281_Line_Side_SET_FW_COMMAND( 2 , 0x8004 , 0x0004 );
	//--------------------------------------------------//
	// Lan3
	//--------------------------------------------------//
	Read_buffer = BRCM_Control_READ_Data( 0x5800E194 , 0 );
	if(Mode_Change==0x01)
		BRCM_Control_WRITE_Data( 0x5800E194 , ( Read_buffer | 0x0002 ) , BRCM_59281_DATA_LENG , 0 );
	else if(Mode_Change==0x02)
		BRCM_Control_WRITE_Data( 0x5800E194 , ( Read_buffer | 0x0006 ) , BRCM_59281_DATA_LENG , 0 );
	else
		BRCM_Control_WRITE_Data( 0x5800E194 , ( Read_buffer & 0xFFF9 ) , BRCM_59281_DATA_LENG , 0 );

	DSP_59281_Line_Side_SET_FW_COMMAND( 3 , 0x8004 , 0x0004 );
	//--------------------------------------------------//
	// Lan4
	//--------------------------------------------------//
	Read_buffer = BRCM_Control_READ_Data( 0x58002194 , 1 );
	if(Mode_Change==0x01)
		BRCM_Control_WRITE_Data( 0x58002194 , ( Read_buffer | 0x0002 ) , BRCM_59281_DATA_LENG , 1 );
	else if(Mode_Change==0x02)
		BRCM_Control_WRITE_Data( 0x58002194 , ( Read_buffer | 0x0006 ) , BRCM_59281_DATA_LENG , 1 );
	else
		BRCM_Control_WRITE_Data( 0x58002194 , ( Read_buffer & 0xFFF9 ) , BRCM_59281_DATA_LENG , 1 );

	DSP_59281_Line_Side_SET_FW_COMMAND( 4 , 0x8004 , 0x0004 );
	//--------------------------------------------------//
	// Lan5
	//--------------------------------------------------//
	Read_buffer = BRCM_Control_READ_Data( 0x58006194 , 1 );
	if(Mode_Change==0x01)
		BRCM_Control_WRITE_Data( 0x58006194 , ( Read_buffer | 0x0002 ) , BRCM_59281_DATA_LENG , 1 );
	else if(Mode_Change==0x02)
		BRCM_Control_WRITE_Data( 0x58006194 , ( Read_buffer | 0x0006 ) , BRCM_59281_DATA_LENG , 1 );
	else
		BRCM_Control_WRITE_Data( 0x58006194 , ( Read_buffer & 0xFFF9 ) , BRCM_59281_DATA_LENG , 1 );

	DSP_59281_Line_Side_SET_FW_COMMAND( 5 , 0x8004 , 0x0004 );
	//--------------------------------------------------//
	// Lan6
	//--------------------------------------------------//
	Read_buffer = BRCM_Control_READ_Data( 0x5800A194 , 1 );
	if(Mode_Change==0x01)
		BRCM_Control_WRITE_Data( 0x5800A194 , ( Read_buffer | 0x0002 ) , BRCM_59281_DATA_LENG , 1 );
	else if(Mode_Change==0x02)
		BRCM_Control_WRITE_Data( 0x5800A194 , ( Read_buffer | 0x0006 ) , BRCM_59281_DATA_LENG , 1 );
	else
		BRCM_Control_WRITE_Data( 0x5800A194 , ( Read_buffer & 0xFFF9 ) , BRCM_59281_DATA_LENG , 1 );

	DSP_59281_Line_Side_SET_FW_COMMAND( 6 , 0x8004 , 0x0004 );
	//--------------------------------------------------//
	// Lan7
	//--------------------------------------------------//
	Read_buffer = BRCM_Control_READ_Data( 0x5800E194 , 1 );
	if(Mode_Change==0x01)
		BRCM_Control_WRITE_Data( 0x5800E194 , ( Read_buffer | 0x0002 ) , BRCM_59281_DATA_LENG , 1 );
	else if(Mode_Change==0x02)
		BRCM_Control_WRITE_Data( 0x5800E194 , ( Read_buffer | 0x0006 ) , BRCM_59281_DATA_LENG , 1 );
	else
		BRCM_Control_WRITE_Data( 0x5800E194 , ( Read_buffer & 0xFFF9 ) , BRCM_59281_DATA_LENG , 1 );

	DSP_59281_Line_Side_SET_FW_COMMAND( 7 , 0x8004 , 0x0004 );

}










