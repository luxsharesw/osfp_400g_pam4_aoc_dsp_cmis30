/*
 * PowerOn_Table_Init.c
 *
 *  Created on: 2017�~3��21��
 *      Author: 100476
 */
#include <SI_EFM8LB1_Register_Enums.h>
#include "EFM8LB_Flash_Map.h"
#include "EFM8LB_Flash_RW.h"

#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"

#include "MA38435_TX1_TX4.h"
#include "MA38435_TX5_TX8.h"
#include "MA38434_RX1_RX4.h"
#include "MA38434_RX5_RX8.h"

#include "BRCM_DSP_59281.h"

#include <string.h>

#define MSA_Version  0x30

xdata unsigned char QSFPDD_A0[256]={0};
xdata unsigned char QSFPDD_Page11[128]={0};
xdata unsigned char QSFPDD_Page1[128]={0};
xdata unsigned char QSFPDD_Page2[128]={0};
xdata unsigned char QSFPDD_Page10[128]={0};
xdata unsigned char CTEL_Table[16];
xdata unsigned char Pro_Table[16];
xdata unsigned char OP_AMP_Table[16];
xdata unsigned char Post_Table[16];

xdata unsigned char	bef_Pre_Cursor_RX12 = 5;
xdata unsigned char	bef_Pre_Cursor_RX34 = 5;
xdata unsigned char	bef_Pre_Cursor_RX56 = 5;
xdata unsigned char	bef_Pre_Cursor_RX78 = 5;

xdata unsigned char	bef_Post_Cursor_RX12 = 0;
xdata unsigned char	bef_Post_Cursor_RX34 = 0;
xdata unsigned char	bef_Post_Cursor_RX56 = 0;
xdata unsigned char	bef_Post_Cursor_RX78 = 0;

xdata unsigned char	bef_AMP_C_RX12 = 3;
xdata unsigned char	bef_AMP_C_RX34 = 3;
xdata unsigned char	bef_AMP_C_RX56 = 3;
xdata unsigned char	bef_AMP_C_RX78 = 3;

// MSA_Option_Table D400 - D40F  TX CTLE Control            16 Byte
//                  D410 - D41F  RX Pro  Control            16 Byte
//                  D420 - D42F  RX AMP  Control            16 Byte
//                  D430 - D43F  Change Leve 1 Password     16 Byte
//                  D440 - D44F  RX Post Control            16 Byte

xdata struct CALIB_MEMORY CALIB_MEMORY_MAP;
xdata struct CALIB_MEMORY_1 CALIB_MEMORY_1_MAP;

xdata unsigned int CHECKSUM = 0;
xdata unsigned char Exended_RateS_Version = 2 ;

void CheckSum_Calculate()
{
	unsigned int TEMP_BUF=0;
	unsigned int IIcount;

	CHECKSUM = 0;
	for( IIcount=0 ; IIcount < Flash_LUT_Start ; IIcount++ )
	{
		TEMP_BUF = FLASH_ByteRead(IIcount);
		CHECKSUM = CHECKSUM + TEMP_BUF ;
	}
	CALIB_MEMORY_MAP.CHECKSUM_V = CHECKSUM ;
	Flash_EWFunction( Calibration_Table  , &CALIB_MEMORY_MAP.VCC_SCALEM    , 128 );
	Flash_EW_NoErase( Cablbration_Table2 , &CALIB_MEMORY_1_MAP.VCC1_SCALEM , 128 );
}

void CheckSum_Calculation()
{
	CheckSum_Calculate();

//	QSFPDD_A0[248] = CALIB_MEMORY_MAP.CHECKSUM_V >> 8 ;
//	QSFPDD_A0[249] = CALIB_MEMORY_MAP.CHECKSUM_V ;

//	QSFPDD_A0[250] = MCU_FW_VERSION >> 8 ;
//	QSFPDD_A0[251] = MCU_FW_VERSION ;
}

void QSFPDD_Table_GetDate()
{
	Flash_ReadFunction( FS_QSFPDD_A0 , &QSFPDD_A0[0]    , 256 );
	Flash_ReadFunction( FS_QSFPDD_P1 , &QSFPDD_Page1[0] , 128 );
	Flash_ReadFunction( FS_QSFPDD_P2 , &QSFPDD_Page2[0] , 128 );
}

void Get_QDD_MSA_Optional_Control_Table()
{
	Flash_ReadFunction(   MSA_Option_Table      , &CTEL_Table   , 16 );
	Flash_ReadFunction( ( MSA_Option_Table+16 ) , &Pro_Table    , 16 );
	Flash_ReadFunction( ( MSA_Option_Table+32 ) , &OP_AMP_Table , 16 );
	Flash_ReadFunction( ( MSA_Option_Table+64 ) , &Post_Table   , 16 );
}

/*
void Get_Flash_PW_LEVE1()
{
	Flash_ReadFunction( ( MSA_Option_Table + 48 ) , &PW_LEVE1 , 4 );
}
*/
void Device_Table_Get_Data()
{
	xdata unsigned char Temp_Buffer[128];
	// Tx LDD AFSI_N74C4S
	Flash_ReadFunction( FS_MA38435_T14_P82  , &MATA38435_TX1_TX4_MEMORY_MAP.CHIP_ID , 128 );
	Flash_ReadFunction( FS_MA38435_T58_P84  , &MATA38435_TX5_TX8_MEMORY_MAP.CHIP_ID , 128 );

	// Rx TIA MATA38434
	Flash_ReadFunction( FS_MA38434_R14_P8A  , &MATA38434_RX1_RX4_TIA_MEMORY_MAP.CHIP_ID , 128 );
	Flash_ReadFunction( FS_MA38434_R58_P8B  , &MATA38434_RX5_RX8_TIA_MEMORY_MAP.CHIP_ID , 128 );

	// DSP Line-side & System-side CH0 - CH7
	Flash_ReadFunction( FS_59281PHY0_LS_P86  , &Temp_Buffer , 128 );
	memcpy(  &BRCM_59281_DSP_LS_PHY0_MEMORY_MAP , &Temp_Buffer , 128 );
	Flash_ReadFunction( FS_59281PHY0_SS_P87  , &Temp_Buffer , 128 );
	memcpy(  &BRCM_59281_DSP_SS_PHY0_MEMORY_MAP , &Temp_Buffer , 128 );
	Flash_ReadFunction( FS_59281PHY1_LS_P8C  , &Temp_Buffer , 128 );
	memcpy(  &BRCM_59281_DSP_LS_PHY1_MEMORY_MAP , &Temp_Buffer , 128 );
	Flash_ReadFunction( FS_59281PHY1_SS_P8E  , &Temp_Buffer , 128 );
	memcpy(  &BRCM_59281_DSP_SS_PHY1_MEMORY_MAP , &Temp_Buffer , 128 );

	Flash_ReadFunction( Calibration_Table  , &CALIB_MEMORY_MAP.VCC_SCALEM    , 128 );
	Flash_ReadFunction( Cablbration_Table2 , &CALIB_MEMORY_1_MAP.VCC1_SCALEM , 128 );

	CALIB_MEMORY_MAP.DDMI_DISABLE = 0 ;
}

void QDD_MSA_Init_SETTING()
{
	memset(&QSFPDD_A0, 0x00, 84);
	// ID
	QSFPDD_A0[0] = QSFPDD_A0[128];
	// MSA Version 3.0 = 30h
	QSFPDD_A0[1] = MSA_Version ;
	// MSA IntL bit
	QSFPDD_A0[2] = 0x00 ;
	// MSA Module Status
	QSFPDD_A0[3] = 0x00 ;
	// MSA module Type Advertising code
	// 01 : MMF
	//QSFPDD_A0[85] = 0x04 ;
	//QSFPDD_A0[86] = 0x0B ;
	//QSFPDD_A0[87] = 0x01 ;
	//QSFPDD_A0[88] = 0x88 ;
	//QSFPDD_A0[89] = 0x01 ;
}

void QDD_MSA_PAGE11_ControlByte()
{
	// Page10 RW Control
	// Page11 Read only
	// Lan0 - Lan7 State
	QSFPDD_Page10[17]  = 0x10 ;
	QSFPDD_Page10[18]  = 0x10 ;
	QSFPDD_Page10[19]  = 0x10 ;
	QSFPDD_Page10[20]  = 0x10 ;
	QSFPDD_Page10[21]  = 0x10 ;
	QSFPDD_Page10[22]  = 0x10 ;
	QSFPDD_Page10[23]  = 0x10 ;
	QSFPDD_Page10[24]  = 0x10 ;

	QSFPDD_Page10[25]  = 0x00 ;

	QSFPDD_Page11[78]  = QSFPDD_Page10[17];
	QSFPDD_Page11[79]  = QSFPDD_Page10[18];
	QSFPDD_Page11[80]  = QSFPDD_Page10[19];
	QSFPDD_Page11[81]  = QSFPDD_Page10[20];
	QSFPDD_Page11[82]  = QSFPDD_Page10[21];
	QSFPDD_Page11[83]  = QSFPDD_Page10[22];
	QSFPDD_Page11[84]  = QSFPDD_Page10[23];
	QSFPDD_Page11[85]  = QSFPDD_Page10[24];
	// Tx CDR ON
	QSFPDD_Page10[32] = 0xFF ;
	QSFPDD_Page11[93] = QSFPDD_Page10[32] ;
	// Rx CDR ON
	QSFPDD_Page10[33] = 0xFF ;
	QSFPDD_Page11[94] = QSFPDD_Page10[33] ;
	// Page11 Data Path Deactivated
	// 04 DataPathActivated State
	QSFPDD_Page11[0] = 0x11 ;
	QSFPDD_Page11[1] = 0x11 ;
	QSFPDD_Page11[2] = 0x11 ;
	QSFPDD_Page11[3] = 0x11 ;

	// DataPathPwrUp
	QSFPDD_Page10[0] = 0x00 ;
	// Polarity Flip
	QSFPDD_Page10[1] = 0x00 ;
	// Tx output Disable
	QSFPDD_Page10[2] = 0x00 ;
	// TX input EQ Setting
	QSFPDD_Page10[28] = 0x00 ;
	QSFPDD_Page10[29] = 0x00 ;
	QSFPDD_Page10[30] = 0x00 ;
	QSFPDD_Page10[31] = 0x00 ;

	if( CALIB_MEMORY_MAP.MSA_Optional_Flag == 0x01 )
	{
		// RX Pre-EM
		QSFPDD_Page10[34] = CALIB_MEMORY_MAP.R12_Pre_Level ;
		QSFPDD_Page10[35] = CALIB_MEMORY_MAP.R34_Pre_Level ;
		QSFPDD_Page10[36] = CALIB_MEMORY_MAP.R56_Pre_Level ;
		QSFPDD_Page10[37] = CALIB_MEMORY_MAP.R78_Pre_Level ;
		// RX Post-EM
		QSFPDD_Page10[38] = CALIB_MEMORY_MAP.R12_Post_Level ;
		QSFPDD_Page10[39] = CALIB_MEMORY_MAP.R34_Post_Level ;
		QSFPDD_Page10[40] = CALIB_MEMORY_MAP.R56_Post_Level ;
		QSFPDD_Page10[41] = CALIB_MEMORY_MAP.R78_Post_Level ;
		// Rx AMP
		QSFPDD_Page10[42] = CALIB_MEMORY_MAP.R12_Swining_Level_2 ;
		QSFPDD_Page10[43] = CALIB_MEMORY_MAP.R34_Swining_Level_2 ;
		QSFPDD_Page10[44] = CALIB_MEMORY_MAP.R56_Swining_Level_2 ;
		QSFPDD_Page10[45] = CALIB_MEMORY_MAP.R78_Swining_Level_2 ;
	}
	else
	{
		QSFPDD_Page10[34] = 0x00 ;
		QSFPDD_Page10[35] = 0x00 ;
		QSFPDD_Page10[36] = 0x00 ;
		QSFPDD_Page10[37] = 0x00 ;

		QSFPDD_Page10[38] = 0x00 ;
		QSFPDD_Page10[39] = 0x00 ;
		QSFPDD_Page10[40] = 0x00 ;
		QSFPDD_Page10[41] = 0x00 ;

		QSFPDD_Page10[42] = CALIB_MEMORY_MAP.R12_Swining_Level ;
		QSFPDD_Page10[43] = CALIB_MEMORY_MAP.R34_Swining_Level ;
		QSFPDD_Page10[44] = CALIB_MEMORY_MAP.R56_Swining_Level ;
		QSFPDD_Page10[45] = CALIB_MEMORY_MAP.R78_Swining_Level ;
	}

	bef_Pre_Cursor_RX12 = QSFPDD_Page10[34];
	bef_Pre_Cursor_RX34 = QSFPDD_Page10[35];
	bef_Pre_Cursor_RX56 = QSFPDD_Page10[36];
	bef_Pre_Cursor_RX78 = QSFPDD_Page10[37];

	QSFPDD_Page11[95] = QSFPDD_Page10[34];
	QSFPDD_Page11[96] = QSFPDD_Page10[35];
	QSFPDD_Page11[97] = QSFPDD_Page10[36];
	QSFPDD_Page11[98] = QSFPDD_Page10[37];

	bef_Post_Cursor_RX12 = QSFPDD_Page10[38];
	bef_Post_Cursor_RX34 = QSFPDD_Page10[39];
	bef_Post_Cursor_RX56 = QSFPDD_Page10[40];
	bef_Post_Cursor_RX78 = QSFPDD_Page10[41];

	QSFPDD_Page11[99]  = QSFPDD_Page10[38];
	QSFPDD_Page11[100] = QSFPDD_Page10[39];
	QSFPDD_Page11[101] = QSFPDD_Page10[40];
	QSFPDD_Page11[102] = QSFPDD_Page10[41];

	bef_AMP_C_RX12 = QSFPDD_Page10[42];
	bef_AMP_C_RX34 = QSFPDD_Page10[43];
	bef_AMP_C_RX56 = QSFPDD_Page10[44];
	bef_AMP_C_RX78 = QSFPDD_Page10[45];

	QSFPDD_Page11[103] = QSFPDD_Page10[42];
	QSFPDD_Page11[104] = QSFPDD_Page10[43];
	QSFPDD_Page11[105] = QSFPDD_Page10[44];
	QSFPDD_Page11[106] = QSFPDD_Page10[45];
}

void PowerON_Table_Init()
{
	QSFPDD_Table_GetDate();
	Device_Table_Get_Data();
//	CheckSum_Calculation();

//	QSFPDD_A0[248] = CALIB_MEMORY_MAP.CHECKSUM_V >> 8 ;
//	QSFPDD_A0[249] = CALIB_MEMORY_MAP.CHECKSUM_V ;

//	QSFPDD_A0[250] = MCU_FW_VERSION >> 8 ;
//	QSFPDD_A0[251] = MCU_FW_VERSION ;

	QDD_MSA_Init_SETTING();
	QDD_MSA_PAGE11_ControlByte();

	Get_QDD_MSA_Optional_Control_Table();
}


