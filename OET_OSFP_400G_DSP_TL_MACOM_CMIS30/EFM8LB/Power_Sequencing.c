/*
 * Power_Sequencing.c
 *
 *  Created on: 2018�~11��9��
 *      Author: Ivan_Lin
 */
#include <SI_EFM8LB1_Register_Enums.h>
#include "Master_I2C_GPIO.h"

SI_SBIT( M77812_EN    , SFR_P0, 1 );
SI_SBIT( INTLG        , SFR_P0, 2 );
SI_SBIT( IRQ_77812    , SFR_P0, 3 );
//SI_SBIT( InitMode     , SFR_P0, 4 );
//SI_SBIT( RESETL_G     , SFR_P0, 5 );
SI_SBIT( MODSELB_DSP  , SFR_P0, 6 );
//SI_SBIT( MODSEL_L     , SFR_P0, 7 );
SI_SBIT( XO_EN        , SFR_P1, 2 );

SI_SBIT( LPMODE_DSP   , SFR_P1, 4 );
SI_SBIT( RESETL_DSP   , SFR_P1, 5 );
SI_SBIT( TRX_3V3_EN   , SFR_P1, 6 );
//SI_SBIT( TRX_4V5_EN   , SFR_P3, 1 );

SI_SBIT( DSP_P1V8_EN   , SFR_P3, 4 );

void Power_sequencing()
{
	/*	Delay_ms(50);
	XO_EN = 1 ;
	M77812_EN = 0 ;
	Delay_ms(20);
	TRX_3V3_EN = 0;
	DSP_P1V8_EN = 1;
	// Low power Enable
	LPMODE_DSP = 1 ;
	MODSELB_DSP = 0 ;

	RESETL_DSP = 0 ;
	Delay_ms(5);
	RESETL_DSP = 1 ;
*/
	Delay_ms(50);
	RESETL_DSP = 0 ;
	M77812_EN = 1 ;
	XO_EN = 0 ;
	DSP_P1V8_EN = 1;
	Delay_ms(10);
	TRX_3V3_EN = 1;
	// Low power Enable
	LPMODE_DSP = 1 ;
	MODSELB_DSP = 0 ;
	Delay_ms(5);
	RESETL_DSP = 1 ;
	Delay_ms(50);
}

unsigned char Get_Power_C_Status()
{
	// bit 0 TRX_3V3_EN
	// bit 1 TRX_4V5_EN
	// bit 2 M77812_EN
	// bit 3 XO_EN
	// bit 4 LPMODE_DSP
	// bit 5 MODSELB_DSP
	// bit 6 RESETL_DSP
	// bit 7 DSP_I2C_EN
	unsigned char Power_Status = 0 ;
	unsigned char Temp_data = 0 ;

	Temp_data = TRX_3V3_EN ;
	Power_Status = Power_Status + Temp_data ;
	//Temp_data = TRX_4V5_EN ;
	//Power_Status = Power_Status + ( Temp_data << 1 );
	Temp_data = M77812_EN ;
	Power_Status = Power_Status + ( Temp_data << 2 );
	Temp_data = XO_EN ;
	Power_Status = Power_Status + ( Temp_data << 3 );
	Temp_data = LPMODE_DSP ;
	Power_Status = Power_Status + ( Temp_data << 4 );
	Temp_data = MODSELB_DSP ;
	Power_Status = Power_Status + ( Temp_data << 5 );
	Temp_data = RESETL_DSP ;
	Power_Status = Power_Status + ( Temp_data << 6 );
	Temp_data = DSP_P1V8_EN ;
	Power_Status = Power_Status + ( Temp_data << 7 );

	return Power_Status;
}

void SET_Power_Control(unsigned char SET_Value)
{
	if( ( SET_Value & 0x01 ) == 0x01 )
		TRX_3V3_EN = 1;
	else
		TRX_3V3_EN = 0;

//	if( ( SET_Value & 0x02 ) == 0x02 )
//		TRX_4V5_EN = 1;
//		else
//		TRX_4V5_EN = 0;

	if( ( SET_Value & 0x04 ) == 0x04 )
		M77812_EN = 1;
	else
		M77812_EN = 0;

	if( ( SET_Value & 0x08 ) == 0x08 )
		XO_EN = 1;
	else
		XO_EN = 0;

	if( ( SET_Value & 0x10 ) == 0x10 )
		LPMODE_DSP = 1;
	else
		LPMODE_DSP = 0;

	if( ( SET_Value & 0x20 ) == 0x20 )
		MODSELB_DSP = 1;
	else
		MODSELB_DSP = 0;

	if( ( SET_Value & 0x40 ) == 0x40 )
		RESETL_DSP = 1;
	else
		RESETL_DSP = 0;

	if( ( SET_Value & 0x80 ) == 0x80 )
		DSP_P1V8_EN = 1;
	else
		DSP_P1V8_EN = 0;
}
/*
void MODSEL_MODE_TRIG_SET()
{
	if(MODSEL_L==1)
		P0MAT = 0xFF;
	else
		P0MAT = 0x7F;
}

//-----------------------------------------------------------------------------//
// MODSEL_L Function
//-----------------------------------------------------------------------------//
void ModSelL_Function()
{
	unsigned char SFRPAGE_SAVE = SFRPAGE;   // preserve SFRPAGE

	SFRPAGE = 0x00;

	if( MODSEL_L == 0 )
		SMB0CF |= 0x80;
	else
		SMB0CF &= 0x7F;

	MODSEL_MODE_TRIG_SET();

	SFRPAGE = SFRPAGE_SAVE;
}

//-----------------------------------------------------------------------------
// PMATCH_ISR
//-----------------------------------------------------------------------------
//
// PMATCH ISR Content goes here. Remember to clear flag bits:
//
//-----------------------------------------------------------------------------
SI_INTERRUPT(PMATCH_ISR, PMATCH_IRQn)
{
	ModSelL_Function();
}

*/

