/*
 * Flash_RW.c
 *
 *  Created on: 2017�~3��8��
 *      Author: 100476
 */
#include <SI_EFM8LB1_Register_Enums.h>

//-----------------------------------------------------------------------------
// FLASH_ByteWrite
//-----------------------------------------------------------------------------
void FLASH_ByteWrite(unsigned int addr, char byte, char Check_byte)
{
	unsigned char SFRPAGE_SAVE = SFRPAGE;   // preserve SFRPAGE
	bit EA_SAVE = IE_EA;                     // preserve EA
	char xdata * data pwrite;                // FLASH write pointer

	SFRPAGE = 0x00;                         // modify SFRPAGE = 0x00

	IE_EA = 0;                               // disable interrupts
											 // change clock speed to slow, then restore later
	VDM0CN = 0x80;                           // enable VDD monitor
	RSTSRC = 0x02;                       // enable VDD monitor as a reset source

	pwrite = (char xdata *) addr;
	if(Check_byte == 0x55)
	{
		FLKEY = 0xA5;                       // Key Sequence 1
		FLKEY = 0xF1;                       // Key Sequence 2
		PSCTL |= 0x01;                       // PSWE = 1
	}
	VDM0CN = 0x80;                           // enable VDD monitor
	RSTSRC = 0x02;                       // enable VDD monitor as a reset source

	*pwrite = byte;                          // write the byte

	PSCTL &= ~0x01;                          // PSWE = 0
	Check_byte = 0;

	IE_EA = EA_SAVE;                         // restore interrupts

	SFRPAGE = SFRPAGE_SAVE;                 // restore SFRPAGE_SAVE
}

//-----------------------------------------------------------------------------
// FLASH_ByteRead
//-----------------------------------------------------------------------------
unsigned char FLASH_ByteRead(unsigned int addr)
{
	unsigned char SFRPAGE_SAVE = SFRPAGE;
	bit EA_SAVE = IE_EA;                          // preserve EA

	char code * data pread;                       // FLASH read pointer
	unsigned char byte;

	IE_EA = 0;                                    // disable interrupts

	SFRPAGE = 0x00;                              // modify SFRPAGE = 0x00

	pread = (char code *) addr;
	byte = *pread;                                // read the byte

	IE_EA = EA_SAVE;                              // restore interrupts

	SFRPAGE = SFRPAGE_SAVE;                      // restore SFRPAGE_SAVE

	return byte;
}

//-----------------------------------------------------------------------------
// FLASH_PageErase
//-----------------------------------------------------------------------------
void FLASH_PageErase(unsigned int addr, char Check_byte)
{
	unsigned char SFRPAGE_SAVE = SFRPAGE;   // preserve SFRPAGE
	bit EA_SAVE = IE_EA;                     // preserve EA
	char xdata * data pwrite;                // FLASH write pointer

	IE_EA = 0;                               // Disable interrupts
											 // Change clock speed to slow, then restore later
	SFRPAGE = 0x00;                         // modify SFRPAGE = 0x00

	VDM0CN = 0x80;                           // Enable VDD monitor
	RSTSRC = 0x02;                       // Enable VDD monitor as a reset source
	pwrite = (char xdata *) addr;
	if (Check_byte == 0x55)
	{
		FLKEY = 0xA5;                       // Key Sequence 1
		FLKEY = 0xF1;                       // Key Sequence 2
		PSCTL |= 0x03;                       // PSWE = 1; PSEE = 1
	}
	VDM0CN = 0x80;                           // enable VDD monitor
	RSTSRC = 0x02;                       // enable VDD monitor as a reset source

	*pwrite = 0;                             // initiate page erase
	PSCTL &= ~0x03;                          // PSWE = 0; PSEE = 0
	Check_byte = 0;

	IE_EA = EA_SAVE;                         // restore interrupts
	SFRPAGE = SFRPAGE_SAVE;                 // restore SFRPAGE_SAVE
}

void Flash_EW_NoErase(unsigned int FlashAddress, unsigned char *Data_Buffer,unsigned int DataL)
{
	unsigned int Data_Count_L = 0;

	for (Data_Count_L = 0; Data_Count_L < DataL; Data_Count_L++)
		FLASH_ByteWrite((FlashAddress + Data_Count_L), *Data_Buffer++, 0x55);

}

void Flash_EWFunction(unsigned int FlashAddress, unsigned char *Data_Buffer,unsigned int DataL)
{
	unsigned int Data_Count_L = 0;
	FLASH_PageErase(FlashAddress, 0x55);

	for (Data_Count_L = 0; Data_Count_L < DataL; Data_Count_L++)
		FLASH_ByteWrite((FlashAddress + Data_Count_L), *Data_Buffer++, 0x55);

}

void Flash_ReadFunction(unsigned int FlashAddress, unsigned char *Data_Buffer,unsigned int DataL)
{
	unsigned int Data_Count_L = 0;
	for (Data_Count_L = 0; Data_Count_L < DataL; Data_Count_L++)
		*Data_Buffer++ = FLASH_ByteRead( FlashAddress + Data_Count_L);
}

void Flash_ReadBlock(unsigned char code *FlashAddress,const unsigned char *Data_Buffer, unsigned int DataL)
{
	unsigned int Data_Count_L = 0;
	char code *pread = (char code *)FlashAddress;
	uint8_t *u8DataAddr = Data_Buffer;
	bit EA_SAVE = IE_EA;
	IE_EA = 0;
	for( Data_Count_L=0 ; Data_Count_L<DataL ; Data_Count_L++ )
	{
		*u8DataAddr = *pread;
		u8DataAddr++;
		pread++;
	}
	IE_EA = EA_SAVE;
}





