/*
 * EFM8LB_ADC.c
 *
 *  Created on: 2017�~3��17��
 *      Author: 100476
 */

#include <SI_EFM8LB1_Register_Enums.h>
#include "EFM8LB_ADC.h"

//-----------------------------------------------------------------------------
// Global Constants
//-----------------------------------------------------------------------------
#define SCALE                     256L // Scale for temp calculations

#define TEMP_CAL_ADDRESS_LOW    0xFFD4 // Address in flash where the
                                       // temp cal low byte value is stored

#define TEMP_CAL_ADDRESS_HIGH   0xFFD5 // Address in flash where the
                                       // temp cal high byte value is stored



//#define ADC_14B      16383
//#define VREF         2.4

#define ADC_COV_100uV  1.46


unsigned int tempsensor_0c;
long ADC_Buffer;

// Calibration value for the temperature sensor at 0 degrees C, stored in CODE space
SI_LOCATED_VARIABLE_NO_INIT(TEMPSENSOR_0C_LOW, uint8_t const, SI_SEG_CODE, TEMP_CAL_ADDRESS_LOW);
SI_LOCATED_VARIABLE_NO_INIT(TEMPSENSOR_0C_HIGH, uint8_t const, SI_SEG_CODE, TEMP_CAL_ADDRESS_HIGH);

void Tempsonr_0C_internal_Cal()
{
	tempsensor_0c = ( (TEMPSENSOR_0C_HIGH << 8) | TEMPSENSOR_0C_LOW )*0.69;
}

unsigned int Get_ADC_Value(unsigned char CH , unsigned char Sampling)
{
	unsigned char SVAE_SFRPAGE;
	unsigned int Tcount;
	unsigned char IIcount;
	long ADC_Buffer = 0 ;
	unsigned int Update_Buffer;

	SVAE_SFRPAGE = SFRPAGE ;
	SFRPAGE = LEGACY_PAGE ;

	for(IIcount=0;IIcount<Sampling;IIcount++)
	{
		ADC0MX = CH;
		for(Tcount=0;Tcount<10;Tcount++);
		ADC0CN0_ADBUSY = 1 ;
		while( ADC0CN0_ADINT == 0 );

		ADC_Buffer = ADC_Buffer + ADC0 ;

		ADC0CN0_ADBUSY = 0 ;
		ADC0CN0_ADINT = 0 ;
	}

	Update_Buffer = ( ADC_Buffer / Sampling ) ;
	SFRPAGE = SVAE_SFRPAGE ;

	return Update_Buffer ;
}

int Get_Tempsensor()
{
	unsigned int Temp;
	int Tempsenor_Buffer;

	Temp = Get_ADC_Value( ADC0MX_ADC0MX__TEMP , 4 );
	Tempsenor_Buffer = ((int)( Temp - tempsensor_0c ) * SCALE ) / 19 ;

	return Tempsenor_Buffer;
}

unsigned int ADCV_Conversion_Voltage(unsigned char ADC_CH)
{
	unsigned int RETURN_Value;

	RETURN_Value = Get_ADC_Value( ADC_CH , 4 );
	RETURN_Value = ( RETURN_Value * ADC_COV_100uV );

	return RETURN_Value;
}







