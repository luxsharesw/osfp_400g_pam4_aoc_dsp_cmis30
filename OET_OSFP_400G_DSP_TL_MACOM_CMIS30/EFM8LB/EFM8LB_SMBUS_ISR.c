/*
 * EFM8LB_SMBUS_ISR.c
 *
 *  Created on: 2017�~10��23��
 *      Author: Ivan_Lin
 */

#include <string.h>
#include <SI_EFM8LB1_Register_Enums.h>
#include "EFM8LB_Flash_Map.h"
#include "EFM8LB_Flash_RW.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "MSA_QSFPDD.h"
#include "Master_I2C_GPIO.h"

#include "MA38435_TX1_TX4.h"
#include "MA38435_TX5_TX8.h"
#include "MA38434_RX1_RX4.h"
#include "MA38434_RX5_RX8.h"
#include "BRCM_DSP_1181.h"
#include "BRCM_DSP_59281.h"


//-----------------------------------------------------------------------------
// SMBUS0_ISR
//-----------------------------------------------------------------------------
#define  SMB_SRADD                0x20 // (SR) slave address received
                                       //    (also could be a lost
                                       //    arbitration)
#define  SMB_SRSTO                0x10 // (SR) STOP detected while SR or ST,
                                       //    or lost arbitration
#define  SMB_SRDB                 0x00 // (SR) data byte received, or
                                       //    lost arbitration
#define  SMB_STDB                 0x40 // (ST) data byte transmitted
#define  SMB_STSTO                0x50 // (ST) STOP detected during a
                                       //    transaction; bus error

//-----------------------------------------------------------------------------
// ID Debug Info table define
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// ID Debug Info table define
//-----------------------------------------------------------------------------
xdata unsigned char ID_Debug_Info[9] =
{
    'S','L','B','F','6','4','Q','3','2',     // MCU                         // 00,01,02,03,04,05,06,07,08
};


#define ID_Debug_Info_SIZE sizeof(ID_Debug_Info)

//-----------------------------------------------------------------------------
// Page Select define
//-----------------------------------------------------------------------------
#define MA38435_T14  	   0x82
#define MA38435_T58        0x84

#define BRCM_11181		   0x85
#define BRCM_59281_LS_PHY0 0x86
#define BRCM_59281_SS_PHY0 0x87

#define MA38434_R14        0x8A
#define MA38434_R58        0x8B

#define BRCM_59281_LS_PHY1 0x8C
#define BRCM_59281_SS_PHY1 0x8E

#define Calibration        0x90
#define CTLE_EM            0x91
#define Calibration_1      0x92

#define ID_Debug       	   0xB0
#define BL_MODE            0xD0

#define Page0              0x00
#define Page1              0x01
#define Page2              0x02
#define Page3              0x03

#define Page10             0x10
#define Page11             0x11

//-----------------------------------------------------------------------------
// SMBUS0_ISR
//-----------------------------------------------------------------------------

// I2C Buffer
#define Slave_A0    0xA0

/*
// V0100 OLD PW
#define Leve1_PS0   0xA6
#define Leve1_PS1   0x06
#define Leve1_PS2   0x4C
#define Leve1_PS3   0x6E

#define Leve2_PS0   0xE0
#define Leve2_PS1   0x55
#define Leve2_PS2   0x3E
#define Leve2_PS3   0x8A
*/

// Luxshare-ICT Password
#define Leve1_PS0   0x93
#define Leve1_PS1   0x78
#define Leve1_PS2   0xCC
#define Leve1_PS3   0xAE

#define Leve2_PS0   0x3A
#define Leve2_PS1   0x18
#define Leve2_PS2   0xB6
#define Leve2_PS3   0x6F

xdata unsigned char I2C_Buffer[129]={0};

// I2C Temp buffer
xdata unsigned char MemAddress = 0 ;
xdata unsigned char Device_Address = 0 ;
xdata unsigned char Get_Data_Count = 0 ;
xdata unsigned char HOSTPASSWROD = 0;
xdata unsigned char I2C_READY_FLG = 0 ;
xdata unsigned char DATA_LEGHT ;
xdata unsigned char UPDATE_FLAG;
xdata unsigned char I2C_old_byte_buffer;

xdata unsigned char CTLE_PRE_POST_AMP_WRITE_EN = 0 ;
xdata unsigned char Page10_Write_flag = 0 ;
//-----------------------------------------------------------------------------
// Update SRAM Data Value
//-----------------------------------------------------------------------------
void PassWord_Function()
{
	if ( ( QSFPDD_A0[122] == Leve1_PS0 ) &&
		 ( QSFPDD_A0[123] == Leve1_PS1 ) &&
		 ( QSFPDD_A0[124] == Leve1_PS2 ) &&
		 ( QSFPDD_A0[125] == Leve1_PS3 ) )
		HOSTPASSWROD = 1;
	else if(( QSFPDD_A0[122] == Leve2_PS0 ) &&
		    ( QSFPDD_A0[123] == Leve2_PS1 ) &&
		    ( QSFPDD_A0[124] == Leve2_PS2 ) &&
		    ( QSFPDD_A0[125] == Leve2_PS3 ) )
		HOSTPASSWROD = 2;
	else
		HOSTPASSWROD = 0;
}

//-----------------------------------------------------------------------------
// Update SRAM Data Value N74C_S1S2_OTP
//-----------------------------------------------------------------------------
void Update_SRAM_Data()
{
	switch(QSFPDD_A0[0x7F])
	{
	/*	case Page0 :
					Flash_ReadFunction( FS_QSFPDD_P0 , &QSFPDD_A0[128],128);
					QSFPDD_A0[248] = CHECKSUM >> 8 ;
					QSFPDD_A0[249] = CHECKSUM ;

					QSFPDD_A0[250] = MCU_FW_VERSION >> 8 ;
					QSFPDD_A0[251] = MCU_FW_VERSION ;
					break;
		case Page1 :
					Flash_ReadFunction( FS_QSFPDD_P1 , &QSFPDD_A0[128],128);
					break;

		case Page2 :
					memcpy(  &QSFPDD_A0[128] , &QSFPDD_Page2[0] , 128 );
					break;

		case Page3 :
					Flash_ReadFunction( FS_QSFPDD_P3 , &QSFPDD_A0[128],128);
					break;

		case Page10 :
					memcpy(  &QSFPDD_A0[128] , &QSFPDD_Page10[0] , 128 );
					break;

		case Page11 :
					memcpy(  &QSFPDD_A0[128] , &QSFPDD_Page11[0] , 128 );
					break;
*/

		case MA38435_T14 :
					if( HOSTPASSWROD >= 1 )
					{
						MALD38435_TX1_TX4_READ_P82();
						memcpy(  &QSFPDD_A0[128] , &MATA38435_TX1_TX4_MEMORY_MAP.CHIP_ID , 128 );
					}
					break;

		case MA38435_T58 :
					if( HOSTPASSWROD >= 1 )
					{
						MALD38435_TX5_TX8_READ_P84();
						memcpy(  &QSFPDD_A0[128] , &MATA38435_TX5_TX8_MEMORY_MAP.CHIP_ID , 128 );
					}
					break;

		case BRCM_11181 :
					if( HOSTPASSWROD > 1 )
					{
						memcpy(  &QSFPDD_A0[128] , &BRCM_11181_DSP_MEMORY_MAP.BRCM_REG_ADDR , 128 );
					}
					break;

		case BRCM_59281_LS_PHY0 :
					if( HOSTPASSWROD >= 1 )
					{
						// Read DSP Data Value
						memcpy(  &QSFPDD_A0[128] , &BRCM_59281_DSP_LS_PHY0_MEMORY_MAP , 128 );
					}
					break;

		case BRCM_59281_SS_PHY0 :
					if( HOSTPASSWROD >= 1 )
					{
						// Read DSP Data Value
						memcpy(  &QSFPDD_A0[128] , &BRCM_59281_DSP_SS_PHY0_MEMORY_MAP , 128 );
					}
					break;

		case MA38434_R14 :
					if( HOSTPASSWROD >= 1 )
					{
						MATA38434_P8A_READ();
						memcpy(  &QSFPDD_A0[128] , &MATA38434_RX1_RX4_TIA_MEMORY_MAP.CHIP_ID , 128 );
					}
					break;

		case MA38434_R58 :
					if( HOSTPASSWROD >= 1 )
					{
						MATA38434_P8B_READ();
						memcpy(  &QSFPDD_A0[128] , &MATA38434_RX5_RX8_TIA_MEMORY_MAP.CHIP_ID , 128 );
					}
					break;

		case BRCM_59281_LS_PHY1 :
					if( HOSTPASSWROD >= 1 )
					{
						// Read DSP Data Value
						memcpy(  &QSFPDD_A0[128] , &BRCM_59281_DSP_LS_PHY1_MEMORY_MAP , 128 );
					}
					break;

		case BRCM_59281_SS_PHY1 :
					if( HOSTPASSWROD >= 1 )
					{
						// Read DSP Data Value
						memcpy(  &QSFPDD_A0[128] , &BRCM_59281_DSP_SS_PHY1_MEMORY_MAP , 128 );
					}
					break;

		case Calibration :

					if( HOSTPASSWROD > 1 )
					{
						CALIB_MEMORY_MAP.FW_VERSION = MCU_FW_VERSION ;
						memcpy( &QSFPDD_A0[128] , &CALIB_MEMORY_MAP, 128);
					}
					else
						memset( &QSFPDD_A0[128] , 0xff , 128 ) ;

					break;

		case Calibration_1 :
					if( HOSTPASSWROD > 1 )
					{
						memcpy( &QSFPDD_A0[128] , &CALIB_MEMORY_1_MAP, 128);
					}
					else
						memset( &QSFPDD_A0[128] , 0xff , 128 ) ;


					break;

		case CTLE_EM :
					if( HOSTPASSWROD > 1 )
					{
						Flash_ReadFunction( MSA_Option_Table , &QSFPDD_A0[128], 128 );
					}
					break;
		case ID_Debug :
					if( HOSTPASSWROD > 1 )
					{
						memcpy( &QSFPDD_A0[128] , &ID_Debug_Info , 9 );
				    	Flash_ReadFunction( ( Product_Line+9 ) , &QSFPDD_A0[137], 119 );
					}
					else
						memset( &QSFPDD_A0[128] , 0xff , 128 ) ;

					break;
		default :
			     break;
	}
}

//-----------------------------------------------------------------------------
// Write SRAM to Flash Value
//-----------------------------------------------------------------------------

void Write_SRAM_TO_Flash()
{
	unsigned char SFRPAGE_SAVE = SFRPAGE;   // preserve SFRPAGE
	xdata unsigned char Temp_Buffer[128];
	switch(QSFPDD_A0[0x7F])
	{
		case Page0 :
					if( HOSTPASSWROD >= 1 )
					{
						Flash_EWFunction( FS_QSFPDD_A0 , &QSFPDD_A0[0] , 128 );
						Flash_EW_NoErase( FS_QSFPDD_P0 , &QSFPDD_A0[128] , 128 );
					}
					break;
		case Page1 :
					if( HOSTPASSWROD >= 1 )
					{
						Flash_EWFunction( FS_QSFPDD_P1 , &QSFPDD_A0[128] , 128 );
						memcpy(  &QSFPDD_Page1[0] , &QSFPDD_A0[128] , 128 );
						Flash_EW_NoErase( FS_QSFPDD_P2 , &QSFPDD_Page2[0] , 128 );
					}
					break;

		case Page2 :
					if( HOSTPASSWROD >= 1 )
					{
						Flash_EWFunction( FS_QSFPDD_P1 , &QSFPDD_Page1[0] , 128 );
						Flash_EW_NoErase( FS_QSFPDD_P2 , &QSFPDD_A0[128]  , 128 );
						memcpy(  &QSFPDD_Page2[0] , &QSFPDD_A0[128] , 128 );
					}

					break;

		case Page10 :
					Page10_Write_flag = 1 ;
					memcpy(  &QSFPDD_Page10[0] , &QSFPDD_A0[128] , 128 );
					break;

		case Page11 :
					 break;

		case MA38435_T14 :
					if( HOSTPASSWROD > 1 )
					{
						if( MemAddress >= 0xF8 )
						{
							if( QSFPDD_A0[255] == 0x01 )
							{
								MATA38435_TX1_TX4_MEMORY_MAP.Direct_AD      = QSFPDD_A0[248] ;
								MATA38435_TX1_TX4_MEMORY_MAP.Direct_Data[0] = QSFPDD_A0[249] ;
								MATA38435_TX1_TX4_MEMORY_MAP.Direct_RW      = QSFPDD_A0[254] ;

								if( MATA38435_TX1_TX4_MEMORY_MAP.Direct_RW == 1 )
									MATA38435_TX1_TX4_MEMORY_MAP.Direct_Data[0] = Master_I2C_ReadByte( MALD38435_SADR , MATA38435_TX1_TX4_MEMORY_MAP.Direct_AD , Master_I2C2 );
								else if( MATA38435_TX1_TX4_MEMORY_MAP.Direct_RW == 0 )
									Master_I2C_WriteByte( MALD38435_SADR , MATA38435_TX1_TX4_MEMORY_MAP.Direct_AD , MATA38435_TX1_TX4_MEMORY_MAP.Direct_Data[0] , Master_I2C2 );
								QSFPDD_A0[255] = 0x00;
							}
						}
						else
						{
							Flash_EWFunction( FS_MA38435_T14_P82 , &QSFPDD_A0[128] , 128 );
							memcpy( &MATA38435_TX1_TX4_MEMORY_MAP.CHIP_ID , &QSFPDD_A0[128] , 128);
							Flash_EW_NoErase( FS_MA38435_T58_P84 , &MATA38435_TX5_TX8_MEMORY_MAP.CHIP_ID , 128 );
							MALD38435_TX1_TX4_WRITE_P82();
						}
					}
					break;

		case MA38435_T58 :
					if( HOSTPASSWROD > 1 )
					{
						if( MemAddress >= 0xF8 )
						{
							if( QSFPDD_A0[255] == 0x01 )
							{
								MATA38435_TX5_TX8_MEMORY_MAP.Direct_AD      = QSFPDD_A0[248] ;
								MATA38435_TX5_TX8_MEMORY_MAP.Direct_Data[0] = QSFPDD_A0[249] ;
								MATA38435_TX5_TX8_MEMORY_MAP.Direct_RW      = QSFPDD_A0[254] ;

								if( MATA38435_TX5_TX8_MEMORY_MAP.Direct_RW == 1 )
									MATA38435_TX5_TX8_MEMORY_MAP.Direct_Data[0] = Master_I2C_ReadByte( MALD38435_SADR , MATA38435_TX5_TX8_MEMORY_MAP.Direct_AD , Master_I2C1 );
								else if( MATA38435_TX5_TX8_MEMORY_MAP.Direct_RW == 0 )
									Master_I2C_WriteByte( MALD38435_SADR, MATA38435_TX5_TX8_MEMORY_MAP.Direct_AD , MATA38435_TX5_TX8_MEMORY_MAP.Direct_Data[0] , Master_I2C1 );
								QSFPDD_A0[255] = 0x00;
							}
						}
						else
						{
							Flash_EWFunction( FS_MA38435_T14_P82 , &MATA38435_TX1_TX4_MEMORY_MAP.CHIP_ID , 128 );
							Flash_EW_NoErase( FS_MA38435_T58_P84 , &QSFPDD_A0[128] , 128 );
							memcpy( &MATA38435_TX5_TX8_MEMORY_MAP.CHIP_ID , &QSFPDD_A0[128], 128 );
							MALD38435_TX5_TX8_WRITE_P84();
						}

					}
					break;

		case BRCM_11181 :
					if( HOSTPASSWROD > 1 )
					{
						memcpy(  &BRCM_11181_DSP_MEMORY_MAP.BRCM_REG_ADDR , &QSFPDD_A0[128] , 128 );
					}

					break ;

		case BRCM_59281_LS_PHY0 :
					if( HOSTPASSWROD > 1 )
					{
						// Line-side save flash form QSFPDD_A0 byte 128 - 256
						Flash_EWFunction( FS_59281PHY0_LS_P86 , &QSFPDD_A0[128] , 128 );
						memcpy(  &BRCM_59281_DSP_LS_PHY0_MEMORY_MAP , &QSFPDD_A0[128] , 128 );
						// System-side save flash from Sram  BRCM_59281_DSP_SS_PHY0_MEMORY_MAP
						memcpy(  &Temp_Buffer , &BRCM_59281_DSP_SS_PHY0_MEMORY_MAP , 128 );
						Flash_EW_NoErase( FS_59281PHY0_SS_P87 , &Temp_Buffer, 128 );
					}
					break;

		case BRCM_59281_SS_PHY0 :
					if( HOSTPASSWROD > 1 )
					{
						// Line-side save flash form sram BRCM_59281_DSP_LS_PHY0_MEMORY_MAP
						memcpy(  &Temp_Buffer , &BRCM_59281_DSP_LS_PHY0_MEMORY_MAP , 128 );
						Flash_EWFunction( FS_59281PHY0_LS_P86 , &Temp_Buffer , 128 );
						// System-side save flash form sram QSFPDD_A0
						Flash_EW_NoErase( FS_59281PHY0_SS_P87 , &QSFPDD_A0[128], 128 );
						memcpy(  &BRCM_59281_DSP_SS_PHY0_MEMORY_MAP , &QSFPDD_A0[128] , 128 );

					}
					break;

		case MA38434_R14 :
					if( HOSTPASSWROD > 1 )
					{
						if( MemAddress >= 0xF8 )
						{
							if( QSFPDD_A0[255] == 0x01 )
							{
								MATA38434_RX1_RX4_TIA_MEMORY_MAP.Direct_AD      = QSFPDD_A0[248] ;
								MATA38434_RX1_RX4_TIA_MEMORY_MAP.Direct_Data[0] = QSFPDD_A0[249] ;
								MATA38434_RX1_RX4_TIA_MEMORY_MAP.Direct_RW      = QSFPDD_A0[254] ;

								if( MATA38434_RX1_RX4_TIA_MEMORY_MAP.Direct_RW == 1 )
								{
									MATA38434_RX1_RX4_SET_PAGE( QSFPDD_A0[250] );
									MATA38434_RX1_RX4_TIA_MEMORY_MAP.Direct_Data[0] = Master_I2C_ReadByte( M38434_SLAVE_AD , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Direct_AD , Master_I2C1 );
								}
								else if( MATA38434_RX1_RX4_TIA_MEMORY_MAP.Direct_RW == 0 )
								{
									MATA38434_RX1_RX4_SET_PAGE( QSFPDD_A0[250] );
									Master_I2C_WriteByte( M38434_SLAVE_AD, MATA38434_RX1_RX4_TIA_MEMORY_MAP.Direct_AD , MATA38434_RX1_RX4_TIA_MEMORY_MAP.Direct_Data[0] , Master_I2C1 );
								}
								QSFPDD_A0[255] = 0x00;
							}
						}
						else
						{
							Flash_EWFunction( FS_MA38434_R14_P8A , &QSFPDD_A0[128] , 128 );
							memcpy( &MATA38434_RX1_RX4_TIA_MEMORY_MAP , &QSFPDD_A0[128], 128 );
							Flash_EW_NoErase( FS_MA38434_R58_P8B , &MATA38434_RX5_RX8_TIA_MEMORY_MAP.CHIP_ID , 128 );
							MATA38434_P8A_Write();
						}
					}
					break;

		case MA38434_R58 :
					if( HOSTPASSWROD > 1 )
					{
						if( MemAddress >= 0xF8 )
						{
							if( QSFPDD_A0[255] == 0x01 )
							{
								MATA38434_RX5_RX8_TIA_MEMORY_MAP.Direct_AD      = QSFPDD_A0[248] ;
								MATA38434_RX5_RX8_TIA_MEMORY_MAP.Direct_Data[0] = QSFPDD_A0[249] ;
								MATA38434_RX5_RX8_TIA_MEMORY_MAP.Direct_RW      = QSFPDD_A0[254] ;

								if( MATA38434_RX5_RX8_TIA_MEMORY_MAP.Direct_RW == 1 )
								{
									MATA38434_RX5_RX8_SET_PAGE( QSFPDD_A0[250] );
									MATA38434_RX5_RX8_TIA_MEMORY_MAP.Direct_Data[0] = Master_I2C_ReadByte( M38434_SLAVE_AD , MATA38434_RX5_RX8_TIA_MEMORY_MAP.Direct_AD , Master_I2C2 );
								}
								else if( MATA38434_RX5_RX8_TIA_MEMORY_MAP.Direct_RW == 0 )
								{
									MATA38434_RX5_RX8_SET_PAGE( QSFPDD_A0[250] );
									Master_I2C_WriteByte( M38434_SLAVE_AD, MATA38434_RX5_RX8_TIA_MEMORY_MAP.Direct_AD , MATA38434_RX5_RX8_TIA_MEMORY_MAP.Direct_Data[0] , Master_I2C2 );
								}
								QSFPDD_A0[255] = 0x00;
							}
						}
						else
						{
							Flash_EWFunction( FS_MA38434_R14_P8A , &MATA38434_RX1_RX4_TIA_MEMORY_MAP.CHIP_ID , 128 );

							Flash_EW_NoErase( FS_MA38434_R58_P8B , &QSFPDD_A0[128] , 128 );
							memcpy( &MATA38434_RX5_RX8_TIA_MEMORY_MAP , &QSFPDD_A0[128], 128 );
							MATA38434_P8B_Write();
						}
					}
					break;

		case BRCM_59281_LS_PHY1 :
					if( HOSTPASSWROD > 1 )
					{
						// Line-side save flash form QSFPDD_cA0 byte 128 - 256
						Flash_EWFunction( FS_59281PHY1_LS_P8C , &QSFPDD_A0[128] , 128 );
						memcpy(  &BRCM_59281_DSP_LS_PHY1_MEMORY_MAP , &QSFPDD_A0[128] , 128 );
						// System-side save flash from Sram  BRCM_59281_DSP_SS_PHY0_MEMORY_MAP
						memcpy(  &Temp_Buffer , &BRCM_59281_DSP_SS_PHY1_MEMORY_MAP , 128 );
						Flash_EW_NoErase( FS_59281PHY1_SS_P8E , &Temp_Buffer, 128 );
					}
					break;

		case BRCM_59281_SS_PHY1 :
					if( HOSTPASSWROD > 1 )
					{
						// Line-side save flash form sram BRCM_59281_DSP_LS_PHY0_MEMORY_MAP
						memcpy(  &Temp_Buffer , &BRCM_59281_DSP_LS_PHY1_MEMORY_MAP , 128 );
						Flash_EWFunction( FS_59281PHY1_LS_P8C , &Temp_Buffer , 128 );
						// System-side save flash form sram QSFPDD_A0
						Flash_EW_NoErase( FS_59281PHY1_SS_P8E , &QSFPDD_A0[128], 128 );
						memcpy(  &BRCM_59281_DSP_SS_PHY1_MEMORY_MAP , &QSFPDD_A0[128] , 128 );

					}
					break;

		case Calibration :
					if( HOSTPASSWROD > 1 )
					{
						memcpy( &QSFPDD_A0[248] , &CALIB_MEMORY_MAP.CHECKSUM_V, 8);
						Flash_EWFunction( Calibration_Table , &QSFPDD_A0[128] , 128 );
						Flash_EW_NoErase( Cablbration_Table2 , &CALIB_MEMORY_1_MAP.VCC1_SCALEM , 128 );
						Flash_ReadFunction( Calibration_Table , &CALIB_MEMORY_MAP.VCC_SCALEM ,128 );
					}
					break ;

		case Calibration_1 :
					if( HOSTPASSWROD > 1 )
					{
						Flash_EWFunction( Calibration_Table  , &CALIB_MEMORY_MAP.VCC_SCALEM , 128 );
						Flash_EW_NoErase( Cablbration_Table2 , &QSFPDD_A0[128] , 128 );
						memcpy( &CALIB_MEMORY_1_MAP.VCC1_SCALEM , &QSFPDD_A0[128] , 128);
					}
					break ;

	    case CTLE_EM :
					if( HOSTPASSWROD > 1 )
						Flash_EWFunction( MSA_Option_Table ,&QSFPDD_A0[128], 128 );
					break ;


		case ID_Debug :
					if( HOSTPASSWROD > 1 )
						Flash_EWFunction( Product_Line , &QSFPDD_A0[128] , 128 );
					break;
		case BL_MODE :
					if( HOSTPASSWROD > 1 )
					{
						if( MemAddress == 0x24 )
						{
							if( (I2C_Buffer[1]==0x11) && (I2C_Buffer[2]==0x22) && (I2C_Buffer[3]==0x33) &&
								(I2C_Buffer[4]==0x55) && (I2C_Buffer[5]==0x66) && (I2C_Buffer[6]==0x55)	)
							{
								// Write R0 and issue a software reset
								*((uint8_t SI_SEG_DATA *)0x00) = 0xA5;
								SFRPAGE = 0x00;
								RSTSRC = RSTSRC_SWRSF__SET | RSTSRC_PORSF__SET;
								SFRPAGE = SFRPAGE_SAVE;
							}
						}
					}
					break;
		default :
			     break;
	}
}

//-----------------------------------------------------------------------------
// SMBUS0_ISR
//-----------------------------------------------------------------------------
// SMBUS0 ISR Content goes here. Remember to clear flag bits:
// SMB0CN0::SI (SMBus Interrupt Flag)
//-----------------------------------------------------------------------------
SI_INTERRUPT (SMBUS0_ISR, SMBUS0_IRQn)
{
    unsigned int Data_count = 0;
	if (SMB0CN0_ARBLOST == 0)
	{

		switch (SMB0CN0 & 0xF0)          // Decode the SMBus status vector
		{
		    // Slave Receiver: Start+Address received
			case  SMB_SRADD:

		          SMB0CN0_STA = 0;           // Clear SMB0CN0_STA bit
		          Device_Address = SMB0DAT ;
		          if( ( Device_Address & 0xFE ) == ( Slave_A0 & 0xFE ) )
		          {
		        	  if( Device_Address & 0x01 == 0x01 )
		        	  {
		        		  if( MemAddress < 128 )
		        		  {
		        			  if(( MemAddress > 117 ) && ( MemAddress < 127 ))
		        			      QSFPDD_A0[MemAddress] = 0x00;

		        			  SMB0DAT = QSFPDD_A0[MemAddress];

		        			  if( MemAddress == 8 )
		        				  Clear_Module_state_Byte8();
		        			  else if( MemAddress == 9 )
							      Clear_VCC_TEMP_Flag();
		        		  }
		        		  else
		        		  {
		        			  if( QSFPDD_A0[127] == 0x01 )
		        				  SMB0DAT = QSFPDD_Page1[ ( MemAddress - 128 )];
		        			  else if( QSFPDD_A0[127] == 0x02 )
		        			  	  SMB0DAT = QSFPDD_Page2[ ( MemAddress - 128 )];
		        			  else if( QSFPDD_A0[127] == 0x10 )
		        				  SMB0DAT = QSFPDD_Page10[ ( MemAddress - 128 )];
		        			  else if( QSFPDD_A0[127] == 0x11 )
							  {
		        				  SMB0DAT = QSFPDD_Page11[ ( MemAddress - 128 )];
								  if( ( MemAddress >= 135 ) && ( MemAddress <= 152 ) )
								      Clear_Flag( MemAddress-128 );
							  }
							  else
								  SMB0DAT = QSFPDD_A0[MemAddress];
		        		  }

		        	  }
		        	  SMB0CN0_ACK = 1 ;
		          }
		          else
		        	  SMB0CN0_ACK = 0;        // NACK received address

		          break;

		         // Slave Receiver: Data received
		    case  SMB_SRDB:

		    	  I2C_Buffer[Get_Data_Count] =  SMB0DAT ;
		    	  MemAddress = I2C_Buffer[0] ;
		    	  Get_Data_Count++;
		    	  SMB0CN0_ACK = 1;

		          break;

		         // Slave Receiver: Stop received while either a Slave Receiver or
		         // Slave Transmitter
		    case  SMB_SRSTO:

		       	  SMB0CN0_STO = 0;           // SMB0CN0_STO must be cleared by software when
		       	                             // a STOP is detected as a slave
		       	  if(Get_Data_Count>=2)                   // I2C Write Data in
		       	  {
					  I2C_READY_FLG = 1 ;
					  DATA_LEGHT = ( Get_Data_Count - 1 ) ;

					  if( ( MemAddress == 0x7F ) && ( Get_Data_Count == 0x02 ) )
					  {
						  QSFPDD_A0[127]=I2C_Buffer[1];
						  if(QSFPDD_A0[127]==0x00)
						  {
								Flash_ReadFunction( FS_QSFPDD_P0 , &QSFPDD_A0[128],128);

								UPDATE_FLAG = 0 ;
						  }
						  else if(QSFPDD_A0[127]==0x01)
						  {
							  memcpy(  &QSFPDD_A0[128] , &QSFPDD_Page1[0] , 128 );
							  UPDATE_FLAG = 0 ;
						  }
						  else if(QSFPDD_A0[127]==0x02)
						  {
							  memcpy(  &QSFPDD_A0[128] , &QSFPDD_Page2[0] , 128 );
							  UPDATE_FLAG = 0 ;
						  }
						  else if(QSFPDD_A0[127]==0x10)
						  {
							  memcpy(  &QSFPDD_A0[128] , &QSFPDD_Page10[0] , 128 );
							  UPDATE_FLAG = 0 ;
						  }
						  else if(QSFPDD_A0[127]==0x11)
						  {
							  memcpy(  &QSFPDD_A0[128] , &QSFPDD_Page11[0] , 128 );
							  UPDATE_FLAG = 0 ;
						  }
						  else
							  UPDATE_FLAG = 1;
					  }
					  else if( ( MemAddress >= 122 ) && ( MemAddress <= 125 ))
						  UPDATE_FLAG = 5;
					  else
					  {
						  if( ( MemAddress + DATA_LEGHT ) > 256 )
						      DATA_LEGHT = ( 256 - MemAddress );

						  if( HOSTPASSWROD >= 1 )
						  {
							  memcpy(&QSFPDD_A0[MemAddress], &I2C_Buffer[1], DATA_LEGHT);
						  }
						  else
						  {
							  if(MemAddress==26)
								  QSFPDD_A0[MemAddress] = I2C_Buffer[1];
							  else if(MemAddress==31)
								  QSFPDD_A0[MemAddress] = ( I2C_Buffer[1] & 0x01 );
							  else if(MemAddress==32)
								  QSFPDD_A0[MemAddress] = I2C_Buffer[1];
							  else if(MemAddress==33)
								  QSFPDD_A0[MemAddress] = I2C_Buffer[1];
							  else if(MemAddress==34)
								  QSFPDD_A0[MemAddress] = I2C_Buffer[1];
						  }

						  if(QSFPDD_A0[127]==0x10)
						  {
							  if(MemAddress>127)
							  {
								  memcpy(&QSFPDD_A0[MemAddress], &I2C_Buffer[1], DATA_LEGHT);
								  Page10_Write_flag = 1 ;
								  memcpy(  &QSFPDD_Page10[0] , &QSFPDD_A0[128] , 128 );
							  }
						  }
						  else
							  UPDATE_FLAG = 7 ;
					  }
		       	   }
		       	   Get_Data_Count = 0 ;

		          break;

		         // Slave Transmitter: Data byte transmitted
		    case  SMB_STDB:

		       	  if( SMB0CN0_ACK == 1 )
		          {
					  MemAddress ++ ;
					  if( MemAddress < 128 )
					  {
						  if(( MemAddress > 117 ) && ( MemAddress < 127 ))
							  QSFPDD_A0[MemAddress] = 0x00;

						  SMB0DAT = QSFPDD_A0[MemAddress];

	        			  if( MemAddress == 8 )
	        				  Clear_Module_state_Byte8();
	        			  else if( MemAddress == 9 )
						      Clear_VCC_TEMP_Flag();
					  }
					  else
					  {
						  if( QSFPDD_A0[127] == 0x01 )
							  SMB0DAT = QSFPDD_Page1[ ( MemAddress - 128 )];
						  else if( QSFPDD_A0[127] == 0x02 )
							  SMB0DAT = QSFPDD_Page2[ ( MemAddress - 128 )];
	        			  else if( QSFPDD_A0[127] == 0x10 )
	        				  SMB0DAT = QSFPDD_Page10[ ( MemAddress - 128 )];
	        			  else if( QSFPDD_A0[127] == 0x11 )
						  {
							  SMB0DAT = QSFPDD_Page11[ ( MemAddress - 128 )];
							  if( ( MemAddress >= 135 ) && ( MemAddress <= 152 ) )
							      Clear_Flag( MemAddress-128 );
						  }
						  else
							  SMB0DAT = QSFPDD_A0[MemAddress];
					  }

		          }
		          break;
		     case  SMB_STSTO:

		           SMB0CN0_STO = 0;           // SMB0CN0_STO must be cleared by software when
		                                       // a STOP is detected as a slave
		           break;

		         // Default: all other cases undefined
		     default:

		            SMB0CF &= ~0x80;           // Reset communication
		            SMB0CF |= 0x80;
		            SMB0CN0_STA = 0;
		            SMB0CN0_STO = 0;
		            SMB0CN0_ACK = 0;
		            break;
		}

	}
		   // SMB0CN0_ARBLOST = 1, Abort failed transfer
    else
	{
    	SMB0CN0_STA = 0;
		SMB0CN0_STO = 0;
		SMB0CN0_ACK = 0;
	}

	SMB0CN0_SI = 0;                     // Clear SMBus interrupt flag
}


//-----------------------------------------------------------------------------
// TIMER3_ISR
//-----------------------------------------------------------------------------
//
// TIMER3 ISR Content goes here. Remember to clear flag bits:
// TMR3CN0::TF3H (Timer # High Byte Overflow Flag)
// TMR3CN0::TF3L (Timer # Low Byte Overflow Flag)
//
//-----------------------------------------------------------------------------
SI_INTERRUPT (TIMER3_ISR, TIMER3_IRQn)
{
	SMB0CF  &= ~0x80;                   // Disable SMBus
	SMB0CF  |=  0x80;                   // Re-enable SMBus
	TMR3CN0 &= ~0x80;                   // Clear Timer3 interrupt-pending flag
}

void SRMA_Flash_Function()
{
	unsigned int Data_count ;

	if( I2C_READY_FLG==1 )
	{
		switch(UPDATE_FLAG)
		{
			case 1 :
				    QSFPDD_A0[127]=I2C_Buffer[1];
				    Update_SRAM_Data();
					break ;

			case 5 :
					for( Data_count=0 ; Data_count < DATA_LEGHT ; Data_count++ )
						QSFPDD_A0[ ( MemAddress + Data_count ) ] = I2C_Buffer[ ( Data_count + 1 ) ];
					PassWord_Function();
					break ;

			case 7 :
					/*if( ( MemAddress + DATA_LEGHT ) > 256 )
						DATA_LEGHT = ( 256 - MemAddress );
					memcpy(&QSFPDD_A0[MemAddress], &I2C_Buffer[1], DATA_LEGHT);*/
					Write_SRAM_TO_Flash();
					break ;
			default :
				break ;
		}

		I2C_READY_FLG = 0 ;
	}

}




