/*
 * Master_I2C_GPIO.c
 *
 *  Created on: 2019�~6��19��
 *      Author: Ivan_Lin
 */


#include <SI_EFM8LB1_Register_Enums.h>                  // SFR declarations
#include "Master_I2C_GPIO.h"



//-------------------------------------//
// I2C 1
//-------------------------------------//
SI_SBIT( Master_SDA1 , SFR_P1, 0 );
SI_SBIT( Master_SCL1 , SFR_P1, 1 );
//-------------------------------------//
// I2C 2
//-------------------------------------//
SI_SBIT( Master_SDA2 , SFR_P3, 2 );
SI_SBIT( Master_SCL2 , SFR_P3, 3 );

//-----------------------------------------------------------------------------------//
//Delay Function
//-----------------------------------------------------------------------------------//
void Delay_1us(void)
{
	int i ;
    for(i=0;i<2;i++);
}

void Delay_us(unsigned char time_us)
{
	int i;
	for(i=0;i<time_us;i++)
		Delay_1us();
}

void Delay_1ms(void)
{
	int i ;
    for(i=0; i < 6000; i++)
    	_nop_();
}
void Delay_ms(int time_ms)
{
	int i;
	for(i = 0; i < time_ms; i++)
		Delay_1ms();
}
//-----------------------------------------------------------------------------------//
// I2C ALL CH SDA / SCL High Low Setting
//-----------------------------------------------------------------------------------//
void I2C_SDA_SCL_SET(unsigned char HighLow , unsigned char I2C_CH )
{
	if( I2C_CH == Master_I2C1 )
	{
		Master_SCL1 = HighLow;
		Master_SDA1 = HighLow;
	}
	else if( I2C_CH == Master_I2C2 )
	{
		Master_SCL2 = HighLow;
		Master_SDA2 = HighLow;
	}
}
void I2C_SDA_SET(unsigned char HighLow , unsigned char I2C_CH )
{
	if( I2C_CH == Master_I2C1 )
		Master_SDA1 = HighLow;
	else if( I2C_CH == Master_I2C2 )
		Master_SDA2 = HighLow;
}
void I2C_SCL_SET(unsigned char HighLow , unsigned char I2C_CH )
{
	if( I2C_CH == Master_I2C1 )
		Master_SCL1 = HighLow;
	else if( I2C_CH == Master_I2C2 )
		Master_SCL2 = HighLow;
}

unsigned char I2C_GET_SDA_Status( unsigned char I2C_CH )
{
	unsigned char buffer;

	if( I2C_CH == Master_I2C1 )
		buffer = Master_SDA1 ;
	else if( I2C_CH == Master_I2C2 )
		buffer = Master_SDA2 ;

	return buffer;
}
//-----------------------------------------------------------------------------------//
//MASTER UP I2C
//-----------------------------------------------------------------------------------//
void I2C_Idle(unsigned char I2C_CH)
{
	// SDA = High
	// SCL = High
	I2C_SDA_SCL_SET( High , I2C_CH );

	Delay_us(1);
}

void I2C_Start( unsigned char I2C_CH )
{
	Delay_1us();
	// SDA = Low
	I2C_SDA_SET( Low , I2C_CH );
	Delay_us(2);
}

void I2C_Stop(unsigned char I2C_CH )
{
	// SDA = Low
	I2C_SDA_SET( Low , I2C_CH );
	Delay_us(2);
	// SCL = High
	I2C_SCL_SET( High , I2C_CH );
	Delay_us(2);
	// SDA = High
	I2C_SDA_SET( High , I2C_CH );
	Delay_1us();
}

//-----------------------------------------------------------------------------------//
// I2C Read Byte
//-----------------------------------------------------------------------------------//
unsigned char I2C_ReadByte(unsigned char I2C_CH)
{
	bit buffer;
	char i;
	unsigned char Rec_Data=0;

	// SCL = Low ( Clock setting to Low )
	I2C_SCL_SET( Low , I2C_CH );

	Delay_1us();
	Delay_1us();
	Delay_1us();
	Delay_1us();

	for(i=7;i>=0;i--)
	{
		Delay_1us();
		// SCL = High ( Clock setting to High for Get SDA Data )
		I2C_SCL_SET( High , I2C_CH );

		Delay_1us();
		Delay_1us();
		// Get SDA is High or Low
		if( I2C_CH == Master_I2C1 )
			buffer = Master_SDA1 ;
		else if( I2C_CH == Master_I2C2 )
			buffer = Master_SDA2 ;

		Rec_Data |= ((unsigned char)(buffer) << i);
		//	Delay_1us();

		// SCL = Low ( Clock setting to Low )
		I2C_SCL_SET( Low , I2C_CH );

		Delay_1us();
	}
	return Rec_Data;
}
//-----------------------------------------------------------------------------------//
// I2C Write Byte
//-----------------------------------------------------------------------------------//
void I2C_WriteByte(unsigned char Reg_Data ,unsigned char I2C_CH)
{
	char i;
	unsigned char Temp_Data=Reg_Data;

	// SCL = Low ( Clock setting to Low )
	I2C_SCL_SET( Low , I2C_CH );

	Delay_1us();
	Delay_1us();
	Delay_1us();
	Delay_1us();

	for(i=7;i>=0;i--)
	{
		Temp_Data = ((Reg_Data >> i) & 0x01);
		if(Temp_Data)
		{
			// SDA = High
			I2C_SDA_SET( High , I2C_CH );
		}
		else
		{
			// SDA = Low
			I2C_SDA_SET( Low , I2C_CH );
		}
		//	Delay_1us();
		// SCL = High ( Clock setting )
		I2C_SCL_SET( High , I2C_CH );

		Delay_1us();
		Delay_1us();

		// SCL = Low ( Clock setting to Low )
		I2C_SCL_SET( Low , I2C_CH );

		Delay_1us();
	}
}

//-------------------------------------------------------//
// UP I2C nACK / ACK
// Master_SDA_UP
// Master_SCL_UP
// TX CH4 - CH7 MAOM38053 UP AFSI-N74C4S2
// RX CH0 - CH3 MASC38040 UP AFSI-R74C4S1
//-------------------------------------------------------//
void I2C_nACK_Master(unsigned char I2C_CH)
{
	Delay_1us();
	// SDA = High
	I2C_SDA_SET( High , I2C_CH );
	Delay_1us();
	// SCL = High
	I2C_SCL_SET( High , I2C_CH );
	Delay_1us();
	Delay_1us();
	// SCL = Low
	I2C_SCL_SET( Low , I2C_CH );
	Delay_1us();
	// SDA = Low
	I2C_SDA_SET( Low , I2C_CH );
	Delay_1us();
}

void I2C_ACK_Slave(unsigned char I2C_CH)
{
	Delay_1us();
	// SCL = High
	I2C_SCL_SET( High , I2C_CH );
	Delay_1us();
	Delay_1us();

	if( I2C_GET_SDA_Status( I2C_CH ) )
	{
		// SCL = Low
		I2C_SCL_SET( Low , I2C_CH );
		Delay_1us();
		// SDA = Low
		I2C_SDA_SET( Low , I2C_CH );
		Delay_1us();

	}
	else
	{
		// SCL = Low
		I2C_SCL_SET( Low , I2C_CH );
		Delay_1us();
		// SDA = High
		I2C_SDA_SET( High , I2C_CH );
		Delay_1us();

	}
}

void I2C_MRSlave_Ack(unsigned char I2C_CH)
{
	// SDA = Low
	I2C_SDA_SET( Low , I2C_CH );
	// SCL = High
	I2C_SCL_SET( High , I2C_CH );
	Delay_1us();
	Delay_1us();
	// SCL = Low
	I2C_SCL_SET( Low , I2C_CH );
	// SDA = High
	I2C_SDA_SET( High , I2C_CH );
}


void I2C_MRSlave_NAck(unsigned char I2C_CH)
{
	// SDA = High
	I2C_SDA_SET( High , I2C_CH );
	// SCL = High
	I2C_SCL_SET( High , I2C_CH );
	Delay_1us();
	Delay_1us();
	// SCL = Low
	I2C_SCL_SET( Low , I2C_CH );
}

//-------------------------------------------------------//
// Master I2C Read/Write Function
// I2C_Idle
// I2C_Start
// I2C_Stop
// I2C_ReadByte
// I2C_WriteByte
// I2C_nACK_Master
// I2C_ACK_Slave
// I2C_MRSlave_Ack
// I2C_MRSlave_NAck
//-------------------------------------------------------//
unsigned char Master_I2C_ReadByte(unsigned char Slave_address,unsigned char Regcommand_Address,unsigned char I2C_CH)
{
	unsigned char Temp_Data;
	I2C_Idle(I2C_CH);
	I2C_Start(I2C_CH);
	I2C_WriteByte(Slave_address,I2C_CH);		// Write
	I2C_ACK_Slave(I2C_CH);

	I2C_WriteByte(Regcommand_Address,I2C_CH);
	I2C_ACK_Slave(I2C_CH);
	// SCL = High
	I2C_SCL_SET( High , I2C_CH );
	Delay_1us();
	I2C_Idle(I2C_CH);

	I2C_Start(I2C_CH);
	I2C_WriteByte( ( Slave_address + 1 ),I2C_CH );		// Read
	I2C_ACK_Slave(I2C_CH);

	Temp_Data = I2C_ReadByte(I2C_CH);
	I2C_nACK_Master(I2C_CH);

	I2C_Stop(I2C_CH);
	I2C_Idle(I2C_CH);
	return Temp_Data;
}

void Master_I2C_WriteByte(unsigned char Slave_address,unsigned char Regcommand_Address,unsigned char SendData,unsigned char I2C_CH)
{
	I2C_Idle(I2C_CH);
	I2C_Start(I2C_CH);
	I2C_WriteByte(Slave_address,I2C_CH);		// Write
	I2C_ACK_Slave(I2C_CH);

	I2C_WriteByte(Regcommand_Address,I2C_CH);
	I2C_ACK_Slave(I2C_CH);

	I2C_WriteByte(SendData,I2C_CH);
	I2C_ACK_Slave(I2C_CH);
	I2C_Stop(I2C_CH);
	I2C_Idle(I2C_CH);
}

unsigned int  Master_I2C_Read_Two_Byte(unsigned char Slave_address,unsigned char Regcommand_Address,unsigned char I2C_CH)
{
	xdata unsigned char MSB_Vaule = 0 ;
	xdata unsigned char LSB_Vaule = 0 ;
	xdata unsigned int Temp_Data = 0 ;

	I2C_Idle(I2C_CH);
	I2C_Start(I2C_CH);
	I2C_WriteByte(Slave_address,I2C_CH);		// Write
	I2C_ACK_Slave(I2C_CH);

	I2C_WriteByte(Regcommand_Address,I2C_CH);
	I2C_ACK_Slave(I2C_CH);

	// SCL = High
	I2C_SCL_SET( High , I2C_CH );
	Delay_1us();
	I2C_Idle(I2C_CH);

	I2C_Start(I2C_CH);
	I2C_WriteByte( ( Slave_address + 1 ),I2C_CH );		// Read
	I2C_ACK_Slave(I2C_CH);

	MSB_Vaule = I2C_ReadByte(I2C_CH);
	I2C_MRSlave_Ack(I2C_CH);

	LSB_Vaule = I2C_ReadByte(I2C_CH);
	I2C_nACK_Master(I2C_CH);

	Temp_Data = (unsigned int)( MSB_Vaule << 4 ) + LSB_Vaule ;

	I2C_Stop(I2C_CH);
	I2C_Idle(I2C_CH);

	return Temp_Data;
}





