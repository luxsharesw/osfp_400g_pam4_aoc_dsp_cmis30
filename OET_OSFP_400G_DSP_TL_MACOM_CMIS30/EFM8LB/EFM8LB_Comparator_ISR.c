/*
 * EFM8LB_Comparator.c
 *
 *  Created on: 2019�~6��27��
 *      Author: Ivan_Lin
 */

#include <SI_EFM8LB1_Register_Enums.h>                  // SFR declarations

#define  HighPower  1
#define  LowPower   0

#define  Moudle_Reset  0
#define  Modele_Ready  1

xdata unsigned char Host_RESET = 1 ;
xdata unsigned char Power_mode_function = 0;

SI_INTERRUPT(CMP0_ISR, CMP0_IRQn)
{
	if(CMP0CN0&0x40)
		Power_mode_function = HighPower ;
	else
		Power_mode_function = LowPower ;
	//	CMP0CN0 &= 0xC0 ;
	//  bit5 CPRIF Must be cleared by firmware
	//  bit4 CPFIF Must be cleared by firmware
	CMP0CN0 &= ~0x30;
}

SI_INTERRUPT(CMP1_ISR, CMP1_IRQn)
{
	if(CMP1CN0&0x40)
		Host_RESET = Modele_Ready ;
	else
		Host_RESET = Moudle_Reset ;
	//	CMP0CN1 &= 0xC0 ;
	//  bit5 CPRIF Must be cleared by firmware
	//  bit4 CPFIF Must be cleared by firmware
	CMP1CN0 &= ~0x30;

}
