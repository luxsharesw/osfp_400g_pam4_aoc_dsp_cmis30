/*
 * QSFPDD_MSA_AW.c
 *
 *  Created on: 2017�~11��9��
 *      Author: Ivan_Lin
 */

#include <SI_EFM8LB1_Register_Enums.h>                  // SFR declarations
#include <stdint.h>
#include <Master_I2C_GPIO.h>
#include "MSA_QSFPDD.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "BRCM_DSP_59281.h"

xdata unsigned char Temperature_LATCH = 0;
xdata unsigned char VCC_Temperature_LATCH = 0;
xdata unsigned char TxP_HA_LATCH = 0;
xdata unsigned char TxP_LA_LATCH = 0;
xdata unsigned char TxP_HW_LATCH = 0;
xdata unsigned char TxP_LW_LATCH = 0;

xdata unsigned char BIAS_HA_LATCH = 0;
xdata unsigned char BIAS_LA_LATCH = 0;
xdata unsigned char BIAS_HW_LATCH = 0;
xdata unsigned char BIAS_LW_LATCH = 0;

xdata unsigned char RxP_HA_LATCH = 0;
xdata unsigned char RxP_LA_LATCH = 0;
xdata unsigned char RxP_HW_LATCH = 0;
xdata unsigned char RxP_LW_LATCH = 0;

unsigned int IntL_Status_TX = 0;
unsigned int IntL_Status_RX = 0;

SI_SBIT(IntL, SFR_P0, 2);

void QSFPDD_Temperature_VCC_AW()
{
	int T_Temp;
	int High_Alarm_Value, Low_Alarm_Value, High_Warning, Low_Warning;
	unsigned int VCC_Temp;
	unsigned int VCC_High_Alarm_Value, VCC_Low_Alarm_Value, VCC_High_Warning, VCC_Low_Warning;
	unsigned char LATCH = 0;
	unsigned char MASK_FLAG;

	T_Temp           = ((int)( QSFPDD_A0[14]   << 8 ) | QSFPDD_A0[15] );
	High_Alarm_Value = ((int)( QSFPDD_Page2[0] << 8 ) | QSFPDD_Page2[1] );
	Low_Alarm_Value  = ((int)( QSFPDD_Page2[2] << 8 ) | QSFPDD_Page2[3] );
	High_Warning     = ((int)( QSFPDD_Page2[4] << 8 ) | QSFPDD_Page2[5] );
	Low_Warning      = ((int)( QSFPDD_Page2[6] << 8 ) | QSFPDD_Page2[7] );

	VCC_Temp             = ((unsigned int)( QSFPDD_A0[16]    << 8 ) | QSFPDD_A0[17] );
	VCC_High_Alarm_Value = ((unsigned int)( QSFPDD_Page2[8]  << 8 ) | QSFPDD_Page2[9] );
	VCC_Low_Alarm_Value  = ((unsigned int)( QSFPDD_Page2[10] << 8 ) | QSFPDD_Page2[11] );
	VCC_High_Warning     = ((unsigned int)( QSFPDD_Page2[12] << 8 ) | QSFPDD_Page2[13] );
	VCC_Low_Warning      = ((unsigned int)( QSFPDD_Page2[14] << 8 ) | QSFPDD_Page2[15] );

	// Temp. VCC Mask flag Address byte32
	MASK_FLAG = QSFPDD_A0[32];

	if( T_Temp < Low_Warning )               // set Low  warning flag bit4
		LATCH |= 0x08;
	if( T_Temp > High_Warning )              // set high warning flag bit3
		LATCH |= 0x04;
	if( T_Temp < Low_Alarm_Value )           // set Low  alarm   flag bit2
		LATCH |= 0x02;
	if( T_Temp > High_Alarm_Value )          // set high alarm   flag bit1
		LATCH |= 0x01;

	if ( VCC_Temp < VCC_Low_Warning )        // set Low  warning flag bit7
		LATCH |= 0x80;
	if ( VCC_Temp > VCC_High_Warning )       // set High warning flag bit6
		LATCH |= 0x40;
	if ( VCC_Temp < VCC_Low_Alarm_Value )    // set Low  alarm   flag bit5
		LATCH |= 0x20;
	if ( VCC_Temp > VCC_High_Alarm_Value )   // set High alarm   flag bit4
		LATCH |= 0x10;

	if ((QSFPDD_A0[9] > 0) && (LATCH == 0))
		LATCH = QSFPDD_A0[9];

	VCC_Temperature_LATCH = LATCH;

	QSFPDD_A0[9] = LATCH;

	if((LATCH) & (~MASK_FLAG))
	{
		IntL = 0;
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_RX |= VCC_TEMP_INT;
	}
}

void QSFP28_TxPower_AW()
{
	unsigned int TXPOWER_Temp[8];
	unsigned char Channel;
	unsigned int High_Alarm_Value, Low_Alarm_Value, High_Warning_Value, Low_Warning_Value;
	unsigned char LOW_A_LATCH  = 0;
	unsigned char HIGH_A_LATCH = 0;
	unsigned char LOW_W_LATCH  = 0;
	unsigned char HIGH_W_LATCH = 0;

	TXPOWER_Temp[0] = ( (unsigned int)( QSFPDD_Page11[26] << 8) | QSFPDD_Page11[27] );
	TXPOWER_Temp[1] = ( (unsigned int)( QSFPDD_Page11[28] << 8) | QSFPDD_Page11[29] );
	TXPOWER_Temp[2] = ( (unsigned int)( QSFPDD_Page11[30] << 8) | QSFPDD_Page11[31] );
	TXPOWER_Temp[3] = ( (unsigned int)( QSFPDD_Page11[32] << 8) | QSFPDD_Page11[33] );
	TXPOWER_Temp[4] = ( (unsigned int)( QSFPDD_Page11[34] << 8) | QSFPDD_Page11[35] );
	TXPOWER_Temp[5] = ( (unsigned int)( QSFPDD_Page11[36] << 8) | QSFPDD_Page11[37] );
	TXPOWER_Temp[6] = ( (unsigned int)( QSFPDD_Page11[38] << 8) | QSFPDD_Page11[39] );
	TXPOWER_Temp[7] = ( (unsigned int)( QSFPDD_Page11[40] << 8) | QSFPDD_Page11[41] );

	High_Alarm_Value   = ( (unsigned int)( QSFPDD_Page2[48] << 8)| QSFPDD_Page2[49] );
	Low_Alarm_Value    = ( (unsigned int)( QSFPDD_Page2[50] << 8)| QSFPDD_Page2[51] );
	High_Warning_Value = ( (unsigned int)( QSFPDD_Page2[52] << 8)| QSFPDD_Page2[53] );
	Low_Warning_Value  = ( (unsigned int)( QSFPDD_Page2[54] << 8)| QSFPDD_Page2[55] );

	for(Channel = 0; Channel <= 7; Channel++)
	{
		// CH0 - CH 7 High Alarm Flag setting
		if( TXPOWER_Temp[Channel] > High_Alarm_Value )
			HIGH_A_LATCH |= ( 1 << Channel );
		// CH0 - CH 7 Low Alarm Flag setting
		if( TXPOWER_Temp[Channel] < Low_Alarm_Value )
			LOW_A_LATCH  |= ( 1 << Channel );
		// CH0 - CH 7 High Warning Flag setting
		if( TXPOWER_Temp[Channel] > High_Warning_Value )
			HIGH_W_LATCH |= ( 1 << Channel );
		// CH0 - CH 7 Low Warning Flag setting
		if( TXPOWER_Temp[Channel] < Low_Warning_Value )
			LOW_W_LATCH  |= ( 1 << Channel );
	}

	//-------------------------------------------------------------//
	// High / Low Alarm & Warning Flag write Value
	//-------------------------------------------------------------//
	if ( ( QSFPDD_Page11[11] > 0 ) && ( HIGH_A_LATCH == 0 ))
		HIGH_A_LATCH = QSFPDD_Page11[11];
	if ( ( QSFPDD_Page11[12] > 0 ) && ( LOW_A_LATCH  == 0 ))
		LOW_A_LATCH = QSFPDD_Page11[12];
	if ( ( QSFPDD_Page11[13] > 0 ) && ( HIGH_W_LATCH == 0 ))
		HIGH_W_LATCH = QSFPDD_Page11[13];
	if ( ( QSFPDD_Page11[14] > 0 ) && ( LOW_W_LATCH  == 0 ))
		LOW_W_LATCH = QSFPDD_Page11[14];

	TxP_HA_LATCH = HIGH_A_LATCH ;
	TxP_LA_LATCH = LOW_A_LATCH ;
	TxP_HW_LATCH = HIGH_W_LATCH ;
	TxP_LW_LATCH = LOW_W_LATCH ;

	QSFPDD_Page11[11] = HIGH_A_LATCH ;
	QSFPDD_Page11[12] = LOW_A_LATCH ;
	QSFPDD_Page11[13] = HIGH_W_LATCH ;
	QSFPDD_Page11[14] = LOW_W_LATCH ;

	//Page11 Byte 0x85 Flag Summary
	//QSFPDD_Page11[5] = HIGH_A_LATCH | LOW_A_LATCH | HIGH_W_LATCH | LOW_W_LATCH ;

	if( TxP_HA_LATCH & (~QSFPDD_Page10[90]))
	{
		IntL = 0;
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_TX |= TXPHA_INT ;
	}
	if( TxP_LA_LATCH & (~QSFPDD_Page10[91]))
	{
		IntL = 0;
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_TX |= TXPLA_INT ;
	}
	if( TxP_HW_LATCH & (~QSFPDD_Page10[92]))
	{
		IntL = 0;
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_TX |= TXPHW_INT ;
	}
	if( TxP_LW_LATCH & (~QSFPDD_Page10[93]))
	{
		IntL = 0;
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_TX |= TXPLW_INT ;
	}

}

void QSFP28_Bias_AW()
{
	unsigned int Bias_Temp[8];
	unsigned char Channel;
	unsigned int High_Alarm_Value, Low_Alarm_Value, High_Warning_Value, Low_Warning_Value;
	unsigned char LOW_A_LATCH  = 0;
	unsigned char HIGH_A_LATCH = 0;
	unsigned char LOW_W_LATCH  = 0;
	unsigned char HIGH_W_LATCH = 0;

	Bias_Temp[0] = ((unsigned int)( QSFPDD_Page11[42] << 8 ) | QSFPDD_Page11[43] );
	Bias_Temp[1] = ((unsigned int)( QSFPDD_Page11[44] << 8 ) | QSFPDD_Page11[45] );
	Bias_Temp[2] = ((unsigned int)( QSFPDD_Page11[46] << 8 ) | QSFPDD_Page11[47] );
	Bias_Temp[3] = ((unsigned int)( QSFPDD_Page11[48] << 8 ) | QSFPDD_Page11[49] );
	Bias_Temp[4] = ((unsigned int)( QSFPDD_Page11[50] << 8 ) | QSFPDD_Page11[51] );
	Bias_Temp[5] = ((unsigned int)( QSFPDD_Page11[52] << 8 ) | QSFPDD_Page11[53] );
	Bias_Temp[6] = ((unsigned int)( QSFPDD_Page11[54] << 8 ) | QSFPDD_Page11[55] );
	Bias_Temp[7] = ((unsigned int)( QSFPDD_Page11[56] << 8 ) | QSFPDD_Page11[57] );

	High_Alarm_Value   = ( (unsigned int)( QSFPDD_Page2[56] << 8)| QSFPDD_Page2[57] );
	Low_Alarm_Value    = ( (unsigned int)( QSFPDD_Page2[58] << 8)| QSFPDD_Page2[59] );
	High_Warning_Value = ( (unsigned int)( QSFPDD_Page2[60] << 8)| QSFPDD_Page2[61] );
	Low_Warning_Value  = ( (unsigned int)( QSFPDD_Page2[62] << 8)| QSFPDD_Page2[63] );

	for( Channel = 0; Channel <= 7; Channel++ )
	{
		// CH0 - CH 7 High Alarm Flag setting
		if( Bias_Temp[Channel] > High_Alarm_Value )
			HIGH_A_LATCH |= ( 1 << Channel );
		// CH0 - CH 7 Low Alarm Flag setting
		if( Bias_Temp[Channel] < Low_Alarm_Value )
			LOW_A_LATCH  |= ( 1 << Channel );
		// CH0 - CH 7 High Warning Flag setting
		if( Bias_Temp[Channel] > High_Warning_Value )
			HIGH_W_LATCH |= ( 1 << Channel );
		// CH0 - CH 7 Low Warning Flag setting
		if( Bias_Temp[Channel] < Low_Warning_Value )
			LOW_W_LATCH  |= ( 1 << Channel );
	}

	//-------------------------------------------------------------//
	// High / Low Alarm & Warning Flag write Value
	//-------------------------------------------------------------//
	if ( ( QSFPDD_Page11[15] > 0 ) && ( HIGH_A_LATCH == 0 ))
		HIGH_A_LATCH = QSFPDD_Page11[15];

	if ( ( QSFPDD_Page11[16] > 0 ) && ( LOW_A_LATCH  == 0 ))
		LOW_A_LATCH = QSFPDD_Page11[16];

	if ( ( QSFPDD_Page11[17] > 0 ) && ( HIGH_W_LATCH == 0 ))
		HIGH_W_LATCH = QSFPDD_Page11[17];

	if ( ( QSFPDD_Page11[18] > 0 ) && ( LOW_W_LATCH  == 0 ))
		LOW_W_LATCH = QSFPDD_Page11[18];

	BIAS_HA_LATCH = HIGH_A_LATCH ;
	BIAS_LA_LATCH = LOW_A_LATCH ;
	BIAS_HW_LATCH = HIGH_W_LATCH ;
	BIAS_LW_LATCH = LOW_W_LATCH ;

	QSFPDD_Page11[15] = HIGH_A_LATCH ;
	QSFPDD_Page11[16] = LOW_A_LATCH ;
	QSFPDD_Page11[17] = HIGH_W_LATCH ;
	QSFPDD_Page11[18] = LOW_W_LATCH ;

	//Page11 Byte 0x85 Flag Summary
	//QSFPDD_Page11[5] = HIGH_A_LATCH | LOW_A_LATCH | HIGH_W_LATCH | LOW_W_LATCH ;


	if( BIAS_HA_LATCH & (~QSFPDD_Page10[94]))
	{
		IntL = 0;
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_TX |= TXBHA_INT ;
	}
	if( BIAS_LA_LATCH & (~QSFPDD_Page10[95]))
	{
		IntL = 0;
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_TX |= TXBLA_INT ;
	}
	if( BIAS_HW_LATCH & (~QSFPDD_Page10[96]))
	{
		IntL = 0;
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_TX |= TXBHW_INT ;
	}
	if( BIAS_LW_LATCH & (~QSFPDD_Page10[97]))
	{
		IntL = 0;
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_TX |= TXBLW_INT ;
	}
}

void QSFPDD_RxPower_AW()
{
	unsigned int RXPOWER_Temp[8];
	unsigned char Channel;
	unsigned int High_Alarm_Value, Low_Alarm_Value, High_Warning_Value, Low_Warning_Value ;
	unsigned char HIGH_A_LATCH = 0;
	unsigned char LOW_A_LATCH = 0;
	unsigned char HIGH_W_LATCH = 0;
	unsigned char LOW_W_LATCH = 0;

	RXPOWER_Temp[0] = ((unsigned int)( QSFPDD_Page11[58] << 8 ) | QSFPDD_Page11[59] );
	RXPOWER_Temp[1] = ((unsigned int)( QSFPDD_Page11[60] << 8 ) | QSFPDD_Page11[61] );
	RXPOWER_Temp[2] = ((unsigned int)( QSFPDD_Page11[62] << 8 ) | QSFPDD_Page11[63] );
	RXPOWER_Temp[3] = ((unsigned int)( QSFPDD_Page11[64] << 8 ) | QSFPDD_Page11[65] );
	RXPOWER_Temp[4] = ((unsigned int)( QSFPDD_Page11[66] << 8 ) | QSFPDD_Page11[67] );
	RXPOWER_Temp[5] = ((unsigned int)( QSFPDD_Page11[68] << 8 ) | QSFPDD_Page11[69] );
	RXPOWER_Temp[6] = ((unsigned int)( QSFPDD_Page11[70] << 8 ) | QSFPDD_Page11[71] );
	RXPOWER_Temp[7] = ((unsigned int)( QSFPDD_Page11[72] << 8 ) | QSFPDD_Page11[73] );

	High_Alarm_Value   = ( (unsigned int)( QSFPDD_Page2[64] << 8)| QSFPDD_Page2[65] );
	Low_Alarm_Value    = ( (unsigned int)( QSFPDD_Page2[66] << 8)| QSFPDD_Page2[67] );
	High_Warning_Value = ( (unsigned int)( QSFPDD_Page2[68] << 8)| QSFPDD_Page2[69] );
	Low_Warning_Value  = ( (unsigned int)( QSFPDD_Page2[70] << 8)| QSFPDD_Page2[71] );

	for( Channel = 0; Channel <= 7; Channel++ )
	{
		// CH0 - CH 7 High Alarm Flag setting
		if( RXPOWER_Temp[Channel] > High_Alarm_Value )
			HIGH_A_LATCH |= ( 1 << Channel );
		// CH0 - CH 7 Low Alarm Flag setting
		if( RXPOWER_Temp[Channel] < Low_Alarm_Value )
			LOW_A_LATCH  |= ( 1 << Channel );
		// CH0 - CH 7 High Warning Flag setting
		if( RXPOWER_Temp[Channel] > High_Warning_Value )
			HIGH_W_LATCH |= ( 1 << Channel );
		// CH0 - CH 7 Low Warning Flag setting
		if( RXPOWER_Temp[Channel] < Low_Warning_Value )
			LOW_W_LATCH  |= ( 1 << Channel );
	}
	//-------------------------------------------------------------//
	// High / Low Alarm & Warning Flag write Value
	//-------------------------------------------------------------//
	if ( ( QSFPDD_Page11[21] > 0 ) && ( HIGH_A_LATCH == 0 ))
		HIGH_A_LATCH = QSFPDD_Page11[21];
	if ( ( QSFPDD_Page11[22] > 0 ) && ( LOW_A_LATCH  == 0 ))
		LOW_A_LATCH = QSFPDD_Page11[22];
	if ( ( QSFPDD_Page11[23] > 0 ) && ( HIGH_W_LATCH == 0 ))
		HIGH_W_LATCH = QSFPDD_Page11[23];
	if ( ( QSFPDD_Page11[24] > 0 ) && ( LOW_W_LATCH  == 0 ))
		LOW_W_LATCH = QSFPDD_Page11[24];

	RxP_HA_LATCH  = HIGH_A_LATCH ;
	RxP_LA_LATCH  = LOW_A_LATCH ;
	RxP_HW_LATCH  = HIGH_W_LATCH ;
	RxP_LW_LATCH  = LOW_W_LATCH ;

	QSFPDD_Page11[21] = HIGH_A_LATCH ;
	QSFPDD_Page11[22] = LOW_A_LATCH ;
	QSFPDD_Page11[23] = HIGH_W_LATCH ;
	QSFPDD_Page11[24] = LOW_W_LATCH ;

	//Page11 Byte 0x85 Flag Summary
	//QSFPDD_Page11[5] = HIGH_A_LATCH | LOW_A_LATCH | HIGH_W_LATCH | LOW_W_LATCH ;

	if( RxP_HA_LATCH & (~QSFPDD_Page10[100]))
	{
		IntL = 0;
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_RX |= RXHA_INT;
	}
	if( RxP_LA_LATCH & (~QSFPDD_Page10[101]))
	{
		IntL = 0;
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_RX |= RXLA_INT;
	}
	if( RxP_HW_LATCH & (~QSFPDD_Page10[102]))
	{
		IntL = 0;
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_RX |= RXHW_INT;
	}
	if( RxP_LW_LATCH & (~QSFPDD_Page10[103]))
	{
		IntL = 0;
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_RX |= RXLW_INT;
	}

}
// TX LOS
void QSFPDD_TXLOS_LOL_AW()
{
	unsigned int MSA_QDD_TX_LOS_LOL_Status ;
	unsigned char Tx_LOS_LATCH_R = 0 , Tx_LOL_LATCH_R = 0 ;

	MSA_QDD_TX_LOS_LOL_Status = DSP59281_TX_SystemSide_LOL_LOS_Status();

	Tx_LOL_LATCH_R = MSA_QDD_TX_LOS_LOL_Status;
//	Tx_LOS_LATCH_R = MSA_QDD_TX_LOS_LOL_Status;
	Tx_LOS_LATCH_R = MSA_QDD_TX_LOS_LOL_Status>>8;

	if( ( QSFPDD_Page11[8] > 0 ) && ( Tx_LOS_LATCH_R == 0x00 ) )
		Tx_LOS_LATCH_R = QSFPDD_Page11[8];

	if( ( QSFPDD_Page11[9] > 0 ) && ( Tx_LOL_LATCH_R == 0x00 ) )
		Tx_LOL_LATCH_R = QSFPDD_Page11[9];

	QSFPDD_Page11[8] = Tx_LOS_LATCH_R ;
	QSFPDD_Page11[9] = Tx_LOL_LATCH_R ;

	if( Tx_LOS_LATCH_R & (~QSFPDD_Page10[87]))
	{
		IntL = 0;
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_RX |= TXLOS_INT;
	}

	if( Tx_LOL_LATCH_R & (~QSFPDD_Page10[88]))
	{
		IntL = 0;
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_RX |= TXCDR_LOL_INT;
	}
}

// RX LOS
void QSFPDD_RXLOS_LOL_AW()
{
	unsigned int MSA_QDD_RX_LOS_LOL_Status ;
	unsigned char Rx_LOS_LATCH_R = 0 , Rx_LOL_LATCH_R = 0 ;

	MSA_QDD_RX_LOS_LOL_Status = DSP59281_RX_LineSide_LOL_LOS_Status();

	Rx_LOL_LATCH_R = MSA_QDD_RX_LOS_LOL_Status;
//	Rx_LOS_LATCH_R = MSA_QDD_RX_LOS_LOL_Status;
	Rx_LOS_LATCH_R = MSA_QDD_RX_LOS_LOL_Status>>8;

	if( ( QSFPDD_Page11[19]>0 ) && ( Rx_LOS_LATCH_R==0x00 ))
		Rx_LOS_LATCH_R = QSFPDD_Page11[19] ;

	if( ( QSFPDD_Page11[20]>0 ) && ( Rx_LOL_LATCH_R==0x00 ))
		Rx_LOL_LATCH_R = QSFPDD_Page11[20];

	QSFPDD_Page11[20] = Rx_LOL_LATCH_R;
	QSFPDD_Page11[19] = Rx_LOS_LATCH_R;

	if( Rx_LOS_LATCH_R & (~QSFPDD_Page10[98]))
	{
		IntL = 0;
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_RX |= RX_ROS_INT;
	}

	if( Rx_LOL_LATCH_R & (~QSFPDD_Page10[99]))
	{
		IntL = 0;
		QSFPDD_A0[3] &= ~0x01;
		IntL_Status_RX |= RXCDR_LOL_INT;
	}

}

void Clear_Flag(unsigned char Current_AD)
{
	switch(Current_AD)
	{
		case ADDR_TXFAULT:
						QSFPDD_Page11[ADDR_TXFAULT] = 0 ;
						break;
		case ADDR_TXLOS:
						QSFPDD_Page11[ADDR_TXLOS] = 0 ;
						IntL_Status_TX &= ~TXLOS_INT ;
						break;
		case ADDR_TXCDR_LOL:
						QSFPDD_Page11[ADDR_TXCDR_LOL] = 0 ;
						IntL_Status_TX &= ~TXCDR_LOL_INT ;
						break;
		case ADDR_TX_EQ_FAULT:
						QSFPDD_Page11[ADDR_TX_EQ_FAULT] = 0 ;
						break;
		case ADDR_TXP_HA:
						QSFPDD_Page11[ADDR_TXP_HA] &= ~TxP_HA_LATCH ;
						IntL_Status_TX &= ~TXPHA_INT ;
						break;
		case ADDR_TXP_LA:
						QSFPDD_Page11[ADDR_TXP_LA] = 0x00 ;
						//QSFPDD_Page11[ADDR_TXP_LA] &= ~TxP_LA_LATCH ;
						//IntL_Status_TX &= ~TXPLA_INT ;
						break;
		case ADDR_TXP_HW:
						QSFPDD_Page11[ADDR_TXP_HW] &= ~TxP_HW_LATCH ;
						IntL_Status_TX &= ~TXPHW_INT ;
						break;
		case ADDR_TXP_LW:
						QSFPDD_Page11[ADDR_TXP_LW] = 0x00;
						//QSFPDD_Page11[ADDR_TXP_LW] &= ~TxP_LW_LATCH ;
						//IntL_Status_TX &= ~TXPLW_INT ;
						break;
		case ADDR_TXB_HA:
						QSFPDD_Page11[ADDR_TXB_HA] &= ~BIAS_HA_LATCH ;
						IntL_Status_TX &= ~TXBHA_INT ;
						break;
		case ADDR_TXB_LA:
						QSFPDD_Page11[ADDR_TXB_LA] = 0x00;
						//QSFPDD_Page11[ADDR_TXB_LA] &= ~BIAS_LA_LATCH ;
						//IntL_Status_TX &= ~TXBLA_INT ;
						break;
		case ADDR_TXB_HW:
						QSFPDD_Page11[ADDR_TXB_HW] &= ~BIAS_HW_LATCH ;
						IntL_Status_TX &= ~TXBHW_INT ;
						break;
		case ADDR_TXB_LW:
						QSFPDD_Page11[ADDR_TXB_LW] = 0x00;
						//QSFPDD_Page11[ADDR_TXB_LW] &= ~BIAS_LW_LATCH ;
						//IntL_Status_TX &= ~TXBLW_INT ;
						break;
		case ADDR_RXLOS:
						QSFPDD_Page11[ADDR_RXLOS] = 0 ;
						IntL_Status_RX &= ~RX_ROS_INT ;
						break;
		case ADDR_RXCDR_LOL:
						QSFPDD_Page11[ADDR_RXCDR_LOL] = 0 ;
						IntL_Status_RX &= ~RXCDR_LOL_INT ;
						break;
		case ADDR_RXP_HA:
						QSFPDD_Page11[ADDR_RXP_HA] &= ~RxP_HA_LATCH ;
						IntL_Status_RX &= ~RXHA_INT ;
						break;
		case ADDR_RXP_LA:
						QSFPDD_Page11[ADDR_RXP_LA] &= ~RxP_LA_LATCH ;
						IntL_Status_RX &= ~RXLA_INT ;
						break;
		case ADDR_RXP_HW:
						QSFPDD_Page11[ADDR_RXP_HW] &= ~RxP_HW_LATCH ;
						IntL_Status_RX &= ~RXHW_INT ;
						break;
		case ADDR_RXP_LW:
						QSFPDD_Page11[ADDR_RXP_LW] &= ~RxP_LW_LATCH ;
						IntL_Status_RX &= ~RXLW_INT ;
						break;
	}

	if( ( IntL_Status_TX == 0x00 ) && ( IntL_Status_RX == 0x00 ) )
	{
		QSFPDD_A0[3] |= 0x01;
		IntL = 1;
	}

}

void Clear_VCC_TEMP_Flag()
{
	QSFPDD_A0[9]    &= ~VCC_Temperature_LATCH ;
	IntL_Status_RX  &= ~VCC_TEMP_INT;

	if( ( IntL_Status_TX == 0x00 ) && ( IntL_Status_RX == 0x00 ) )
	{
		QSFPDD_A0[3] |= 0x01;
		IntL = 1;
	}

}

void Clear_Module_state_Byte8()
{
	QSFPDD_A0[8] = 0 ;
	if( ( IntL_Status_TX == 0x00 ) && ( IntL_Status_RX == 0x00 ) )
	{
		QSFPDD_A0[3] |= 0x01;
		IntL = 1;
	}

}


void DDMI_AW_Monitor()
{
	QSFPDD_Temperature_VCC_AW();
	QSFPDD_RxPower_AW();

//	QSFP28_Bias_AW();
//	QSFP28_TxPower_AW();

}




