/*
 * QDD_MSA_Optional_Control.c
 *
 *  Created on: 2018�~12��10��
 *      Author: Ivan_Lin
 */

#include "MSA_QSFPDD.h"
#include "Master_I2C_GPIO.h"
#include "BRCM_DSP_59281.h"
#include "BRCM_DSP58281_Optional.h"
#include "Calibration_Struct.h"

#define PR_Post_Level_Max    7
#define DSP_FIR_MAX        168

extern xdata unsigned char	bef_Pre_Cursor_RX12;
extern xdata unsigned char	bef_Pre_Cursor_RX34;
extern xdata unsigned char	bef_Pre_Cursor_RX56;
extern xdata unsigned char	bef_Pre_Cursor_RX78;

extern xdata unsigned char	bef_Post_Cursor_RX12;
extern xdata unsigned char	bef_Post_Cursor_RX34;
extern xdata unsigned char	bef_Post_Cursor_RX56;
extern xdata unsigned char	bef_Post_Cursor_RX78;

extern xdata unsigned char	bef_AMP_C_RX12;
extern xdata unsigned char	bef_AMP_C_RX34;
extern xdata unsigned char	bef_AMP_C_RX56;
extern xdata unsigned char	bef_AMP_C_RX78;

// BRCM Main / Pre / Post control
// if Main or Pre or Post modify value , tap load Must be executed

void Tx_CTLE_Control(unsigned char Control_Write_Enable)
{
	unsigned char CTLE_TX12,CTLE_TX34,CTLE_TX56,CTLE_TX78 ;
	// MSA PAGE10 Byte 156 TX12 CTLE CONTROL
	// MSA PAGE10 Byte 157 TX34 CTLE CONTROL
	// MSA PAGE10 Byte 158 TX56 CTLE CONTROL
	// MSA PAGE10 Byte 159 TX78 CTLE CONTROL
	CTLE_TX12 = QSFPDD_Page10[28] ;
	CTLE_TX34 = QSFPDD_Page10[29] ;
	CTLE_TX56 = QSFPDD_Page10[30] ;
	CTLE_TX78 = QSFPDD_Page10[31] ;

	QSFPDD_Page11[89] = QSFPDD_Page10[28] ;
	QSFPDD_Page11[90] = QSFPDD_Page10[29] ;
	QSFPDD_Page11[91] = QSFPDD_Page10[30] ;
	QSFPDD_Page11[92] = QSFPDD_Page10[31] ;
	//-----------------------------------------------------------------------------------------//
	// MSA TX1 - TX4
	//-----------------------------------------------------------------------------------------//
	//-----------------------------------------------------------------------------------------//
	// MSA TX5 - TX8
	//-----------------------------------------------------------------------------------------//
	//-----------------------------------------------------------------------------------------//
	// Write CTLE Setting to MAOM38053 chip register.
	//-----------------------------------------------------------------------------------------//
	if(Control_Write_Enable==1)
	{

	}
}

void RX_Pre_Control( unsigned char Control_Write_Enable )
{
	unsigned char Flag_ADDR;
	unsigned char Pre_Cursor_RX12,Pre_Cursor_RX34,Pre_Cursor_RX56,Pre_Cursor_RX78 ;

	// MSA PAGE10 Byte 162 RX12 Pre-cursor CONTROL
	// MSA PAGE10 Byte 163 RX34 Pre-cursor CONTROL
	// MSA PAGE10 Byte 164 RX56 Pre-cursor CONTROL
	// MSA PAGE10 Byte 165 RX78 Pre-cursor CONTROL

	// Device 3 level Control 09 / 0A / 0B
	Pre_Cursor_RX12 = QSFPDD_Page10[34] ;
	Pre_Cursor_RX34 = QSFPDD_Page10[35] ;
	Pre_Cursor_RX56 = QSFPDD_Page10[36] ;
	Pre_Cursor_RX78 = QSFPDD_Page10[37] ;
	//-----------------------------------------------------------------------------------------//
	// MSA Pre-Cursor RX1 - RX4
	//-----------------------------------------------------------------------------------------//
	// RX1 Pre_Cursor
	Flag_ADDR = ( Pre_Cursor_RX12 & 0x0F );
	if(Flag_ADDR>=PR_Post_Level_Max)
		Flag_ADDR = PR_Post_Level_Max ;
	if(Flag_ADDR==0)
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH0 = 0x0000;
	else
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH0 = ( (unsigned int)Pro_Table[Flag_ADDR] | 0xFF00 );
	// RX2 Pre_Cursor
	Flag_ADDR = ( ( Pre_Cursor_RX12 & 0xF0 ) >> 4 );
	if(Flag_ADDR>=PR_Post_Level_Max)
		Flag_ADDR = PR_Post_Level_Max ;
	if(Flag_ADDR==0)
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH1 = 0x0000;
	else
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH1 = ( (unsigned int)Pro_Table[Flag_ADDR] | 0xFF00 );
	// RX3 Pre_Cursor
	Flag_ADDR = ( Pre_Cursor_RX34 & 0x0F );
	if(Flag_ADDR>=PR_Post_Level_Max)
		Flag_ADDR = PR_Post_Level_Max ;
	if(Flag_ADDR==0)
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH2 = 0x0000;
	else
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH2 = ( (unsigned int)Pro_Table[Flag_ADDR] | 0xFF00 );
	// RX4 Pre_Cursor
	Flag_ADDR = ( ( Pre_Cursor_RX34 & 0xF0 ) >> 4 );
	if(Flag_ADDR>=PR_Post_Level_Max)
		Flag_ADDR = PR_Post_Level_Max ;
	if(Flag_ADDR==0)
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH3 = 0x0000;
	else
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_PRE1_CH3 = ( (unsigned int)Pro_Table[Flag_ADDR] | 0xFF00 );
	//-----------------------------------------------------------------------------------------//
	// MSA Pre-Cursor RX5 - RX8
	//-----------------------------------------------------------------------------------------//
	// RX5 Pre_Cursor
	Flag_ADDR = ( Pre_Cursor_RX56 & 0x0F );
	if(Flag_ADDR>=PR_Post_Level_Max)
		Flag_ADDR = PR_Post_Level_Max ;
	if(Flag_ADDR==0)
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH0 = 0x0000;
	else
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH0 = ( (unsigned int)Pro_Table[Flag_ADDR] | 0xFF00 );
	// RX6 Pre_Cursor
	Flag_ADDR = ( ( Pre_Cursor_RX56 & 0xF0 ) >> 4 );
	if(Flag_ADDR>=PR_Post_Level_Max)
		Flag_ADDR = PR_Post_Level_Max ;
	if(Flag_ADDR==0)
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH1 = 0x0000;
	else
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH1 = ( (unsigned int)Pro_Table[Flag_ADDR] | 0xFF00 );
	// RX7 Pre_Cursor
	Flag_ADDR = ( Pre_Cursor_RX78 & 0x0F );
	if(Flag_ADDR>=PR_Post_Level_Max)
		Flag_ADDR = PR_Post_Level_Max ;
	if(Flag_ADDR==0)
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH2 = 0x0000;
	else
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH2 = ( (unsigned int)Pro_Table[Flag_ADDR] | 0xFF00 );
	// RX8 Pre_Cursor
	Flag_ADDR = ( ( Pre_Cursor_RX78 & 0xF0 ) >> 4 );
	if(Flag_ADDR>=PR_Post_Level_Max)
		Flag_ADDR = PR_Post_Level_Max ;
	if(Flag_ADDR==0)
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH3 = 0x0000;
	else
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_PRE1_CH3 = ( (unsigned int)Pro_Table[Flag_ADDR] | 0xFF00 );
	//-----------------------------------------------------------------------------------------//
	// Write Pre-Cursor Setting to MASC38040 chip register.
	//-----------------------------------------------------------------------------------------//
	if(Control_Write_Enable==1)
	{
		if( bef_Pre_Cursor_RX12 != Pre_Cursor_RX12)
		{
			System_Side_PRE1(1);
			System_Side_PRE1(2);
		}

		if( bef_Pre_Cursor_RX34 != Pre_Cursor_RX34)
		{
			System_Side_PRE1(3);
			System_Side_PRE1(4);
		}

		if( bef_Pre_Cursor_RX56 != Pre_Cursor_RX56)
		{
			System_Side_PRE1(5);
			System_Side_PRE1(6);
		}

		if( bef_Pre_Cursor_RX78 != Pre_Cursor_RX78)
		{
			System_Side_PRE1(7);
			System_Side_PRE1(8);
		}
		bef_Pre_Cursor_RX12 = Pre_Cursor_RX12;
		bef_Pre_Cursor_RX34 = Pre_Cursor_RX34;
		bef_Pre_Cursor_RX56 = Pre_Cursor_RX56;
		bef_Pre_Cursor_RX78 = Pre_Cursor_RX78;
	}
}

void RX_Post_Control( unsigned char Control_Write_Enable )
{
	unsigned char Flag_ADDR;
	unsigned char Post_Cursor_RX12,Post_Cursor_RX34,Post_Cursor_RX56,Post_Cursor_RX78 ;

	// MSA PAGE10 Byte 166 RX12 Post-cursor CONTROL
	// MSA PAGE10 Byte 167 RX34 Post-cursor CONTROL
	// MSA PAGE10 Byte 168 RX56 Post-cursor CONTROL
	// MSA PAGE10 Byte 169 RX78 Post-cursor CONTROL

	// Device 3 level Control 0C / 0D / 0E

	Post_Cursor_RX12 = QSFPDD_Page10[38] ;
	Post_Cursor_RX34 = QSFPDD_Page10[39] ;
	Post_Cursor_RX56 = QSFPDD_Page10[40] ;
	Post_Cursor_RX78 = QSFPDD_Page10[41] ;

	//-----------------------------------------------------------------------------------------//
	// MSA Post-Cursor RX1 - RX4
	//-----------------------------------------------------------------------------------------//
	// RX1 Post_Cursor
	Flag_ADDR = ( Post_Cursor_RX12 & 0x0F );
	if(Flag_ADDR>=PR_Post_Level_Max)
		Flag_ADDR = PR_Post_Level_Max ;
	if(Flag_ADDR==0)
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH0 = 0x0000;
	else
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH0 = ( (unsigned int)Post_Table[Flag_ADDR] | 0xFF00 );
	// RX2 Post_Cursor
	Flag_ADDR = ( ( Post_Cursor_RX12 & 0xF0 ) >> 4 );
	if(Flag_ADDR>=PR_Post_Level_Max)
		Flag_ADDR = PR_Post_Level_Max ;
	if(Flag_ADDR==0)
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH1 = 0x0000;
	else
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH1 = ( (unsigned int)Post_Table[Flag_ADDR] | 0xFF00 );

	// RX3 Post_Cursor
	Flag_ADDR = ( Post_Cursor_RX34 & 0x0F );
	if(Flag_ADDR>=PR_Post_Level_Max)
		Flag_ADDR = PR_Post_Level_Max ;
	if(Flag_ADDR==0)
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH2 = 0x0000;
	else
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH2 = ( (unsigned int)Post_Table[Flag_ADDR] | 0xFF00 );

	// RX4 Post_Cursor
	Flag_ADDR = ( ( Post_Cursor_RX34 & 0xF0 ) >> 4 );
	if(Flag_ADDR>=PR_Post_Level_Max)
		Flag_ADDR = PR_Post_Level_Max ;
	if(Flag_ADDR==0)
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH3 = 0x0000;
	else
		BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_POST1_CH3 = ( (unsigned int)Post_Table[Flag_ADDR] | 0xFF00 );
	//-----------------------------------------------------------------------------------------//
	// MSA Post-Cursor RX - RX8
	//-----------------------------------------------------------------------------------------//
	// RX5 Post_Cursor
	Flag_ADDR = ( Post_Cursor_RX56 & 0x0F );
	if(Flag_ADDR>=PR_Post_Level_Max)
		Flag_ADDR = PR_Post_Level_Max ;
	if(Flag_ADDR==0)
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH0 = 0x0000;
	else
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH0 = ( (unsigned int)Post_Table[Flag_ADDR] | 0xFF00 );
	// RX6 Post_Cursor
	Flag_ADDR = ( ( Post_Cursor_RX56 & 0xF0 ) >> 4 );
	if(Flag_ADDR>=PR_Post_Level_Max)
		Flag_ADDR = PR_Post_Level_Max ;
	if(Flag_ADDR==0)
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH1 = 0x0000;
	else
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH1 = ( (unsigned int)Post_Table[Flag_ADDR] | 0xFF00 );

	// RX7 Post_Cursor
	Flag_ADDR = ( Post_Cursor_RX78 & 0x0F );
	if(Flag_ADDR>=PR_Post_Level_Max)
		Flag_ADDR = PR_Post_Level_Max ;
	if(Flag_ADDR==0)
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH2 = 0x0000;
	else
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH2 = ( (unsigned int)Post_Table[Flag_ADDR] | 0xFF00 );
	// RX8 Post_Cursor
	Flag_ADDR = ( ( Post_Cursor_RX78 & 0xF0 ) >> 4 );
	if(Flag_ADDR>=PR_Post_Level_Max)
		Flag_ADDR = PR_Post_Level_Max ;
	if(Flag_ADDR==0)
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH3 = 0x0000;
	else
		BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_POST1_CH3 = ( (unsigned int)Post_Table[Flag_ADDR] | 0xFF00 );
	//-----------------------------------------------------------------------------------------//
	// Write Post-Cursor Setting to MASC38040 chip register.
	//-----------------------------------------------------------------------------------------//
	if(Control_Write_Enable==1)
	{
		if(bef_Post_Cursor_RX12!=Post_Cursor_RX12)
		{
			System_Side_POST1(1);
			System_Side_POST1(2);
		}

		if(bef_Post_Cursor_RX34!=Post_Cursor_RX34)
		{
			System_Side_POST1(3);
			System_Side_POST1(4);
		}

		if(bef_Post_Cursor_RX56!=Post_Cursor_RX56)
		{
			System_Side_POST1(5);
			System_Side_POST1(6);
		}

		if(bef_Post_Cursor_RX78!=Post_Cursor_RX78)
		{
			System_Side_POST1(7);
			System_Side_POST1(8);
		}

		bef_Post_Cursor_RX12 = Post_Cursor_RX12 ;
		bef_Post_Cursor_RX34 = Post_Cursor_RX34 ;
		bef_Post_Cursor_RX56 = Post_Cursor_RX56 ;
		bef_Post_Cursor_RX78 = Post_Cursor_RX78 ;
	}
}

/*
	AMP_C_RX12 = QSFPDD_Page10[42] ;
	AMP_C_RX34 = QSFPDD_Page10[43] ;
	AMP_C_RX56 = QSFPDD_Page10[44] ;
	AMP_C_RX78 = QSFPDD_Page10[45] ;
 */

void RX_AMP_OutSwing_Control( unsigned char Control_Write_Enable )
{
	unsigned char Flag_ADDR;
	unsigned char AMP_C_RX12,AMP_C_RX34,AMP_C_RX56,AMP_C_RX78 ;

	// MSA PAGE10 Byte 170 RX12 AMP CONTROL
	// MSA PAGE10 Byte 171 RX34 AMP CONTROL
	// MSA PAGE10 Byte 172 RX56 AMP CONTROL
	// MSA PAGE10 Byte 173 RX78 AMP CONTROL

	// Device 3 level Control 00 / 01 / 02
	AMP_C_RX12 = QSFPDD_Page10[42] ;
	AMP_C_RX34 = QSFPDD_Page10[43] ;
	AMP_C_RX56 = QSFPDD_Page10[44] ;
	AMP_C_RX78 = QSFPDD_Page10[45] ;
	//-----------------------------------------------------------------------------------------//
	// MSA AMP Output Swing RX1 - RX4 / Data Path Polarity inversion
	//----------------------------------------------------------------------------------------//
	// RX1 Output Swing setting
	Flag_ADDR = ( AMP_C_RX12 & 0x0F );
	if(Flag_ADDR>=4)
		Flag_ADDR = 3 ;
	BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH0 = ( (unsigned int)OP_AMP_Table[Flag_ADDR] & 0x00FF );
	// RX2 Output Swing setting
	Flag_ADDR = ( ( AMP_C_RX12 & 0xF0 ) >> 4 );
	if(Flag_ADDR>=4)
		Flag_ADDR = 3 ;
	BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH1 = ( (unsigned int)OP_AMP_Table[Flag_ADDR] & 0x00FF );
	// RX3 Output Swing setting
	Flag_ADDR = ( AMP_C_RX34 & 0x0F );
	if(Flag_ADDR>=4)
		Flag_ADDR = 3 ;
	BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH2 = ( (unsigned int)OP_AMP_Table[Flag_ADDR] & 0x00FF );
	// RX4 Output Swing setting
	Flag_ADDR = ( ( AMP_C_RX34 & 0xF0 ) >> 4 );
	if(Flag_ADDR>=4)
		Flag_ADDR = 3 ;
	BRCM_59281_DSP_SS_PHY0_MEMORY_MAP.System_Side_Main_CH3 = ( (unsigned int)OP_AMP_Table[Flag_ADDR] & 0x00FF );
	//-----------------------------------------------------------------------------------------//
	// MSA AMP Output Swing RX5 - RX8 / Data Path Polarity normal
	//-----------------------------------------------------------------------------------------//
	// RX5 Output Swing setting
	Flag_ADDR = ( AMP_C_RX56 & 0x0F );
	if(Flag_ADDR>=4)
		Flag_ADDR = 3 ;
	BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH0 = ( (unsigned int)OP_AMP_Table[Flag_ADDR] & 0x00FF );
	// RX6 Output Swing setting
	Flag_ADDR = ( ( AMP_C_RX56 & 0xF0 ) >> 4 );
	if(Flag_ADDR>=4)
		Flag_ADDR = 3 ;
	BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH1 = ( (unsigned int)OP_AMP_Table[Flag_ADDR] & 0x00FF );
	// RX7 Output Swing setting
	Flag_ADDR = ( AMP_C_RX78 & 0x0F );
	if(Flag_ADDR>=4)
		Flag_ADDR = 3 ;
	BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH2 = ( (unsigned int)OP_AMP_Table[Flag_ADDR] & 0x00FF );
	// RX8 Output Swing setting
	Flag_ADDR = ( ( AMP_C_RX78 & 0xF0 ) >> 4 );
	if(Flag_ADDR>=4)
		Flag_ADDR = 3 ;
	BRCM_59281_DSP_SS_PHY1_MEMORY_MAP.System_Side_Main_CH3 = ( (unsigned int)OP_AMP_Table[Flag_ADDR] & 0x00FF );
	//-----------------------------------------------------------------------------------------//
	// Write AMP�]output swing) Setting to MASC38040 chip register.
	//-----------------------------------------------------------------------------------------//
	if(Control_Write_Enable==1)
	{
		if(bef_AMP_C_RX12!=AMP_C_RX12)
		{
			System_Side_Main(1);
			System_Side_Main(2);
		}

		if(bef_AMP_C_RX34!=AMP_C_RX34)
		{
			System_Side_Main(3);
			System_Side_Main(4);
		}

		if(bef_AMP_C_RX56!=AMP_C_RX56)
		{
			System_Side_Main(5);
			System_Side_Main(6);
		}

		if(bef_AMP_C_RX78!=AMP_C_RX78)
		{
			System_Side_Main(7);
			System_Side_Main(8);
		}

		bef_AMP_C_RX12 = AMP_C_RX12 ;
		bef_AMP_C_RX34 = AMP_C_RX34 ;
		bef_AMP_C_RX56 = AMP_C_RX56 ;
		bef_AMP_C_RX78 = AMP_C_RX78 ;
	}
}

void QDD_DSP_Tap_ReLoader( unsigned char Control_Write_Enable )
{
	if( Control_Write_Enable == 1 )
	{
		System_Side_PRE2_Tap_Load(1);
		System_Side_PRE2_Tap_Load(2);
		System_Side_PRE2_Tap_Load(3);
		System_Side_PRE2_Tap_Load(4);
		System_Side_PRE2_Tap_Load(5);
		System_Side_PRE2_Tap_Load(6);
		System_Side_PRE2_Tap_Load(7);
		System_Side_PRE2_Tap_Load(8);
	}
}

void Rx_AMP_PRE_POST_Control()
{
	unsigned char Flag_ADDR;
	unsigned char AMP_C_RX12,AMP_C_RX34,AMP_C_RX56,AMP_C_RX78 ;

	// MSA PAGE10 Byte 170 RX12 AMP CONTROL
	// MSA PAGE10 Byte 171 RX34 AMP CONTROL
	// MSA PAGE10 Byte 172 RX56 AMP CONTROL
	// MSA PAGE10 Byte 173 RX78 AMP CONTROL

	// Device 3 level Control 00 / 01 / 02
	AMP_C_RX12 = QSFPDD_Page10[42] ;
	AMP_C_RX34 = QSFPDD_Page10[43] ;
	AMP_C_RX56 = QSFPDD_Page10[44] ;
	AMP_C_RX78 = QSFPDD_Page10[45] ;
	//-----------------------------------------------------------------------------------------//
	// MSA AMP Output Swing RX1 - RX4 / Data Path Polarity inversion
	//----------------------------------------------------------------------------------------//
	// RX1 Output Swing setting
	Flag_ADDR = ( AMP_C_RX12 & 0x0F );
	if(Flag_ADDR>=9)
		Flag_ADDR = 9 ;
	BRCM_SUGGEST_LOSS_RX_OUTPUT_SETTING( Flag_ADDR , 1 );
	// RX2 Output Swing setting
	Flag_ADDR = ( ( AMP_C_RX12 & 0xF0 ) >> 4 );
	if(Flag_ADDR>=9)
		Flag_ADDR = 9 ;
	BRCM_SUGGEST_LOSS_RX_OUTPUT_SETTING( Flag_ADDR , 2 );
	// RX3 Output Swing setting
	Flag_ADDR = ( AMP_C_RX34 & 0x0F );
	if(Flag_ADDR>=9)
		Flag_ADDR = 9 ;
	BRCM_SUGGEST_LOSS_RX_OUTPUT_SETTING( Flag_ADDR , 3 );
	// RX4 Output Swing setting
	Flag_ADDR = ( ( AMP_C_RX34 & 0xF0 ) >> 4 );
	if(Flag_ADDR>=9)
		Flag_ADDR = 9 ;
	BRCM_SUGGEST_LOSS_RX_OUTPUT_SETTING( Flag_ADDR , 4 );
	//-----------------------------------------------------------------------------------------//
	// MSA AMP Output Swing RX5 - RX8 / Data Path Polarity normal
	//-----------------------------------------------------------------------------------------//
	// RX5 Output Swing setting
	Flag_ADDR = ( AMP_C_RX56 & 0x0F );
	if(Flag_ADDR>=9)
		Flag_ADDR = 9 ;
	BRCM_SUGGEST_LOSS_RX_OUTPUT_SETTING( Flag_ADDR , 5 );
	// RX6 Output Swing setting
	Flag_ADDR = ( ( AMP_C_RX56 & 0xF0 ) >> 4 );
	if(Flag_ADDR>=9)
		Flag_ADDR = 9 ;
	BRCM_SUGGEST_LOSS_RX_OUTPUT_SETTING( Flag_ADDR , 6 );
	// RX7 Output Swing setting
	Flag_ADDR = ( AMP_C_RX78 & 0x0F );
	if(Flag_ADDR>=9)
		Flag_ADDR = 9 ;
	BRCM_SUGGEST_LOSS_RX_OUTPUT_SETTING( Flag_ADDR , 7 );
	// RX8 Output Swing setting
	Flag_ADDR = ( ( AMP_C_RX78 & 0xF0 ) >> 4 );
	if(Flag_ADDR>=9)
		Flag_ADDR = 9 ;
	BRCM_SUGGEST_LOSS_RX_OUTPUT_SETTING( Flag_ADDR , 8 );
}

void QDD_MSA_CTLE_PRE_POST_AMP_CONTROL( unsigned char Control_Write_Enable )
{
	if(Control_Write_Enable)
	{
		if( CALIB_MEMORY_MAP.MSA_Optional_Flag == 0x01 )
		{
			RX_AMP_OutSwing_Control( Control_Write_Enable );
			RX_Pre_Control( Control_Write_Enable );
			RX_Post_Control( Control_Write_Enable );
			QDD_DSP_Tap_ReLoader( Control_Write_Enable );
		}
		else
		{
			Rx_AMP_PRE_POST_Control();
			QDD_DSP_Tap_ReLoader( Control_Write_Enable );
		}
		QSFPDD_Page11[95]  = QSFPDD_Page10[34];
		QSFPDD_Page11[96]  = QSFPDD_Page10[35];
		QSFPDD_Page11[97]  = QSFPDD_Page10[36];
		QSFPDD_Page11[98]  = QSFPDD_Page10[37];
		QSFPDD_Page11[99]  = QSFPDD_Page10[38];
		QSFPDD_Page11[100] = QSFPDD_Page10[39];
		QSFPDD_Page11[101] = QSFPDD_Page10[40];
		QSFPDD_Page11[102] = QSFPDD_Page10[41];
		QSFPDD_Page11[103] = QSFPDD_Page10[42];
		QSFPDD_Page11[104] = QSFPDD_Page10[43];
		QSFPDD_Page11[105] = QSFPDD_Page10[44];
		QSFPDD_Page11[106] = QSFPDD_Page10[45];

	}

}



