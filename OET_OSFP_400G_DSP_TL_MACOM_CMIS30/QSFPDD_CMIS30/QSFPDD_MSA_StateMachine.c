

#include <SI_EFM8LB1_Register_Enums.h>                  // SFR declarations
#include <string.h>
#include "EFM8LB_ADC.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"

#include "MA38435_TX1_TX4.h"
#include "MA38435_TX5_TX8.h"
#include "MA38434_RX1_RX4.h"
#include "MA38434_RX5_RX8.h"

#include "MSA_QSFPDD.h"
#include "BRCM_DSP_59281.h"
#include "BRCM_DSP_1181.h"
#include "Master_I2C_GPIO.h"
// 2019_0828 Ivan new Funciton / SPRQ / PQBS31Q / System side Loopback
#include "BRCM_DSP58281_Optional.h"

//--------------------------------------------------------//
//Error Define
//--------------------------------------------------------//
#define Error_code_NoStatus              0x00
#define Error_code_ConfigAccept          0x01
#define Error_code_RejectedUnknown       0x02
#define Error_code_RejectedInvalidCode   0x03
#define Error_code_RejectedInvalidCombo  0x04
#define Error_code_RejectedInvalidSI     0x05
#define Error_code_RejectedInUse         0x06
#define Error_code_RejectedIncomplete    0x07

//--------------------------------------------------------//
//MODULE State
//--------------------------------------------------------//
#define MODULE_MGMT_INIT    0x0
#define MODULE_LOW_PWR      0x2
#define MODULE_PWR_UP       0x4
#define MODULE_READY        0x6
#define MODULE_PWR_DN       0x8
#define MODULE_FAULT        0xA
//--------------------------------------------------------//
//DATA PATH State
//--------------------------------------------------------//
#define DEACTIVATED_DATHPATH  1
#define INIT_DATHPATH         2
#define DATHPATH_DEINIT       3
#define ACTIVATED_DATHPATH    4

SI_SBIT( InitMode   , SFR_P0, 4 );
SI_SBIT( IntL       , SFR_P0, 2 );
SI_SBIT( LPMODE_DSP , SFR_P1, 4 );

unsigned char Module_Status = 0 ;
unsigned char SoftWare_Init_flag = 0 ;
unsigned int  DDMI_DataCount = 0 ;
unsigned char DDMI_Update_C = 0 ;

unsigned char A0_26_Write_LowPower_flag = 0 ;
extern xdata unsigned char Page10_Write_flag;

unsigned char DSP_MODE_SET = 0x00;
unsigned char Error_code_Pass = 0x00 ;
//------------------------------------------------------//
// DataPath_Power up 0xFF
unsigned char Bef_DataPath_Power = 0xFF;
// Tx Disable is Power up
unsigned char Bef_TxDIS_Power = 0x00;
// Module_Global_Control is 0x00
unsigned char Bef_Module_Global_Control = 0x00 ;
// Rx output is enable
unsigned char Bef_Rx_output = 0x00 ;

xdata unsigned char Bef_APP_Byte145_Lan0 = 0x10 ;
xdata unsigned char Bef_APP_Byte146_Lan1 = 0x10 ;
xdata unsigned char Bef_APP_Byte147_Lan2 = 0x10 ;
xdata unsigned char Bef_APP_Byte148_Lan3 = 0x10 ;
xdata unsigned char Bef_APP_Byte149_Lan4 = 0x10 ;
xdata unsigned char Bef_APP_Byte150_Lan5 = 0x10 ;
xdata unsigned char Bef_APP_Byte151_Lan6 = 0x10 ;
xdata unsigned char Bef_APP_Byte152_Lan7 = 0x10 ;

xdata unsigned char Power_on_flag = 0 ;
xdata unsigned char AW_DDMI_Count = 0 ;

extern xdata unsigned char Power_mode_function;
//------------------------------------------------------//
// other ddmi function
//------------------------------------------------------//
extern unsigned char Get_Power_C_Status();

void Get_module_Power_Monitor()
{
	CALIB_MEMORY_1_MAP.TX_CDR_P1V8_ADC = ADCV_Conversion_Voltage( CDR_0V8_TX ) ;
	CALIB_MEMORY_1_MAP.RX_CDR_P1V8_ADC = ADCV_Conversion_Voltage( PDCDD ) ;
	CALIB_MEMORY_1_MAP.TX_P3V3_ADC     = ADCV_Conversion_Voltage( P3V3_TRX_VCC )*2 ;
	CALIB_MEMORY_1_MAP.TRX_P4V5_ADC    = ADCV_Conversion_Voltage( P1V8_TRX_VCC );
	CALIB_MEMORY_1_MAP.TEMP_ADC        = Get_Tempsensor();
	CALIB_MEMORY_1_MAP.Power_C_Status  = Get_Power_C_Status();
}
//------------------------------------------------------//
// TX_DIS_FUNCTION
//------------------------------------------------------//
void TX_DIS_FUNCTION(unsigned char TxDIS_Control )
{
	// Macom 38435 IC1
	// HW Tx8 - Tx3
	// HW Tx7 - Tx2
	// HW Tx6 - Tx1
	// HW Tx5 - Tx0
	// Macom 38435 IC2
	// HW Tx4 - Tx3
	// HW Tx3 - Tx2
	// HW Tx2 - Tx1
	// HW Tx1 - Tx0
	//--------------------------------------------------//
	// Tx1 MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE
	//--------------------------------------------------//
	if( TxDIS_Control & 0x01 )
		MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE |= 0x01 ;
	else
		MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE &= ~0x01 ;
	//--------------------------------------------------//
	// Tx2
	//--------------------------------------------------//
	if( TxDIS_Control & 0x02 )
		MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE |= 0x02 ;
	else
		MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE &= ~0x02 ;
	//--------------------------------------------------//
	// Tx3
	//--------------------------------------------------//
	if( TxDIS_Control & 0x04 )
		MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE |= 0x04 ;
	else
		MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE &= ~0x04 ;
	//--------------------------------------------------//
	// Tx4
	//--------------------------------------------------//
	if( TxDIS_Control & 0x08 )
		MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE |= 0x08 ;
	else
		MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE &= ~0x08 ;
	//--------------------------------------------------//
	// Tx5
	//--------------------------------------------------//
	if( TxDIS_Control & 0x10 )
		MATA38435_TX5_TX8_MEMORY_MAP.TX_DISABLE |= 0x01 ;
	else
		MATA38435_TX5_TX8_MEMORY_MAP.TX_DISABLE &= ~0x01 ;
	//--------------------------------------------------//
	// Tx6
	//--------------------------------------------------//
	if( TxDIS_Control & 0x20 )
		MATA38435_TX5_TX8_MEMORY_MAP.TX_DISABLE |= 0x02 ;
	else
		MATA38435_TX5_TX8_MEMORY_MAP.TX_DISABLE &= ~0x02 ;
	//--------------------------------------------------//
	// Tx7
	//--------------------------------------------------//
	if( TxDIS_Control & 0x40 )
		MATA38435_TX5_TX8_MEMORY_MAP.TX_DISABLE |= 0x04 ;
	else
		MATA38435_TX5_TX8_MEMORY_MAP.TX_DISABLE &= ~0x04 ;
	//--------------------------------------------------//
	// Tx7
	//--------------------------------------------------//
	if( TxDIS_Control & 0x80 )
		MATA38435_TX5_TX8_MEMORY_MAP.TX_DISABLE |= 0x08 ;
	else
		MATA38435_TX5_TX8_MEMORY_MAP.TX_DISABLE &= ~0x08 ;
	// Tx1-Tx4 I2C is Master IC1
	Master_I2C_WriteByte( MALD38435_SADR , 0x41 , MATA38435_TX1_TX4_MEMORY_MAP.TX_DISABLE , Master_I2C1 );
	// Tx5-Tx8 I2C is Master IC2
	Master_I2C_WriteByte( MALD38435_SADR , 0x41 , MATA38435_TX5_TX8_MEMORY_MAP.TX_DISABLE , Master_I2C2 );

}
//------------------------------------------------------//
// Data Path Error_Applcation
//------------------------------------------------------//
void MSA_DATAPATH_ERROR_CODE()
{
	unsigned char Check_Pass_Count = 0 ;
	unsigned char FOR_COUNT = 0;
	QSFPDD_Page11[74]  = 0x00;
	QSFPDD_Page11[75]  = 0x00;
	QSFPDD_Page11[76]  = 0x00;
	QSFPDD_Page11[77]  = 0x00;
	QSFPDD_Page11[78]  = 0x00;
	QSFPDD_Page11[79]  = 0x00;
	QSFPDD_Page11[80]  = 0x00;
	QSFPDD_Page11[81]  = 0x00;
	QSFPDD_Page11[82]  = 0x00;
	QSFPDD_Page11[83]  = 0x00;
	QSFPDD_Page11[84]  = 0x00;
	QSFPDD_Page11[85]  = 0x00;

	//-----------------------------------------------------//
	// Lan1 page10 145
	//-----------------------------------------------------//
	if((QSFPDD_Page10[17]==QDD400G_PAM4_MODE)||(QSFPDD_Page10[17]==QDD400G_NRZ_MODE)||(QSFPDD_Page10[17]==QDD400G_PAM4_8P_PAM4_MODE))
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[78]  = QSFPDD_Page10[17];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[74]  |= Error_code_ConfigAccept ;
	}
	else
		// feedback error code 202 lan1 Rejected Invalid Code
		QSFPDD_Page11[74]  |= Error_code_RejectedInvalidCode ;
	//-----------------------------------------------------//
	// Lan2 page10 146
	//-----------------------------------------------------//
	if((QSFPDD_Page10[18]==QDD400G_PAM4_MODE)||(QSFPDD_Page10[18]==QDD400G_NRZ_MODE)||(QSFPDD_Page10[18]==QDD400G_PAM4_8P_PAM4_MODE))
	{
		// page10 146 value write to page11 207
		QSFPDD_Page11[79]  = QSFPDD_Page10[18];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[74]  |= Error_code_ConfigAccept<<4 ;
	}
	else
		// feedback error code 202 lan1 Rejected Invalid Code
		QSFPDD_Page11[74]  |= Error_code_RejectedInvalidCode<<4 ;
	//-----------------------------------------------------//
	// Lan3 page10 147
	//-----------------------------------------------------//
	if((QSFPDD_Page10[19]==QDD400G_PAM4_MODE)||(QSFPDD_Page10[19]==QDD400G_NRZ_MODE)||(QSFPDD_Page10[19]==QDD400G_PAM4_8P_PAM4_MODE))
	{
		// page10 147 value write to page11 208
		QSFPDD_Page11[80]  = QSFPDD_Page10[19];
		// feedback error code 203 lan1 successfully
		QSFPDD_Page11[75]  |= Error_code_ConfigAccept ;
	}
	else
		// feedback error code 203 lan1 Rejected Invalid Code
		QSFPDD_Page11[75]  |= Error_code_RejectedInvalidCode ;
	//-----------------------------------------------------//
	// Lan4 page10 148
	//-----------------------------------------------------//
	if((QSFPDD_Page10[20]==QDD400G_PAM4_MODE)||(QSFPDD_Page10[20]==QDD400G_NRZ_MODE)||(QSFPDD_Page10[20]==QDD400G_PAM4_8P_PAM4_MODE))
	{
		// page10 148 value write to page11 209
		QSFPDD_Page11[81]  = QSFPDD_Page10[20];
		// feedback error code 203 lan1 successfully
		QSFPDD_Page11[75]  |= Error_code_ConfigAccept<<4 ;
	}
	else
		// feedback error code 203 lan1 Rejected Invalid Code
		QSFPDD_Page11[75]  |= Error_code_RejectedInvalidCode<<4 ;
	//-----------------------------------------------------//
	// Lan5 page10 149
	//-----------------------------------------------------//
	if((QSFPDD_Page10[21]==QDD400G_PAM4_MODE)||(QSFPDD_Page10[21]==QDD400G_NRZ_MODE)||(QSFPDD_Page10[21]==QDD400G_PAM4_8P_PAM4_MODE))
	{
		// page10 149 value write to page11 210
		QSFPDD_Page11[82]  = QSFPDD_Page10[21];
		// feedback error code 204 lan1 successfully
		QSFPDD_Page11[76]  |= Error_code_ConfigAccept ;
	}
	else
		// feedback error code 204 lan1 Rejected Invalid Code
		QSFPDD_Page11[76]  |= Error_code_RejectedInvalidCode ;
	//-----------------------------------------------------//
	// Lan6 page10 150
	//-----------------------------------------------------//
	if((QSFPDD_Page10[22]==QDD400G_PAM4_MODE)||(QSFPDD_Page10[22]==QDD400G_NRZ_MODE)||(QSFPDD_Page10[22]==QDD400G_PAM4_8P_PAM4_MODE))
	{
		// page10 150 value write to page11 211
		QSFPDD_Page11[83]  = QSFPDD_Page10[22];
		// feedback error code 204 lan1 successfully
		QSFPDD_Page11[76]  |= Error_code_ConfigAccept<<4 ;
	}
	else
		// feedback error code 204 lan1 Rejected Invalid Code
		QSFPDD_Page11[76]  |= Error_code_RejectedInvalidCode<<4 ;
	//-----------------------------------------------------//
	// Lan6 page10 151
	//-----------------------------------------------------//
	if((QSFPDD_Page10[23]==QDD400G_PAM4_MODE)||(QSFPDD_Page10[23]==QDD400G_NRZ_MODE)||(QSFPDD_Page10[23]==QDD400G_PAM4_8P_PAM4_MODE))
	{
		// page10 151 value write to page11 212
		QSFPDD_Page11[84]  = QSFPDD_Page10[23];
		// feedback error code 205 lan1 successfully
		QSFPDD_Page11[77]  |= Error_code_ConfigAccept ;
	}
	else
		// feedback error code 205 lan1 Rejected Invalid Code
		QSFPDD_Page11[77]  |= Error_code_RejectedInvalidCode ;
	//-----------------------------------------------------//
	// Lan6 page10 152
	//-----------------------------------------------------//
	if((QSFPDD_Page10[24]==QDD400G_PAM4_MODE)||(QSFPDD_Page10[24]==QDD400G_NRZ_MODE)||(QSFPDD_Page10[24]==QDD400G_PAM4_8P_PAM4_MODE))
	{
		// page10 152 value write to page11 213
		QSFPDD_Page11[85]  = QSFPDD_Page10[24];
		// feedback error code 205 lan1 successfully
		QSFPDD_Page11[77]  |= Error_code_ConfigAccept<<4 ;
	}
	else
		// feedback error code 205 lan1 Rejected Invalid Code
		QSFPDD_Page11[77]  |= Error_code_RejectedInvalidCode<<4 ;

	//  confirm 206 - 213 value is sams or not ?
	for(FOR_COUNT=1;FOR_COUNT<8;FOR_COUNT++)
	{
		if( QSFPDD_Page11[78] == QSFPDD_Page11[78+FOR_COUNT])
			Check_Pass_Count++;
	}

	if(Check_Pass_Count==7)
	{
		if( QSFPDD_Page11[78] == QDD400G_PAM4_MODE )
			DSP_MODE_SET = QDD400G_PAM4_MODE ;
		else if( QSFPDD_Page11[78] == QDD400G_NRZ_MODE )
			DSP_MODE_SET = QDD400G_NRZ_MODE ;
		else if( QSFPDD_Page11[78] == QDD400G_PAM4_8P_PAM4_MODE )
			DSP_MODE_SET = QDD400G_PAM4_8P_PAM4_MODE ;

		Error_code_Pass = 1 ;
	}
	Check_Pass_Count = 0 ;
}

//------------------------------------------------------//
// Lower Page Control
//------------------------------------------------------//
void Module_Global_Control_P0H_26()
{
	unsigned char SFRPAGE_SAVE = SFRPAGE;   // preserve SFRPAGE
	if( Bef_Module_Global_Control!=QSFPDD_A0[26] )
	{
		if( QSFPDD_A0[26] & 0x08 )
		{
			SFRPAGE = 0x00;
			RSTSRC = RSTSRC_SWRSF__SET | RSTSRC_PORSF__SET;
			SFRPAGE = SFRPAGE_SAVE;
		}

/*		if( QSFPDD_A0[26] & 0x80 )
		{
			MALD38435_TX1_TX4_RESET();
			MALD38435_TX5_TX8_RESET();
			QSFPDD_A0[26] = 0x00;
		}

		if( QSFPDD_A0[26] & 0x10 )
		{
			// LDD Disable
			TX_DIS_FUNCTION(0xFF);
			// Enable DSP low power mode
			LPMODE_DSP = 1;
			IntL = 0 ;
			QSFPDD_A0[3] = MODULE_LOW_PWR ;
			QSFPDD_A0[8] |= 0x01 ;
		}
		else
		{
			// Disable DSP low power mode
			LPMODE_DSP = 0;
			Delay_ms(80);
			DSP59281_SET_ALL_CH_FIR();
			// LDD Enable
			TX_DIS_FUNCTION(0x00);

			IntL = 0 ;
			QSFPDD_A0[3] = MODULE_READY ;
			QSFPDD_A0[8] |= 0x01 ;
		}*/
	}

	Bef_Module_Global_Control = QSFPDD_A0[26];
}
//------------------------------------------------------//
// DataPath Power Control 0x80
//------------------------------------------------------//
void DataPath_Power_control_P10H_80()
{
	unsigned int System_side_squelch_Value = 0;

	if( Bef_DataPath_Power != QSFPDD_Page10[0] )
	{
		QSFPDD_Page11[0] = 0 ;
		QSFPDD_Page11[1] = 0 ;
		QSFPDD_Page11[2] = 0 ;
		QSFPDD_Page11[3] = 0 ;
		//-----------------------------------------------------//
		// TXR 1
		//-----------------------------------------------------//
		if( QSFPDD_Page10[0] & 0x01)
		{
			QSFPDD_Page11[0] |= 0x04 ;
			System_side_squelch_Value &= ~0x0101 ;
		}
		else
		{
			QSFPDD_Page11[0] |= 0x01 ;
			System_side_squelch_Value |= 0x0101 ;
		}
		//-----------------------------------------------------//
		// TXR 2
		//-----------------------------------------------------//
		if( QSFPDD_Page10[0] & 0x02)
		{
			QSFPDD_Page11[0] |= 0x40 ;
			System_side_squelch_Value &= ~0x0202 ;
		}
		else
		{
			QSFPDD_Page11[0] |= 0x10 ;
			System_side_squelch_Value |= 0x0202 ;
		}
		//-----------------------------------------------------//
		// TXR 3
		//-----------------------------------------------------//
		if( QSFPDD_Page10[0] & 0x04)
		{
			QSFPDD_Page11[1] |= 0x04 ;
			System_side_squelch_Value &= ~0x0404 ;
		}
		else
		{
			QSFPDD_Page11[1] |= 0x01 ;
			System_side_squelch_Value |= 0x0404 ;
		}
		//-----------------------------------------------------//
		// TXR 4
		//-----------------------------------------------------//
		if( QSFPDD_Page10[0] & 0x08)
		{
			QSFPDD_Page11[1] |= 0x40 ;
			System_side_squelch_Value &= ~0x0808 ;
		}
		else
		{
			QSFPDD_Page11[1] |= 0x10 ;
			System_side_squelch_Value |= 0x0808 ;
		}
		//-----------------------------------------------------//
		// TXR 5
		//-----------------------------------------------------//
		if( QSFPDD_Page10[0] & 0x10)
		{
			QSFPDD_Page11[2] |= 0x04 ;
			System_side_squelch_Value &= ~0x1010 ;
		}
		else
		{
			QSFPDD_Page11[2] |= 0x01 ;
			System_side_squelch_Value |= 0x1010 ;
		}
		//-----------------------------------------------------//
		// TXR 6
		//-----------------------------------------------------//
		if( QSFPDD_Page10[0] & 0x20)
		{
			QSFPDD_Page11[2] |= 0x40 ;
			System_side_squelch_Value &= ~0x2020 ;
		}
		else
		{
			QSFPDD_Page11[2] |= 0x10 ;
			System_side_squelch_Value |= 0x2020 ;
		}
		//-----------------------------------------------------//
		// TXR 7
		//-----------------------------------------------------//
		if( QSFPDD_Page10[0] & 0x40)
		{
			QSFPDD_Page11[3] |= 0x04 ;
			System_side_squelch_Value &= ~0x4040 ;
		}
		else
		{
			QSFPDD_Page11[3] |= 0x01 ;
			System_side_squelch_Value |= 0x4040 ;
		}
		//-----------------------------------------------------//
		// TXR 8
		//-----------------------------------------------------//
		if( QSFPDD_Page10[0] & 0x80)
		{
			QSFPDD_Page11[3] |= 0x40 ;
			System_side_squelch_Value &= ~0x8080 ;
		}
		else
		{
			QSFPDD_Page11[3] |= 0x10 ;
			System_side_squelch_Value |= 0x8080 ;
		}

		System_side_MSA_Squelch_Control( System_side_squelch_Value );
	}
	Bef_DataPath_Power = QSFPDD_Page10[0] ;
}

//------------------------------------------------------//
// Rx Ouput DIS 0x8A
//------------------------------------------------------//
void MSA_Rx_OUTPUT_Control_P10H_8A()
{
	unsigned int RX_Squelch_Data = 0 ;
	// Rx1
	if(QSFPDD_Page10[10]&0x01)
		RX_Squelch_Data |= 0x0100 ;
	else
		RX_Squelch_Data &= ~0x0100 ;
	// Rx2
	if(QSFPDD_Page10[10]&0x02)
		RX_Squelch_Data |= 0x0200 ;
	else
		RX_Squelch_Data &= ~0x0200 ;
	// Rx3
	if(QSFPDD_Page10[10]&0x04)
		RX_Squelch_Data |= 0x0400 ;
	else
		RX_Squelch_Data &= ~0x0400 ;
	// Rx4
	if(QSFPDD_Page10[10]&0x08)
		RX_Squelch_Data |= 0x0800 ;
	else
		RX_Squelch_Data &= ~0x0800 ;
	// Rx5
	if(QSFPDD_Page10[10]&0x10)
		RX_Squelch_Data |= 0x1000 ;
	else
		RX_Squelch_Data &= ~0x1000 ;
	// Rx6
	if(QSFPDD_Page10[10]&0x20)
		RX_Squelch_Data |= 0x2000 ;
	else
		RX_Squelch_Data &= ~0x2000 ;
	// Rx7
	if(QSFPDD_Page10[10]&0x40)
		RX_Squelch_Data |= 0x4000 ;
	else
		RX_Squelch_Data &= ~0x4000 ;
	// Rx8
	if(QSFPDD_Page10[10]&0x80)
		RX_Squelch_Data |= 0x8000 ;
	else
		RX_Squelch_Data &= ~0x8000 ;

	if(Bef_Rx_output!=QSFPDD_Page10[10])
		System_Side_Squelch_Control( RX_Squelch_Data );

	Bef_Rx_output = QSFPDD_Page10[10];
}
//------------------------------------------------------//
// Modeule_MGMT_INIT
//------------------------------------------------------//
void Modeule_MGMT_INIT()
{
	unsigned int Get_ADC_Value ;
	// LDD Disable
	TX_DIS_FUNCTION(0xff);
	// Enable DSP low power mode
	LPMODE_DSP = 1;

	if( CALIB_MEMORY_MAP.QDD_SoftInitM_EN == 0x01 )
	{	// Software Init Mode and Low Power mode

		//Get_ADC_Value = ADCV_Conversion_Voltage(LPMODE_ADC);
		if( Power_mode_function == 0 )                // Low Power Software Init
		{
			SoftWare_Init_flag = 1 ;
			DSP_MODE_SET = QDD400G_PAM4_MODE ;
			Module_Status = MODULE_LOW_PWR ;
			//Module_Status |= 0x01 ;
			IntL = 0 ;
			QSFPDD_A0[3] = Module_Status ;
			QSFPDD_A0[8] |= 0x01 ;
		}
		// HW Init Mode
		else                                        // High Power HW Init
		{
			DSP_MODE_SET = QDD400G_PAM4_MODE ;
			SoftWare_Init_flag = 0 ;

			Module_Status = MODULE_LOW_PWR ;
			IntL = 0 ;
			QSFPDD_A0[3] = Module_Status ;
			QSFPDD_A0[8] |= 0x01 ;
		}
	}
	else
	{
		DSP_MODE_SET = QDD400G_PAM4_MODE ;
		SoftWare_Init_flag = 0 ;

		Module_Status = MODULE_LOW_PWR ;
		IntL = 0 ;
		QSFPDD_A0[3] = Module_Status ;
		QSFPDD_A0[8] |= 0x01 ;
	}
}
//------------------------------------------------------//
// Application Select Controls
//------------------------------------------------------//
void CMIS30_APP_SELECT_Control()
{
	unsigned char SFRPAGE_SAVE = SFRPAGE;   // preserve SFRPAGE

	SFRPAGE = 0x00;
/*
	application code 0x10 = PAM4 MODE 1 * 400G
	application code 0x20 = NRZ MODE
	application code 0x30 = PAM4 MODE 8 * 53G
	application code 0x40 = Enable SSPRQ
	application code 0x50 = Disable SSPRQ
	application code 0x60 = Enable PRBS13Q
	application code 0x70 = Disable PRBS13Q
	application code 0x80 = System side remote loopback Enable
	application code 0x90 = System side remote loopback Disable
	application code 0xA0 = Line side G loopback Enable
	application code 0xB0 = Line side G loopback Disable


	QSFPDD_Page10[17] Lan0  QSFPDD_Page10[18] Lan1 QSFPDD_Page10[19] Lan2
	QSFPDD_Page10[20] Lan3  QSFPDD_Page10[21] Lan4 QSFPDD_Page10[22] Lan5
	QSFPDD_Page10[23] Lan6  QSFPDD_Page10[24] Lan7
*/
	//------------------------------------------------------------------//
	// QSFPDD_Page10[17] Lan0 application
	//------------------------------------------------------------------//
	if(QSFPDD_Page10[17]==0x40)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[78]  = QSFPDD_Page10[17];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[74]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_SSPRQ_Control_Enable(0);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[17]==0x50)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[78]  = QSFPDD_Page10[17];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[74]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_SSPRQ_Control_Disable(0);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[17]==0x60)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[78]  = QSFPDD_Page10[17];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[74]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_PRBS13Q_Control_Enable(0);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[17]==0x70)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[78]  = QSFPDD_Page10[17];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[74]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_PRBS13Q_Control_Disable(0);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[17]==0x80)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[78]  = QSFPDD_Page10[17];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[74]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_System_side_Remote_Loopback_Control(0,1);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[17]==0x90)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[78]  = QSFPDD_Page10[17];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[74]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_System_side_Remote_Loopback_Control(0,0);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[17]==0xA0)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[78]  = QSFPDD_Page10[17];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[74]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_Line_Side_G_Loopback_Control(0,1);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[17]==0xB0)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[78]  = QSFPDD_Page10[17];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[74]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_Line_Side_G_Loopback_Control(0,0);
		SMB0CF |= 0x80 ;
	}
	//------------------------------------------------------------------//
	// QSFPDD_Page10[18] Lan1 application
	//------------------------------------------------------------------//
	if(QSFPDD_Page10[18]==0x40)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[79]  = QSFPDD_Page10[18];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[74]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_SSPRQ_Control_Enable(1);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[18]==0x50)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[79]  = QSFPDD_Page10[18];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[74]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_SSPRQ_Control_Disable(1);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[18]==0x60)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[79]  = QSFPDD_Page10[18];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[74]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_PRBS13Q_Control_Enable(1);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[18]==0x70)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[79]  = QSFPDD_Page10[18];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[74]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_PRBS13Q_Control_Disable(1);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[18]==0x80)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[79]  = QSFPDD_Page10[18];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[74]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_System_side_Remote_Loopback_Control(1,1);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[18]==0x90)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[79]  = QSFPDD_Page10[18];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[74]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_System_side_Remote_Loopback_Control(1,0);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[18]==0xA0)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[79]  = QSFPDD_Page10[18];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[74]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_Line_Side_G_Loopback_Control(1,1);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[18]==0xB0)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[79]  = QSFPDD_Page10[18];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[74]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_Line_Side_G_Loopback_Control(1,0);
		SMB0CF |= 0x80 ;
	}
	//------------------------------------------------------------------//
	// QSFPDD_Page10[19] Lan2 application
	//------------------------------------------------------------------//
	if(QSFPDD_Page10[19]==0x40)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[80]  = QSFPDD_Page10[19];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[75]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_SSPRQ_Control_Enable(2);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[19]==0x50)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[80]  = QSFPDD_Page10[19];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[75]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_SSPRQ_Control_Disable(2);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[19]==0x60)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[80]  = QSFPDD_Page10[19];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[75]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_PRBS13Q_Control_Enable(2);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[19]==0x70)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[80]  = QSFPDD_Page10[19];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[75]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_PRBS13Q_Control_Disable(2);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[19]==0x80)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[80]  = QSFPDD_Page10[19];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[75]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_System_side_Remote_Loopback_Control(2,1);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[19]==0x90)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[80]  = QSFPDD_Page10[19];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[75]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_System_side_Remote_Loopback_Control(2,0);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[19]==0xA0)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[80]  = QSFPDD_Page10[19];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[75]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_Line_Side_G_Loopback_Control(2,1);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[19]==0xB0)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[80]  = QSFPDD_Page10[19];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[75]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_Line_Side_G_Loopback_Control(2,0);
		SMB0CF |= 0x80 ;
	}
	//------------------------------------------------------------------//
	// QSFPDD_Page10[20] Lan3 application
	//------------------------------------------------------------------//
	if(QSFPDD_Page10[20]==0x40)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[81]  = QSFPDD_Page10[20];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[75]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_SSPRQ_Control_Enable(3);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[20]==0x50)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[81]  = QSFPDD_Page10[20];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[75]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_SSPRQ_Control_Disable(3);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[20]==0x60)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[81]  = QSFPDD_Page10[20];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[75]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_PRBS13Q_Control_Enable(3);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[20]==0x70)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[81]  = QSFPDD_Page10[20];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[75]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_PRBS13Q_Control_Disable(3);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[20]==0x80)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[81]  = QSFPDD_Page10[20];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[75]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_System_side_Remote_Loopback_Control(3,1);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[20]==0x90)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[81]  = QSFPDD_Page10[20];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[75]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_System_side_Remote_Loopback_Control(3,0);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[20]==0xA0)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[81]  = QSFPDD_Page10[20];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[75]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_Line_Side_G_Loopback_Control(3,1);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[20]==0xB0)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[81]  = QSFPDD_Page10[20];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[75]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_Line_Side_G_Loopback_Control(3,0);
		SMB0CF |= 0x80 ;
	}
	//------------------------------------------------------------------//
	// QSFPDD_Page10[21] Lan4 application
	//------------------------------------------------------------------//
	if(QSFPDD_Page10[21]==0x40)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[82]  = QSFPDD_Page10[21];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[76]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_SSPRQ_Control_Enable(4);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[21]==0x50)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[82]  = QSFPDD_Page10[21];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[76]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_SSPRQ_Control_Disable(4);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[21]==0x60)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[82]  = QSFPDD_Page10[21];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[76]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_PRBS13Q_Control_Enable(4);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[21]==0x70)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[82]  = QSFPDD_Page10[21];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[76]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_PRBS13Q_Control_Disable(4);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[21]==0x80)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[82]  = QSFPDD_Page10[21];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[76]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_System_side_Remote_Loopback_Control(4,1);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[21]==0x90)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[82]  = QSFPDD_Page10[21];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[76]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_System_side_Remote_Loopback_Control(4,0);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[21]==0xA0)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[82]  = QSFPDD_Page10[21];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[76]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_Line_Side_G_Loopback_Control(4,1);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[21]==0xB0)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[82]  = QSFPDD_Page10[21];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[76]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_Line_Side_G_Loopback_Control(4,0);
		SMB0CF |= 0x80 ;
	}
	//------------------------------------------------------------------//
	// QSFPDD_Page10[22] Lan5 application
	//------------------------------------------------------------------//
	if(QSFPDD_Page10[22]==0x40)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[83]  = QSFPDD_Page10[22];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[76]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_SSPRQ_Control_Enable(5);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[22]==0x50)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[83]  = QSFPDD_Page10[22];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[76]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_SSPRQ_Control_Disable(5);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[22]==0x60)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[83]  = QSFPDD_Page10[22];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[76]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_PRBS13Q_Control_Enable(5);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[22]==0x70)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[83]  = QSFPDD_Page10[22];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[76]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_PRBS13Q_Control_Disable(5);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[22]==0x80)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[83]  = QSFPDD_Page10[22];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[76]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_System_side_Remote_Loopback_Control(5,1);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[22]==0x90)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[83]  = QSFPDD_Page10[22];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[76]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_Line_Side_G_Loopback_Control(5,0);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[22]==0xA0)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[83]  = QSFPDD_Page10[22];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[76]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_Line_Side_G_Loopback_Control(5,1);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[22]==0xB0)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[83]  = QSFPDD_Page10[22];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[76]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_Line_Side_G_Loopback_Control(5,0);
		SMB0CF |= 0x80 ;
	}
	//------------------------------------------------------------------//
	// QSFPDD_Page10[23] Lan6 application
	//------------------------------------------------------------------//
	if(QSFPDD_Page10[23]==0x40)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[84]  = QSFPDD_Page10[23];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[77]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_SSPRQ_Control_Enable(6);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[23]==0x50)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[84]  = QSFPDD_Page10[23];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[77]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_SSPRQ_Control_Disable(6);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[23]==0x60)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[84]  = QSFPDD_Page10[23];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[77]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_PRBS13Q_Control_Enable(6);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[23]==0x70)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[84]  = QSFPDD_Page10[23];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[77]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_PRBS13Q_Control_Disable(6);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[23]==0x80)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[84]  = QSFPDD_Page10[23];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[77]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_System_side_Remote_Loopback_Control(6,1);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[23]==0x90)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[84]  = QSFPDD_Page10[23];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[77]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_System_side_Remote_Loopback_Control(6,0);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[23]==0xA0)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[84]  = QSFPDD_Page10[23];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[77]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_Line_Side_G_Loopback_Control(6,1);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[23]==0xB0)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[84]  = QSFPDD_Page10[23];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[77]  |= Error_code_ConfigAccept ;
		SMB0CF &= 0x7F;
		DSP59281_Line_Side_G_Loopback_Control(6,0);
		SMB0CF |= 0x80 ;
	}
	//------------------------------------------------------------------//
	// QSFPDD_Page10[24] Lan7 application
	//------------------------------------------------------------------//
	if(QSFPDD_Page10[24]==0x40)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[85]  = QSFPDD_Page10[24];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[77]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_SSPRQ_Control_Enable(7);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[24]==0x50)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[85]  = QSFPDD_Page10[24];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[77]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_SSPRQ_Control_Disable(7);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[24]==0x60)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[85]  = QSFPDD_Page10[24];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[77]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_PRBS13Q_Control_Enable(7);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[24]==0x70)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[85]  = QSFPDD_Page10[24];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[77]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_PRBS13Q_Control_Disable(7);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[24]==0x80)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[85]  = QSFPDD_Page10[24];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[77]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_System_side_Remote_Loopback_Control(7,1);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[24]==0x90)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[85]  = QSFPDD_Page10[24];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[77]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_System_side_Remote_Loopback_Control(7,0);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[24]==0xA0)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[85]  = QSFPDD_Page10[24];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[77]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_Line_Side_G_Loopback_Control(7,1);
		SMB0CF |= 0x80 ;
	}
	else if(QSFPDD_Page10[24]==0xB0)
	{
		// page10 145 value write to page11 206
		QSFPDD_Page11[85]  = QSFPDD_Page10[24];
		// feedback error code 202 lan1 successfully
		QSFPDD_Page11[77]  |= Error_code_ConfigAccept<<4 ;
		SMB0CF &= 0x7F;
		DSP59281_Line_Side_G_Loopback_Control(7,0);
		SMB0CF |= 0x80 ;
	}

	SFRPAGE = SFRPAGE_SAVE;
}

void CMIS30_APP_SELECT_Control_Change_Check()
{
	unsigned char Change_flag = 0;

    if( Bef_APP_Byte145_Lan0 != QSFPDD_Page10[17] )
    {
    	Change_flag = 0x01 ;
    	QSFPDD_Page10[17] &= 0xF0;
    }
    if( Bef_APP_Byte146_Lan1 != QSFPDD_Page10[18] )
    {
    	Change_flag = 0x02 ;
    	QSFPDD_Page10[18] &= 0xF0;
    }
    if( Bef_APP_Byte147_Lan2 != QSFPDD_Page10[19] )
    {
    	Change_flag = 0x04 ;
    	QSFPDD_Page10[19] &= 0xF0;
    }
    if( Bef_APP_Byte148_Lan3 != QSFPDD_Page10[20] )
    {
    	Change_flag = 0x08 ;
    	QSFPDD_Page10[20] &= 0xF0;
    }

	if( Bef_APP_Byte149_Lan4 != QSFPDD_Page10[21] )
	{
		Change_flag = 0x10 ;
		QSFPDD_Page10[21] &= 0xF0;
	}
	if( Bef_APP_Byte150_Lan5 != QSFPDD_Page10[22] )
	{
		Change_flag = 0x20 ;
		QSFPDD_Page10[22] &= 0xF0;
	}
	if( Bef_APP_Byte151_Lan6 != QSFPDD_Page10[23] )
	{
		Change_flag = 0x40 ;
		QSFPDD_Page10[23] &= 0xF0;
	}
	if( Bef_APP_Byte152_Lan7 != QSFPDD_Page10[24] )
	{
		Change_flag = 0x80 ;
		QSFPDD_Page10[24] &= 0xF0;
	}

	if(Change_flag)
		CMIS30_APP_SELECT_Control();

    Bef_APP_Byte145_Lan0 = QSFPDD_Page10[17] ;
    Bef_APP_Byte146_Lan1 = QSFPDD_Page10[18] ;
    Bef_APP_Byte147_Lan2 = QSFPDD_Page10[19] ;
    Bef_APP_Byte148_Lan3 = QSFPDD_Page10[20] ;
    Bef_APP_Byte149_Lan4 = QSFPDD_Page10[21] ;
    Bef_APP_Byte150_Lan5 = QSFPDD_Page10[22] ;
    Bef_APP_Byte151_Lan6 = QSFPDD_Page10[23] ;
    Bef_APP_Byte152_Lan7 = QSFPDD_Page10[24] ;
}

//------------------------------------------------------//
// QSFPDD_MSA_StateMachine
//------------------------------------------------------//
void QSFPDD_MSA_StateMachine()
{
	unsigned char SFRPAGE_SAVE = SFRPAGE;   // preserve SFRPAGE
	switch( Module_Status )
	{
		case MODULE_MGMT_INIT:
			 Modeule_MGMT_INIT();
			 break;

		case MODULE_LOW_PWR:
			 // Software Init Mode
			 if(SoftWare_Init_flag==1)
			 {
				 if(QSFPDD_Page10[15]==0xFF)
				 {
					 MSA_DATAPATH_ERROR_CODE();
					 QSFPDD_Page10[15] = 0x00 ;
				 }

				// if(Error_code_Pass)
				// {
				 if(QSFPDD_Page10[0]==0xFF)
				 {
					 Module_Status = MODULE_PWR_UP ;
					 IntL = 0 ;
					 Error_code_Pass = 0x00 ;

					 QSFPDD_A0[3] = MODULE_PWR_UP ;
					 QSFPDD_A0[8] |= 0x01 ;
				 }
				// }
			 }
			 // HW Init Mode
			 else
			 {
				 Module_Status = MODULE_PWR_UP ;
				 IntL = 0 ;

				 QSFPDD_A0[3] = MODULE_PWR_UP ;
				 QSFPDD_A0[8] |= 0x01 ;
			 }

			 break;

		case MODULE_PWR_UP:

			 // Disable DSP low power mode
			 LPMODE_DSP = 0;

			 SFRPAGE = 0x00;
			 // Disable I2C
			 SMB0CF &= 0x7F;

			 SMB0CN0_STA = 0;
			 SMB0CN0_STO = 0;
			 SMB0CN0_ACK = 0;
			 SMB0CN0_SI = 0;

			// LDD
			MALD38435_TX1_TX4_RESET();
			MALD38435_TX1_TX4_Control_Write_ALL();
			MALD38435_TX5_TX8_RESET();
			MALD38435_TX5_TX8_Control_Write_ALL();

			 DSP59281_Init(DSP_MODE_SET);

			 // Enable I2C
			 SMB0CF |= 0x80;
			 SFRPAGE = SFRPAGE_SAVE;

			 Module_Status = MODULE_READY ;
			 IntL = 0 ;

			 QSFPDD_A0[3] = Module_Status ;
			 QSFPDD_A0[8] |= 0x01 ;

			 QSFPDD_Page10[0] = 0xFF ;
			 // LDD Enable
			 // HW Init auto Enable Tx power
			 //if(SoftWare_Init_flag==0)
			 //{
			 //QSFPDD_Page10[2] = 0x00 ;
			 TX_DIS_FUNCTION(QSFPDD_Page10[2]);
			 //}
			 //QSFPDD_Page10[2] = 0x00 ;
			 //TX_DIS_FUNCTION(0x00);

			 QSFPDD_Page11[0] = 0x44 ;
			 QSFPDD_Page11[1] = 0x44 ;
			 QSFPDD_Page11[2] = 0x44 ;
			 QSFPDD_Page11[3] = 0x44 ;

			 break;

		case MODULE_READY:
			 // DDMI Function update
			 if( DDMI_DataCount > 600 )
			 {
				 if( CALIB_MEMORY_MAP.DDMI_DISABLE == 1)
					 QSFPDD_DDMI_ONLY_ADC_GET();
				 else
				 {
					 // Status Machine Loop
					 QSFPDD_DDMI_StateMachine( DDMI_Update_C );
					 DDMI_Update_C++;
					 //Status Machine flag
					 if( DDMI_Update_C >= 21 )
					 {
						 DDMI_Update_C = 0;
						 if(Power_on_flag==0)
						 {
							 AW_DDMI_Count ++ ;
							 if(AW_DDMI_Count>20)
								 Power_on_flag = 1;
						 }
					 }
				 }
				 DDMI_DataCount = 0;
			 }
			 DDMI_DataCount++;
			 break;

		case MODULE_PWR_DN:
			 break;

		case MODULE_FAULT:
			 break;
	}

	if( Module_Status != MODULE_READY )
	{
		VCC_Monitor(ADCV_Conversion_Voltage( P3V3_TRX_VCC )*2);
		Temperature_Monitor(Get_Tempsensor());
	}

	if(( Module_Status == MODULE_READY )||( Module_Status == MODULE_PWR_UP ))
	{
		// MSA I2C Read/Write Control
		if(Page10_Write_flag==1)
		{
			DataPath_Power_control_P10H_80();
			MSA_Rx_OUTPUT_Control_P10H_8A();

			if(Bef_TxDIS_Power!=QSFPDD_Page10[2])
				TX_DIS_FUNCTION(QSFPDD_Page10[2]);

			Bef_TxDIS_Power=QSFPDD_Page10[2];
			// 2019_08_27 Ivan modify for no change output setting by power on
			if( Module_Status == MODULE_READY )
			{
				QDD_MSA_CTLE_PRE_POST_AMP_CONTROL(Page10_Write_flag);
				CMIS30_APP_SELECT_Control_Change_Check();
			}

			Page10_Write_flag = 0 ;
		}

	}

	Module_Global_Control_P0H_26();
}







