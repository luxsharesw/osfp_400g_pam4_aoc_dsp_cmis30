/*
 * QSFPDD_MSA_DDMI.c
 *
 *  Created on: 2017�~11��9��
 *      Author: Ivan_Lin
 */
#include <SI_EFM8LB1_Register_Enums.h>                  // SFR declarations
#include <stdint.h>
#include <Master_I2C_GPIO.h>
#include "MA38435_TX1_TX4.h"
#include "MA38435_TX5_TX8.h"
#include "MA38434_RX1_RX4.h"
#include "MA38434_RX5_RX8.h"

#include "MSA_QSFPDD.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "EFM8LB_ADC.h"

#define ADC_SM            0
#define RXP_CH0           1
#define RXP_CH1           2
#define RXP_CH2           3
#define RXP_CH3           4
#define RXP_CH4           5
#define RXP_CH5           6
#define RXP_CH6           7
#define RXP_CH7           8
#define TXB_TXP_0_SM      9
#define TXB_TXP_1_SM     10
#define TXB_TXP_2_SM     11
#define TXB_TXP_3_SM     12
#define TXB_TXP_4_SM     13
#define TXB_TXP_5_SM     14
#define TXB_TXP_6_SM     15
#define TXB_TXP_7_SM     16
#define OP_SM            17
#define TXLOLLOS         18
#define RXLOLLOS         19
#define Other_DDMI       20

#define RSSI_ADC_CH      ADC0MX_ADC0MX__ADC0P19
#define V1V8_TX_CH       ADC0MX_ADC0MX__ADC0P17
#define V1V8_RX_CH       ADC0MX_ADC0MX__ADC0P16
#define VCC_RX_CH        ADC0MX_ADC0MX__ADC0P15
#define VCC_TX_CH        ADC0MX_ADC0MX__ADC0P13
#define VCC_MCU_CH       ADC0MX_ADC0MX__VDD
#define IBisa_MON_CH     ADC0MX_ADC0MX__ADC0P6

#define SHOW_RSSI_Enable   1
#define SHOW_RSSI_Disable  0

xdata unsigned char SHOW_RSSI_CH0 = 1 ;
xdata unsigned char SHOW_RSSI_CH1 = 1 ;
xdata unsigned char SHOW_RSSI_CH2 = 1 ;
xdata unsigned char SHOW_RSSI_CH3 = 1 ;
xdata unsigned char SHOW_RSSI_CH4 = 1 ;
xdata unsigned char SHOW_RSSI_CH5 = 1 ;
xdata unsigned char SHOW_RSSI_CH6 = 1 ;
xdata unsigned char SHOW_RSSI_CH7 = 1 ;

xdata char Temperature_Indx = 0 ;

xdata unsigned char Bef_ForceMute_CH0_CH3 = 0 ;
xdata unsigned char Bef_ForceMute_CH4_CH7 = 0 ;

xdata unsigned int Bias_Current_CH0,Bias_Current_CH1,Bias_Current_CH2,Bias_Current_CH3;
xdata unsigned int Bias_Current_CH4,Bias_Current_CH5,Bias_Current_CH6,Bias_Current_CH7;

extern xdata unsigned char Power_on_flag;


int Temp_CAL( int Vaule , unsigned char Slop , unsigned char Shift , int Offset)
{
	long Buffer_temp ;

	Buffer_temp = (long)Vaule * Slop ;
	Buffer_temp = (long)Buffer_temp >> Shift ;

	Vaule = (long)Buffer_temp + Offset * 256 ;

    return Vaule ;
}

unsigned int CAL_Function( unsigned int Vaule , unsigned char Slop , unsigned char Shift , int Offset )
{
	long Buffer_temp ;

	Buffer_temp = (long)Vaule * Slop ;
	Buffer_temp = (long)Buffer_temp >> Shift ;

	Vaule = (long)Buffer_temp + Offset;

	return Vaule;
}

void Temperature_Monitor(int tempsensor_value)
{
	int Temp_Buffer;
	char Indx_Temp ;

	Temp_Buffer = tempsensor_value ;

	Temp_Buffer = Temp_CAL(tempsensor_value,CALIB_MEMORY_MAP.TEMP_SCALE1M,CALIB_MEMORY_MAP.TEMP_SCALE1L,CALIB_MEMORY_MAP.TEMP_OFFSET1);

	QSFPDD_A0[14] = Temp_Buffer >> 8 ;
	QSFPDD_A0[15] = Temp_Buffer ;

	Indx_Temp = QSFPDD_A0[14];

	Temperature_Indx = ( Indx_Temp+40 )/2 ;
	if(Indx_Temp > 64) Indx_Temp = 64;
}

void VCC_Monitor(unsigned int VCC_Vaule)
{
	VCC_Vaule = CAL_Function( VCC_Vaule , CALIB_MEMORY_MAP.VCC_SCALEM , CALIB_MEMORY_MAP.VCC_SCALEL , CALIB_MEMORY_MAP.VCC_OFFSET ) ;

	QSFPDD_A0[16] = VCC_Vaule >> 8 ;
	QSFPDD_A0[17] = VCC_Vaule ;
}

void BIAS_TxPower_Monitor(unsigned char Channel)
{
	unsigned int MOD_Temp_Buffer = 0;
	unsigned int TXP_BUFFER = 0 ;

	switch(Channel)
	{
		case 0:
				Bias_Current_CH0 = TXBIAS_TX1_TX4(0x00) ;
				TXP_BUFFER = Bias_Current_CH0 ;

				Bias_Current_CH0 = CAL_Function( Bias_Current_CH0 , CALIB_MEMORY_MAP.IBias0_SCALEM , CALIB_MEMORY_MAP.IBias0_SCALE1L , CALIB_MEMORY_MAP.IBias0_OFFSET ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_MAP.TXP0_SCALEM , CALIB_MEMORY_MAP.TXP0_SCALEL , CALIB_MEMORY_MAP.TXP0_OFFSET ) ;

				if( QSFPDD_Page10[2] & 0x01 ) //|| ( LPMODE_STATUS == 1 )  )
				{
					QSFPDD_Page11[42] = 0 ;
					QSFPDD_Page11[43] = 0 ;
					// CH0  Tx Power
					QSFPDD_Page11[26] = 0 ;
					QSFPDD_Page11[27] = 0 ;

					// Alarm & Waring trigger
					QSFPDD_Page11[12] |= 0x01 ;
					QSFPDD_Page11[14] |= 0x01 ;
					QSFPDD_Page11[16] |= 0x01 ;
					QSFPDD_Page11[18] |= 0x01 ;
				}
				else
				{
					QSFPDD_Page11[42] = Bias_Current_CH0 >> 8 ;
					QSFPDD_Page11[43] = Bias_Current_CH0 ;
					// CH0 TxPower
					QSFPDD_Page11[26] = TXP_BUFFER >> 8 ;
					QSFPDD_Page11[27] = TXP_BUFFER ;
				}
				break;
		case 1:
				Bias_Current_CH1 = TXBIAS_TX1_TX4(0x20) ;
				TXP_BUFFER = Bias_Current_CH1 ;

				Bias_Current_CH1 = CAL_Function( Bias_Current_CH1 , CALIB_MEMORY_MAP.IBias1_SCALEM , CALIB_MEMORY_MAP.IBias1_SCALE1L , CALIB_MEMORY_MAP.IBias1_OFFSET ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_MAP.TXP1_SCALEM , CALIB_MEMORY_MAP.TXP1_SCALEL , CALIB_MEMORY_MAP.TXP1_OFFSET ) ;

				if( QSFPDD_Page10[2] & 0x02 ) //|| ( LPMODE_STATUS == 1 )  )
				{
					QSFPDD_Page11[44] = 0 ;
					QSFPDD_Page11[45] = 0 ;
					// CH1  Tx Power
					QSFPDD_Page11[28] = 0 ;
					QSFPDD_Page11[29] = 0 ;

					// Alarm & Waring trigger
					QSFPDD_Page11[12] |= 0x02 ;
					QSFPDD_Page11[14] |= 0x02 ;
					QSFPDD_Page11[16] |= 0x02 ;
					QSFPDD_Page11[18] |= 0x02 ;
				}
				else
				{
					QSFPDD_Page11[44] = Bias_Current_CH1 >> 8 ;
					QSFPDD_Page11[45] = Bias_Current_CH1 ;
					// CH1  Tx Power
					QSFPDD_Page11[28] = TXP_BUFFER >> 8 ;
					QSFPDD_Page11[29] = TXP_BUFFER ;
				}
				break;
		case 2:
				Bias_Current_CH2 = TXBIAS_TX1_TX4(0x40) ;
				TXP_BUFFER = Bias_Current_CH2 ;

				Bias_Current_CH2 = CAL_Function( Bias_Current_CH2 , CALIB_MEMORY_MAP.IBias2_SCALEM , CALIB_MEMORY_MAP.IBias2_SCALE1L , CALIB_MEMORY_MAP.IBias2_OFFSET ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_MAP.TXP2_SCALEM , CALIB_MEMORY_MAP.TXP2_SCALEL , CALIB_MEMORY_MAP.TXP2_OFFSET ) ;

				if( QSFPDD_Page10[2] & 0x04 )// || ( LPMODE_STATUS == 1 )  )
				{
					QSFPDD_Page11[46] = 0 ;
					QSFPDD_Page11[47] = 0 ;
					// CH2  Tx Power
					QSFPDD_Page11[30] = 0 ;
					QSFPDD_Page11[31] = 0 ;

					// Alarm & Waring trigger
					QSFPDD_Page11[12] |= 0x04 ;
					QSFPDD_Page11[14] |= 0x04 ;
					QSFPDD_Page11[16] |= 0x04 ;
					QSFPDD_Page11[18] |= 0x04 ;
				}
				else
				{
					QSFPDD_Page11[46] = Bias_Current_CH2 >> 8 ;
					QSFPDD_Page11[47] = Bias_Current_CH2 ;
					// CH2  Tx Power
					QSFPDD_Page11[30] = TXP_BUFFER >> 8 ;
					QSFPDD_Page11[31] = TXP_BUFFER ;
				}
			    break;
		case 3:
				Bias_Current_CH3 = TXBIAS_TX1_TX4(0x60) ;
				TXP_BUFFER = Bias_Current_CH3 ;

				Bias_Current_CH3 = CAL_Function( Bias_Current_CH3 , CALIB_MEMORY_MAP.IBias3_SCALEM , CALIB_MEMORY_MAP.IBias3_SCALE1L , CALIB_MEMORY_MAP.IBias3_OFFSET ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_MAP.TXP3_SCALEM , CALIB_MEMORY_MAP.TXP3_SCALEL , CALIB_MEMORY_MAP.TXP3_OFFSET ) ;

				if( QSFPDD_Page10[2] & 0x08 ) //|| ( LPMODE_STATUS == 1 )  )
				{
					QSFPDD_Page11[48] = 0 ;
					QSFPDD_Page11[49] = 0 ;
					// CH3  Tx Power
					QSFPDD_Page11[32] = 0 ;
					QSFPDD_Page11[33] = 0 ;

					// Alarm & Waring trigger
					QSFPDD_Page11[12] |= 0x08 ;
					QSFPDD_Page11[14] |= 0x08 ;
					QSFPDD_Page11[16] |= 0x08 ;
					QSFPDD_Page11[18] |= 0x08 ;
				}
				else
				{
					QSFPDD_Page11[48] = Bias_Current_CH3 >> 8 ;
					QSFPDD_Page11[49] = Bias_Current_CH3 ;
					// CH3  Tx Power
					QSFPDD_Page11[32] = TXP_BUFFER >> 8 ;
					QSFPDD_Page11[33] = TXP_BUFFER ;
				}
				break;
		case 4:
			    // HW Tx CH7
				Bias_Current_CH4 = TXBIAS_TX5_TX8(0x00);
				TXP_BUFFER = Bias_Current_CH4 ;

				Bias_Current_CH4 = CAL_Function( Bias_Current_CH4 , CALIB_MEMORY_1_MAP.IBias4_SCALEM , CALIB_MEMORY_1_MAP.IBias4_SCALE1L , CALIB_MEMORY_1_MAP.IBias4_OFFSET ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_1_MAP.TXP4_SCALEM , CALIB_MEMORY_1_MAP.TXP4_SCALEL , CALIB_MEMORY_1_MAP.TXP4_OFFSET ) ;

				if( QSFPDD_Page10[2] & 0x10 ) //|| ( LPMODE_STATUS == 1 )  )
				{
					QSFPDD_Page11[50] = 0 ;
					QSFPDD_Page11[51] = 0 ;
					// CH4  Tx Power
					QSFPDD_Page11[34] = 0 ;
					QSFPDD_Page11[35] = 0 ;

					// Alarm & Waring trigger
					QSFPDD_Page11[12] |= 0x10 ;
					QSFPDD_Page11[14] |= 0x10 ;
					QSFPDD_Page11[16] |= 0x10 ;
					QSFPDD_Page11[18] |= 0x10 ;
				}
				else
				{
					QSFPDD_Page11[50] = Bias_Current_CH4 >> 8 ;
					QSFPDD_Page11[51] = Bias_Current_CH4 ;
					// CH4  Tx Power
					QSFPDD_Page11[34] = TXP_BUFFER >> 8 ;
					QSFPDD_Page11[35] = TXP_BUFFER ;
				}
				break;
		case 5:
				// HW Tx CH6
				Bias_Current_CH5 = TXBIAS_TX5_TX8(0x20);
				TXP_BUFFER = Bias_Current_CH5 ;

				Bias_Current_CH5 = CAL_Function( Bias_Current_CH5 , CALIB_MEMORY_1_MAP.IBias5_SCALEM , CALIB_MEMORY_1_MAP.IBias5_SCALE1L , CALIB_MEMORY_1_MAP.IBias5_OFFSET ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_1_MAP.TXP5_SCALEM , CALIB_MEMORY_1_MAP.TXP5_SCALEL , CALIB_MEMORY_1_MAP.TXP5_OFFSET ) ;

				if( QSFPDD_Page10[2] & 0x20 ) //|| ( LPMODE_STATUS == 1 )  )
				{
					QSFPDD_Page11[52] = 0 ;
					QSFPDD_Page11[53] = 0 ;
					// CH5  Tx Power
					QSFPDD_Page11[36] = 0 ;
					QSFPDD_Page11[37] = 0 ;
					// Alarm & Waring trigger
					QSFPDD_Page11[12] |= 0x20 ;
					QSFPDD_Page11[14] |= 0x20 ;
					QSFPDD_Page11[16] |= 0x20 ;
					QSFPDD_Page11[18] |= 0x20 ;
				}
				else
				{
					QSFPDD_Page11[52] = Bias_Current_CH5 >> 8 ;
					QSFPDD_Page11[53] = Bias_Current_CH5 ;
					// CH5  Tx Power
					QSFPDD_Page11[36] = TXP_BUFFER >> 8 ;
					QSFPDD_Page11[37] = TXP_BUFFER ;
				}
				break;
		case 6:
				// HW Tx CH5
				Bias_Current_CH6 = TXBIAS_TX5_TX8(0x40);
				TXP_BUFFER = Bias_Current_CH6 ;

				Bias_Current_CH6 = CAL_Function( Bias_Current_CH6 , CALIB_MEMORY_1_MAP.IBias6_SCALEM , CALIB_MEMORY_1_MAP.IBias6_SCALE1L , CALIB_MEMORY_1_MAP.IBias6_OFFSET ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_1_MAP.TXP6_SCALEM , CALIB_MEMORY_1_MAP.TXP6_SCALEL , CALIB_MEMORY_1_MAP.TXP6_OFFSET ) ;

				if( QSFPDD_Page10[2] & 0x40 ) //|| ( LPMODE_STATUS == 1 )  )
				{
					QSFPDD_Page11[54] = 0 ;
					QSFPDD_Page11[55] = 0 ;
					// CH6  Tx Powee
					QSFPDD_Page11[38] = 0 ;
					QSFPDD_Page11[39] = 0 ;
					// Alarm & Waring trigger
					QSFPDD_Page11[12] |= 0x40 ;
					QSFPDD_Page11[14] |= 0x40 ;
					QSFPDD_Page11[16] |= 0x40 ;
					QSFPDD_Page11[18] |= 0x40 ;
				}
				else
				{
					QSFPDD_Page11[54] = Bias_Current_CH6 >> 8 ;
					QSFPDD_Page11[55] = Bias_Current_CH6 ;
					// CH6  Tx Power
					QSFPDD_Page11[38] = TXP_BUFFER >> 8 ;
					QSFPDD_Page11[39] = TXP_BUFFER ;
				}
				break;
		case 7:
				// HW Tx CH4
				Bias_Current_CH7 = TXBIAS_TX5_TX8(0x60);
				TXP_BUFFER = Bias_Current_CH7 ;

				Bias_Current_CH7 = CAL_Function( Bias_Current_CH7 , CALIB_MEMORY_1_MAP.IBias7_SCALEM , CALIB_MEMORY_1_MAP.IBias7_SCALE1L , CALIB_MEMORY_1_MAP.IBias7_OFFSET ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_1_MAP.TXP7_SCALEM , CALIB_MEMORY_1_MAP.TXP7_SCALEL , CALIB_MEMORY_1_MAP.TXP7_OFFSET ) ;

				if( QSFPDD_Page10[2] & 0x80 ) //|| ( LPMODE_STATUS == 1 )  )
				{
					QSFPDD_Page11[56] = 0 ;
					QSFPDD_Page11[57] = 0 ;
					// CH7  Tx Power
					QSFPDD_Page11[40] = 0 ;
					QSFPDD_Page11[41] = 0 ;
					// Alarm & Waring trigger
					QSFPDD_Page11[12] |= 0x80 ;
					QSFPDD_Page11[14] |= 0x80 ;
					QSFPDD_Page11[16] |= 0x80 ;
					QSFPDD_Page11[18] |= 0x80 ;
				}
				else
				{
					QSFPDD_Page11[56] = Bias_Current_CH7 >> 8 ;
					QSFPDD_Page11[57] = Bias_Current_CH7 ;
					// CH7  Tx Power
					QSFPDD_Page11[40] = TXP_BUFFER >> 8 ;
					QSFPDD_Page11[41] = TXP_BUFFER ;
				}
				break;
		default:
				break;
	}

}

void RX_POWER_M_CH0_CH7(unsigned char Rx_CH)
{
	unsigned int RX_POWER_CH0,RX_POWER_CH1,RX_POWER_CH2,RX_POWER_CH3;
	unsigned int RX_POWER_CH4,RX_POWER_CH5,RX_POWER_CH6,RX_POWER_CH7;

	switch(Rx_CH)
	{
		case 0:
			    RX_POWER_CH0 = GET_RRSI_CH0_CH3( 3 );
			    RX_POWER_CH0 = CAL_Function( RX_POWER_CH0 , CALIB_MEMORY_MAP.RX0_SCALEM , CALIB_MEMORY_MAP.RX0_SCALEL , CALIB_MEMORY_MAP.RX0_OFFSET );
			    if(RX_POWER_CH0>0xF000)
			    	RX_POWER_CH0 = 0;
				// RX RSSI CH 0
				if( RX_POWER_CH0 < CALIB_MEMORY_MAP.Rx0_LOS_Assret )
					SHOW_RSSI_CH0 = SHOW_RSSI_Disable ;

				if ( RX_POWER_CH0 >= CALIB_MEMORY_MAP.Rx0_LOS_DeAssret )
					SHOW_RSSI_CH0 = SHOW_RSSI_Enable ;

				if( SHOW_RSSI_CH0 == SHOW_RSSI_Enable )
				{
					QSFPDD_Page11[58] = RX_POWER_CH0 >> 8 ;
					QSFPDD_Page11[59] = RX_POWER_CH0 ;
				}
				else
				{
					QSFPDD_Page11[58] = 0 ;
					QSFPDD_Page11[59] = 1 ;
				}

				break;
		case 1:
			    RX_POWER_CH1 = GET_RRSI_CH0_CH3( 2 );
			    RX_POWER_CH1 = CAL_Function( RX_POWER_CH1 , CALIB_MEMORY_MAP.RX1_SCALEM , CALIB_MEMORY_MAP.RX1_SCALEL , CALIB_MEMORY_MAP.RX1_OFFSET );
			    if(RX_POWER_CH1>0xF000)
			    	RX_POWER_CH1 = 0;
				// RX RSSI CH 1
				if( RX_POWER_CH1 < CALIB_MEMORY_MAP.Rx1_LOS_Assret )
					SHOW_RSSI_CH1 = SHOW_RSSI_Disable;

				if( RX_POWER_CH1 >= CALIB_MEMORY_MAP.Rx1_LOS_DeAssret )
					SHOW_RSSI_CH1 = SHOW_RSSI_Enable;

				if( SHOW_RSSI_CH1 == SHOW_RSSI_Enable )
				{
					QSFPDD_Page11[60] = RX_POWER_CH1 >> 8 ;
					QSFPDD_Page11[61] = RX_POWER_CH1 ;
				}
				else
				{
					QSFPDD_Page11[60] = 0;
					QSFPDD_Page11[61] = 1 ;
				}

				break;
		case 2:
				RX_POWER_CH2 = GET_RRSI_CH0_CH3( 1 );
				RX_POWER_CH2 = CAL_Function( RX_POWER_CH2 , CALIB_MEMORY_MAP.RX2_SCALEM , CALIB_MEMORY_MAP.RX2_SCALEL , CALIB_MEMORY_MAP.RX2_OFFSET );
				if(RX_POWER_CH2>0xF000)
					RX_POWER_CH2 = 0;
				// RX RSSI CH 2
				if( RX_POWER_CH2 < CALIB_MEMORY_MAP.Rx2_LOS_Assret )
					SHOW_RSSI_CH2 = SHOW_RSSI_Disable;

				if( RX_POWER_CH2 >= CALIB_MEMORY_MAP.Rx2_LOS_DeAssret )
					SHOW_RSSI_CH2 = SHOW_RSSI_Enable;

				if(SHOW_RSSI_CH2 == SHOW_RSSI_Enable)
				{
					QSFPDD_Page11[62] = RX_POWER_CH2 >> 8 ;
					QSFPDD_Page11[63] = RX_POWER_CH2 ;
				}
				else
				{
					QSFPDD_Page11[62] = 0;
					QSFPDD_Page11[63] = 1 ;
				}

				break;
		case 3:
				RX_POWER_CH3 = GET_RRSI_CH0_CH3( 0 );
				RX_POWER_CH3 = CAL_Function( RX_POWER_CH3 , CALIB_MEMORY_MAP.RX3_SCALEM , CALIB_MEMORY_MAP.RX3_SCALEL , CALIB_MEMORY_MAP.RX3_OFFSET );
				if(RX_POWER_CH3>0xF000)
					RX_POWER_CH3 = 0;
				// RX RSSI CH 3
				if( RX_POWER_CH3 < CALIB_MEMORY_MAP.Rx3_LOS_Assret )
					SHOW_RSSI_CH3 = SHOW_RSSI_Disable;

				if( RX_POWER_CH3 >= CALIB_MEMORY_MAP.Rx3_LOS_DeAssret )
					SHOW_RSSI_CH3 = SHOW_RSSI_Enable;

				if(SHOW_RSSI_CH3==SHOW_RSSI_Enable)
				{
					QSFPDD_Page11[64] = RX_POWER_CH3 >> 8 ;
					QSFPDD_Page11[65] = RX_POWER_CH3 ;
				}
				else
				{
					QSFPDD_Page11[64] = 0;
					QSFPDD_Page11[65] = 1 ;
				}
				break;
		case 4:
				RX_POWER_CH4 = GET_RRSI_CH4_CH7( 3 );
				RX_POWER_CH4 = CAL_Function( RX_POWER_CH4 , CALIB_MEMORY_1_MAP.RX4_SCALEM , CALIB_MEMORY_1_MAP.RX4_SCALEL , CALIB_MEMORY_1_MAP.RX4_OFFSET );
				if(RX_POWER_CH4>0xF000)
					RX_POWER_CH4 = 0;
				// RX RSSI CH 4
				if( RX_POWER_CH4 < CALIB_MEMORY_1_MAP.Rx4_LOS_Assret )
					SHOW_RSSI_CH4 = SHOW_RSSI_Disable;

				if( RX_POWER_CH4 >= CALIB_MEMORY_1_MAP.Rx4_LOS_DeAssret )
					SHOW_RSSI_CH4 = SHOW_RSSI_Enable;

				if(SHOW_RSSI_CH4 == SHOW_RSSI_Enable)
				{
					QSFPDD_Page11[66] = RX_POWER_CH4 >> 8 ;
					QSFPDD_Page11[67] = RX_POWER_CH4 ;
				}
				else
				{
					QSFPDD_Page11[66] = 0 ;
					QSFPDD_Page11[67] = 1 ;
				}
				break;
		case 5:
				RX_POWER_CH5 = GET_RRSI_CH4_CH7( 2 );
				RX_POWER_CH5 = CAL_Function( RX_POWER_CH5 , CALIB_MEMORY_1_MAP.RX5_SCALEM , CALIB_MEMORY_1_MAP.RX5_SCALEL , CALIB_MEMORY_1_MAP.RX5_OFFSET );
				if(RX_POWER_CH5>0xF000)
					RX_POWER_CH5 = 0;
				// RX RSSI CH 5
				if( RX_POWER_CH5 < CALIB_MEMORY_1_MAP.Rx5_LOS_Assret )
					SHOW_RSSI_CH5 = SHOW_RSSI_Disable;

				if( RX_POWER_CH5 >= CALIB_MEMORY_1_MAP.Rx5_LOS_DeAssret )
					SHOW_RSSI_CH5 = SHOW_RSSI_Enable;

				if(SHOW_RSSI_CH5 == SHOW_RSSI_Enable)
				{
					QSFPDD_Page11[68] = RX_POWER_CH5 >> 8 ;
					QSFPDD_Page11[69] = RX_POWER_CH5 ;
				}
				else
				{
					QSFPDD_Page11[68] = 0;
					QSFPDD_Page11[69] = 1 ;
				}

				break;
		case 6:
				RX_POWER_CH6 = GET_RRSI_CH4_CH7( 1 );
				RX_POWER_CH6 = CAL_Function( RX_POWER_CH6 , CALIB_MEMORY_1_MAP.RX6_SCALEM , CALIB_MEMORY_1_MAP.RX6_SCALEL , CALIB_MEMORY_1_MAP.RX6_OFFSET );
				if(RX_POWER_CH6>0xF000)
					RX_POWER_CH6 = 0;

				// RX RSSI CH 6
				if( RX_POWER_CH6 < CALIB_MEMORY_1_MAP.Rx6_LOS_Assret )
					SHOW_RSSI_CH6 = SHOW_RSSI_Disable;

				if( RX_POWER_CH6 >= CALIB_MEMORY_1_MAP.Rx6_LOS_DeAssret )
					SHOW_RSSI_CH6 = SHOW_RSSI_Enable;

				if(SHOW_RSSI_CH6 == SHOW_RSSI_Enable)
				{
					QSFPDD_Page11[70] = RX_POWER_CH6 >> 8 ;
					QSFPDD_Page11[71] = RX_POWER_CH6 ;
				}
				else
				{
					QSFPDD_Page11[70] = 0;
					QSFPDD_Page11[71] = 1 ;
				}

				break;
		case 7:
				RX_POWER_CH7 = GET_RRSI_CH4_CH7( 0 );
				RX_POWER_CH7 = CAL_Function( RX_POWER_CH7 , CALIB_MEMORY_1_MAP.RX7_SCALEM , CALIB_MEMORY_1_MAP.RX7_SCALEL , CALIB_MEMORY_1_MAP.RX7_OFFSET );
				if(RX_POWER_CH7>0xF000)
					RX_POWER_CH7 = 0;
				// RX RSSI CH 7
				if( RX_POWER_CH7 < CALIB_MEMORY_1_MAP.Rx7_LOS_DeAssret )
					SHOW_RSSI_CH7 = SHOW_RSSI_Disable;

				if( RX_POWER_CH7 >= CALIB_MEMORY_1_MAP.Rx7_LOS_DeAssret )
					SHOW_RSSI_CH7 = SHOW_RSSI_Enable;

				if(SHOW_RSSI_CH7 == SHOW_RSSI_Enable)
				{
					QSFPDD_Page11[72] = RX_POWER_CH7 >> 8 ;
					QSFPDD_Page11[73] = RX_POWER_CH7 ;
				}
				else
				{
					QSFPDD_Page11[72] = 0;
					QSFPDD_Page11[73] = 1 ;
				}
				break;
		default:
			    break;
	}
}


void QSFPDD_DDMI_ONLY_ADC_GET()
{
	Temperature_Monitor(Get_Tempsensor());
	VCC_Monitor( ADCV_Conversion_Voltage( P3V3_TRX_VCC )*2 );
	Get_module_Power_Monitor();
}

void QSFPDD_DDMI_StateMachine(unsigned char StateMachine )
{
	switch(StateMachine)
	{
		case ADC_SM:
					Temperature_Monitor(Get_Tempsensor());
					VCC_Monitor( ADCV_Conversion_Voltage( P3V3_TRX_VCC )*2 );
					break;
		case RXP_CH0:
					RX_POWER_M_CH0_CH7(0);
					break;
		case RXP_CH1:
					RX_POWER_M_CH0_CH7(1);
					break;
		case RXP_CH2:
					RX_POWER_M_CH0_CH7(2);
					break;
		case RXP_CH3:
					RX_POWER_M_CH0_CH7(3);
					break;
		case RXP_CH4:
					RX_POWER_M_CH0_CH7(4);
					break;
		case RXP_CH5:
					RX_POWER_M_CH0_CH7(5);
					break;
		case RXP_CH6:
					RX_POWER_M_CH0_CH7(6);
					break;
		case RXP_CH7:
					RX_POWER_M_CH0_CH7(7);
					break;

		case TXB_TXP_0_SM:
					BIAS_TxPower_Monitor( 0 );
					break;

		case TXB_TXP_1_SM:
					BIAS_TxPower_Monitor( 1 );
					break;

		case TXB_TXP_2_SM:
					BIAS_TxPower_Monitor( 2 );
					break;

		case TXB_TXP_3_SM:
					BIAS_TxPower_Monitor( 3 );
					break;

		case TXB_TXP_4_SM:
					BIAS_TxPower_Monitor( 4 );
					break;

		case TXB_TXP_5_SM:
					BIAS_TxPower_Monitor( 5 );
					break;

		case TXB_TXP_6_SM:
					BIAS_TxPower_Monitor( 6 );
					break;

		case TXB_TXP_7_SM:
					BIAS_TxPower_Monitor( 7 );
					break;
		case OP_SM:
					if(Power_on_flag==1)
						DDMI_AW_Monitor();
					break;
		case TXLOLLOS:
					if(Power_on_flag==1)
						QSFPDD_TXLOS_LOL_AW();
			        break;
		case RXLOLLOS:
					if(Power_on_flag==1)
						QSFPDD_RXLOS_LOL_AW();
			        break;
		case Other_DDMI:
					Get_module_Power_Monitor();
					break;

		default:
				break;
	}
}

